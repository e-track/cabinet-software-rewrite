################################################################################
# Config.
################################################################################
CONFIG += ordered     # Build subdir projects in the order specified below.

TEMPLATE = subdirs

################################################################################
# Dependencies.
################################################################################
SUBDIRS = \
    $$PWD/UnitTestSuite \
    $$PWD/DatabaseControl \
    $$PWD/HardwareControl \
    $$PWD/CabinetUI
