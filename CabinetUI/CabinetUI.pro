################################################################################
# Config.
################################################################################
CONFIG += c++11
QT += quick

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = $$PWD/qml/
QML2_IMPORT_PATH = $$PWD/qml/

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH = $$PWD/qml/

################################################################################
# Dependencies.
################################################################################
include( $$PWD/../DatabaseControl/DatabaseControl.pri )
include( $$PWD/../HardwareControl/HardwareControl.pri )

################################################################################
# Headers.
################################################################################
HEADERS += \

################################################################################
# Sources.
################################################################################
SOURCES += \
    $$PWD/main.cpp

################################################################################
# Resources.
################################################################################
RESOURCES += \
    $$PWD/qml.qrc

################################################################################
# Installation
################################################################################
qnx: target.path = /tmp/$${TARGET}/bin
unix:!android: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET
win32: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET

!isEmpty(target.path): INSTALLS += target
