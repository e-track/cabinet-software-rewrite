// Qt.
import QtQuick

// Library.
import Data     1.0 as Data
import Database 1.0 as Database
import Pages    1.0 as Pages

////////////////////////////////////////////////////////////////////////////////
/// \brief  Page navigation controller.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    property var stack

    Component { id: pinLoginPage;             Pages.LoginPin {} }
    Component { id: welcomePage;              Pages.Welcome {} }
    Component { id: homePage;                 Pages.Home {} }
    Component { id: takeKeyPage;              Pages.TakeKey {} }
    Component { id: returnKeyPage;            Pages.ReturnKey {} }
    Component { id: openCabinetReturnKeyPage; Pages.OpenCabinetReturnKey {} }
    Component { id: openCabinetTakeKeyPage;   Pages.OpenCabinetTakeKey {} }
    Component { id: searchPage;               Pages.Search {} }
    Component { id: setupPage;                Pages.Setup {} }
    Component { id: setupPinPage;             Pages.SetupPin {} }
    Component { id: fingerprintSetupPage;     Pages.FingerprintSetup {} }
    Component { id: cardreaderSetupPage;      Pages.CardreaderSetup {} }
    Component { id: checkedOutTagsPage;       Pages.CheckedOutTags {} }
    Component { id: tagInfoPage;              Pages.TagInfo {} }
    Component { id: tagAvailablePage;         Pages.TagAvailable {} }
    Component { id: tagUnavailablePage;       Pages.TagUnavailable {} }
    Component { id: tagBasketPage;            Pages.TagBasket {} }

    Pages.Cabinet { id: cabinetPage }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates backwards.
    ////////////////////////////////////////////////////////////////////////////////
    function back()
    {
        stack.pop()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the take key page.
    ////////////////////////////////////////////////////////////////////////////////
    function takeKey()
    {
        Database.Control.countCabinets()

        if ( stack.currentItem.objectName === qsTr( "TakeKeyPage" ) )
        {
            stack.pop()
        }

        stack.push( takeKeyPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the return key page.
    ////////////////////////////////////////////////////////////////////////////////
    function returnKey()
    {
        Database.Control.countCabinets()

        if ( stack.currentItem.objectName === "ReturnKeyPage" )
        {
            stack.pop()
        }

        stack.push( returnKeyPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the open cabinet return key page.
    ////////////////////////////////////////////////////////////////////////////////
    function openCabinetReturnKey( cabinet )
    {
        if ( stack.currentItem.objectName === "OpenCabinetReturnKeyPage" )
        {
            stack.pop()
        }

        stack.push( openCabinetReturnKeyPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the open cabinet take key page.
    ////////////////////////////////////////////////////////////////////////////////
    function openCabinetTakeKey( cabinet )
    {
        if ( stack.currentItem.objectName === "OpenCabinetTakeKeyPage" )
        {
            stack.pop()
        }

        stack.push( openCabinetTakeKeyPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the home page.
    ////////////////////////////////////////////////////////////////////////////////
    function home()
    {
        if ( stack.currentItem.objectName === "HomePage" )
        {
            stack.pop()
        }
        else
        {
            stack.pop( null )
        }

        stack.push( homePage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the welcome page.
    ////////////////////////////////////////////////////////////////////////////////
    function welcome()
    {
        if ( stack.depth > 1 )
        {
            stack.pop( null )
        }
        else
        {
            stack.push( welcomePage )
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the search page.
    ////////////////////////////////////////////////////////////////////////////////
    function search( text )
    {
        stack.push( searchPage, { searchText: text } )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the setup page.
    ////////////////////////////////////////////////////////////////////////////////
    function setup()
    {
        stack.push( setupPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the setup PIN page.
    ////////////////////////////////////////////////////////////////////////////////
    function setupPin()
    {
        stack.push( setupPinPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the fingerprint setup page.
    ////////////////////////////////////////////////////////////////////////////////
    function fingerprintSetup( text )
    {
        stack.push( fingerprintSetupPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the cardreader setup page.
    ////////////////////////////////////////////////////////////////////////////////
    function cardreaderSetup( text )
    {
        stack.push( cardreaderSetupPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the tag statuses page.
    ////////////////////////////////////////////////////////////////////////////////
    function checkedOutTags()
    {
        stack.push( checkedOutTagsPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the tag info page.
    ////////////////////////////////////////////////////////////////////////////////
    function tagInfo( tag )
    {
        var data = { tagData: tag }
        stack.push( tagInfoPage, data )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the tag available page.
    ////////////////////////////////////////////////////////////////////////////////
    function tagAvailable( tag )
    {
        var data = { tagData: tag }
        stack.push( tagAvailablePage, data )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the tag unavailable page.
    ////////////////////////////////////////////////////////////////////////////////
    function tagUnavailable( tag )
    {
        var data = { tagData: tag }

        stack.push( tagUnavailablePage, data )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the cabinet page.
    ////////////////////////////////////////////////////////////////////////////////
    function cabinet( cabinetId )
    {
        Database.Control.queryTagsForCabinet( cabinetId, Data.Config.numTagsPerPage )

        stack.push( cabinetPage )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigates to the tag basket page.
    ////////////////////////////////////////////////////////////////////////////////
    function tagBasket()
    {
        stack.push( tagBasketPage )
    }
}
