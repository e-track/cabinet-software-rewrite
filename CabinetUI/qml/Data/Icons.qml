pragma Singleton

// Qt.
import QtQuick

///////////////////////////////////////////////////////////////
/// \brief  .
///////////////////////////////////////////////////////////////
QtObject
{
    readonly property string arrow_backward:         "qrc:///icons/arrow_back_ios_white_24dp.svg"
    readonly property string arrow_forward:          "qrc:///icons/arrow_forward_ios_white_24dp.svg"
    readonly property string assignment_returned:    "qrc:///icons/assignment_returned_white_24dp.svg"
    readonly property string fingerprint:            "qrc:///icons/fingerprint.png"
    readonly property string fingerprint_highlight:  "qrc:///icons/fingerprintGreen.png"
    readonly property string home:                   "qrc:///icons/home_white_24dp.svg"
    readonly property string key:                    "qrc:///icons/vpn_key_white_24dp.svg"
    readonly property string keyboard_return:        "qrc:///icons/keyboard_return_white_24dp.svg"
    readonly property string location:               "qrc:///icons/location_off_white_24dp.svg"
    readonly property string lock_closed:            "qrc:///icons/lock_white_24dp.svg"
    readonly property string lock_open:              "qrc:///icons/lock_open_white_24dp.svg"
    readonly property string lock_open_outline:      "qrc:///icons/lock_white_outline_24dp.svg"
    readonly property string logo:                   "qrc:///icons/white e-track june.png"
    readonly property string search:                 "qrc:///icons/search_white_24dp.svg"
    readonly property string toggle:                 "qrc:///icons/toggle_off_white_24dp.svg"

    readonly property string fa_backspace:      "\uf55a"
    readonly property string fa_bars:           "\uf0c9"
    readonly property string fa_basket:         "\uf291"
    readonly property string fa_bracket_from:   "\uf2f5"
    readonly property string fa_bracket_to:     "\uf2f6"
    readonly property string fa_calc:           "\uf1ec"
    readonly property string fa_camera:         "\uf030"
    readonly property string fa_cog:            "\uf013"
    readonly property string fa_card:           "\uf2c2"
    readonly property string fa_download:       "\uf019"
    readonly property string fa_envelope:       "\uf0e0"
    readonly property string fa_fingerprint:    "\uf577"
    readonly property string fa_home:           "\uf015"
    readonly property string fa_key:            "\uf084"
    readonly property string fa_lock:           "\uf023"
    readonly property string fa_server:         "\uf233"
    readonly property string fa_toggle:         "\uf205"
    readonly property string fa_user:           "\uf007"

    // Tag / key categories.
    readonly property string fa_bus:            "\uf207"
    readonly property string fa_car:            "\uf1b9"
    readonly property string fa_car_crash:      "\uf5e1"
    readonly property string fa_motorcycle:     "\uf21c"
    readonly property string fa_road:           "\uf018"
    readonly property string fa_shield:         "\uf3ed"

    // Arrows.
    readonly property string fa_left:           "\uf104"
    readonly property string fa_right:          "\uf105"
    readonly property string fa_up:             "\uf106"
    readonly property string fa_down:           "\uf107"
    readonly property string fa_arrow_right:    "\uf061"
    readonly property string fa_arrow_up:       "\uf062"

    // Status.
    readonly property string fa_unchecked:      "\uf0c8"
    readonly property string fa_checked:        "\uf14a"
    readonly property string fa_tick:           "\uf00c"
    readonly property string fa_cross:          "\uf00d"

    // Key category maps.
    readonly property var tagCategories:
    {
       "car_crash"  : fa_car_crash,
       "shield"     : fa_shield,
       "road"       : fa_road,
       "car"        : fa_car,
       "motorcycle" : fa_motorcycle,
       "bus"        : fa_bus
    }
}
