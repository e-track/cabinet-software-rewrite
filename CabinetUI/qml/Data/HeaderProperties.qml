// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Header properties.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    id: self

    // Customisation.
    property string title: ""               ///< Title of the page.

    // Back button.
    property bool showBack: true            ///< Back button visible property.
    property bool enableBack: true          ///< Back button enabled property.

    // Home button.
    property bool showHome: true            ///< Home button visible property.
    property bool enableHome: true          ///< Home button enabled property.

    // Forward button.
    property bool showForward: false        ///< Forward button visible property.
    property bool enableForward: true       ///< Forward button enabled property.

    // Logout button.
    property bool showLogout: false         ///< Logout button visible property.
    property bool enableLogout: true        ///< Logout button enabled property.
    property string logoutText: "Sign out"  ///< Logout button text.

    // Register button.
    property bool showRegister: false       ///< Register button visible property.

    // Search button
    property bool showSearch: false         ///< Search bar visible property.

    // Locked icon.
    property bool showLocked: false         ///< Lock icon visible property.

    // Divider.
    property bool showDivider: true         ///< Divider visible property.
}
