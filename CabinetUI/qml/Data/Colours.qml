pragma Singleton

// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Colour definitions.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    id: self

    // Primary brand colours.
    readonly property color brand_blue: "#ff26a1cb"             ///< Blue brand.
    readonly property color brand_black: "#ff000000"            ///< Black brand.
    readonly property color brand_white: "#ffffffff"            ///< White brand.
    readonly property color brand_dark: "#ff18232a"             ///< Dark brand.

    // General.
    readonly property color background: self.brand_dark         ///< Background.
    readonly property color foreground: self.brand_white        ///< Foreground (text, borders etc).
    readonly property color error: "#ffff4848"                  ///< Error.
    readonly property color control: "#ff707070"                ///< Controls (buttons etc).
    readonly property color selection: "#ff0080ff"              ///< Selection.
    readonly property color important: "#ff1ae49e"              ///< Highlight (selection etc).
    readonly property color transparent: "transparent"          ///< Transparent.
    readonly property color temp_dull: "grey"                   ///< Temporary grey colour.

    // Keypad.
    readonly property color key: self.background                ///< Keypad key.
    readonly property color keypad: self.transparent            ///< Keypad background.

    // Tags.
    readonly property color tag: self.important                 ///< Normal tag background.
    readonly property color tagText: self.foreground            ///< Normal tag text.
    readonly property color tagWrongSlot: self.error            ///< Wrong slot tag background.
    readonly property color tagWrongSlotText: self.foreground   ///< Wrong slot text.
    readonly property color tagOut: self.control                ///< Tag out background.
    readonly property color tagOutText: self.foreground         ///< Tag out text.
}
