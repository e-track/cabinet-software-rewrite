pragma Singleton

// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Configuraiton constants.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    property bool smallScreen: false                            ///< 5 inch screen mode.
    readonly property bool largeScreen: !smallScreen            ///< 10 inch screen mode.
    readonly property bool metPolice: true                      ///< Indicates whether the Met police UI should be shown.

    readonly property int numTagsPerPage: smallScreen           ///< Number of tags / keys per page.
                                            ? 10
                                            : 100
}
