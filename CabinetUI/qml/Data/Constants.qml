pragma Singleton

// Qt.
import QtQuick

// Project.
import Data 1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Constants definitions.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    id: self

    // Screen.
    readonly property real pixelDensity:  typeof Qt.application.screens[ Debugging.screen ] === "undefined"
                                            ? Screen.pixelDensity
                                            : Qt.application.screens[ Debugging.screen ].pixelDensity

    readonly property int screenWidth: Data.Config.smallScreen                      ///< Screen width.
                                            ? pixelDensity * 109                    ///< Small 5 inch screen width (mm).
                                            : pixelDensity * 135                    ///< Large 10 inch screen width (mm).
    readonly property int screenHeight:  Data.Config.smallScreen                    ///< Screen height.
                                            ? pixelDensity * 66                     ///< Small 5 inch screen height (mm).
                                            : pixelDensity * 217                    ///< Large 10 inch screen height (mm).

    // Conversions.
    readonly property real inchesToMm: 25.4                                         ///< Conversion from inches to mm.
    readonly property real aspectRatio: 1.777                                       ///< 16:9 aspect ratio.

    // General.
    readonly property int logoHeightMm: pixelDensity * 20                           ///< Logo height (mm).
    readonly property int headerHeightMm:  Data.Config.smallScreen                  ///< Height of the header (mm).
                                            ? pixelDensity * 10
                                            : pixelDensity * 15
    readonly property int toolSizeMm:  Data.Config.smallScreen                      ///< Size of toolbar controls (mm).
                                      ? pixelDensity * 8
                                      : pixelDensity * 12

    readonly property int badgeSizeMm:  Data.Config.smallScreen                     ///< Size of badges (mm).
                                        ? pixelDensity * 8
                                        : pixelDensity * 8

    // Layout.
    // TODO: In the code we switch between small and large gaps depending on the
    // size of the screen. We should really do that in these definitions and then
    // only reference one size of gap.
    readonly property int smallGapMm: pixelDensity * 2                              ///< Small gap.
    readonly property int mediumGapMm: pixelDensity * 5                             ///< Medium gap.
    readonly property int largeGapMm: pixelDensity * 7.5                            ///< Large gap.

    // Buttons.
    readonly property int buttonSizeSmallMm: pixelDensity * 20
    readonly property int buttonSizeLargeMm: pixelDensity * 30

    readonly property int buttonSizeMm:  Data.Config.smallScreen                    ///< Size of buttons (mm).
                                            ? buttonSizeSmallMm
                                            : buttonSizeLargeMm
    readonly property int buttonIconSizeMm:  Data.Config.smallScreen                ///< Size of button icons (mm).
                                                ? pixelDensity * 10
                                                : pixelDensity * 15
    readonly property int toolButtonSizeMm: toolSizeMm                              ///< Size of tool buttons (mm).
    readonly property int toolButtonIconSizeMm:   Data.Config.smallScreen           ///< Size of tool button icons (mm).
                                                    ? pixelDensity * 5
                                                    : pixelDensity * 8

    // Delegates.
    readonly property int tagSizeMm:  Data.Config.smallScreen                       ///< Size of tags (mm).
                                        ? pixelDensity * 12
                                        : pixelDensity * 12

    readonly property int tagBadgeSizeMm:  Data.Config.smallScreen                  ///< Size of tag badge (mm).
                                            ? pixelDensity * 2
                                            : pixelDensity * 4
    // Fonts.
    readonly property int tinyFontPx: pixelDensity * 2.5                            ///< Tiny font (button text).
    readonly property int smallFontPx:  Data.Config.smallScreen                     ///< Small font (standard text).
                                            ? pixelDensity * 4
                                            : pixelDensity * 5
    readonly property int mediumFontPx: pixelDensity * 10                           ///< Medium font (PIN text).
    readonly property int largeFontPx: pixelDensity * 15                            ///< Large font (main company logo).

    readonly property string fontSource: "qrc:///fonts/SourceSansPro-Regular.ttf"   ///< Default font source.
    readonly property string iconFontSource: "qrc:///fonts/fa-solid-900.ttf"        ///< Icon font source.

    // Keypad.
    readonly property int keySizeMm:  Data.Config.smallScreen                       ///< Key size (mm).
                                        ? pixelDensity * 10
                                        : pixelDensity * 10
    readonly property int keySpacingMm:  Data.Config.smallScreen                    ///< Spacing between keys (mm).
                                        ? pixelDensity * 1
                                        : pixelDensity * 2

    // Timers.
    readonly property int popupTimeoutMs: 2500                                      ///< Timeout for popups.
}
