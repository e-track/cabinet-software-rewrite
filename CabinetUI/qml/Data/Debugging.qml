pragma Singleton

// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Debugging constants definitions.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    id: self

    // General.
    property bool enabled: false                                        ///< Toggles debug mode.
    property bool topFramed: false                                      ///< App is top level framed window.
    property bool showActiveFocus: false                                ///< Active focus will be logged.
    property int screen: Qt.application.screens.length > 0 ? 2 : 0      ///< Screen to show UI on.

    readonly property real opacity: 0.25                                ///< Debug layer opacity.

    // Pages.
    property bool togglePages: true                                     ///< Toggle between two versions of a page.
}
