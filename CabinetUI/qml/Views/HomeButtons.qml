// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Data     1.0 as Data
import Controls 1.0 as Controls

////////////////////////////////////////////////////////////////////////////////
/// \brief  Buttons wrapper.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: name

    Layout.fillHeight: true
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Button layout.
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        anchors.centerIn: parent
        spacing: Data.Constants.largeGapMm

        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Remove button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EButton
        {
            id: takeButton
            text: "Withdraw"
            iconSource: Data.Icons.fa_bracket_from

            onClicked:
            {
                navigate.takeKey()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Return button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EButton
        {
            id: returnButton
            text: "Deposit"
            iconSource: Data.Icons.fa_bracket_to

            onClicked:
            {
                navigate.returnKey()
            }
        }
    }
}
