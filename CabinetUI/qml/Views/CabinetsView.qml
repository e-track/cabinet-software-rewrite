// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Database 1.0 as Database

////////////////////////////////////////////////////////////////////////////////
/// \brief  Flickable cabinets area.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.fillWidth: true
    Layout.fillHeight: true

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property bool showBadges: false                 ///< Visibility of cabinets' badges.

    property alias delegate: repeater.delegate      ///< Alias to the delegate.
    property alias model: repeater.model			///< Alias to the model.

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Adds swiping functionality to the cabinets area.
    ////////////////////////////////////////////////////////////////////////////////
    Flickable
    {
        id: flickable
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: Math.min( contentWidth, parent.width )
        height: contentHeight
        clip: true
        contentWidth: cabinets.width
        contentHeight: cabinets.height
        visible: Database.Control.cabinets.length > 0

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Cabinets layout.
        ////////////////////////////////////////////////////////////////////////////////
        RowLayout
        {
            id: cabinets
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Data.Constants.smallGapMm
            clip: true

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Cabinet repeater.
            ////////////////////////////////////////////////////////////////////////////////
            Repeater
            {
                id: repeater
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  No cabinets prompt.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.EText
    {
        id: noCabinets
        anchors.centerIn: parent
        visible: Database.Control.cabinets.length === 0
        text: "No cabinets found."
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Left indicator.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.EIcon
    {
        anchors.left: flickable.left
        anchors.verticalCenter: flickable.verticalCenter
        text: Data.Icons.fa_left
        visible: !flickable.atXBeginning
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Right indicator.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.EIcon
    {
        anchors.right: flickable.right
        anchors.verticalCenter: flickable.verticalCenter
        text: Data.Icons.fa_right
        visible: !flickable.atXEnd
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "green"
    }
}

