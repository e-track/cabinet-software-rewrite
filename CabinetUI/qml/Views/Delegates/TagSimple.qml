// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Simple tag delegate that is best used when lots of tags need to be
///         shown on screen at the same time. For example, when browsing a
///         cabinet.
////////////////////////////////////////////////////////////////////////////////
Delegates.Tag
{
    id: self
    text: tagData.tagId
    opacity: self.tagData.isOut ? 0.25 : 1.0

    Layout.alignment: Qt.AlignVCenter
    Layout.preferredWidth: Data.Constants.tagSizeMm
    Layout.preferredHeight: Data.Constants.tagSizeMm

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 1
        border.color: self.pressed
                        ? priv.backgroundColour
                        : priv.foregroundColour
        color: self.pressed
                    ? priv.foregroundColour
                    : priv.backgroundColour
        radius: Math.max( width, height )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property color backgroundColour: Data.Colours.selection

        property color foregroundColour: self.tagData.isOut
                                            ? Data.Colours.tagOutText
                                            : Data.Colours.tagText
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Badge indicating a notification or warning.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.Badge
    {
        id: warningBadge
        color: Data.Colours.tagWrongSlot
        textColour: Data.Colours.tagWrongSlotText
        border.color: Data.Colours.tagWrongSlotText
        size: Data.Constants.tagBadgeSizeMm
        fontSize: Data.Constants.tagBadgeSizeMm
        text: "!"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        visible: tagData.isWrongSlot
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Badge indicating the tag is in the basket.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.Badge
    {
        id: basketBadge
        color: Data.Colours.brand_black
        textColour: Data.Colours.foreground
        border.color: Data.Colours.brand_black
        size: Data.Constants.tagBadgeSizeMm
        fontSize: Data.Constants.tagBadgeSizeMm * 0.75
        text: Data.Icons.fa_basket
        anchors.top: parent.top
        anchors.right: parent.right
        visible: tagData.inBasket
    }
}
