// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Detailed Tag delegate that is best used when fewer tags are visible
///         on screen and more detail is required. For example, when viewing
///         currently checked out tags.
////////////////////////////////////////////////////////////////////////////////
Delegates.Tag
{
    id: self

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property real cellWidth: self.width / 4
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 1
        border.color: Data.Colours.transparent

        color: self.pressed
                    ? Data.Colours.foreground
                    : Data.Colours.transparent

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Debug rectangle.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.DebugRect
        {
            color: "magenta"
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout.
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        id: row
        anchors.centerIn: parent
        anchors.leftMargin: Data.Constants.smallGapMm
        spacing: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Icon item.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.preferredWidth: priv.cellWidth
            Layout.preferredHeight: Data.Constants.tagSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Icon.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EIcon
            {
                id: tagIcon
                text: Data.Icons.fa_key
                color: Data.Colours.foreground
                font.pixelSize: Data.Constants.toolButtonIconSizeMm
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter

                anchors.centerIn: parent

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Badge indicating a notification or warning.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Badge
                {
                    id: badge
                    color: Data.Colours.error
                    border.color: Data.Colours.foreground
                    size: Data.Constants.tagBadgeSizeMm
                    fontSize: Data.Constants.tagBadgeSizeMm
                    text: Data.Config.smallScreen ? "" : "!"
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    visible: tagData.isWrongSlot
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tag name.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            id: tag
            text: self.tagData.name
            color: Data.Colours.foreground
            font.pixelSize: parent.height / 2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Status item.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            visible: !self.tagData.isReturning

            Layout.preferredWidth: priv.cellWidth
            Layout.preferredHeight: Data.Constants.tagSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Status.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: self.tagData.isOut
                            ? "OUT"
                            : "IN"
                color: self.tagData.isOut
                            ? Data.Colours.error
                            : Data.Colours.important
                font.pixelSize: parent.height / 2

                anchors.centerIn: parent
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Status item.
        ////////////////////////////////////////////////////////////////////////////////
        RowLayout
        {
            visible: self.tagData.isReturning
            spacing: 0

            Layout.preferredWidth: priv.cellWidth
            Layout.preferredHeight: Data.Constants.tagSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Arrow.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EIcon
            {
                text: Data.Icons.fa_arrow_right
                color: Data.Colours.foreground
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Slot icon.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EIcon
            {
                text: Data.Icons.fa_server
                color: Data.Colours.selection
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Slot number.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: self.tagData.slot
                color: Data.Colours.selection
            }
        }
    }
}
