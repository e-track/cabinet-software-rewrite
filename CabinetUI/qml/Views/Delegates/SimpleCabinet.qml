// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Simple cabinet delegate.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.alignment: Qt.AlignVCenter
    Layout.preferredWidth: Data.Constants.buttonSizeMm
    Layout.preferredHeight: Data.Constants.buttonSizeMm +
                            ( self.showBadge ? ( badge.height / 2 ) : 0 )

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property alias showBadge: badge.visible     ///< Visibility of the badge.

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Signals.
    ////////////////////////////////////////////////////////////////////////////////
    signal clicked()

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Cabinets button.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.EButton
    {
        id: cabinet
        width: Data.Constants.buttonSizeMm
        height: Data.Constants.buttonSizeMm
        text: index
        font.pixelSize: Data.Constants.mediumFontPx
        anchors.top: parent.top

        onClicked: self.clicked()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Badge indicating number of keys currently out of that cabinet.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.Badge
    {
        id: badge
        color: cabinet.enabled ? Data.Colours.important : Data.Colours.brand_dark
        border.color: Data.Colours.foreground
        size: Data.Constants.toolButtonSizeMm
        text: cabinet.enabled ? ( 3 - index ) : 0
        anchors.verticalCenter: cabinet.bottom
        anchors.horizontalCenter: cabinet.horizontalCenter
        visible: false
    }
}
