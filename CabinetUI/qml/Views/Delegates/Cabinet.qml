// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Cabinets delegate.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.alignment: Qt.AlignVCenter
    Layout.preferredWidth: Data.Constants.buttonSizeMm
    Layout.preferredHeight: Data.Constants.buttonSizeMm +
                            ( self.showBadge ? ( badge.height / 2 ) : 0 )

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property alias showBadge: badge.visible     ///< Visibility of the badge.

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Signals.
    ////////////////////////////////////////////////////////////////////////////////
    signal clicked()

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Cabinets button.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.EButton
    {
        id: cabinet
        width: Data.Constants.buttonSizeMm
        height: Data.Constants.buttonSizeMm
        text: index
        font.pixelSize: Data.Constants.smallFontPx
        anchors.bottom: parent.bottom
        iconSource: Data.Icons.fa_server

        onClicked: self.clicked()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Badge indicating number of keys currently out of that cabinet.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.Badge
    {
        id: badge
        color: cabinet.enabled ? ( text !== "0" ? Data.Colours.important : Data.Colours.brand_dark ) : Data.Colours.brand_dark
        border.color: Data.Colours.foreground
        size: Data.Constants.toolButtonSizeMm
        text: cabinet.enabled ? ( 3 - index > 1 ? ( 3 - index ) : 0 ) : 0
        anchors.verticalCenter: cabinet.top
        anchors.horizontalCenter: cabinet.horizontalCenter
        visible: false
    }
}
