// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Tag / key delegate that contains tag / key category information
///         that is best used when a lot of tags need to be shown and grouped
///         by their category.
///         For example, on the welcome page.
////////////////////////////////////////////////////////////////////////////////
Controls.EToolButton
{
    id: self
    text: ""

    Layout.alignment: Qt.AlignVCenter
    Layout.fillWidth: true
    Layout.maximumWidth: Data.Constants.buttonSizeMm
    Layout.preferredHeight: row.height

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property var tagCategoryData

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 1
        border.color: self.pressed
                      ? Data.Colours.background
                      : Data.Colours.foreground
        color: self.pressed
                    ? Data.Colours.foreground
                    : Data.Colours.background
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout.
    ////////////////////////////////////////////////////////////////////////////////
    ColumnLayout
    {
        id: row
        anchors.centerIn: parent
        anchors.leftMargin: Data.Constants.smallGapMm
        spacing: 0

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tag / key category name.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: typeof self.tagCategoryData === "undefined"
                    ? ""
                    : self.tagCategoryData.tagCategoryShortName
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tag / key category icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EIcon
        {
            text: typeof self.tagCategoryData === "undefined"
                    ? ""
                    : Data.Icons.tagCategories[self.tagCategoryData.tagCategoryIcon]
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tag / key category count.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: typeof self.tagCategoryData === "undefined"
                    ? ""
                    : modelData.length
            color: Data.Colours.selection
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: typeof self.tagCategoryData === "undefined"
                    ? ""
                    : "Available"
            color: Data.Colours.selection

            Layout.alignment: Qt.AlignCenter
        }
    }
}
