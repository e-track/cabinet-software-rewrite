// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Database 1.0 as Database

////////////////////////////////////////////////////////////////////////////////
/// \brief  Base tag delegate provides tag data and additional properties.
////////////////////////////////////////////////////////////////////////////////
Controls.EToolButton
{
    id: self

    Layout.alignment: Qt.AlignVCenter
    Layout.fillWidth: true
    Layout.preferredHeight: Data.Constants.toolButtonSizeMm

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property bool taking: false
    property bool selecting: false

    property var tagData: Database.TagData    ///< Tag data to be displayed.
    {
        tagId: typeof modelData === "undefined" ? -1 : modelData.tagId
        cabinet: typeof modelData === "undefined" ? -1 : modelData.cabinet
        userId: typeof modelData === "undefined" ? -1 : modelData.userId
        slot: typeof modelData === "undefined" ? -1 : modelData.slot
        currentSlot: typeof modelData === "undefined" ? -1 : modelData.currentSlot
        fob: typeof modelData === "undefined" ? -1 : modelData.fob
        mileage: typeof modelData === "undefined" ? -1 : modelData.mileage
        name: typeof modelData === "undefined" ? "" : modelData.name
        tagDescription: typeof modelData === "undefined" ? "" : modelData.tagDescription
        serial: typeof modelData === "undefined" ? "" : modelData.serial
        dateAdded: typeof modelData === "undefined" ? "" : modelData.dateAdded
        startTime: typeof modelData === "undefined" ? "" : modelData.startTime
        endTime: typeof modelData === "undefined" ? "" : modelData.endTime
        location: typeof modelData === "undefined" ? "" : modelData.location
        site: typeof modelData === "undefined" ? "" : modelData.site
        isStatic: typeof modelData === "undefined" ? false : modelData.isStatic
        isOut: typeof modelData === "undefined" ? false : modelData.isOut
        isWrongSlot: typeof modelData === "undefined" ? false : modelData.isWrongSlot
        booked: typeof modelData === "undefined" ? false : modelData.booked
        inBasket: typeof modelData === "undefined" ? false : modelData.inBasket
        isReturning: typeof modelData === "undefined" ? false : modelData.isReturning
    }
}
