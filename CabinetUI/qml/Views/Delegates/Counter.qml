// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Titled delegate that shows a quantity next to an icon.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.alignment: Qt.AlignVCenter
    Layout.fillWidth: true

    implicitWidth: columnLayout.width
    implicitHeight: columnLayout.height + ( badge.size / 2 )

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property int quantity
    property color tint : Data.Colours.foreground

    property alias iconSource: icon.iconSource
    property alias showBadge: badge.visible
    property alias title: titleText.text

    property var model: ListModel {}

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Signals.
    ////////////////////////////////////////////////////////////////////////////////
    signal clicked()

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout.
    ////////////////////////////////////////////////////////////////////////////////
    ColumnLayout
    {
        id: columnLayout
        spacing: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Title.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            id: titleText
            color: self.tint
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Button row.
        ////////////////////////////////////////////////////////////////////////////////
        RowLayout
        {
            spacing: Data.Constants.smallGapMm

            Layout.alignment: Qt.AlignCenter

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Spacer.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Spacer {}

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Icon button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: icon
                font.pixelSize: Data.Constants.smallFontPx
                iconColor: !icon.pressed
                           ? self.tint
                           : Data.Colours.background

                Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: Data.Constants.buttonIconSizeMm
                Layout.preferredHeight: Data.Constants.buttonIconSizeMm

                onClicked: self.clicked()

                background: Rectangle
                {
                    id: background
                    anchors.fill: parent
                    border.width: 1
                    border.color: !icon.pressed
                                    ? Data.Colours.transparent
                                    : self.tint
                    color: !icon.pressed
                           ? Data.Colours.transparent
                           : self.tint
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Badge indicating warning.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Badge
                {
                    id: badge
                    color: Data.Colours.error
                    border.color: Data.Colours.foreground
                    size: Data.Constants.badgeSizeMm
                    text: "!"
                    anchors.top: icon.verticalCenter
                    anchors.left: icon.horizontalCenter
                    visible: false
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Quantity.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                id: quantityText
                font.pixelSize: Data.Constants.mediumFontPx
                text: "x " + self.quantity
                color: self.tint
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Spacer.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Spacer {}
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "green"
    }
}
