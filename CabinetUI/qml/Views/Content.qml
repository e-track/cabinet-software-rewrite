// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Main layout.
////////////////////////////////////////////////////////////////////////////////
ColumnLayout
{
    id: column
    anchors.fill: parent
    spacing: Data.Config.smallScreen
                ? Data.Constants.smallGapMm
                : Data.Constants.largeGapMm
    anchors.margins: 0
}
