// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import HwControl       1.0 as HwControl
import HwControl.Enums 1.0 as HwControlEnum
import Pages           1.0 as Pages
import Views           1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Cardreader setup page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "CardreaderSetupPage"

    headerData
    {
        enableBack: true
        enableHome: false
        title: ( Data.Debugging.enabled ? objectName : "" ) + "Registration"
    }

    onActiveChanged:
    {
        if ( self.active )
        {
            HwControl.Cardreader.registerCard()
        }

        timer.stop()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property string prompt: ""
        property string promptDetail: ""

        property bool complete: ( HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_SUCCESS ||
                                  HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_ERROR )

        property bool error: ( HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_ERROR ||
                               HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_NULL )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt area.
        ////////////////////////////////////////////////////////////////////////////////
        ColumnLayout
        {
            spacing: Data.Constants.smallGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.prompt
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: Data.Colours.important

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                Layout.leftMargin: Data.Constants.largeGapMm
                Layout.rightMargin: Data.Constants.largeGapMm
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt detail.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.promptDetail
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                Layout.leftMargin: Data.Constants.largeGapMm
                Layout.rightMargin: Data.Constants.largeGapMm
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Cardreader status area.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm
            Layout.leftMargin: Data.Constants.largeGapMm
            Layout.rightMargin: Data.Constants.largeGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Cardreader icon.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EIcon
            {
                id: cardreader
                text: Data.Icons.fa_card
                font.pixelSize: Data.Constants.buttonIconSizeMm * 2
                anchors.centerIn: parent
                color: HwControl.Cardreader.swiping
                            ? Data.Colours.important
                            : Data.Colours.foreground
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Success or failure badge.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Badge
            {
                text: priv.error
                            ? Data.Icons.fa_cross
                            : Data.Icons.fa_tick
                visible: priv.complete
                anchors.right: cardreader.right
                anchors.bottom: cardreader.bottom

                color: priv.error
                            ? Data.Colours. error
                            : Data.Colours.important
                border.color: Data.Colours.foreground
                size: Data.Constants.toolButtonSizeMm
            }

            Controls.DebugRect
            {
                color: "cyan"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Timer to allow navigation to the setup page.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        interval: 2000

        onTriggered:
        {
            if ( HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_ERROR )
            {
                HwControl.Cardreader.registerCard()
            }
            else if ( HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_SUCCESS )
            {
                navigate.back()
            }
            else
            {
                console.log( "[Cardreader] Timer triggered in incorrect state." )
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Handles state changes.
    ////////////////////////////////////////////////////////////////////////////////
    onStateChanged:
    {
        if ( HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_SUCCESS ||
             HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_ERROR )
        {
            // TODO: After 3 attempts exit as a failed state.
            timer.start()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  States.
    ////////////////////////////////////////////////////////////////////////////////
    states:
    [
        State
        {
            when: HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_NULL
            PropertyChanges { target: priv; prompt: "There is a problem with the card reader." }
            PropertyChanges { target: priv; promptDetail: "Please contact your system administrator." }
        },

        State
        {
            when: HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER
            PropertyChanges { target: priv; prompt: "Please present your RFID card to the card reader." }
            PropertyChanges { target: priv; promptDetail: "" }
        },

        State
        {
            when: HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_ERROR
            PropertyChanges { target: priv; prompt: "Failed to read RFID card." }
            PropertyChanges { target: priv; promptDetail: "Please try again." }
        },

        State
        {
            when: HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_SUCCESS
            PropertyChanges { target: priv; prompt: "RFID card read successfully." }
            PropertyChanges { target: priv; promptDetail: "Returning to the registration page." }
        }
    ]
}
