// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Data            1.0 as Data
import Database        1.0 as Database
import Controls        1.0 as Controls
import Pages           1.0 as Pages
import Views           1.0 as Views
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Open cabinet return key page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "OpenCabinetReturnKeyPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + Database.Control.selectedCabinet.name
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tags info.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: tagsInfo
        spacing: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Information area.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.fillWidth: true
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Tags info.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: "Please return keys to the correct slot."
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                visible: !tagsView.selecting
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Cancel select all button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EToolButton
            {
                text: "Cancel"
                width: Data.Constants.buttonSizeMm
                height: parent.height
                visible: tagsView.selecting
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter

                onClicked:
                {
                    tagsView.selecting = false
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Select all button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EToolButton
            {
                text: "Select all"
                width: Data.Constants.buttonSizeMm * 2
                height: parent.height
                checkable: true
                visible: tagsView.selecting
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                palette.buttonText: Data.Colours.foreground

                onCheckedChanged:
                {
                    tagsView.selectAll( checked )
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Background rectangle.
                ////////////////////////////////////////////////////////////////////////////////
                background: Rectangle
                {
                    id: background
                    anchors.fill: parent
                    border.width: 1
                    border.color: Data.Colours.transparent

                    color: self.pressed
                                ? Data.Colours.foreground
                                : Data.Colours.transparent

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Debug rectangle.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.DebugRect
                    {
                        color: "magenta"
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tags area.
        ////////////////////////////////////////////////////////////////////////////////
        Views.TagsList
        {
            id: tagsView
            model: Database.Control.cabinetTags

            delegate: Delegates.TagDetail
            {
                Layout.preferredWidth: tagsView.width
                Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                onClicked: function()
                {
                    navigate.tagInfo( tagData )
                }
            }
        }
    }
}
