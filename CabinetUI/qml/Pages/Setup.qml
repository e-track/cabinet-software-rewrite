// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import HwControl       1.0 as HwControl
import HwControl.Enums 1.0 as HwControlEnum
import Pages           1.0 as Pages
import Views           1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  First time setup page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "SetupPage"

    headerData
    {
        showLogout: true
        enableBack: true
        enableHome: false
        enableLogout: priv.fingerprint || priv.cardreader
        logoutText: "Save all"

        title: ( Data.Debugging.enabled ? objectName : "" ) + "Registration"
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        ///< Fingerprint has been registered (TODO: replace with hardware and database properties).
        property bool fingerprint: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_SUCCESS

        ///< Card has been registered (TODO: replace with hardware and database properties).
        property bool cardreader: HwControl.Cardreader.status === HwControlEnum.Cardreader.STATUS_REGISTER_SUCCESS

        ///< Facial recognition has been set up (TODO: replace with hardware and database properties).
        property bool facialRecognition: false

        property string promptNewPin: HwControl.Cabinet.internetAccess
                                        ? promptAutoPinEmailed
                                        : promptAutoPinAdmin

        property string promptAutoPinEmailed: "A new PIN has been generated for you and sent to your email address."
        property string promptAutoPinAdmin: "A new PIN has been generated for you which you can get from your system administrator."
        property string promptNewPinAccepted: "Your new PIN has been accepted."
        property string promptAuthenticationComplete: "Identification accepted."
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Override to allow the back button to navigate to the welcome page.
    ////////////////////////////////////////////////////////////////////////////////
    navigateBack: function()
    {
        Database.Control.logout()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Override to allow the logout button to save all changes before
    ///         navigating to the welcome page.
    ////////////////////////////////////////////////////////////////////////////////
    navigateLogout: function()
    {
        Database.Control.saveAll()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt area.
        ////////////////////////////////////////////////////////////////////////////////
        ColumnLayout
        {
            id: column
            spacing: Data.Constants.smallGapMm
            anchors.margins: 0

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.leftMargin: Data.Constants.largeGapMm
            Layout.rightMargin: Data.Constants.largeGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text:  priv.fingerprint || priv.cardreader
                            ? priv.promptAuthenticationComplete
                            : ( HwControl.Cabinet.autoGeneratePin
                                    ? priv.promptNewPin
                                    : priv.promptNewPinAccepted )

                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                color: Data.Colours.important

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt detail.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.fingerprint || priv.cardreader
                        ? "Please sign out to complete setup."
                        : "Please set up additional identification below."
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Fingerprint and swipe card buttons.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm
            Layout.leftMargin: Data.Constants.largeGapMm
            Layout.rightMargin: Data.Constants.largeGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Button layout.
            ////////////////////////////////////////////////////////////////////////////////
            RowLayout
            {
                anchors.centerIn: parent
                spacing: Data.Constants.largeGapMm

                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Add fingerprint icon.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EButton
                {
                    id: fingerprint
                    text: "Fingerprint"
                    iconSource: Data.Icons.fa_fingerprint
                    font.pixelSize: Data.Constants.tinyFontPx
                    palette.buttonText: HwControl.Fingerprint.pressed && !priv.fingerprint
                                            ? Data.Colours.important
                                            : Data.Colours.foreground
                    visible: true

                    onClicked:
                    {
                        navigate.fingerprintSetup()
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Fingerprint completed badge.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.Badge
                    {
                        visible: priv.fingerprint
                        text: Data.Icons.fa_tick
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.right

                        color: Data.Colours.important
                        border.color: Data.Colours.foreground
                        size: Data.Constants.toolButtonSizeMm
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Add swipe card icon.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EButton
                {
                    id: cardreader
                    text: "Swipe card"
                    iconSource: Data.Icons.fa_card
                    font.pixelSize: Data.Constants.tinyFontPx
                    palette.buttonText: HwControl.Cardreader.swiping && !priv.cardreader
                                            ? Data.Colours.important
                                            : Data.Colours.foreground
                    visible: true

                    onClicked:
                    {
                        navigate.cardreaderSetup()
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Cardreader completed badge.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.Badge
                    {
                        visible: priv.cardreader
                        text: Data.Icons.fa_tick
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.right

                        color: Data.Colours.important
                        border.color: Data.Colours.foreground
                        size: Data.Constants.toolButtonSizeMm
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Set up facial recognition icon.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EButton
                {
                    id: facialRecognition
                    text: "Facial recognition"
                    iconSource: Data.Icons.fa_camera
                    font.pixelSize: Data.Constants.tinyFontPx
                    palette.buttonText: priv.facialRecognition
                                            ? Data.Colours.important
                                            : Data.Colours.foreground
                    visible: false

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Facial recognition completed badge.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.Badge
                    {
                        visible: priv.facialRecognition
                        text: Data.Icons.fa_tick
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.right

                        color: Data.Colours.important
                        border.color: Data.Colours.foreground
                        size: Data.Constants.toolButtonSizeMm
                    }
                }
            }

            Controls.DebugRect
            {
                color: "green"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Connection to cardreader controller.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: HwControl.Cardreader

        function onAccepted()
        {
            priv.cardreader = true
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Connection to database controller.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Database.Control

        function onSaveAllSuccess()
        {
            Database.Control.logout()
        }
    }
}
