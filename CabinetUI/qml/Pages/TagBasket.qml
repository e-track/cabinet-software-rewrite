// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Popups          1.0 as Popups
import Views           1.0 as Views
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "TagBasket"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "Keys Basket" )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tags basket.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: tagsInfo
        spacing: Data.Config.smallScreen
                 ? Data.Constants.smallGapMm
                 : Data.Constants.largeGapMm

        anchors.margins: Data.Config.smallScreen
                            ? Data.Constants.smallGapMm
                            : Data.Constants.largeGapMm
        anchors.bottomMargin: 0

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tags basket heading.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: "Keys ready to be checked out:"
            color: Data.Colours.important
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tags basket list.
        ////////////////////////////////////////////////////////////////////////////////
        Views.TagsList
        {
            id: basketTags
            model: Database.Control.basketTags

            delegate: Delegates.TagDetail
            {
                taking: true

                Layout.preferredWidth: basketTags.width
                Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                onClicked: function()
                {
                    navigate.tagInfo( tagData )
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Remove tags / keys button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EButton
        {
            id: removeButton
            text: "Remove Keys"

            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: Data.Constants.buttonSizeMm * 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm
            Layout.bottomMargin: Data.Constants.largeGapMm

            onClicked:
            {
                Database.Control.completeCheckoutBasket()

                messageBox.open()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Notification pop-up.
    ////////////////////////////////////////////////////////////////////////////////
    Popups.MessageBox
    {
        id: messageBox
        title: "Cabinet is now open"
        message: "Please remove the selected keys."

        onClosed:
        {
            Database.Control.queryTagsForCabinet( Database.Control.selectedCabinet.cabinetId, Data.Config.numTagsPerPage )

            navigate.back()
        }
    }
}
