// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import HwControl       1.0 as HwControl
import HwControl.Enums 1.0 as HwControlEnum
import Pages           1.0 as Pages
import Views           1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Fingerprint setup page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "FingerprintSetupPage"

    headerData
    {
        enableBack: true
        enableHome: false
        title: ( Data.Debugging.enabled ? objectName : "" ) + "Registration"
    }

    onActiveChanged:
    {
        if ( self.active )
        {
            HwControl.Fingerprint.enroll()
        }

        timer.stop()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property string prompt: ""
        property string promptDetail: ""

        property bool enrollComplete: ( HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_SUCCESS ||
                                        HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_ERROR )

        property bool error: ( HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_ERROR ||
                               HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_NULL )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt area.
        ////////////////////////////////////////////////////////////////////////////////
        ColumnLayout
        {
            spacing: Data.Constants.smallGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.prompt
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: Data.Colours.important

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                Layout.leftMargin: Data.Constants.largeGapMm
                Layout.rightMargin: Data.Constants.largeGapMm
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt detail.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.promptDetail
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                Layout.leftMargin: Data.Constants.largeGapMm
                Layout.rightMargin: Data.Constants.largeGapMm
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Fingerprint status area.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm
            Layout.leftMargin: Data.Constants.largeGapMm
            Layout.rightMargin: Data.Constants.largeGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Fingerprint icon.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EIcon
            {
                id: fingerprint
                text: Data.Icons.fa_fingerprint
                font.pixelSize: Data.Constants.buttonIconSizeMm * 2
                anchors.centerIn: parent
                color: HwControl.Fingerprint.pressed
                            ? Data.Colours.important
                            : Data.Colours.foreground
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Success or failure badge.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Badge
            {
                text: priv.error
                            ? Data.Icons.fa_cross
                            : Data.Icons.fa_tick
                visible: priv.enrollComplete
                anchors.right: fingerprint.right
                anchors.bottom: fingerprint.bottom

                color: priv.error
                            ? Data.Colours. error
                            : Data.Colours.important
                border.color: Data.Colours.foreground
                size: Data.Constants.toolButtonSizeMm
            }

            Controls.DebugRect
            {
                color: "cyan"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Timer to allow navigation to the setup page.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        interval: 2000

        onTriggered:
        {
            if ( HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_ERROR )
            {
                HwControl.Fingerprint.enroll()
            }
            else if ( HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_SUCCESS )
            {
                navigate.back()
            }
            else
            {
                console.log( "[FingerprintSetup] Timer triggered in incorrect state." )
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Handles state changes.
    ////////////////////////////////////////////////////////////////////////////////
    onStateChanged:
    {
        if ( priv.enrollComplete )
        {
            // TODO: After 3 attempts exit as a failed state.
            timer.start()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  States.
    ////////////////////////////////////////////////////////////////////////////////
    states:
    [
        State
        {
            when: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_NULL
            PropertyChanges { target: priv; prompt: "There is a problem with the fingerprint reader." }
            PropertyChanges { target: priv; promptDetail: "Please contact your system administrator." }
        },

        State
        {
            when: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL
            PropertyChanges { target: priv; prompt: "Please press your finger on the panel." }
            PropertyChanges { target: priv; promptDetail: "" }
        },

        State
        {
            when: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_ERROR
            PropertyChanges { target: priv; prompt: "Failed to read fingerprint." }
            PropertyChanges { target: priv; promptDetail: "Please try again." }
        },

        State
        {
            when: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_IN_PROGRESS
            PropertyChanges { target: priv; prompt: "Fingerprint read successfully." }
            PropertyChanges { target: priv; promptDetail: "Please press your finger on the panel again." }
        },

        State
        {
            when: HwControl.Fingerprint.status === HwControlEnum.Fingerprint.STATUS_ENROLL_SUCCESS
            PropertyChanges { target: priv; prompt: "Fingerprint read successfully." }
            PropertyChanges { target: priv; promptDetail: "Returning to the registration page." }
        }
    ]
}
