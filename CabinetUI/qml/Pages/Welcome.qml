// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls         1.0 as Controls
import Controls.Keypad  1.0 as KeypadControls
import Data             1.0 as Data
import Database         1.0 as Database
import HwControl        1.0 as HwControl
import Keypad           1.0 as Keypad
import Pages            1.0 as Pages
import Views            1.0 as Views
import Views.Delegates  1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Welcome page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "WelcomePage"

    headerData
    {
        showBack: false
        showHome: false
        showLocked: true
        showDivider: false
        showRegister: true
        title: ( Data.Debugging.enabled ? objectName : "" )
    }

    onActiveChanged:
    {
        startAutoIdentify()

        if ( active )
        {
            pin.forceActiveFocus()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

         property string prompt: priv.authenticated
                                    ? "Authorised, please enter your PIN."
                                    : ( timer.running
                                        ? ( priv.accessDenied ? "Access denied, please contact your system administrator."
                                                              : "Incorrect PIN, please try again." )
                                        : "Please identify yourself." )
        property bool authenticated: false
        property bool accessDenied: false
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout.
    ///         TODO: Use a Loader for the small / large screen then we will be able to
    ///         force the active focus in the Component.onCompleted function. I think
    ///         this is a better architecture.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {
            visible: !Data.Config.metPolice
        }

        spacing: Data.Config.metPolice
                    ? 0
                    : ( Data.Config.smallScreen
                            ? Data.Constants.smallGapMm
                            : Data.Constants.largeGapMm )

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Welcome and logo area.
        ////////////////////////////////////////////////////////////////////////////////
        ColumnLayout
        {
            spacing: 0

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Welcome text.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                id: welcome
                text: "Welcome to"
                color: Data.Colours.important

                Layout.alignment: Qt.AlignHCenter
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Company logo.
            ////////////////////////////////////////////////////////////////////////////////
            Image
            {
                id: etrack
                source: Data.Icons.logo
                fillMode: Image.PreserveAspectFit
                smooth: true

                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                Layout.preferredHeight: Data.Constants.logoHeightMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Debug rectangle.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.DebugRect
                {
                    color: "yellow"
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            id: prompt
            text: priv.prompt + "\n"
            maximumLineCount: 2

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  PIN entry.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.ETextInput
        {
            id: pin
            font.pixelSize:  Data.Constants.mediumFontPx
            focus: true
            objectName: "WelcomePIN"
            echoMode: TextInput.Password
            passwordCharacter: "*"
            text: priv.authenticated
                    ? Keypad.Control.entry
                    : ""
            borderColour: Data.Colours.transparent

            cursorDelegate: Rectangle
            {
                color: Data.Colours.transparent
            }

            verticalAlignment: Data.Config.smallScreen
                                ? Text.AlignTop
                                : Text.AlignVCenter

            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: Data.Constants.buttonSizeMm
            Layout.preferredHeight: !Data.Config.metPolice
                                        ? Data.Constants.buttonSizeMm
                                        : -1
            Layout.topMargin: Data.Config.smallScreen
                                ? -Data.Constants.largeGapMm    // TODO: Fix this alignment bug on small screens.
                                : 0

            Controls.DebugRect
            {
                color: "white"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tag / key categories.
        ////////////////////////////////////////////////////////////////////////////////
        GridLayout
        {
            rows: 2
            columns: 3
            columnSpacing: Data.Constants.largeGapMm
            rowSpacing: Data.Constants.largeGapMm
            visible: Data.Config.largeScreen &&
                     Data.Config.metPolice

            Layout.alignment: Qt.AlignCenter

            Repeater
            {
                model: Database.Control.categoryTags

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Currently available tags / keys.
                ////////////////////////////////////////////////////////////////////////////////
                Delegates.TagCategory
                {
                    tagCategoryData: Database.Control.tagCategories[index]
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  PIN keypad.
        ////////////////////////////////////////////////////////////////////////////////
        KeypadControls.Keypad
        {
            visible: Data.Config.largeScreen
            model: KeypadControls.Numeric_EnUK {}

            Layout.alignment: Qt.AlignHCenter
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {
            visible: !Data.Config.metPolice
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Button layout.
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        spacing: Data.Constants.smallGapMm

        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Add fingerprint icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EIcon
        {
            id: fingerprint
            text: Data.Icons.fa_fingerprint
            color: HwControl.Fingerprint.pressed
                        ? Data.Colours.important
                        : Data.Colours.foreground
            visible: true
            font.pixelSize: Data.Constants.toolButtonIconSizeMm
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Add swipe card icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EIcon
        {
            id: cardreader
            text: Data.Icons.fa_card
            color: HwControl.Cardreader.swiping
                        ? Data.Colours.important
                        : Data.Colours.foreground
            visible: true
            font.pixelSize: Data.Constants.toolButtonIconSizeMm
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Override to allow the registration button to navigate to the registration page.
    ////////////////////////////////////////////////////////////////////////////////
    navigateForward: function()
    {
        navigate.setupPin()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Keypad control connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Keypad.Control

        function onKeyPressed( keyCode )
        {
            if ( priv.authenticated )
            {
                if ( keyCode === Qt.Key_Enter ||
                     keyCode === Qt.Key_Return )
                {
                    Database.Control.loginPin( Keypad.Control.entry )
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Database connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Database.Control

        function onLoginSuccess()
        {
            if ( self.active )
            {
                Keypad.Control.entry = ""
                navigate.home()
            }
        }

        function onLoginFail()
        {
            if ( self.active )
            {
                Keypad.Control.entry = ""
                timer.start()
            }
        }

        function onAuthenticatedUserChanged()
        {
            if ( self.active )
            {
                Keypad.Control.entry = ""

                priv.authenticated = ( Database.Control.authenticatedUser.id > -1 )
            }
        }


        function onAccessDenied()
        {
            if ( self.active )
            {
                Keypad.Control.entry = ""
                priv.accessDenied = true
                timer.start()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Data connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Data.Config

        function onSmallScreenChanged()
        {
            if ( self.active )
            {
                pin.forceActiveFocus()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Timer to show log in error.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        interval: 2000

        onTriggered:
        {
            priv.accessDenied = false
            // Start auto identify again.
            self.startAutoIdentify()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Starts card and fingerprint automatic identification.
    ////////////////////////////////////////////////////////////////////////////////
    function startAutoIdentify()
    {
        HwControl.Fingerprint.autoIdentify( self.active )
        HwControl.Cardreader.identify( self.active )

        priv.authenticated = false
    }
}
