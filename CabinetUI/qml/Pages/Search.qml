// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Views           1.0 as Views
import Views.Delegates 1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Search page.
///         TODO: Derive from Pages.Base.
////////////////////////////////////////////////////////////////////////////////
Pages.OpenCabinetTakeKey
{
    id: self
    objectName: "SearchPage"
    searching: true
    model: Database.Control.searchTags

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + "Searching..."
    }
}
