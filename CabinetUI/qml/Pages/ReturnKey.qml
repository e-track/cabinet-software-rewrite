// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Views           1.0 as Views
import Views.Delegates 1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Return key page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "ReturnKeyPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + Database.Control.selectedCabinet.name
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        spacing: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tags info.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: "You currently have 5 keys out."
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Layout.preferredWidth: parent.width / 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm
            Layout.alignment: Qt.AlignHCenter
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Cabinets view.
        ////////////////////////////////////////////////////////////////////////////////
        Views.CabinetsView
        {
            delegate: Views.Cabinet
            {
                showBadge: true

                onClicked:
                {
                    var cab = Database.Control.cabinets[index]
                    navigate.openCabinetReturnKey( cab )
                }
            }
        }
    }
}
