// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Popups          1.0 as Popups
import Views           1.0 as Views
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "CheckedOutTagsPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "Return keys" )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tags info.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: tagsInfo

        anchors.margins: Data.Config.smallScreen
                            ? Data.Constants.smallGapMm
                            : Data.Constants.largeGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Currently checked out heading.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            text: "Please select keys to return:"
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Currently checked out tags.
        ////////////////////////////////////////////////////////////////////////////////
        Views.TagsList
        {
            id: checkedOutTags
            model: Database.Control.userTags
            selecting: true

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Tag delegate.
            ////////////////////////////////////////////////////////////////////////////////
            delegate: Item
            {
                Layout.preferredWidth: checkedOutTags.width
                Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Tag delegate layout.
                ////////////////////////////////////////////////////////////////////////////////
                RowLayout
                {
                    anchors.fill: parent

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Check button.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.EButton
                    {
                        Layout.preferredWidth: Data.Constants.toolButtonSizeMm
                        Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                        badgeSize: Data.Constants.toolButtonSizeMm * 0.75
                        checkable: true

                        onCheckedChanged:
                        {
                            if ( tag.tagData.isReturning )
                            {
                                tag.tagData.isReturning = false
                                Database.Control.removeTagFromReturning( tag.tagData )
                            }
                            else
                            {
                                tag.tagData.isReturning = true
                                Database.Control.addTagToReturning( tag.tagData )
                            }
                        }
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Tag details.
                    ////////////////////////////////////////////////////////////////////////////////
                    Delegates.TagDetail
                    {
                        id: tag

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onClicked: function()
                        {
                            navigate.tagInfo( tagData )
                        }
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Return tags / keys button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EButton
        {
            id: returnButton
            text: "Return keys"

            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: Data.Constants.buttonSizeMm * 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm
            Layout.bottomMargin: Data.Constants.largeGapMm

            onClicked:
            {
                Database.Control.completeReturnTags()

                messageBox.open()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Notification pop-up.
    ////////////////////////////////////////////////////////////////////////////////
    Popups.MessageBox
    {
        id: messageBox
        title: "Cabinet is now open"
        message: "Please return the selected keys to the correct slots."

        onClosed:
        {
            navigate.back()
        }
    }
}
