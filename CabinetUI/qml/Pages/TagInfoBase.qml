// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Database 1.0 as Database
import Pages    1.0 as Pages
import Views    1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "TagInfoBase"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + tagData.name
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property var tagData: Database.TagData {}
}
