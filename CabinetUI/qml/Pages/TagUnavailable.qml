// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Pages    1.0 as Pages
import Views    1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.TagInfoBase
{
    id: self
    objectName: "TagUnavailable"

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout content.
    ///         TODO: If all these fields are simple key / value text items then
    ///         consider using a Repeater so any data can be represented in it.
    ///         This could then be an 'Info' control.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content
        spacing: Data.Config.smallScreen
                 ? Data.Constants.smallGapMm
                 : Data.Constants.largeGapMm

        anchors.margins: Data.Config.smallScreen
                            ? Data.Constants.smallGapMm
                            : Data.Constants.largeGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Border.
        ///         TODO: Create Views.BorderContent for this layout since it is
        ///         duplicated in other items.
        ////////////////////////////////////////////////////////////////////////////////
        Rectangle
        {
            id: box
            color: Data.Colours.transparent
            border.color: Data.Colours.error

            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout
            {
                spacing: Data.Constants.smallGapMm

                anchors.fill: parent
                anchors.margins: Data.Config.smallScreen
                                    ? Data.Constants.smallGapMm
                                    : Data.Constants.largeGapMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Tag icon.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EIcon
                {
                    id: icon
                    font.pixelSize: Data.Constants.buttonIconSizeMm
                    text: Data.Icons.fa_key
                    visible: Data.Config.largeScreen

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Badge indicating a notification or warning.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.Badge
                    {
                        id: badge
                        color: Data.Colours.error
                        border.color: Data.Colours.foreground
                        size: Data.Constants.badgeSizeMm
                        text: "!"
                        anchors.bottom: icon.bottom
                        anchors.right: icon.right
                        visible: self.tagData.isWrongSlot
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Prompt.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EText
                {
                    id: prompt
                    text: "This key is not available."
                    color: Data.Colours.error
                }


                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Spacer.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Spacer
                {

                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Checked out user.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: checkedOutUser
                    label.text: "Checked out with user"
                    value.text: tagData.userId
                    value.color: Data.Colours.error
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Site.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: site
                    label.text: "Site"
                    value.text: tagData.site
                    value.color: Data.Colours.error
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Location.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: location
                    label.text: "Location"
                    value.text: tagData.location
                    value.color: Data.Colours.error
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Spacer.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Spacer
                {

                }
            }
        }
    }
}
