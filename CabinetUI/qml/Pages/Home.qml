// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls         1.0 as Controls
import Controls.Keypad  1.0 as KeypadControls
import Data             1.0 as Data
import Database         1.0 as Database
import Keypad           1.0 as Keypad
import Pages            1.0 as Pages
import Views            1.0 as Views
import Views.Delegates  1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Home page / dashboard.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "HomePage"

    property bool searching: false

    headerData
    {
        showLogout: true
        enableBack: false
        enableHome: false
        title: ( Data.Debugging.enabled ? objectName : "" ) + Database.Control.loggedInUser.firstName
    }

    onActiveChanged:
    {
        if ( active )
        {
            Database.Control.queryCabinets()

            contentLoader.forceActiveFocus()
        }
    }

    Connections
    {
        target: Database.Control

        function onCabinetsChanged()
        {
            // TODO: Until all queries are asyncronous we need to wait for the cabinets query
            // to complete before we can start the user tags query.
            Database.Control.queryTagsForUser( Database.Control.loggedInUser.id );
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Loader
    {
        id: contentLoader
        sourceComponent: Data.Config.smallScreen
                            ? smallComponent
                            : largeComponent

        anchors.fill: parent
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Small 5 inch content component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: smallComponent

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Small 5 inch content.
        ////////////////////////////////////////////////////////////////////////////////
        Views.Content
        {
            Views.HomeButtons
            {

            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Large 10 inch content component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: largeComponent

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Large 10 inch content.
        ////////////////////////////////////////////////////////////////////////////////
        Views.Content
        {
            anchors.fill: parent
            spacing: Data.Constants.largeGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Top area (TODO: remove when design approved).
            ////////////////////////////////////////////////////////////////////////////////
            Loader
            {
                sourceComponent: Data.Debugging.togglePages
                                    ? cabinetsComponent
                                    : tagStatusComponent

                Layout.fillWidth: true
                Layout.fillHeight: Data.Debugging.togglePages
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Basket button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EToolButton
            {
                text: "Return keys"

                Layout.preferredWidth: Data.Constants.buttonSizeMm * 2

                onClicked: function()
                {
                    navigate.checkedOutTags()
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Divider.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Divider
            {

            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Bottom area (TODO: remove when design approved).
            ////////////////////////////////////////////////////////////////////////////////
            Loader
            {
                sourceComponent: Data.Debugging.togglePages
                                    ? tagStatusComponent
                                    : cabinetsComponent

                Layout.fillWidth: true
                Layout.fillHeight: !Data.Debugging.togglePages
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Divider.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.Divider
            {

            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Keypad.
            ////////////////////////////////////////////////////////////////////////////////
            KeypadControls.Keypad
            {
                id: keypad
                model: KeypadControls.AlphaNumericCompact_EnUK {}

                Layout.alignment: Qt.AlignHCenter
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tag status component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: tagStatusComponent

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Row of key statuses.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            implicitWidth: row.width
            implicitHeight: row.height

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Row layout.
            ////////////////////////////////////////////////////////////////////////////////
            RowLayout
            {
                id: row
                anchors.centerIn: parent
                spacing: Data.Constants.largeGapMm

                implicitHeight: checkedOutTags.height

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Checked out tags status.
                ////////////////////////////////////////////////////////////////////////////////
                Delegates.Counter
                {
                    id: checkedOutTags
                    title: "You currently have checked out:"
                    iconSource: Data.Icons.fa_key
                    showBadge: false
                    model: Database.Control.userTags
                    quantity: Database.Control.userTags.length

                    onModelChanged:
                    {
                        var checkedOut = 0

                        // TODO: Use DB query to get list of overdue keys
                        // and use the length of that to determine whether or not
                        // to show the badge.
                        for( var i = 0; i < model.length; i++ )
                        {
                            var tag = model[ i ]

                            if ( tag.isWrongSlot === true )
                            {
                                checkedOutTags.showBadge = true
                            }
                        }
                    }

                    onClicked:
                    {
                        navigate.checkedOutTags()
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Debug rectangle.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.DebugRect
            {
                color: "blue"
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Cabinets area component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: cabinetsComponent

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Cabinets area.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Column layout.
            ////////////////////////////////////////////////////////////////////////////////
            ColumnLayout
            {
                anchors.fill: parent
                spacing: Data.Constants.largeGapMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Search item.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.ETextInput
                {
                    id: search
                    placeholderText: "Search for a key..."
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    Layout.preferredWidth: parent.width / 2
                    Layout.preferredHeight: Data.Constants.toolButtonSizeMm
                    Layout.alignment: Qt.AlignHCenter
                    text: Keypad.Control.entry

                    onTextChanged:
                    {
                        timer.restart()
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Cabinets view.
                ////////////////////////////////////////////////////////////////////////////////
                Views.CabinetsView
                {
                    model: Database.Control.cabinets

                    delegate: Delegates.Cabinet
                    {
                        onClicked:
                        {
                            Database.Control.selectCabinet( modelData.cabinetId )
                            navigate.cabinet( modelData.cabinetId )
                        }
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Debug rectangle.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.DebugRect
            {
                color: "magenta"
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Timer to allow navigation to the search page.
            ////////////////////////////////////////////////////////////////////////////////
            Timer
            {
                id: timer
                interval: 2000

                onTriggered:
                {
                    if ( self.active )
                    {
                        var searchText = search.text.trim();

                        if ( searchText !== "" && !self.searching )
                        {
                            self.searching = true
                            navigate.search( search.text )

                            Database.Control.querySearchTags( search.text )

                            self.searching = false
                            search.text = ""
                        }
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Parent connections.
            ////////////////////////////////////////////////////////////////////////////////
            Connections
            {
                target: self

                function onActiveChanged()
                {
                    // Allow search input to have focus.
                    search.forceActiveFocus()
                }

                function onHeightChanged()
                {
                    // Allow search input to have focus.
                    search.forceActiveFocus()
                }
            }

            Component.onCompleted:
            {
                search.forceActiveFocus()
            }
        }
    }
}
