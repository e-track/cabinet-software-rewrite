// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Views           1.0 as Views
import Views.Delegates 1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Take key page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "TakeKeyPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + Database.Control.selectedCabinet.name
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        spacing: Data.Constants.largeGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Search item.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.ETextInput
        {
            id: search
            placeholderText: "Search for a key..."
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Layout.preferredWidth: parent.width / 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm
            Layout.alignment: Qt.AlignHCenter

            onTextChanged:
            {
                timer.restart()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Cabinets view.
        ////////////////////////////////////////////////////////////////////////////////
        Views.CabinetsView
        {
            delegate: Views.Cabinet
            {
                onClicked:
                {
                    var cab = Database.Control.cabinets[index]
                    navigate.openCabinetTakeKey( cab )
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Timer to allow navigation to the search page.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        interval: 2000

        onTriggered:
        {
            if ( self.active )
            {
                //Database.Control.querySearchTags( search.text )
            }
        }
    }
}
