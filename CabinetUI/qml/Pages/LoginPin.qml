//Qt.
import QtQuick

// Library.
import Controls        1.0 as Controls
import Controls.Keypad 1.0 as Keypad
import Data            1.0 as Data
import Pages           1.0 as Pages

////////////////////////////////////////////////////////////////////////////////
/// \brief  PIN login page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "LoginPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" )
        showBack: false
        showHome: false
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Keypad control.
    ////////////////////////////////////////////////////////////////////////////////
    Keypad.Keypad
    {
        id: keypad
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
