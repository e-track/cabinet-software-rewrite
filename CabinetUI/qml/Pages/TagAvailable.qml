// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Database 1.0 as Database
import Pages    1.0 as Pages
import Popups   1.0 as Popups
import Views    1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.TagInfoBase
{
    id: self
    objectName: "TagAvailable"

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property string prompt: tagData.inBasket
                                    ? "This key is in the basket ready to be checked out."
                                    : "This key is available in the system."
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout content.
    ///         TODO: If all these fields are simple key / value text items then
    ///         consider using a Repeater so any data can be represented in it.
    ///         This could then be an 'Info' control.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content
        spacing: Data.Config.smallScreen
                 ? Data.Constants.smallGapMm
                 : Data.Constants.largeGapMm

        anchors.margins: Data.Config.smallScreen
                            ? Data.Constants.smallGapMm
                            : Data.Constants.largeGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Border.
        ///         TODO: Create Views.BorderContent for this layout since it is
        ///         duplicated in other items.
        ////////////////////////////////////////////////////////////////////////////////
        Rectangle
        {
            id: box
            color: Data.Colours.transparent
            border.color: Data.Colours.important

            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout
            {
                spacing: Data.Constants.smallGapMm

                anchors.fill: parent
                anchors.margins: Data.Config.smallScreen
                                    ? Data.Constants.smallGapMm
                                    : Data.Constants.largeGapMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Tag icon.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EIcon
                {
                    id: icon
                    font.pixelSize: Data.Constants.buttonIconSizeMm
                    text: Data.Icons.fa_key
                    visible: Data.Config.largeScreen

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Badge indicating a notification or warning.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.Badge
                    {
                        id: badge
                        color: Data.Colours.error
                        border.color: Data.Colours.foreground
                        size: Data.Constants.badgeSizeMm
                        text: "!"
                        anchors.bottom: icon.bottom
                        anchors.right: icon.right
                        visible: self.tagData.isWrongSlot
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Prompt.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EText
                {
                    id: prompt
                    text: priv.prompt
                    horizontalAlignment: Text.AlignHCenter
                    color: Data.Colours.important

                    Layout.fillWidth: true
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Spacer.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Spacer
                {

                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Slot.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: slot
                    label.text: "Slot"
                    value.text: tagData.slot
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Cabinet.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: cabinet
                    label.text: "Cabinet"
                    value.text: tagData.cabinet
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Site.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: site
                    label.text: "Site"
                    value.text: tagData.site
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Location.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.LabelValueText
                {
                    id: location
                    label.text: "Location"
                    value.text: tagData.location
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Spacer.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Spacer
                {

                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Check out button.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.EButton
                {
                    id: checkOutButton
                    text: tagData.inBasket ? "Undo" : "Check out"

                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredWidth: Data.Constants.buttonSizeMm
                    Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                    onClicked:
                    {
                        if ( tagData.inBasket )
                        {
                            tagData.inBasket = false
                            Database.Control.removeTagFromBasket( tagData )
                        }
                        else
                        {
                            tagData.inBasket = true
                            Database.Control.addTagToBasket( tagData )
                        }

                        messageBox.open()
                    }
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Notification pop-up.
    ////////////////////////////////////////////////////////////////////////////////
    Popups.MessageBox
    {
        id: messageBox
        title: tagData.name
        message: "The key was added to the basket."

        onClosed:
        {
            navigate.back()
        }
    }
}
