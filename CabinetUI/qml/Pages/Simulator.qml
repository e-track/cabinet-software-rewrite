// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls         1.0 as Controls
import Controls.Keypad  1.0 as KeypadControls
import Data             1.0 as Data
import Database         1.0 as Database
import HwControl        1.0 as HwControl
import Keypad           1.0 as Keypad
import Message          1.0 as Message

////////////////////////////////////////////////////////////////////////////////
/// \brief  Provides controls to simulate external devices (i.e. key-pad, card
///         and fingerprint readers etc).
////////////////////////////////////////////////////////////////////////////////
Rectangle
{
    color: Data.Colours.background

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main layout.
    ////////////////////////////////////////////////////////////////////////////////
    ColumnLayout
    {
        anchors.centerIn: parent
        spacing: Data.Constants.largeGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Version text.
        ////////////////////////////////////////////////////////////////////////////////
        GridLayout
        {
            rows: 2
            columns: 2

            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm

            Controls.EText
            {
                text: "Bootloader version:";
                color:  Data.Colours.foreground
            }

            Controls.EText
            {
                text: HwControl.Cabinet.bootloaderVersion
                color:  Data.Colours.important
            }

            Controls.EText
            {
                text: "Application version: "
                color:  Data.Colours.foreground
            }

            Controls.EText
            {
                text: HwControl.Cabinet.applicationVersion
                color:  Data.Colours.important
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top button area.
        ////////////////////////////////////////////////////////////////////////////////
        RowLayout
        {
            spacing: Data.Constants.smallGapMm

            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Quick login button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: quickLoginButton
                text: "Quick login"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    Database.Control.simulateLogin()
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Debug mode button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: debugModeButton
                text: "F1 Debug mode"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    Keypad.Control.invokeKeyEvent( Qt.Key_F1 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Design toggle button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: designToggleButton
                text: "F2 Toggle design"
                font.pixelSize: Data.Constants.smallFontPx
                enabled: false

                onClicked:
                {
                    Keypad.Control.invokeKeyEvent( Qt.Key_F2 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Notification button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: notification
                text: "F3 Notify"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    Keypad.Control.invokeKeyEvent( Qt.Key_F3 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Screen size button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: orientation
                text: "F4 Size"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    Keypad.Control.invokeKeyEvent( Qt.Key_F4 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Keypad toggle button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: keypadModelToggle
                text: "F5 Keypad Model"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    if ( keypad.model === alphaNumeric )
                    {
                        keypad.model = numeric
                    }
                    else
                    {
                        keypad.model = alphaNumeric
                    }

                    Keypad.Control.invokeKeyEvent( Qt.Key_F5 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Keypad toggle button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: keypadModeToggle
                text: "F6 Keypad Function"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    if ( keypad.func < 1 )
                    {
                        keypad.func++
                    }
                    else
                    {
                        keypad.func = 0
                    }

                    Keypad.Control.invokeKeyEvent( Qt.Key_F6 )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Keypad toggle button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: layoutOptionToggle
                text: "F7 Toggle Layout"
                font.pixelSize: Data.Constants.smallFontPx

                onClicked:
                {
                    Keypad.Control.invokeKeyEvent( Qt.Key_F7 )
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Keypad.
        ////////////////////////////////////////////////////////////////////////////////
        KeypadControls.Keypad
        {
            id: keypad
            model: alphaNumeric

            Layout.alignment: Qt.AlignHCenter

            KeypadControls.AlphaNumeric_EnUK
            {
                id: alphaNumeric
            }

            KeypadControls.Numeric_EnUK
            {
                id: numeric
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom button area.
        ////////////////////////////////////////////////////////////////////////////////
        RowLayout
        {
            spacing: Data.Constants.smallGapMm

            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Data.Constants.buttonSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Fingerprint button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: fingerprintButton
                text: "Fingerprint"
                iconSource: Data.Icons.fa_fingerprint
                font.pixelSize: Data.Constants.tinyFontPx
                visible: false // Disabled until hardware control changes implemented.

                onPressed: HwControl.Fingerprint.simulatePressed()
                onReleased: HwControl.Fingerprint.simulateReleased()
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Card reader button.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: cardReaderButton
                text: "Card reader"
                iconSource: Data.Icons.fa_card
                font.pixelSize: Data.Constants.tinyFontPx
                visible: false // Disabled until hardware control changes implemented.

                onClicked:
                {
                    HwControl.Cardreader.simulateSwipe()
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Topic text entry.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.ETextInput
            {
                id: topic
                placeholderText: "Topic"

                Layout.preferredWidth: Data.Constants.buttonSizeMm * 2
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Message text entry.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.ETextInput
            {
                id: message
                placeholderText: "Message"

                Layout.preferredWidth: Data.Constants.buttonSizeMm * 2
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Publish custom message.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: publishMessage
                text: "Publish\nMessage"
                iconSource: Data.Icons.fa_envelope
                font.pixelSize: Data.Constants.tinyFontPx

                onClicked:
                {
                    Message.Manager.publish( topic.text, message.text )
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Event combobox.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EComboBox
            {
                id: eventCombo

                model: ListModel
                {
                    ListElement { text: "login_success" }
                    ListElement { text: "login_failed" }
                    ListElement { text: "logout" }
                    ListElement { text: "access_denied" }
                    ListElement { text: "key_in" }
                    ListElement { text: "key_out" }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Publish event.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EButton
            {
                id: publishEvent
                text: "Publish\nEvent"
                iconSource: Data.Icons.fa_envelope
                font.pixelSize: Data.Constants.tinyFontPx

                onClicked:
                {
                    Message.Manager.simulateEvent( eventCombo.currentText )
                }
            }
        }
    }
}
