// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Data            1.0 as Data
import Database        1.0 as Database
import Pages           1.0 as Pages
import Views           1.0 as Views
import Views.Delegates 1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Status page for currently checked out / available tags.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "Cabinet"

    headerData
    {
        // TODO: Selected cabinet must be set so this line doesn't produce a warning.
        title: ( Data.Debugging.enabled ? objectName : "" ) + ( typeof Database.Control.selectedCabinet === "undefined"
                                                               ? ""
                                                               : Database.Control.selectedCabinet.cabinetName )
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property var model

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tags info.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: tagsInfo

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tab bar shows a button for each group of tags / keys.
        ////////////////////////////////////////////////////////////////////////////////
        TabBar
        {
            id: tabBar

            Layout.fillWidth: true
            Layout.preferredHeight: Data.Constants.toolSizeMm

            Repeater
            {
                model: Database.Control.cabinetTags

                Controls.ETabButton
                {
                    property int startId: modelData[0].tagId
                    property int endId: modelData[modelData.length - 1].tagId

                    text: startId + " to " + endId
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Stack for each group of tags / keys.
        ////////////////////////////////////////////////////////////////////////////////
        StackLayout
        {
            currentIndex: tabBar.currentIndex

            Layout.fillWidth: true
            Layout.fillHeight: true

            Repeater
            {
                id: repeater
                model: Database.Control.cabinetTags

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Currently checked out tags.
                ////////////////////////////////////////////////////////////////////////////////
                delegate: Views.TagsGrid
                {
                    id: tags
                    model: modelData


                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    delegate: Delegates.TagSimple
                    {
                        Layout.alignment: Qt.AlignVCenter
                        Layout.preferredWidth: Data.Constants.toolButtonSizeMm
                        Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                        onClicked: function()
                        {
                            if ( tagData.isOut )
                            {
                                navigate.tagUnavailable( tagData )
                            }
                            else
                            {
                                navigate.tagAvailable( tagData )
                            }
                        }
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Basket button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            text: "Basket"

            Layout.preferredWidth: Data.Constants.buttonSizeMm * 2
            Layout.bottomMargin: Data.Constants.largeGapMm

            onClicked: function()
            {
                navigate.tagBasket()
            }
        }
    }
}
