// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls         1.0 as Controls
import Controls.Keypad  1.0 as KeypadControls
import Data             1.0 as Data
import Database         1.0 as Database
import Pages            1.0 as Pages
import Views            1.0 as Views
import Views.Delegates  1.0 as Delegates

////////////////////////////////////////////////////////////////////////////////
/// \brief  Open cabinet take key page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "OpenCabinetTakeKeyPage"

    headerData
    {
        title: ( Data.Debugging.enabled ? objectName : "" ) + Database.Control.selectedCabinet.name

        showForward: true
        enableForward: tagsView.selecting && tagsView.selected > 0
    }

    onVisibleChanged:
    {
        // Always disable multiple selection when the page is shown.
        tagsView.selecting = false
    }

    onActiveChanged:
    {
        if ( self.active )
        {
            search.forceActiveFocus()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property bool searching: false              ///< True if searching is active.
    property alias searchText: search.text      ///< Alias to the search text.
    property alias model:  tagsView.model

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Override to allow the back button to first disable multiple selection
    ///         of tags before navigating back.
    ////////////////////////////////////////////////////////////////////////////////
    navigateBack: function()
    {
        if ( tagsView.selecting )
        {
            tagsView.selecting = false
        }
        else
        {
            navigate.back()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Tags info.
    ///         TODO: Use a Loader for the small / large screen then we will be able to
    ///         force the active focus in the Component.onCompleted function. I think
    ///         this is a better architecture.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: tagsInfo

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Search item.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.ETextInput
        {
            id: search
            placeholderText: "Search for a key..."
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            visible: self.searching

            Layout.preferredWidth: parent.width / 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm
            Layout.alignment: Qt.AlignHCenter

            onTextChanged:
            {
                timer.restart()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Tags area.
        ////////////////////////////////////////////////////////////////////////////////
        Views.TagsList
        {
            id: tagsView
            taking: true
            model: Database.Control.cabinetTags

            Layout.leftMargin: Data.Constants.largeGapMm - Data.Constants.smallGapMm
            Layout.rightMargin: Data.Constants.largeGapMm - Data.Constants.smallGapMm

            delegate: Delegates.TagDetail
            {
                Layout.preferredWidth: tagsView.width
                Layout.preferredHeight: Data.Constants.toolButtonSizeMm

                onClicked: function()
                {
                    if ( tagData.isOut )
                    {
                        navigate.tagUnavailable( tagData )
                    }
                    else
                    {
                        navigate.tagAvailable( tagData )
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Divider.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Divider
        {

        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Keypad.
        ////////////////////////////////////////////////////////////////////////////////
        KeypadControls.Keypad
        {
            id: keypad
            model: KeypadControls.AlphaNumericCompact_EnUK {}
            visible: Data.Config.largeScreen

            Layout.alignment: Qt.AlignHCenter
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Data connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Data.Config

        function onSmallScreenChanged()
        {
            if ( self.active )
            {
                search.forceActiveFocus()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Timer to allow navigation to the search page.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        interval: 2000

        onTriggered:
        {
            if ( self.active )
            {
                var searchText = search.text.trim();

                if ( searchText !== "" )
                {
                    //Database.Control.querySearchTags( search.text )
                }
            }
        }
    }
}
