// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls         1.0 as Controls
import Controls.Keypad  1.0 as KeypadControls
import Data             1.0 as Data
import Database         1.0 as Database
import HwControl        1.0 as HwControl
import Keypad           1.0 as Keypad
import Pages            1.0 as Pages
import Views            1.0 as Views

////////////////////////////////////////////////////////////////////////////////
/// \brief  First time setup page.
////////////////////////////////////////////////////////////////////////////////
Pages.Base
{
    id: self
    objectName: "SetupPinPage"

    headerData
    {
        enableHome: false
        title: ( Data.Debugging.enabled ? objectName : "" ) + "Registration"
    }

    onVisibleChanged:
    {
        if ( visible )
        {
            Keypad.Control.entry = ""
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Override to allow the back button to navigate to the welcome page.
    ////////////////////////////////////////////////////////////////////////////////
    navigateBack: function()
    {
        if ( state === "enter_temporary_pin" ||
             state === "login_failed" ||
             state === "access_denied" )
        {
            navigate.back()
        }
        else
        {
            Database.Control.logout()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property string prompt: ""
        property string promptDetail: ""

        property string pin: ""
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content.
    ////////////////////////////////////////////////////////////////////////////////
    Views.Content
    {
        id: content

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Top spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Prompt area.
        ////////////////////////////////////////////////////////////////////////////////
        ColumnLayout
        {
            spacing: Data.Constants.smallGapMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.prompt
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                color: Data.Colours.important

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Prompt detail.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.EText
            {
                text: priv.promptDetail
                font.pixelSize: Data.Constants.smallFontPx
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  PIN icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EIcon
        {
            text: Data.Icons.fa_calc
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: Data.Constants.largeFontPx

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  PIN entry.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.ETextInput
        {
            id: pin
            font.pixelSize:  Data.Constants.mediumFontPx
            focus: true
            objectName: "WelcomePIN"
            echoMode: TextInput.Password
            passwordCharacter: "*"
            text: Keypad.Control.entry
            borderColour: Data.Colours.transparent

            cursorDelegate: Rectangle
            {
                color: Data.Colours.transparent
            }

            Layout.alignment: Qt.AlignCenter

            Controls.DebugRect
            {
                color: "white"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  PIN keypad.
        ////////////////////////////////////////////////////////////////////////////////
        KeypadControls.Keypad
        {
            visible: Data.Config.largeScreen
            model: KeypadControls.Numeric_EnUK {}

            Layout.alignment: Qt.AlignHCenter
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Bottom spacer.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.Spacer
        {

        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Keypad control connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Keypad.Control

        function onKeyPressed( keyCode )
        {
            if ( keyCode === Qt.Key_Enter ||
                 keyCode === Qt.Key_Return )
            {
                if ( state === "enter_temporary_pin" ||
                     state === "login_failed" ||
                     state === "access_denied" )
                {
                    Database.Control.loginTempPin( Keypad.Control.entry )
                }
                else if ( state === "enter_new_pin" ||
                          state === "pins_dont_match" )
                {
                    // Save first PIN.
                    priv.pin = Keypad.Control.entry

                    state = "enter_confirm_new_pin"
                }
                else if ( state === "enter_confirm_new_pin" )
                {
                    if ( Keypad.Control.entry === priv.pin )
                    {
                        // Update PIN.
                        Database.Control.savePin( Keypad.Control.entry )
                        navigate.setup()
                    }
                    else
                    {
                        state = "pins_dont_match"
                    }
                }
            }
        }
    }

    onStateChanged:
    {
        // Always clear keypad entry when changing states.
        Keypad.Control.entry = ""
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Database connections.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Database.Control

        function onTempLoginSuccess()
        {
            if ( self.active )
            {
                if ( state === "enter_temporary_pin" ||
                     state === "login_failed" ||
                     state === "access_denied" )
                {
                    Keypad.Control.entry = ""

                    if ( HwControl.Cabinet.autoGeneratePin )
                    {
                        navigate.setup()
                    }
                    else
                    {
                        state = "enter_new_pin"
                    }
                }
            }
        }

        function onTempLoginFail()
        {
            if ( self.active )
            {
                state = "login_failed"
            }
        }

        function onAccessDenied()
        {
            if ( self.active )
            {
                state = "access_denied"
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  States. TODO: Use StateMachine.
    ////////////////////////////////////////////////////////////////////////////////
    state: "enter_temporary_pin"
    states:
    [
        State
        {
            name: "login_failed";
            PropertyChanges { target: priv; prompt: "Temporary PIN not recognised." }
            PropertyChanges { target: priv; promptDetail: "Please try again." }
        },

        State
        {
            name: "access_denied";
            PropertyChanges { target: priv; prompt: "Access denied." }
            PropertyChanges { target: priv; promptDetail: "Please contact your system administrator." }
        },

        State
        {
            name: "enter_temporary_pin";
            PropertyChanges { target: priv; prompt: "Please log in with your temporary PIN." }
            PropertyChanges { target: priv; promptDetail: "" }
        },

        State
        {
            name: "enter_new_pin";
            PropertyChanges { target: priv; prompt: "Please enter your new PIN." }
            PropertyChanges { target: priv; promptDetail: "" }
        },

        State
        {
            name: "enter_confirm_new_pin";
            PropertyChanges { target: priv; prompt: "Please confirm your new PIN." }
            PropertyChanges { target: priv; promptDetail: "" }
        },

        State
        {
            name: "pins_dont_match";
            PropertyChanges { target: priv; prompt: "PINs don't match." }
            PropertyChanges { target: priv; promptDetail: "Please enter your new PIN." }
        }
    ]
}
