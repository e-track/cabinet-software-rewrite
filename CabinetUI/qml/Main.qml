// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls  1.0 as Controls
import Data      1.0 as Data
import Database  1.0 as Database
import HwControl 1.0 as HwControl
import Keypad    1.0 as Keypad
import Message   1.0 as Message
import Pages     1.0 as Pages
import Popups    1.0 as Popups
import Tools     1.0 as Tools

////////////////////////////////////////////////////////////////////////////////
/// \brief  Main application window.
////////////////////////////////////////////////////////////////////////////////
Window
{
    id: self
    width: Data.Config.smallScreen
                ? Data.Constants.screenWidth * 3
                : Data.Constants.screenWidth * 3.2
    height: Data.Config.smallScreen
                ? Data.Constants.screenHeight * 3
                : Data.Constants.screenHeight * 1.2

    visible: true
    flags: Data.Debugging.topFramed
            ? Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint
            : Qt.Window
    color: Data.Colours.brand_blue

    // In order to show the app on a different monitor you should only need to set the screen property.
    // However, this isn't working for some reason so we need to position the Window manutally using the
    // virtualX and virtualY properties.
    screen: Qt.application.screens[ Data.Debugging.screen ]
    x: Qt.application.screens[ Data.Debugging.screen ].virtualX
    y: Qt.application.screens[ Data.Debugging.screen ].virtualY

    onActiveFocusItemChanged:
    {
        if ( Data.Debugging.showActiveFocus )
        {
            console.log( "[Main] ActiveFocusItem: " + self.activeFocusItem )
        }
    }

    /////////////////////////////////////////////////
    /// \brief  Fonts.
    /////////////////////////////////////////////////
    FontLoader
    {
        id: appFont
        source: Qt.resolvedUrl( Data.Constants.fontSource )
    }

    /////////////////////////////////////////////////
    /// \brief  Icon fonts.
    /////////////////////////////////////////////////
    FontLoader
    {
        id: iconFont
        source: Qt.resolvedUrl( Data.Constants.iconFontSource )
    }

    /////////////////////////////////////////////////
    /// \brief  Keypad controller connections.
    /////////////////////////////////////////////////
    Connections
    {
        target: Keypad.Control

        function onKeyPressed( keyCode )
        {
            switch ( keyCode )
            {
                case Qt.Key_F1:
                {
                    Data.Debugging.enabled = !Data.Debugging.enabled
                    break
                }

                case Qt.Key_F3:
                {
                    tagNotification.open()
                    break
                }

                case Qt.Key_F4:
                {
                    Data.Config.smallScreen = !Data.Config.smallScreen
                    break
                }

                case Qt.Key_F7:
                {
                    Data.Debugging.togglePages = !Data.Debugging.togglePages
                    break
                }

                default:
                {
                    break
                }
            }
        }
    }

    /////////////////////////////////////////////////
    /// \brief  Tag inserted / removed notification.
    /////////////////////////////////////////////////
    Popups.TagNotification
    {
        id: tagNotification
        x: parent.x
        y: parent.y
        width: Data.Constants.screenWidth
        height: Data.Constants.screenHeight
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Page navigation controller.
    ////////////////////////////////////////////////////////////////////////////////
    Tools.Navigation
    {
        id: navigate
        stack: stack
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Database controller.
    ////////////////////////////////////////////////////////////////////////////////
    Connections
    {
        target: Database.Control

        function onTempLoginFail()
        {
            Keypad.Control.entry = ""
            console.log( "Failed to log in." )
        }

        function onLogoutSuccess()
        {
            Database.Control.clearBasketTags()
            navigate.welcome()
        }

        function onTagCategoriesChanged()
        {
            // Get a list of all tags / keys grouped by category only after the categories have been obtained.
            Database.Control.queryCategoryTags( Database.Control.tagCategories.length )
        }
    }

    Component.onCompleted:
    {
        Database.Control.queryTagCategories()

        // TODO: Init based on config.
        // Initialise the cabinet's interface card.
        //HwControl.Cabinet.initialise()

        // TODO: Add initialise function and set up serial port connection there.
        //HwControl.Cardreader.initialise()

        // Initialse the fingerprint reader when the UI is ready.
        //HwControl.Fingerprint.initialise()
        //HwControl.Fingerprint.sdkVersion()

        navigate.welcome()
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main page layout.
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        id: ui
        anchors.fill: parent
        anchors.margins: Data.Constants.largeGapMm
        spacing: Data.Constants.largeGapMm

        Rectangle
        {
            id: application

            gradient: Gradient
            {
                GradientStop { position: 0; color: Data.Colours.background }
                GradientStop { position: 1; color: Data.Colours.brand_black }
            }

            Layout.preferredWidth: Data.Constants.screenWidth
            Layout.preferredHeight: Data.Constants.screenHeight
            Layout.alignment: Qt.AlignHCenter

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Application area layout.
            ////////////////////////////////////////////////////////////////////////////////
            ColumnLayout
            {
                anchors.fill: parent
                anchors.margins: Data.Constants.smallGapMm
                spacing: Data.Constants.smallGapMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Header.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Header
                {
                    id: header

                    headerData: typeof stack.currentItem === "undefined"
                                    ? null
                                    : stack.currentItem.headerData

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Handles back button by passing control to the page.
                    ////////////////////////////////////////////////////////////////////////////////
                    onBack:
                    {
                        stack.currentItem.navigateBack()
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Handles back button by passing control to the page.
                    ////////////////////////////////////////////////////////////////////////////////
                    onForward:
                    {
                        stack.currentItem.navigateForward()
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Handles home button by passing control to the page.
                    ////////////////////////////////////////////////////////////////////////////////
                    onHome:
                    {
                        stack.currentItem.navigateHome()
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Handles logout button by passing control to the page.
                    ////////////////////////////////////////////////////////////////////////////////
                    onLogout:
                    {
                        stack.currentItem.navigateLogout()
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Dividing line.
                ////////////////////////////////////////////////////////////////////////////////
                Controls.Divider
                {
                    visible: header.headerData.showDivider
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Content.
                ////////////////////////////////////////////////////////////////////////////////
                Item
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Stack containing all pages.
                    ////////////////////////////////////////////////////////////////////////////////
                    StackView
                    {
                        id: stack
                        anchors.fill: parent

                        popEnter: Transition {}
                        popExit: Transition {}
                        pushEnter: Transition {}
                        pushExit: Transition {}
                    }

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  Debug rectangle.
                    ////////////////////////////////////////////////////////////////////////////////
                    Controls.DebugRect
                    {
                        color: "blue"
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Simulator.
        ////////////////////////////////////////////////////////////////////////////////
        Pages.Simulator
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
