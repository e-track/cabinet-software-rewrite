// Qt.
import QtQuick

// Library.
import Data 1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Semi-transparent rectangle that is only visible during debug mode.
////////////////////////////////////////////////////////////////////////////////
Rectangle
{
    id: self
    anchors.fill: parent
    visible: Data.Debugging.enabled
    opacity: Data.Debugging.opacity
    border.color: self.color.lighter()
    z: 10
}
