// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard icon.
////////////////////////////////////////////////////////////////////////////////
Controls.EText
{
    id: self
    font.family: iconFont.name
    font.pixelSize: Data.Constants.smallFontPx
}
