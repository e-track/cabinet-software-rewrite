// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Dividing line.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.fillWidth: true
    Layout.preferredHeight: 1
    Layout.alignment: Qt.AlignHCenter

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  The actual line.
    ////////////////////////////////////////////////////////////////////////////////
    Rectangle
    {
        id: line
        width: self.height
        height: self.width
        rotation: 90
        anchors.centerIn: parent

        gradient: Gradient
        {
            GradientStop { position: 0;   color: Data.Colours.transparent }
            GradientStop { position: 0.5; color: Data.Colours.foreground }
            GradientStop { position: 1;   color: Data.Colours.transparent }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "green"
    }
}
