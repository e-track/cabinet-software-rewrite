// Qt.
import QtQuick
import QtQuick.Controls

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard label.
////////////////////////////////////////////////////////////////////////////////
TextField
{
    id: self
    font.pixelSize: Data.Constants.smallFontPx
    color: Data.Colours.foreground
    placeholderTextColor: Data.Colours.control
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter

    property color borderColour: Data.Colours.selection

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 2
        border.color: self.borderColour
        color: Data.Colours.transparent
        radius: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Debug rectangle.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.DebugRect
        {
            color: "magenta"
        }
    }
}
