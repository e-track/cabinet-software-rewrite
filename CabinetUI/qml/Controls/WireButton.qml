// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard tool button.
////////////////////////////////////////////////////////////////////////////////
Controls.EButton
{
    id: self

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 1
        border.color: Data.Colours.transparent
        color: Data.Colours.transparent
        radius: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Debug rectangle.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.DebugRect
        {
            color: "magenta"
        }
    }
}
