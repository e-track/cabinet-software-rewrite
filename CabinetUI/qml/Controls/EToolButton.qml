// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard tool button.
////////////////////////////////////////////////////////////////////////////////
Controls.EButton
{
    id: self
    display: EButton.TextBesideIcon

    Layout.alignment: Qt.AlignCenter
    Layout.preferredWidth: Data.Constants.toolButtonSizeMm
    Layout.preferredHeight: Data.Constants.toolButtonSizeMm

    palette.buttonText: self.pressed
                            ? Data.Colours.background
                            : Data.Colours.foreground

    iconSize: Data.Constants.toolButtonIconSizeMm
}
