// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Header.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.fillWidth: true
    Layout.preferredHeight: Data.Constants.headerHeightMm

    Component.onCompleted: updateMainComponent()

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property Data.HeaderProperties headerData: Data.HeaderProperties {}     ///< Configures the header.

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Signals.
    ////////////////////////////////////////////////////////////////////////////////
    signal back()
    signal forward()
    signal home()
    signal logout()

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Updates the main component to show search or title.
    ////////////////////////////////////////////////////////////////////////////////
    function updateMainComponent()
    {
        mainItem.sourceComponent = headerData.showSearch
                                    ? searchComponent
                                    : titleComponent
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main area (top-centre).
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        id: topCentre
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height
        anchors.margins: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  The main item loader.
        ////////////////////////////////////////////////////////////////////////////////
        Loader
        {
            id: mainItem

            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: self.width / 2
            Layout.preferredHeight: Data.Constants.toolButtonSizeMm

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Debug rectangle.
            ////////////////////////////////////////////////////////////////////////////////
            Controls.DebugRect
            {
                color: "red"
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Title component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: titleComponent

        Controls.EText
        {
            id: title
            anchors.centerIn: parent
            font.pixelSize: Data.Constants.smallFontPx
            color: Data.Colours.foreground
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: headerData.title
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Search component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: searchComponent

        Controls.ETextInput
        {
            anchors.centerIn: parent
            color: Data.Colours.foreground
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            placeholderText: "Search for a key..."
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigation area (top-left).
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        id: topLeft
        anchors.left: parent.left
        height: parent.height
        anchors.margins: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Back button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            id: back
            iconSource: Data.Icons.fa_left
            visible: headerData.showBack
            enabled: headerData.enableBack

            onClicked:
            {
                self.back()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Home button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            id: home
            iconSource: Data.Icons.fa_home
            visible: headerData.showHome
            enabled: headerData.enableHome

            onClicked:
            {
                self.home()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Home button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            id: register
            text: "Register"
            visible: headerData.showRegister

            Layout.preferredWidth: Data.Constants.buttonSizeMm

            onClicked:
            {
                self.forward()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Navigation area (top-right).
    ////////////////////////////////////////////////////////////////////////////////
    RowLayout
    {
        id: topRight
        anchors.right: parent.right
        height: parent.height
        anchors.margins: Data.Constants.smallGapMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Lock icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EIcon
        {
            id: locked
            text: Data.Icons.fa_lock
            visible: headerData.showLocked
            font.pixelSize: Data.Constants.toolButtonIconSizeMm
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Log-out button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            id: logout
            text: headerData.logoutText
            visible: headerData.showLogout
            enabled: headerData.enableLogout

            Layout.preferredWidth: Data.Constants.buttonSizeMm

            onClicked:
            {
                self.logout()
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Forward button.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EToolButton
        {
            id: forward
            iconSource: Data.Icons.fa_right
            visible: headerData.showForward
            enabled: headerData.enableForward

            onClicked:
            {
                self.forward()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "yellow"
    }
}
