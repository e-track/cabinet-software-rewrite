pragma Singleton

// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Constants definitions.
////////////////////////////////////////////////////////////////////////////////
QtObject
{
    id: self

    // Function modes.
    readonly property int funcAlphaNum: 0       ///< Alpha numeric function mode.
    readonly property int funcSymbols: 1        ///< Symbol function mode.
}
