// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Full qwerty alpha-numeric keypad.
////////////////////////////////////////////////////////////////////////////////
ListModel
{
    id: model

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_QuoteLeft }, ListElement { _code: Qt.Key_notsign } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_1 },         ListElement { _code: Qt.Key_Exclam } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_2 },         ListElement { _code: Qt.Key_QuoteDbl } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_3 },         ListElement { _code: Qt.Key_sterling } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_4 },         ListElement { _code: Qt.Key_Dollar } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_5 },         ListElement { _code: Qt.Key_Percent } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_6 },         ListElement { _code: Qt.Key_AsciiCircum } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_7 },         ListElement { _code: Qt.Key_Ampersand } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_8 },         ListElement { _code: Qt.Key_Asterisk } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_9 },         ListElement { _code: Qt.Key_ParenLeft } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_0 },         ListElement { _code: Qt.Key_ParenRight } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Minus },     ListElement { _code: Qt.Key_Underscore } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Plus },      ListElement { _code: Qt.Key_Equal } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Backspace }, ListElement { _code: Qt.Key_Backspace } ] _scaleWidth: 2.5 }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Tab },          ListElement { _code: Qt.Key_Tab } ] _scaleWidth: 1.75 },
            ListElement { _keys: [ ListElement { _code: 0x71 },                ListElement { _code: Qt.Key_Q } ] },
            ListElement { _keys: [ ListElement { _code: 0x77 },                ListElement { _code: Qt.Key_W } ] },
            ListElement { _keys: [ ListElement { _code: 0x65 },                ListElement { _code: Qt.Key_E } ] },
            ListElement { _keys: [ ListElement { _code: 0x72 },                ListElement { _code: Qt.Key_R } ] },
            ListElement { _keys: [ ListElement { _code: 0x74 },                ListElement { _code: Qt.Key_T } ] },
            ListElement { _keys: [ ListElement { _code: 0x79 },                ListElement { _code: Qt.Key_Y } ] },
            ListElement { _keys: [ ListElement { _code: 0x75 },                ListElement { _code: Qt.Key_U } ] },
            ListElement { _keys: [ ListElement { _code: 0x69 },                ListElement { _code: Qt.Key_I } ] },
            ListElement { _keys: [ ListElement { _code: 0x6f },                ListElement { _code: Qt.Key_O } ] },
            ListElement { _keys: [ ListElement { _code: 0x70 },                ListElement { _code: Qt.Key_P } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_BracketLeft },  ListElement { _code: Qt.Key_BraceLeft } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_BracketRight }, ListElement { _code: Qt.Key_BraceRight } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Enter },        ListElement { _code: Qt.Key_Enter } ] _offsetX: 0.5; _scaleWidth: 1.25; _scaleHeight: 2.0 }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_CapsLock },   ListElement { _code: Qt.Key_CapsLock } ] _scaleWidth: 2.25 },
            ListElement { _keys: [ ListElement { _code: 0x61 },              ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: 0x73 },              ListElement { _code: Qt.Key_S } ] },
            ListElement { _keys: [ ListElement { _code: 0x64 },              ListElement { _code: Qt.Key_D } ] },
            ListElement { _keys: [ ListElement { _code: 0x66 },              ListElement { _code: Qt.Key_F } ] },
            ListElement { _keys: [ ListElement { _code: 0x67 },              ListElement { _code: Qt.Key_G } ] },
            ListElement { _keys: [ ListElement { _code: 0x68 },              ListElement { _code: Qt.Key_H } ] },
            ListElement { _keys: [ ListElement { _code: 0x6a },              ListElement { _code: Qt.Key_J } ] },
            ListElement { _keys: [ ListElement { _code: 0x6b },              ListElement { _code: Qt.Key_K } ] },
            ListElement { _keys: [ ListElement { _code: 0x6c },              ListElement { _code: Qt.Key_L } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Semicolon },  ListElement { _code: Qt.Key_Colon } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Apostrophe }, ListElement { _code: Qt.Key_At } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_NumberSign }, ListElement { _code: Qt.Key_AsciiTilde } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Shift },     ListElement { _code: Qt.Key_Shift } ] _scaleWidth: 1.25 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Backslash }, ListElement { _code: Qt.Key_Bar } ] },
            ListElement { _keys: [ ListElement { _code: 0x7a },             ListElement { _code: Qt.Key_Z } ] },
            ListElement { _keys: [ ListElement { _code: 0x78 },             ListElement { _code: Qt.Key_X } ] },
            ListElement { _keys: [ ListElement { _code: 0x63 },             ListElement { _code: Qt.Key_C } ] },
            ListElement { _keys: [ ListElement { _code: 0x76 },             ListElement { _code: Qt.Key_V } ] },
            ListElement { _keys: [ ListElement { _code: 0x62 },             ListElement { _code: Qt.Key_B } ] },
            ListElement { _keys: [ ListElement { _code: 0x6e },             ListElement { _code: Qt.Key_N } ] },
            ListElement { _keys: [ ListElement { _code: 0x6d },             ListElement { _code: Qt.Key_M } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Comma },     ListElement { _code: Qt.Key_Less } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Period },    ListElement { _code: Qt.Key_Greater } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Slash },     ListElement { _code: Qt.Key_Question } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Shift },     ListElement { _code: Qt.Key_Shift } ] _scaleWidth: 3.25 }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Control }, ListElement { _code: Qt.Key_Control } ] _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Meta },    ListElement { _code: Qt.Key_Meta } ]    _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Alt },     ListElement { _code: Qt.Key_Alt } ]     _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Space },   ListElement { _code: Qt.Key_Space } ]   _scaleWidth: 6.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_AltGr },   ListElement { _code: Qt.Key_AltGr } ]   _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Menu },    ListElement { _code: Qt.Key_Menu } ]    _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Control }, ListElement { _code: Qt.Key_Control } ] _scaleWidth: 1.5 }
        ]
    }
}
