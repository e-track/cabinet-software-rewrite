// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Numeric keypad.
////////////////////////////////////////////////////////////////////////////////
ListModel
{
    id: model

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_1 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_2 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_3 }, ListElement { _code: Qt.Key_A } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_4 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_5 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_6 }, ListElement { _code: Qt.Key_A } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_7 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_8 }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_9 }, ListElement { _code: Qt.Key_A } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Backspace }, ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_0 },         ListElement { _code: Qt.Key_A } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Enter },     ListElement { _code: Qt.Key_A } ] }
        ]
    }
}
