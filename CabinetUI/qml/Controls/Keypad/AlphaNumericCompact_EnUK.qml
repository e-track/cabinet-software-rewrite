// Qt.
import QtQuick

////////////////////////////////////////////////////////////////////////////////
/// \brief  Compact alpha-numeric keypad.
////////////////////////////////////////////////////////////////////////////////
ListModel
{
    id: model

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: 0x31 },                ListElement { _code: Qt.Key_1 } ] },
            ListElement { _keys: [ ListElement { _code: 0x32 },                ListElement { _code: Qt.Key_2 } ] },
            ListElement { _keys: [ ListElement { _code: 0x33 },                ListElement { _code: Qt.Key_3 } ] },
            ListElement { _keys: [ ListElement { _code: 0x34 },                ListElement { _code: Qt.Key_4 } ] },
            ListElement { _keys: [ ListElement { _code: 0x35 },                ListElement { _code: Qt.Key_5 } ] },
            ListElement { _keys: [ ListElement { _code: 0x36 },                ListElement { _code: Qt.Key_6 } ] },
            ListElement { _keys: [ ListElement { _code: 0x37 },                ListElement { _code: Qt.Key_7 } ] },
            ListElement { _keys: [ ListElement { _code: 0x38 },                ListElement { _code: Qt.Key_8 } ] },
            ListElement { _keys: [ ListElement { _code: 0x39 },                ListElement { _code: Qt.Key_9 } ] },
            ListElement { _keys: [ ListElement { _code: 0x30 },                ListElement { _code: Qt.Key_0 } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: 0x71 },                ListElement { _code: Qt.Key_Q } ] },
            ListElement { _keys: [ ListElement { _code: 0x77 },                ListElement { _code: Qt.Key_W } ] },
            ListElement { _keys: [ ListElement { _code: 0x65 },                ListElement { _code: Qt.Key_E } ] },
            ListElement { _keys: [ ListElement { _code: 0x72 },                ListElement { _code: Qt.Key_R } ] },
            ListElement { _keys: [ ListElement { _code: 0x74 },                ListElement { _code: Qt.Key_T } ] },
            ListElement { _keys: [ ListElement { _code: 0x79 },                ListElement { _code: Qt.Key_Y } ] },
            ListElement { _keys: [ ListElement { _code: 0x75 },                ListElement { _code: Qt.Key_U } ] },
            ListElement { _keys: [ ListElement { _code: 0x69 },                ListElement { _code: Qt.Key_I } ] },
            ListElement { _keys: [ ListElement { _code: 0x6f },                ListElement { _code: Qt.Key_O } ] },
            ListElement { _keys: [ ListElement { _code: 0x70 },                ListElement { _code: Qt.Key_P } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: 0x61 },         ListElement { _code: Qt.Key_A } ] _offsetX: 0.5 },
            ListElement { _keys: [ ListElement { _code: 0x73 },         ListElement { _code: Qt.Key_S } ] },
            ListElement { _keys: [ ListElement { _code: 0x64 },         ListElement { _code: Qt.Key_D } ] },
            ListElement { _keys: [ ListElement { _code: 0x66 },         ListElement { _code: Qt.Key_F } ] },
            ListElement { _keys: [ ListElement { _code: 0x67 },         ListElement { _code: Qt.Key_G } ] },
            ListElement { _keys: [ ListElement { _code: 0x68 },         ListElement { _code: Qt.Key_H } ] },
            ListElement { _keys: [ ListElement { _code: 0x6a },         ListElement { _code: Qt.Key_J } ] },
            ListElement { _keys: [ ListElement { _code: 0x6b },         ListElement { _code: Qt.Key_K } ] },
            ListElement { _keys: [ ListElement { _code: 0x6c },         ListElement { _code: Qt.Key_L } ] }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Shift },     ListElement { _code: Qt.Key_Shift } ] _scaleWidth: 1.5 },
            ListElement { _keys: [ ListElement { _code: 0x7a },             ListElement { _code: Qt.Key_Z } ] },
            ListElement { _keys: [ ListElement { _code: 0x78 },             ListElement { _code: Qt.Key_X } ] },
            ListElement { _keys: [ ListElement { _code: 0x63 },             ListElement { _code: Qt.Key_C } ] },
            ListElement { _keys: [ ListElement { _code: 0x76 },             ListElement { _code: Qt.Key_V } ] },
            ListElement { _keys: [ ListElement { _code: 0x62 },             ListElement { _code: Qt.Key_B } ] },
            ListElement { _keys: [ ListElement { _code: 0x6e },             ListElement { _code: Qt.Key_N } ] },
            ListElement { _keys: [ ListElement { _code: 0x6d },             ListElement { _code: Qt.Key_M } ] },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Backspace }, ListElement { _code: Qt.Key_Backspace } ] _scaleWidth: 1.5 }
        ]
    }

    ListElement
    {
        rows:
        [
            ListElement { _keys: [ ListElement { _code: Qt.Key_Menu },    ListElement { _code: Qt.Key_Menu } ]   _scaleWidth: 1.0 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Comma },   ListElement { _code: Qt.Key_Comma } ]  _scaleWidth: 1.0 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Space },   ListElement { _code: Qt.Key_Space } ]  _scaleWidth: 5.5 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Period },  ListElement { _code: Qt.Key_Period } ] _scaleWidth: 1.0 },
            ListElement { _keys: [ ListElement { _code: Qt.Key_Enter },   ListElement { _code: Qt.Key_Enter } ]  _scaleWidth: 1.5 }
        ]
    }
}
