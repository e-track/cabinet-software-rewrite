// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls        1.0 as Controls
import Controls.Keypad 1.0 as KeypadControls
import Data            1.0 as Data
import Keypad          1.0 as Keypad

////////////////////////////////////////////////////////////////////////////////
/// \brief  Virtual keypad.
////////////////////////////////////////////////////////////////////////////////
Rectangle
{
    id: self
    color: Data.Colours.keypad
    implicitWidth: grid.width + Data.Constants.smallGapMm
    implicitHeight: grid.height + Data.Constants.smallGapMm

    property int func: KeypadControls.Constants.funcAlphaNum        ///< Keypad modifier index (shift, alt etc). TODO: Use enum.
    property var model                                              ///< Keypad model (alpha numeric, numeric etc).

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Grid layout for keys.
    ////////////////////////////////////////////////////////////////////////////////
    ColumnLayout
    {
        id: grid
        anchors.centerIn: parent
        spacing: Data.Constants.keySpacingMm

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Allows multiple keys to be created based on the model.
        ////////////////////////////////////////////////////////////////////////////////
        Repeater
        {
            model: self.model

            RowLayout
            {
                spacing: Data.Constants.keySpacingMm

                Layout.alignment: Qt.AlignTop
                Layout.maximumHeight: Data.Constants.keySizeMm

                Repeater
                {
                    model: rows

                    ////////////////////////////////////////////////////////////////////////////////
                    /// \brief  A key on the virtual keypad.
                    ////////////////////////////////////////////////////////////////////////////////
                    Key
                    {
                        text: codeToText( data._code )
                        iconSource: codeToIcon( data._code )

                        scaleWidth: typeof _scaleWidth === "undefined"
                                        ? 1.0
                                        : Math.max( 1.0, _scaleWidth )

                        scaleHeight: typeof _scaleHeight === "undefined"
                                        ? 1.0
                                        : Math.max( 1.0, _scaleHeight )

                        offsetX: typeof _offsetX === "undefined"
                                    ? 0.0
                                    : _offsetX

                        offsetY: typeof _offsetY === "undefined"
                                    ? 0.0
                                    : _offsetY

                        property var data: _keys.get( self.func )

                        onClicked:
                        {
                            // Inject as an actual key event.
                            Keypad.Control.invokeKeyEvent( data._code )
                        }
                    }
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Returns a string representing the code's icon.
    ////////////////////////////////////////////////////////////////////////////////
    function codeToText( code )
    {
        var text = ""

        switch( code )
        {
            case Qt.Key_Backspace:
            case Qt.Key_Enter:
            case Qt.Key_CapsLock:
            case Qt.Key_Shift:
            case Qt.Key_Meta:
            case Qt.Key_Menu:
            {
                text = ""
                break
            }

            case Qt.Key_Tab:     { text = "Tab";    break }
            case Qt.Key_Control: { text = "Ctrl";   break }
            case Qt.Key_Alt:     { text = "Alt";    break }
            case Qt.Key_Space:   { text = "";       break }
            case Qt.Key_AltGr:   { text = "Alt Gr"; break }

            default:
            {
                text = String.fromCharCode( code )
                break
            }
        }

        return text
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Returns a string representing the code's icon.
    ////////////////////////////////////////////////////////////////////////////////
    function codeToIcon( code )
    {
        var text = ""

        switch( code )
        {
            case Qt.Key_Backspace: { text = Data.Icons.fa_backspace;  break }
            case Qt.Key_Enter:     { text = Data.Icons.fa_bracket_to; break }
            case Qt.Key_CapsLock:  { text = Data.Icons.fa_lock;       break }
            case Qt.Key_Shift:     { text = Data.Icons.fa_arrow_up;   break }
            case Qt.Key_Meta:      { text = Data.Icons.fa_cog;        break }
            case Qt.Key_Menu:      { text = Data.Icons.fa_bars;       break }

            default:
            {
                break
            }
        }

        return text
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "green"
    }
}
