// Qt.
import QtQuick
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  A key on the virtual keypad.
////////////////////////////////////////////////////////////////////////////////
Controls.EButton
{
    id: self
    iconSize: font.pixelSize
    focusPolicy: Qt.NoFocus     // The virtual keyboard should not grab focus away from input controls.

    Layout.alignment: Qt.AlignTop
    Layout.preferredWidth: ( Data.Constants.keySizeMm * self.scaleWidth ) + ( Data.Constants.keySpacingMm * ( self.scaleWidth - 1 ) )
    Layout.preferredHeight: ( Data.Constants.keySizeMm * self.scaleHeight ) + ( Data.Constants.keySpacingMm * ( self.scaleHeight - 1 ) )
    Layout.leftMargin: ( Data.Constants.keySizeMm + Data.Constants.keySpacingMm ) * self.offsetX
    Layout.topMargin: ( Data.Constants.keySizeMm + Data.Constants.keySpacingMm ) * self.offsetY

    property real scaleWidth: 1.0       ///< Width scale factor.
    property real scaleHeight: 1.0      ///< Height scale factor.
    property real offsetX: 0.0          ///< Horizontal offset for alignment.
    property real offsetY: 0.0          ///< Vertical offset for alignment.

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Debug rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.DebugRect
    {
        color: "blue"
    }
}
