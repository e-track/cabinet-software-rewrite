// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard tool button.
////////////////////////////////////////////////////////////////////////////////
Button
{
    id: self
    font.family: appFont.name
    font.pixelSize: Data.Constants.smallFontPx
    display: Button.TextUnderIcon
    opacity: enabled ? 1.0 : 0.25
    hoverEnabled: false     // Prevents QML warning due to QML bug: QTBUG-92825

    palette.buttonText: self.pressed
                            ? Data.Colours.background
                            : Data.Colours.foreground

    Layout.alignment: Qt.AlignVCenter
    Layout.preferredWidth: Data.Constants.buttonSizeMm
    Layout.preferredHeight: Data.Constants.buttonSizeMm

    property string iconSource: ""
    property int iconSize: Data.Constants.buttonIconSizeMm
    property color iconColor: palette.buttonText

    property alias badgeSize: badge.size

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 2
        border.color: self.pressed
                        ? Data.Colours.transparent
                        : ( self.checkable ? Data.Colours.transparent : Data.Colours.selection )
        color: self.pressed
                    ? ( self.checkable ? Data.Colours.transparent : Data.Colours.selection )
                    : Data.Colours.transparent
        radius: Data.Constants.smallGapMm
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property bool hasIcon: iconSource !== ""
        property bool hasText: self.text !== ""
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Foreground text and icon.
    ////////////////////////////////////////////////////////////////////////////////
    contentItem: ColumnLayout
    {
        anchors.centerIn: parent
        anchors.margins: 0
        spacing: ( priv.hasIcon && priv.hasText ) ? Data.Constants.smallGapMm : 0

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Icon.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            id: icon
            color: Data.Debugging.enabled
                        ? "blue"
                        : self.iconColor
            font.pixelSize: self.iconSize
            font.family: iconFont.name
            text: self.iconSource
            visible: priv.hasIcon
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Layout.margins: 0
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true

            Controls.DebugRect
            {
                color: "yellow"
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Text.
        ////////////////////////////////////////////////////////////////////////////////
        Controls.EText
        {
            id: text
            color: Data.Debugging.enabled
                        ? "red"
                        : self.palette.buttonText
            font: self.font
            text: self.text
            visible: priv.hasText
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Layout.margins: 0
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true

            Controls.DebugRect
            {
                color: "yellow"
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Checkable badge.
    ////////////////////////////////////////////////////////////////////////////////
    Controls.Badge
    {
        id: badge
        anchors.centerIn: parent
        color: self.enabled ? Data.Colours.selection : Data.Colours.brand_dark
        border.color: Data.Colours.foreground
        size: Data.Constants.toolButtonSizeMm
        text: self.checked ? Data.Icons.fa_checked : Data.Icons.fa_unchecked
        visible: self.checkable
    }
}
