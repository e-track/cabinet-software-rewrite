// Qt.
import QtQuick
import QtQuick.Controls

// Library.
import Data 1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard combo box.
////////////////////////////////////////////////////////////////////////////////
ComboBox
{
    id: self

    contentItem: Text
    {
        font.family: appFont.name
        font.pixelSize: Data.Constants.smallFontPx

        text: Data.Colours.foreground
        color: Data.Colours.background
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle
    {
        border.color: Data.Colours.selection
        color: Data.Colours.background
        border.width: 2
        radius: Data.Constants.smallGapMm
    }
}
