// Qt.
import QtQuick
import QtQuick.Layouts

////////////////////////////////////////////////////////////////////////////////
/// \brief  Spacer item pushes together any items in a layout.
////////////////////////////////////////////////////////////////////////////////
Item
{
    id: self

    Layout.fillWidth: true
    Layout.fillHeight: true
}
