// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Standard tab button.
////////////////////////////////////////////////////////////////////////////////
TabButton
{
    id: self
    font.family: appFont.name
    font.pixelSize: Data.Constants.smallFontPx
    palette.buttonText: self.pressed
                            ? Data.Colours.foreground
                            : ( self.checked ? Data.Colours.brand_black : Data.Colours.selection )

    property string iconSource: ""
    property int iconSize: Data.Constants.buttonIconSizeMm
    property color iconColor: palette.buttonText

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Background rectangle.
    ////////////////////////////////////////////////////////////////////////////////
    background: Rectangle
    {
        id: background
        anchors.fill: parent
        border.width: 2
        border.color: self.pressed
                        ? Data.Colours.foreground
                        : ( self.checked ? Data.Colours.brand_black : Data.Colours.selection )
        color: self.pressed
                    ? ( self.checked ? Data.Colours.transparent : Data.Colours.selection )
                    : ( self.checked ? Data.Colours.selection : Data.Colours.transparent )
        radius: Data.Constants.smallGapMm
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Private properties.
    ////////////////////////////////////////////////////////////////////////////////
    QtObject
    {
        id: priv

        property bool hasIcon: iconSource !== ""
        property bool hasText: self.text !== ""
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Foreground text and icon.
    ////////////////////////////////////////////////////////////////////////////////
//    contentItem: ColumnLayout
//    {
//        anchors.centerIn: parent
//        anchors.margins: 0
//        spacing: ( priv.hasIcon && priv.hasText ) ? Data.Constants.smallGapMm : 0

//        ////////////////////////////////////////////////////////////////////////////////
//        /// \brief  Icon.
//        ////////////////////////////////////////////////////////////////////////////////
//        Controls.EText
//        {
//            id: icon
//            color: Data.Debugging.enabled
//                        ? "blue"
//                        : self.iconColor
//            font.pixelSize: self.iconSize
//            font.family: iconFont.name
//            text: self.iconSource
//            visible: priv.hasIcon
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter

//            Layout.margins: 0
//            Layout.alignment: Qt.AlignHCenter
//            Layout.fillWidth: true

//            Controls.DebugRect
//            {
//                color: "yellow"
//            }
//        }

//        ////////////////////////////////////////////////////////////////////////////////
//        /// \brief  Text.
//        ////////////////////////////////////////////////////////////////////////////////
//        Controls.EText
//        {
//            id: text
//            color: Data.Debugging.enabled
//                        ? "red"
//                        : self.palette.buttonText
//            font: self.font
//            text: self.text
//            visible: priv.hasText
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter

//            Layout.margins: 0
//            Layout.alignment: Qt.AlignHCenter
//            Layout.fillWidth: true

//            Controls.DebugRect
//            {
//                color: "yellow"
//            }
//        }
//    }
}
