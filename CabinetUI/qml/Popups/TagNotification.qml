// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data     1.0 as Data
import Popups   1.0 as Popups

////////////////////////////////////////////////////////////////////////////////
/// \brief  Popup showing changes to tags.
////////////////////////////////////////////////////////////////////////////////
Popups.Base
{
    id: self
    source: content

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Content component.
    ////////////////////////////////////////////////////////////////////////////////
    Component
    {
        id: content

        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Content item.
        ////////////////////////////////////////////////////////////////////////////////
        Item
        {
            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Content layout.
            ////////////////////////////////////////////////////////////////////////////////
            ColumnLayout
            {
                anchors.fill: parent
                anchors.margins: Data.Constants.smallGapMm
                spacing: Data.Constants.smallGapMm

                Layout.alignment: Qt.AlignCenter

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Name.
                ////////////////////////////////////////////////////////////////////////////////
                ColumnLayout
                {
                    spacing: 0
                    Layout.alignment: Qt.AlignHCenter

                    Controls.EText
                    {
                        text: "Name"
                        color: Data.Colours.foreground
                        font.pixelSize: Data.Constants.tinyFontPx
                    }

                    Controls.EText
                    {
                        text: "Key 1"
                        color: Data.Colours.important
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Location.
                ////////////////////////////////////////////////////////////////////////////////
                ColumnLayout
                {
                    spacing: 0

                    Layout.alignment: Qt.AlignHCenter

                    Controls.EText
                    {
                        text: "Location"
                        color: Data.Colours.foreground
                        font.pixelSize: Data.Constants.tinyFontPx
                    }

                    Controls.EText
                    {
                        text: "Main office"
                        color: Data.Colours.important
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Mileage.
                ////////////////////////////////////////////////////////////////////////////////
                ColumnLayout
                {
                    spacing: 0
                    Layout.alignment: Qt.AlignHCenter

                    Controls.EText
                    {
                        text: "Mileage"
                        color: Data.Colours.foreground
                        font.pixelSize: Data.Constants.tinyFontPx
                    }

                    Controls.EText
                    {
                        text: "10,000"
                        color: Data.Colours.important
                    }
                }
            }
        }
    }
}
