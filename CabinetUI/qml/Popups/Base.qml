// Qt.
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Library.
import Controls 1.0 as Controls
import Data 1.0 as Data

////////////////////////////////////////////////////////////////////////////////
/// \brief  Base popup.
////////////////////////////////////////////////////////////////////////////////
Popup
{
    id: self
    modal: false
    focus: true
    parent: application
    anchors.centerIn: parent
    width: parent.width
    height: parent.height
    leftPadding: Data.Constants.largeGapMm + Data.Constants.smallGapMm
    rightPadding: Data.Constants.largeGapMm + Data.Constants.smallGapMm
    topPadding: Data.Constants.largeGapMm + Data.Constants.smallGapMm
    bottomPadding: Data.Constants.largeGapMm + Data.Constants.smallGapMm

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Properties.
    ////////////////////////////////////////////////////////////////////////////////
    property alias source: content.sourceComponent      ///< Alias to the content loader.
    property alias closeTimeoutMs: timer.interval       ///< Time to auto close (use 0 to keep open).

    background: Rectangle
    {
        anchors.fill: parent
        color: Data.Colours.background
        opacity: 0.75
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Main popup content. This is automatically re-sized to fit the pop-up.
    ////////////////////////////////////////////////////////////////////////////////
    contentItem: Item
    {
        ////////////////////////////////////////////////////////////////////////////////
        /// \brief  Border.
        ////////////////////////////////////////////////////////////////////////////////
        Rectangle
        {
            width: parent.width
            height: parent.height / 2
            anchors.centerIn: parent
            anchors.margins: Data.Constants.smallGapMm

            border.color: Data.Colours.foreground

            gradient: Gradient
            {
                GradientStop { position: 0; color: Data.Colours.background }
                GradientStop { position: 1; color: Data.Colours.brand_black }
            }

            ////////////////////////////////////////////////////////////////////////////////
            /// \brief  Main layout.
            ////////////////////////////////////////////////////////////////////////////////
            ColumnLayout
            {
                anchors.fill: parent
                spacing: Data.Constants.smallGapMm

                ////////////////////////////////////////////////////////////////////////////////
                /// \brief  Loader to display content.
                ////////////////////////////////////////////////////////////////////////////////
                Loader
                {
                    id: content

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// \brief  Auto hide timer.
    ////////////////////////////////////////////////////////////////////////////////
    Timer
    {
        id: timer
        running: self.opened && interval > 0
        interval: Data.Constants.popupTimeoutMs

        onTriggered:
        {
            self.close()
        }
    }
}
