// Qt.
#include <QGuiApplication>
#include <QQmlApplicationEngine>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <DatabaseControl/messagemanager.h>
#include <HardwareControl/cabinetcontroller.h>
#include <HardwareControl/cardreadercontroller.h>
#include <HardwareControl/fingerprintcontroller.h>
#include <HardwareControl/keypadcontrol.h>

////////////////////////////////////////////////////////////////////////////////
/// \brief  Application entry point.
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[] )
{
#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    QGuiApplication app( argc, argv );

    QQmlApplicationEngine engine;
    const QUrl url( QStringLiteral( "qrc:/qml/Main.qml" ) );
    engine.addImportPath( "qrc:/" );
    engine.addImportPath( "qrc:/qml/" );

    QObject::connect( &engine,
                      &QQmlApplicationEngine::objectCreated,
                      &app,
                      [url] ( QObject* obj, const QUrl& objUrl )
    {
        if ( !obj && url == objUrl )
        {
            QCoreApplication::exit( -1 );
        }
    }, Qt::QueuedConnection );

    // Register hardware controllers.
    KeypadControl::registerClass();
    CabinetController::registerClass();
    CardreaderController::registerClass();
    FingerprintController::registerClass();

    // Register and initialise database controllers.
    DatabaseControl::registerClass();
    MessageManager::registerClass();

    // Start QML engine.
    engine.load( url );

    return app.exec();
}
