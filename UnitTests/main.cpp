// Qt.
#include <QGuiApplication>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <DatabaseControlTest/tst_constants.h>
#include <DatabaseControlTest/tst_databasecontrol.h>
#include <DatabaseControlTest/tst_messagemanager.h>
#include <DatabaseControlTest/tst_querymanager.h>
#include <DatabaseControlTest/tst_tagcategory.h>
#include <DatabaseControlTest/tst_userprofilepermissions.h>
#include <DatabaseControlTest/tst_userprofiles.h>
#include <HardwareControlTest/tst_cabinetcontroller.h>
#include <HardwareControlTest/tst_cardreadercontroller.h>
#include <HardwareControlTest/tst_commsdefs.h>
#include <HardwareControlTest/tst_commsprotocol.h>
#include <HardwareControlTest/tst_fingerprintcontroller.h>
#include <HardwareControlTest/tst_fingerprintoperationthread.h>
#include <HardwareControlTest/tst_keypadcontrol.h>
#include <HardwareControlTest/tst_serialportprotocol.h>
#include <HardwareControlTest/tst_simulatedprotocol.h>
#include <HardwareControlTest/tst_subject.h>
#include <HardwareControlTest/tst_uartprotocol.h>
#include <UnitTestSuite/unittest.h>

////////////////////////////////////////////////////////////////////////////////
/// \brief  Application entry point.
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // A GUI application is required for the event system.
    QGuiApplication app( argc, argv );

    // DatabaseControl tests.
    DECLARE_UNIT_TEST( ConstantsTest )
    DECLARE_UNIT_TEST( DatabaseControlTest )
    DECLARE_UNIT_TEST( MessageManagerTest )
    DECLARE_UNIT_TEST( QueryManagerTest )
    DECLARE_UNIT_TEST( TagCategoryTest )
    DECLARE_UNIT_TEST( UserProfilePermissionsTest )
    DECLARE_UNIT_TEST( UserProfilesTest )

    // HardwareControl tests.
    DECLARE_UNIT_TEST( CabinetControllerTest )
    DECLARE_UNIT_TEST( CardreaderControllerTest )
    DECLARE_UNIT_TEST( CommsDefsTest )
    DECLARE_UNIT_TEST( CommsProtocolTest )
    DECLARE_UNIT_TEST( FingerprintControllerTest )
    DECLARE_UNIT_TEST( FingerprintOperationThreadTest )
    DECLARE_UNIT_TEST( KeypadControlTest )
    DECLARE_UNIT_TEST( SerialPortProtocolTest )
    DECLARE_UNIT_TEST( SimulatedProtocolTest )
    DECLARE_UNIT_TEST( SubjectTest )
    DECLARE_UNIT_TEST( UartProtocolTest )

    return UnitTest::run();
}
