#ifndef UNITTEST_H
#define UNITTEST_H

// Qt.
#include <QList>
#include <QObject>

// Project.
#include "unittestsuite_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Base class for unit tests.
////////////////////////////////////////////////////////////////////////////////
class UNITTESTSUITE_EXPORT UnitTest : public QObject
{
    Q_OBJECT

public:
    explicit UnitTest( QObject* parent = nullptr );

    static bool run();

    static QList<QObject*>& tests();
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Convenient unit test declaration.
////////////////////////////////////////////////////////////////////////////////
#define DECLARE_UNIT_TEST(class_name) static class_name Test_##class_name;

#endif // UNITTEST_H
