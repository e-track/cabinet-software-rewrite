// Qt.
#include <QDebug>
#include <QtTest/QtTest>

// Project.
#include "unittest.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     UnitTest::UnitTest()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UnitTest::UnitTest( QObject* parent )
    : QObject( parent )
{
    UnitTest::tests().append( this );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UnitTest::run()
///
/// \brief  Runs all the unit tests.
////////////////////////////////////////////////////////////////////////////////
bool UnitTest::run()
{
    int failed = false;

    QList<QObject*>& tests = UnitTest::tests();
    QList<QObject*>::iterator i;

    for ( i = tests.begin(); i != tests.end(); ++i )
    {
        failed += QTest::qExec( *i );
    }

    if ( tests.count() > 0 )
    {
        qDebug() << QString( "***********************************" );
        qDebug() << ( failed > 0
                    ? QString( "%1 test(s) failed" ).arg( failed )
                    : QString( "All tests passed successfully!" ) );
        qDebug() << QString( "***********************************" );
    }
    else
    {
        qDebug() << QString( "***********************************" );
        qDebug() << QString( "No unit tests run." );
        qDebug() << QString( "***********************************" );
    }

    return failed;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UnitTest::tests()
///
/// \return Returns the list of tests to be run.
////////////////////////////////////////////////////////////////////////////////
QList<QObject*>& UnitTest::tests()
{
    static QList<QObject*> tests;

    return tests;
}
