The Cabinet UI project consists of multiple libraries and applications. These can be built and installed alone or part of a top level SUBDIRS project.

To install a library or application add 'install' to the make arguments and ensure that the environment variable DISPLAYUI_INSTALLATION is set with a suitable path (i.e. C:\ETrack). You can add this in the 'projects', 'build' and 'build environment' section when a project is opened in QtCreator.

BuildAll.pro - Top level SUBDIRS project. Builds all libraries and the CabinetUI application which is subsequently run.
CabinetUI\CabinetUI.pro - Main application project.
DatabaseControl\DatabaseControl.pro - The database and messenger library.
HardwareControl\HardwareControl.pro - The hardware library.

Each library has a project (.pro) file for building and a project include (.pri) file for including. Libraries and applications can be built using the corresponding .pro file. If a library depends on others then please ensure the project include file includes the relevant project include files.

To avoid unnecessary rebuilds it is recommended that only the required libraries are opened and built. For example, if a developer is working on the HardwareControl code then only open the HardwareControl library and either the CabinetUI or HardwareControlTest applications. Similarly if only the QML code is being modified then only open the CabinetUI application.

TODO: Add unit test description.
