#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H

// Qt.
#include <QList>
#include <QMqttMessage>
#include <QObject>
#include <QString>

// Project.
#include "databasecontrol_global.h"

class QMqttClient;
class QMqttSubscription;

////////////////////////////////////////////////////////////////////////////////
/// \brief  MQTT message manager.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT MessageManager : public QObject
{
    Q_OBJECT

public:
    explicit MessageManager( QObject* parent = nullptr );

    // Setup.
    DATABASECONTROL_EXPORT static void registerClass();

    Q_INVOKABLE void connectToHost( const QString& address, int port );
    Q_INVOKABLE void disconnectFromHost();

    // Subscribe.
    Q_INVOKABLE void subscribe( const QString& topic );

    // Publish.
    Q_INVOKABLE virtual void publish( const QString& message );
    Q_INVOKABLE virtual void simulateEvent( const QString& event );

    // Timestamp. TODO: Move to utility class / project.
    virtual QString getTimestamp();

    // Statics.
    static bool convertStringToJson( const QString& json, QJsonObject& jsonObject );

    // Topic statics.
    DATABASECONTROL_EXPORT static const char* const _topicQuery;      ///< Query topic.
    DATABASECONTROL_EXPORT static const char* const _topic;           ///< The default topic. TODO: Remove and read from database.
    DATABASECONTROL_EXPORT static const char* const _debugTopic;      ///< The debug topic.

    // General statics.
    DATABASECONTROL_EXPORT static const char* const _command;
    DATABASECONTROL_EXPORT static const char* const _reply;
    DATABASECONTROL_EXPORT static const char* const _event;
    DATABASECONTROL_EXPORT static const char* const _data;
    DATABASECONTROL_EXPORT static const char* const _packetId;
    DATABASECONTROL_EXPORT static const char* const _timestamp;
    DATABASECONTROL_EXPORT static const char* const _result;
    DATABASECONTROL_EXPORT static const char* const _success;
    DATABASECONTROL_EXPORT static const char* const _fail;
    DATABASECONTROL_EXPORT static const char* const _info;
    DATABASECONTROL_EXPORT static const char* const _tables;
    DATABASECONTROL_EXPORT static const char* const _trailingDataToRemove;

    // Command statics.
    DATABASECONTROL_EXPORT static const char* const _helloWorld;
    DATABASECONTROL_EXPORT static const char* const _newUser;
    DATABASECONTROL_EXPORT static const char* const _updateUser;
    DATABASECONTROL_EXPORT static const char* const _removeUser;
    DATABASECONTROL_EXPORT static const char* const _newUserGroup;
    DATABASECONTROL_EXPORT static const char* const _updateUserGroup;
    DATABASECONTROL_EXPORT static const char* const _removeUserGroup;
    DATABASECONTROL_EXPORT static const char* const _newUserToGroup;
    DATABASECONTROL_EXPORT static const char* const _removeUserFromGroup;

    DATABASECONTROL_EXPORT static const char* const _newKey;
    DATABASECONTROL_EXPORT static const char* const _removeKey;
    DATABASECONTROL_EXPORT static const char* const _updateKey;

    DATABASECONTROL_EXPORT static const char* const _newKeyUserPermission;
    DATABASECONTROL_EXPORT static const char* const _removeKeyUserPermission;
    DATABASECONTROL_EXPORT static const char* const _updateKeyUserPermission;
    DATABASECONTROL_EXPORT static const char* const _newKeyGroupPermission;
    DATABASECONTROL_EXPORT static const char* const _removeKeyGroupPermission;
    DATABASECONTROL_EXPORT static const char* const _updateKeyGroupPermission;
    DATABASECONTROL_EXPORT static const char* const _resetUserPin;

    // Event statics.
    DATABASECONTROL_EXPORT static const char* const _loginSuccess;
    DATABASECONTROL_EXPORT static const char* const _loginFail;
    DATABASECONTROL_EXPORT static const char* const _logout;
    DATABASECONTROL_EXPORT static const char* const _accessDenied;
    DATABASECONTROL_EXPORT static const char* const _keyIn;
    DATABASECONTROL_EXPORT static const char* const _keyOut;

    // Statics.
    DATABASECONTROL_EXPORT static MessageManager* instance();
    DATABASECONTROL_EXPORT static void destroy();

signals:
    void connected();
    void payloadReceived( QString payload );

private slots:
    void onMessageReceived( QMqttMessage message );
    void onConnected();
    void onSubscriptionStateChanged();

private:
    QMqttClient* m_client;                  ///< MQTT client.
    QMqttSubscription* m_subscription;      ///< MQTT subscriptions (presently only one is required).
};

#endif // MESSAGEMANAGER_H
