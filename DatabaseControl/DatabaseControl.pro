################################################################################
# Config.
################################################################################
QT += quick

TEMPLATE = lib
DEFINES += DATABASECONTROL_LIBRARY

CONFIG += build_databasecontrol

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

################################################################################
# Dependencies.
################################################################################
include( $$PWD/DatabaseControl.pri )

################################################################################
# Installation
################################################################################
TARGET = DatabaseControl
unix: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET
win32: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET

!isEmpty(target.path): INSTALLS += target
