#ifndef SYSTEM_H
#define SYSTEM_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_systems database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT System
{
    Q_GADGET
    Q_PROPERTY( QString name                 MEMBER m_name )
    Q_PROPERTY( QString welcomeText          MEMBER m_welcomeText )
    Q_PROPERTY( QString timezone             MEMBER m_timezone )
    Q_PROPERTY( QString registeredDate       MEMBER m_registeredDate )
    Q_PROPERTY( QString lastCom              MEMBER m_lastCom )
    Q_PROPERTY( bool cardAuthentication      MEMBER m_cardAuthentication )
    Q_PROPERTY( bool biometricAuthentication MEMBER m_biometricAuthentication )
    Q_PROPERTY( bool compound                MEMBER m_compound )
    Q_PROPERTY( bool valet                   MEMBER m_valet )

public:
    explicit System();

    QString m_name;
    QString m_welcomeText;
    QString m_timezone;
    QString m_registeredDate;
    QString m_lastCom;

    bool m_cardAuthentication;
    bool m_biometricAuthentication;
    bool m_compound;
    bool m_valet;

    bool operator==( const System& lhs ) const;
    bool operator!=( const System& lhs ) const;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_group_system_access database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT GroupSystemAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( int system           MEMBER m_system )
    Q_PROPERTY( int group            MEMBER m_group )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit GroupSystemAccess();

    int m_id;
    int m_system;
    int m_group;
    bool m_isAccessGranted;

    bool operator==( const GroupSystemAccess& lhs ) const;
    bool operator!=( const GroupSystemAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _groupSystemAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userGroup;
    DATABASECONTROL_EXPORT static const char* const _systemId;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_user_system_access database table.
///         TODO: Consider whether to have a single SystemAccess class for both
///         user and user group system access since the properties are similar.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT UserSystemAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( int system           MEMBER m_system )
    Q_PROPERTY( int user             MEMBER m_user )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit UserSystemAccess();

    int m_id;
    int m_system;
    int m_user;
    bool m_isAccessGranted;

    bool operator==( const UserSystemAccess& lhs ) const;
    bool operator!=( const UserSystemAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _userSystemAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userId;
    DATABASECONTROL_EXPORT static const char* const _systemId;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

#endif // SYSTEM_H
