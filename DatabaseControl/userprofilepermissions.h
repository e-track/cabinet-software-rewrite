#ifndef USERPROFILEPERMISSIONS_H
#define USERPROFILEPERMISSIONS_H

// Qt.
#include <QMap>
#include <QString>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the user_profile_permissions database table.
////////////////////////////////////////////////////////////////////////////////
struct DATABASECONTROL_EXPORT UserProfilePermissions
{
    // Statics.
    DATABASECONTROL_EXPORT static const char* _id;
    DATABASECONTROL_EXPORT static const char* _permissionName;

    DATABASECONTROL_EXPORT static const char* _accessUsers;
    DATABASECONTROL_EXPORT static const char* _manageUsers;
    DATABASECONTROL_EXPORT static const char* _accessUserGroups;
    DATABASECONTROL_EXPORT static const char* _manageUserGroups;
    DATABASECONTROL_EXPORT static const char* _accessKeys;
    DATABASECONTROL_EXPORT static const char* _manageKeys;
    DATABASECONTROL_EXPORT static const char* _accessSettings;
    DATABASECONTROL_EXPORT static const char* _accessReports;
    DATABASECONTROL_EXPORT static const char* _accessSystems;
    DATABASECONTROL_EXPORT static const char* _accessBookings;
    DATABASECONTROL_EXPORT static const char* _makeBooking;
    DATABASECONTROL_EXPORT static const char* _manageBooking;
    DATABASECONTROL_EXPORT static const char* _isHandover;
    DATABASECONTROL_EXPORT static const char* _autoApprove;
    DATABASECONTROL_EXPORT static const char* _hasGlobalSearch;
    DATABASECONTROL_EXPORT static const char* _webLogin;
    DATABASECONTROL_EXPORT static const char* _manageRoles;

    DATABASECONTROL_EXPORT static QMap<QString, unsigned int> _map;        ///< Maps the user profile permission name to ID.
};

#endif // USERPROFILEPERMISSIONS_H
