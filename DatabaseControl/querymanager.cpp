// Qt.
#include <QCryptographicHash>
#include <QDateTime>
#include <QDebug>
#include <QGuiApplication>
#include <QList>
#include <QMutexLocker>
#include <QQmlApplicationEngine>
#include <QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>

// Project.
#include "constants.h"
#include "querymanager.h"
#include "tag.h"
#include "tagcategory.h"
#include "user.h"
#include "usergroup.h"

const char* QueryManager::_defaultDatabaseName = "DEFAULT_DB_CONNECTION";

// Statics 'entry' table.
const int QueryManager::_entryUsePin = 1;
const int QueryManager::_entryUseFingerprint = 2;
const int QueryManager::_entryUseCard = 3;

// Statics 'event_action' table.
const int QueryManager::_eventActionLogin = 1;
const int QueryManager::_eventActionLoginFail = 2;
const int QueryManager::_eventActionLogout = 3;
const int QueryManager::_eventActionAccessDenied = 18;
const int QueryManager::_eventActionKeyIn = 6;
const int QueryManager::_eventActionKeyOut = 7;

#ifdef Q_OS_WIN
    const char* _database( "QODBC" );
#else
    const char* _database( "QMYSQL" );
#endif // Q_OS_WIN

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::QueryManager( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
QueryManager::QueryManager( QObject* parent )
    : QObject( parent )
    , m_connectionType( Qt::QueuedConnection )
    , m_query( nullptr )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::initDatabase()
///
/// \brief  Initialises the database and attempts to connect to it.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::initDatabase()
{
    qDebug() << "[QueryManager::initDatabase]";

    QSqlDatabase db = QSqlDatabase::database( QueryManager::_defaultDatabaseName );
    bool success = db.isValid();

    if ( success )
    {
        qDebug() << QString( "[DatabaseControl::initDatabase] Attempting to init database that already exists." );
    }
    else
    {
        QSqlDatabase db = QSqlDatabase::addDatabase( _database, QueryManager::_defaultDatabaseName );

    #ifdef Q_OS_WIN
        db.setDatabaseName( "Driver={MySQL ODBC 8.0 ANSI Driver};DATABASE=sapi;" );
    #else
        db.setDatabaseName( "sapi" );
    #endif // Q_OS_WIN

        db.setHostName( "127.0.0.1" );

        // TODO: Read from config file.
        db.setUserName( "root" );
        //db.setPassword( "Keytracker$$1" );

        success = db.isValid();

        qDebug() << QString( "[DatabaseControl::initDatabase] Creating database %1, success: %2" ).arg( QueryManager::_defaultDatabaseName ).arg( success );
    }

    // Create query for the new database.
    m_query = new QSqlQuery( QSqlDatabase::database( QueryManager::_defaultDatabaseName ) );

    emit initDatabaseComplete( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::openDatabase()
///
/// \brief  Opens the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::openDatabase()
{
    qDebug() << "[QueryManager::openDatabase]";

    QSqlDatabase db = QSqlDatabase::database( QueryManager::_defaultDatabaseName );

    if ( !db.isOpen() )
    {
        db.open();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::closeDatabase()
///
/// \brief  Closes the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::closeDatabase()
{
    qDebug() << "[QueryManager::closeDatabase]";

    QSqlDatabase db = QSqlDatabase::database( QueryManager::_defaultDatabaseName );

    if ( db.isOpen() )
    {
        db.close();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::loginTempPin( const QString& tempPin )
///
/// \brief  Attempts to log-in using the given temporary PIN.
///
/// \param  Temporary PIN.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::loginTempPin( const QString& tempPin )
{
    qDebug() << "[QueryManager::loginTempPin]";

    // Create a query.
    m_query->prepare( "SELECT * FROM `tbl_users` WHERE `id` = ( SELECT `user_id` FROM `tbl_user_entry_ids` WHERE `entry_value` = :temp_pin ) AND `is_temp` = 1" );
    m_query->bindValue( ":temp_pin", tempPin );
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::loginPin( const User& user, const QString& pin )
///
/// \brief  Attempts to log-in using the given PIN.
///
/// \param  Authenticated user attempting to log in.
/// \param  PIN.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::loginPin( const User& user, const QString& pin )
{
    qDebug() << "[QueryManager::loginPin]";

    // Create a query.
    m_query->prepare( "SELECT * FROM `tbl_users` WHERE `id` = ( SELECT `user_id` FROM `tbl_user_entry_ids` WHERE `user_id` = :userId AND `entry_type` = 1 AND `entry_value` = :pin ) AND `is_temp` = 0" );
    m_query->bindValue( ":userId", user.m_id );
    m_query->bindValue( ":pin", pin );
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogLoginSuccess( const User& user )
///
/// \brief  Adds a successful login event to the database.
///
/// \param  Authenticated user that logged in.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::eventLogLoginSuccess( const User& user )
{
    qDebug() << "[QueryManager::eventLogLoginSuccess]";

    QStringList columns = { DatabaseControlConstants::_eventAction,
                            DatabaseControlConstants::_userId,
                            DatabaseControlConstants::_eventMessage,
                            DatabaseControlConstants::_parentSystem,
                            DatabaseControlConstants::_eventDate };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_eventsLogTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventAction ),  QueryManager::_eventActionLogin );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userId ),       user.m_id );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventMessage ), QVariant() );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_parentSystem ), 1 );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventDate ),    getTimestamp() );

    // Get the result.
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogLoginFail( const User& user )
///
/// \brief  Adds a failed login event to the database.
///
/// \param  Authenticated user that failed to log in.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::eventLogLoginFail( const User& user )
{
    qDebug() << "[QueryManager::eventLogLoginFail]";

    QStringList columns = { DatabaseControlConstants::_eventAction,
                            DatabaseControlConstants::_userId,
                            DatabaseControlConstants::_eventMessage,
                            DatabaseControlConstants::_parentSystem,
                            DatabaseControlConstants::_eventDate };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_eventsLogTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventAction ),  QueryManager::_eventActionLoginFail );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userId ),       user.m_id );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventMessage ), QVariant() );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_parentSystem ), 1 );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventDate ),    getTimestamp() );

    // Get the result.
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogLogout( const User& user )
///
/// \brief  Adds a successful logout event to the database.
///
/// \param  Authenticated user that logged out.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::eventLogLogout( const User& user )
{
    qDebug() << "[QueryManager::eventLogLogout]";

    QStringList columns = { DatabaseControlConstants::_eventAction,
                            DatabaseControlConstants::_userId,
                            DatabaseControlConstants::_eventMessage,
                            DatabaseControlConstants::_parentSystem,
                            DatabaseControlConstants::_eventDate };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_eventsLogTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventAction ),  QueryManager::_eventActionLogout );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userId ),       user.m_id );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventMessage ), QVariant() );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_parentSystem ), 1 );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventDate ),    getTimestamp() );

    // Get the result.
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogAccessDenied( const User& user )
///
/// \brief  Adds a successful access denied event to the database.
///
/// \param  User that was denied access.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::eventLogAccessDenied( const User& user )
{
    qDebug() << "[QueryManager::eventLogAccessDenied]";

    QStringList columns = { DatabaseControlConstants::_eventAction,
                            DatabaseControlConstants::_userId,
                            DatabaseControlConstants::_eventMessage,
                            DatabaseControlConstants::_parentSystem,
                            DatabaseControlConstants::_eventDate };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_eventsLogTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventAction ),  QueryManager::_eventActionAccessDenied );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userId ),       user.m_id );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventMessage ), QVariant() );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_parentSystem ), 1 );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventDate ),    getTimestamp() );

    // Get the result.
    m_query->exec();

    // Expect only one result.
    return m_query->first();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogUpdateTag( Tag* tag, const User& user  )
///
/// \brief  Adds a tag / key updated event to the database.
///
/// \param  Tag that was updated.
/// \param  Authenticated user that logged in.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::eventLogUpdateTag( Tag* tag, const User& user )
{
    QMutexLocker locker( &m_mutex );

    openDatabase();

    qDebug() << "[QueryManager::eventLogUpdateTag]";

    QStringList columns = { DatabaseControlConstants::_eventAction,
                            DatabaseControlConstants::_userId,
                            DatabaseControlConstants::_eventMessage,
                            DatabaseControlConstants::_parentSystem,
                            DatabaseControlConstants::_eventDate };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_eventsLogTable, columns );
    m_query->prepare( query );

    int action = ( tag->m_isOut ? QueryManager::_eventActionKeyOut : QueryManager::_eventActionKeyIn );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventAction ),  action );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userId ),       user.m_id );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_parentSystem ), 1 );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_eventDate ),    getTimestamp() );

    // Get the result.
    bool success = m_query->exec();

    emit eventLogUpdateTagComplete( tag, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updatePin( const QString& pin, const User& user )
///
/// \brief  Attempts to update the PIN of the given user.
///
/// \param  The new PIN.
/// \param  The user to update.
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::updatePin( const QString& pin, const User& user )
{
    qDebug() << "[QueryManager::updatePin]";

    openDatabase();

    QStringList columns = { UserEntry::_entryValue };

    QString updateQuery = QueryManager::getUpdateQuery( UserEntry::_userEntryIdsTable, columns );
    QString updateQuery2 = QueryManager::appendWhereClause( updateQuery, UserEntry::_userId );
    QString query = QueryManager::appendAndClause( updateQuery2, UserEntry::_entryType );

    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( UserEntry::_userId ), user.m_id );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryValue ), pin );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryType ), _entryUsePin );

    qDebug() << query;

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::resetTempPin( const User& user )
///
/// \brief  Attempts to reset the temporary PIN of the given user.
///
/// \param  The user to update.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::resetTempPin( const User& user )
{
    qDebug() << "[QueryManager::resetTempPin]";

    // Create a query.
    m_query->prepare( "UPDATE `tbl_users` SET `is_temp` = 0 WHERE `id` = :id" );
    m_query->bindValue( ":id", user.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addFingerprint( const int user, const int fingerprintId )
///
/// \brief  Writes the given fingerprint ID to the given user's record in the database.
///
/// \param  The user to update.
/// \param  The fingerprint ID.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addFingerprint( const User& user, const int fingerprintId )
{
    qDebug() << "[QueryManager::addFingerprint]";

    if ( fingerprintId == -1 )
    {
        // Do nothing if the fingerprint ID is null. TODO: Use const int for -1 IDs.
        return true;
    }

    QStringList columns = { UserEntry::_userId,
                            UserEntry::_entryType,
                            UserEntry::_entryValue };

    QString query = QueryManager::getInsertQuery( UserEntry::_userEntryIdsTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( UserEntry::_userId ), user.m_id );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryType ), _entryUseFingerprint );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryValue ), fingerprintId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addCard( const int user, const int cardId )
///
/// \brief  Writes the given card ID to the given user's record in the database.
///
/// \param  The user to update.
/// \param  The card ID.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addCard( const User& user, const QString& cardId )
{
    qDebug() << "[QueryManager::addCard]";

    if ( cardId.isEmpty() )
    {
        // Do nothing if the card ID is null.
        return true;
    }

    QStringList columns = { UserEntry::_userId,
                            UserEntry::_entryType,
                            UserEntry::_entryValue };

    QString query = QueryManager::getInsertQuery( UserEntry::_userEntryIdsTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( UserEntry::_userId ), user.m_id );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryType ), _entryUseCard );
    m_query->bindValue( QueryManager::getBinding( UserEntry::_entryValue ), cardId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addUser( const User& user )
///
/// \brief  Writes the given user to the database.
///
/// \param  The user to add.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addUser( const User& user )
{
    qDebug() << "[QueryManager::addUser]";

    QStringList columns = { DatabaseControlConstants::_username,
                            DatabaseControlConstants::_password,
                            DatabaseControlConstants::_firstName,
                            DatabaseControlConstants::_lastName,
                            DatabaseControlConstants::_isVisitor,
                            DatabaseControlConstants::_profileImage,
                            DatabaseControlConstants::_phoneNumber,
                            DatabaseControlConstants::_isActive,
                            DatabaseControlConstants::_activeFrom,
                            DatabaseControlConstants::_keyLimit,
                            DatabaseControlConstants::_isTemp,
                            DatabaseControlConstants::_isFirstLogin,
                            DatabaseControlConstants::_userProfile };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_usersTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_username ),     user.m_username );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_password ),     user.m_password );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_firstName ),    user.m_firstName );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_lastName ),     user.m_lastName );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_isVisitor ),    user.m_isVisitor );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_profileImage ), user.m_profileImage );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_phoneNumber ),  user.m_phoneNumber );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_isActive ),     user.m_isActive );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_activeFrom ),   user.m_activeFrom );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_keyLimit ),     user.m_keyLimit );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_isTemp ),       user.m_isTemp );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_isFirstLogin ), user.m_isFirstLogin );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_userProfile ),  user.m_roleId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateUser( const User& user )
///
/// \brief  Updates the given user in the database.
///
/// \param  The user to update.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::updateUser( const User& user )
{
    qDebug() << "[QueryManager::updateUser]";

    // Create a query.
    m_query->prepare( "UPDATE `tbl_users` "
        "SET `username` = :username, `password` = :password, `first_name` = :first_name, `last_name` = :last_name, `is_visitor` = :is_visitor, `profile_image` = :profile_image, `phone_number` = :phone_number, `is_active` = :is_active, `active_from` = :active_from, `key_limit` = :key_limit, `is_temp` = :is_temp, `is_first_login` = :is_first_login, `user_profile` = :user_profile "
        "WHERE `id` = :id" );
    m_query->bindValue( ":id",             user.m_id );
    m_query->bindValue( ":username",       user.m_username );
    m_query->bindValue( ":password",       user.m_password );
    m_query->bindValue( ":first_name",     user.m_firstName );
    m_query->bindValue( ":last_name",      user.m_lastName );
    m_query->bindValue( ":is_visitor",     user.m_isVisitor );
    m_query->bindValue( ":profile_image",  user.m_profileImage );
    m_query->bindValue( ":phone_number",   user.m_phoneNumber );
    m_query->bindValue( ":is_active",      user.m_isActive );
    m_query->bindValue( ":active_from",    user.m_activeFrom );
    m_query->bindValue( ":key_limit",      user.m_keyLimit );
    m_query->bindValue( ":is_temp",        user.m_isTemp );
    m_query->bindValue( ":is_first_login", user.m_isFirstLogin );
    m_query->bindValue( ":user_profile",   user.m_roleId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeUser( const User& user, const QString& table )
///
/// \brief  Removes the given user from the given table database.
///
/// \param  The user to remove.
/// \param  The table to remove the user from.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeUser( const User& user, const QString& table )
{
    qDebug() << "[QueryManager::removeUser]";

    if ( table.compare( DatabaseControlConstants::_usersTable ) == 0 )
    {
        QString query = QueryManager::getDeleteQuery( table, DatabaseControlConstants::_id );
        m_query->prepare( query );
        m_query->bindValue( ":id", user.m_id );
    }
    else
    {
        QString query = QueryManager::getDeleteQuery( table, DatabaseControlConstants::_userId );
        m_query->prepare( query );
        m_query->bindValue( ":user_id", user.m_id ); // TODO: Check.
    }

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUser( const int userId )
///
/// \brief  Gets the user that corresponds to the given user ID.
///
/// \param  The user ID to match.
///
/// \return True if the user exists in the database, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUser( const int userId )
{
    qDebug() << "[QueryManager::getUser]";

    // Create a query.
    m_query->prepare( "SELECT `id`,`first_name`,`last_name` FROM `tbl_users` WHERE `id`=:id" );
    m_query->bindValue( ":id", userId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserEntry( const int fingerprintId, const int cardId )
///
/// \brief  Gets the user entry that corresponds to the given fingerprint ID.
///
/// \param  The fingerprint ID to match.
/// \param  The fingerprint ID to match.
///
/// \return True if the user entry exists in the database, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserEntry( const int fingerprintId, const QString& cardId )
{
    qDebug() << "[QueryManager::getUserEntry]";

    // Create a query.
    m_query->prepare( "SELECT user_id,entry_type,entry_value FROM tbl_user_entry_ids WHERE (entry_value=:fingerprint_id AND entry_type=:use_fingerprint) OR (entry_value=:card_id AND entry_type=:use_card)" );
    m_query->bindValue( ":fingerprint_id", fingerprintId );
    m_query->bindValue( ":use_fingerprint", _entryUseFingerprint );
    m_query->bindValue( ":card_id", cardId );
    m_query->bindValue( ":use_card", _entryUseCard );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUsersGroup( UserEntry& userEntry )
///
/// \brief  Gets the group ID for the given user.
///
/// \param  The user to search for.
///
/// \return True if the user has a group entry in the database, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUsersGroup( const User& user )
{
    qDebug() << "[QueryManager::getUsersGroup]";

    // Create a query.
    m_query->prepare( "SELECT `user_id`,`user_group` FROM tbl_user_to_groups WHERE user_id=:user_id" );
    m_query->bindValue( ":user_id", user.m_id );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserGroup( const int id )
///
/// \brief  Gets the user group for the given user group ID.
///
/// \param  The user group ID to search for.
///
/// \return True if the user group exists in the database, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserGroup( const int id )
{
    qDebug() << "[QueryManager::getUserGroup]";

    // Create a query.
    m_query->prepare( "SELECT `id`,`group_name`,`key_limit` FROM tbl_user_group WHERE id=:id" );
    m_query->bindValue( ":id", id );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addUserGroup( const UserGroup& userGroup )
///
/// \brief  Adds a user group.
///
/// \param  The user group to be added.
///
/// \return True if the user group was added, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addUserGroup( const UserGroup& userGroup )
{
    qDebug() << "[QueryManager::addUserGroup]";

    // Create a query.
    m_query->prepare( "INSERT INTO tbl_user_group "
        "(`group_name`, `key_limit`)"
        "VALUES (:group_name, :key_limit)" );
    m_query->bindValue( ":group_name", userGroup.m_groupName );
    m_query->bindValue( ":key_limit",  userGroup.m_keyLimit );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeUserGroup( const UserGroup& userGroup, const QString& table )
///
/// \brief  Removes the given user group from the given table.
///
/// \param  The user group to be removed.
/// \param  The table to remove the user from.
///
/// \return True if the user group was removed, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeUserGroup( const UserGroup& userGroup, const QString& table )
{
    qDebug() << "[QueryManager::removeUserGroup]";

    if ( table.compare( UserGroup::_userGroupTable ) == 0 )
    {
        QString query = QueryManager::getDeleteQuery( table, DatabaseControlConstants::_id );
        m_query->prepare( query );
        m_query->bindValue( ":id", userGroup.m_id );
    }
    else
    {
        QString query = QueryManager::getDeleteQuery( table, UserGroup::_userGroup );
        m_query->prepare( query );
        m_query->bindValue( ":user_group", userGroup.m_id ); // TODO: Check.
    }

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateUserGroup( const UserGroup& userGroup )
///
/// \brief  Updates the given user group.
///
/// \param  The user group to be updated, containing the new data.
///
/// \return True if the user group was updated, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::updateUserGroup( const UserGroup& userGroup )
{
    qDebug() << "[QueryManager::updateUserGroup]";

    // Create a query.
    m_query->prepare( "UPDATE `tbl_user_group` "
        "SET `group_name` = :group_name, `key_limit` = :key_limit "
        "WHERE `id` = :id" );
    m_query->bindValue( ":id",         userGroup.m_id );
    m_query->bindValue( ":group_name", userGroup.m_groupName );
    m_query->bindValue( ":key_limit",  userGroup.m_keyLimit );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addUserToUserGroup( const int& user, const int& userGroupId )
///
/// \brief  Adds a user to a user group.
///
/// \param  The user ID to be added.
/// \param  The user group ID to add the user to.
///
/// \return True if the user ID was added to the user group ID, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addUserToUserGroup(const int& userId, const int& userGroupId )
{
    qDebug() << "[QueryManager::addUserToUserGroup]";

    // Create a query.
    m_query->prepare( "INSERT INTO tbl_user_to_groups "
        "(`user_id`, `user_group`) "
        "VALUES (:user_id, :user_group)" );
    m_query->bindValue( ":user_id",    userId );
    m_query->bindValue( ":user_group", userGroupId );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeUserFromUserGroup( const int& user, const int& userGroupId )
///
/// \brief  Removes a user from a user group.
///
/// \param  The user ID to be remove.
/// \param  The user group ID to remove ther user from.
///
/// \return True if the user was removed from the user group, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeUserFromUserGroup( const int& userId, const int& userGroupId )
{
    qDebug() << "[QueryManager::removeUserFromUserGroup]";

    QString deleteQuery = QueryManager::getDeleteQuery( UserGroup::_userToGroupTable, UserGroup::_userId );
    QString query = QueryManager::appendAndClause( deleteQuery, UserGroup::_userGroup );

    m_query->prepare( query );
    m_query->bindValue( QueryManager::getBinding( UserGroup::_userId ), userId );
    m_query->bindValue( QueryManager::getBinding( UserGroup::_userGroup ), userGroupId );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserProfiles()
///
/// \brief  Gets the user profiles from the database.
///
/// \return True if the user profiles were obtained successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserProfiles()
{
    qDebug() << "[QueryManager::getUserProfiles]";

    QStringList columns = { DatabaseControlConstants::_id,
                            DatabaseControlConstants::_profileName };
    QString query = getSelectQuery( DatabaseControlConstants::_userProfilesTable, columns );
    m_query->prepare( query );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserProfilePermissions()
///
/// \brief  Gets the user profile permissions from the database.
///
/// \return True if the user profile permissions were obtained successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserProfilePermissions()
{
    qDebug() << "[QueryManager::getUserProfilePermissions]";

    QStringList columns = { DatabaseControlConstants::_id,
                            DatabaseControlConstants::_permissionName };
    QString query = getSelectQuery( DatabaseControlConstants::_userProfilePermissionsTable, columns );
    m_query->prepare( query );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getCabinets()
///
/// \brief  Executes a query to get all cabinets in the database.
///
/// \return True if query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::getCabinets()
{
    qDebug() << "[QueryManager::getCabinets]";

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QString query = getSelectAllQuery( DatabaseControlConstants::_cabinetsTable );
    m_query->prepare( query );

    QList<Cabinet*>* cabinets = new QList<Cabinet*> ();

    if ( m_query->exec() )
    {
        while( m_query->next() )
        {
            int id = m_query->value( Cabinet::_id ).toInt();
            int slotCapacity = m_query->value( Cabinet::_slotCapacity ).toInt();
            int startSlot = m_query->value( Cabinet::_startSlot ).toInt();
            int endSlot = m_query->value( Cabinet::_endSlot ).toInt();
            int slotWidth = m_query->value( Cabinet::_slotWidth ).toInt();
            int noOfLocks = m_query->value( Cabinet::_noOfLocks ).toInt();
            QString name = m_query->value( Cabinet::_cabinetName ).toString();

            Cabinet* cabinet = new Cabinet();
            cabinet->m_id = id;
            cabinet->m_slotCapacity = slotCapacity;
            cabinet->m_startSlot = startSlot;
            cabinet->m_endSlot = endSlot;
            cabinet->m_slotWidth = slotWidth;
            cabinet->m_noOfLocks = noOfLocks;
            cabinet->m_cabinetName = name;

            cabinets->append( cabinet );
        }
    }

    // TODO: Reinstate when all DB calls are asyncronous.
    //closeDatabase();

    emit getCabinetsComplete( cabinets );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getRegisteredCabinet()
///
/// \brief  Executes a query to get the registered cabinet in the database.
///         Presently, all cabinets are obtained and it is assumed that the
///         first cabinet is the registered one.
///         TODO: Update this following real cabinet registration.
///
/// \return True if query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getRegisteredCabinet()
{
    qDebug() << "[QueryManager::getRegisteredCabinet]";

    QString query = getSelectAllQuery( DatabaseControlConstants::_cabinetsTable );
    m_query->prepare( query );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserGroupCabinetAccess( const UserGroup& group )
///
/// \brief  Gets the cabinet access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserGroupCabinetAccess( const UserGroup& group )
{
    qDebug() << "[QueryManager::getUserGroupCabinetAccess]";

    QString selectAllQuery = getSelectAllQuery( GroupCabinetAccess::_groupCabinetAccessTable );
    QString query = appendWhereClause( selectAllQuery, GroupCabinetAccess::_userGroup );

    m_query->prepare( query );
    m_query->bindValue( ":user_id", group.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserCabinetAccess( const User& user )
///
/// \brief  Gets the cabinet access for the given user.
///
/// \param  The user to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserCabinetAccess( const User& user )
{
    qDebug() << "[QueryManager::getUserCabinetAccess]";

    QString selectAllQuery = getSelectAllQuery( UserCabinetAccess::_userCabinetAccessTable );
    QString query = appendWhereClause( selectAllQuery, UserCabinetAccess::_userId );

    m_query->prepare( query );
    m_query->bindValue( ":user_id", user.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::searchTags( const QString& text )
///
/// \brief  Executes a query to search all tags (i.e. keys) for the given text.
///
/// \param  The text to be searched for.
///
/// \return True if query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::searchTags( const QString& text )
{
    qDebug() << QString( "[QueryManager::searchTags] %1" ).arg( text );

    QMutexLocker locker( &m_mutex );

    openDatabase();

    m_query->prepare("SELECT * FROM `tbl_keys` WHERE `key_name` LIKE :text");
    m_query->bindValue( ":text", QString("%%1%").arg( text ) );

    bool success = m_query->exec();

    emit searchTagsComplete( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getTags( const QString& column, const QVariant& value )
///
/// \brief  Executes a query to get all tags that match the value to the given column.
///
/// \param  The column to be matched.
/// \param  The value to match.
///
/// \return True if query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getTags( const QString& column, const QVariant& value )
{
    QMutexLocker locker( &m_mutex );

    qDebug() << "[QueryManager::getTags]";

    QString selectQuery = QueryManager::getSelectAllQuery( Tag::_keysTable );
    QString query = QueryManager::appendWhereClause( selectQuery, column );

    m_query->prepare( query );
    m_query->bindValue( QueryManager::getBinding( column ), value );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addTag( Tag* tag )
///
/// \brief  Adds the given tag / key to the database.
///
/// \param  The tag / key to add.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addTag( Tag* tag )
{
    qDebug() << "[QueryManager::addTag]";

    // TODO: Refactor to helper function.
    QStringList columns = { Tag::_cabinetId,
                            Tag::_category,
                            Tag::_slot,
                            Tag::_currentSlot,
                            Tag::_fob,
                            Tag::_keyName,
                            Tag::_keyDescription,
                            Tag::_keySerial,
                            Tag::_dateAdded,
                            Tag::_startTime,
                            Tag::_endTime,
                            Tag::_isStatic,
                            Tag::_isOut,
                            Tag::_isWrongSlot };

    QString query = QueryManager::getInsertQuery( Tag::_keysTable, columns );
    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( Tag::_cabinetId ),      tag->m_cabinetId );
    m_query->bindValue( QueryManager::getBinding( Tag::_category ),       tag->m_category );
    m_query->bindValue( QueryManager::getBinding( Tag::_slot ),           tag->m_slot );
    m_query->bindValue( QueryManager::getBinding( Tag::_currentSlot ),    tag->m_currentSlot );
    m_query->bindValue( QueryManager::getBinding( Tag::_fob ),            tag->m_fob );
    m_query->bindValue( QueryManager::getBinding( Tag::_keyName ),        tag->m_tagName );
    m_query->bindValue( QueryManager::getBinding( Tag::_keyDescription ), tag->m_tagDescription );
    m_query->bindValue( QueryManager::getBinding( Tag::_keySerial ),      tag->m_tagSerial );
    m_query->bindValue( QueryManager::getBinding( Tag::_dateAdded ),      tag->m_dateAdded );
    m_query->bindValue( QueryManager::getBinding( Tag::_startTime ),      tag->m_startTime );
    m_query->bindValue( QueryManager::getBinding( Tag::_endTime ),        tag->m_endTime );
    m_query->bindValue( QueryManager::getBinding( Tag::_isStatic ),       tag->m_isStatic );
    m_query->bindValue( QueryManager::getBinding( Tag::_isOut ),          tag->m_isOut );
    m_query->bindValue( QueryManager::getBinding( Tag::_isWrongSlot ),    tag->m_isWrongSlot );


    QString m_tagName;          ///< Tag name.
    QString m_tagDescription;   ///< Tag description.
    QString m_tagSerial;        ///< Tag serial number.

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeTag( Tag* tag, const QString& table )
///
/// \brief  Removes the given tag / key from the database.
///
/// \param  The tag / key to removed.
/// \param  The table to remove the tag / key from.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeTag( Tag* tag, const QString& table )
{
    qDebug() << "[QueryManager::removeTag]";

    if ( table.compare( Tag::_keysTable ) == 0 )
    {
        QString query = QueryManager::getDeleteQuery( table, Tag::_id );
        m_query->prepare( query );
        m_query->bindValue( QueryManager::getBinding( Tag::_id ), tag->m_id );
    }
    else
    {
        QString query = QueryManager::getDeleteQuery( table, Tag::_keyId );
        m_query->prepare( query );
        m_query->bindValue( QueryManager::getBinding( Tag::_keyId ), tag->m_id ); // TODO: Check.
    }

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateTag( Tag* tag )
///
/// \brief  Updates the tag in the database using the fob as the identifier.
///
/// \param  The tag containing the updated data.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::updateTag( Tag* tag )
{
    qDebug() << "[QueryManager::updateTag]";

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QStringList columns = { Tag::_cabinetId,
                            Tag::_userId,
                            Tag::_category,
                            Tag::_slot,
                            Tag::_currentSlot,
                            Tag::_fob,
                            Tag::_keyName,
                            Tag::_keyDescription,
                            Tag::_keySerial,
                            Tag::_dateAdded,
                            Tag::_startTime,
                            Tag::_endTime,
                            Tag::_isStatic,
                            Tag::_isOut,
                            Tag::_isWrongSlot };

    QString updateQuery = QueryManager::getUpdateQuery( Tag::_keysTable, columns );
    QString query = QueryManager::appendWhereClause( updateQuery, Tag::_fob );

    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( Tag::_cabinetId ),      tag->m_cabinetId );
    m_query->bindValue( QueryManager::getBinding( Tag::_userId ),         ( tag->m_userId == -1 ? QVariant() : tag->m_userId ) );
    m_query->bindValue( QueryManager::getBinding( Tag::_category ),       tag->m_category );
    m_query->bindValue( QueryManager::getBinding( Tag::_slot ),           tag->m_slot );
    m_query->bindValue( QueryManager::getBinding( Tag::_currentSlot ),    tag->m_currentSlot );
    m_query->bindValue( QueryManager::getBinding( Tag::_fob ),            tag->m_fob );
    m_query->bindValue( QueryManager::getBinding( Tag::_keyName ),        tag->m_tagName );
    m_query->bindValue( QueryManager::getBinding( Tag::_keyDescription ), tag->m_tagDescription );
    m_query->bindValue( QueryManager::getBinding( Tag::_keySerial ),      tag->m_tagSerial );
    m_query->bindValue( QueryManager::getBinding( Tag::_dateAdded ),      tag->m_dateAdded );
    m_query->bindValue( QueryManager::getBinding( Tag::_startTime ),      tag->m_startTime );
    m_query->bindValue( QueryManager::getBinding( Tag::_endTime ),        tag->m_endTime );
    m_query->bindValue( QueryManager::getBinding( Tag::_isStatic ),       tag->m_isStatic );
    m_query->bindValue( QueryManager::getBinding( Tag::_isOut ),          tag->m_isOut );
    m_query->bindValue( QueryManager::getBinding( Tag::_isWrongSlot ),    tag->m_isWrongSlot );

    bool success = m_query->exec();

    emit updateTagComplete( tag, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getTagsForCabinet( const int cabinetId, const int groupSize )
///
/// \brief  Executes a query to get all tags / keys for the given cabinet.
///
/// \param  The ID of the cabinet.
/// \param  The size of the groups that will be returned.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::getTagsForCabinet( const int cabinetId, const int groupSize )
{
    qDebug() << QString( "[QueryManager::getTagsForCabinet] Cabinet ID: %1, group size: %2" ).arg( cabinetId, groupSize );

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QString selectQuery = QueryManager::getSelectAllQuery( Tag::_keysTable );
    QString query = QueryManager::appendWhereClause( selectQuery, Tag::_cabinetId );

    m_query->prepare( query );
    m_query->bindValue( QueryManager::getBinding(  Tag::_cabinetId ), cabinetId );

    QList<QList<Tag*>>* cabinetTags = new QList<QList<Tag*>> ();

    if ( m_query->exec() )
    {
        unsigned int count = 0;
        unsigned int listCount = 0;

        // Insert a list for each group of tags / keys.
        QList<Tag*>* list = new QList<Tag*> ();

        // Iterate through all tags / keys.
        while( m_query->next() )
        {
            // Get the tag and add it to the list.
            Tag* tag = new Tag();

            tag->m_id = m_query->value( Tag::_id ).toInt();
            tag->m_cabinetId = m_query->value( Tag::_cabinetId ).toInt();
            tag->m_userId = m_query->value( Tag::_userId ).toInt();
            tag->m_category = m_query->value( Tag::_category ).toInt();
            tag->m_slot = m_query->value( Tag::_slot ).toInt();
            tag->m_currentSlot = m_query->value( Tag::_currentSlot ).toInt();
            tag->m_fob = m_query->value( Tag::_fob ).toInt();
            tag->m_mileage = 12345;                                                         // TODO: Use correct mileage (not in DB).
            tag->m_tagName = m_query->value( Tag::_keyName ).toString();
            tag->m_tagDescription = m_query->value( Tag::_keyDescription ).toString();
            tag->m_tagSerial = m_query->value( Tag::_keySerial ).toString();
            tag->m_dateAdded = m_query->value( Tag::_dateAdded ).toString();
            tag->m_startTime = m_query->value( Tag::_startTime ).toString();
            tag->m_endTime = m_query->value( Tag::_endTime ).toString();
            tag->m_location = "Location TODO";                                              // TODO: Use correct location (not in DB).
            tag->m_site = "Site TODO";                                                      // TODO: Use correct site (not in DB).
            tag->m_isStatic = m_query->value( Tag::_isStatic ).toBool();
            tag->m_isOut = m_query->value( Tag::_isOut ).toBool();
            tag->m_isWrongSlot = m_query->value( Tag::_isWrongSlot ).toBool();
            tag->m_booked = false;                                                          // TODO: Use correct booking (not in DB).
            tag->m_inBasket = false;

            // Ensure the tag data is moved to the main GUI thread to
            // avoid access issues with QML getting data across threads.
            tag->moveToThread( QGuiApplication::instance()->thread() );

            if ( count < groupSize )
            {
                // Add tags to the temporary list.
                list->append( tag );
                ++count;
            }
            else
            {
                // When the tag / key group size is reached, insert the list.
                cabinetTags->append( *list );
                count = 0;
                list = new QList<Tag*> ();
                ++listCount;
            }
        }

        // Add any trailing data to the final group.
        if ( list->count() > 0 )
        {
            cabinetTags->append( *list );
        }
    }

    // TODO: Reinstate when all DB calls are asyncronous.
    //closeDatabase();

    emit getTagsForCabinetComplete( cabinetTags );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getCategoryTags()
///
/// \brief  Executes a query to get all tags grouped by tag / key category.
///
/// \param  The number of tag categories.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::getCategoryTags( const int groupSize )
{
    qDebug() << QString( "[QueryManager::getCategoryTags]" );

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QString query = QueryManager::getSelectAllQuery( Tag::_keysTable );
    m_query->prepare( query );

    QList<QList<Tag*>>* categoryTags = new QList<QList<Tag*>> ();

    if ( m_query->exec() )
    {
        // TODO: Tag / key categories should be obtained from the database
        // when that functionality is complete.
        for ( int i = 0; i < groupSize; ++i )
        {
            QList<Tag*>* list = new QList<Tag*> ();
            categoryTags->append( *list );
        }

        unsigned int count = 0;

        // Iterate through all tags / keys.
        while( m_query->next() )
        {
            // Get the tag and add it to the list.
            Tag* tag = new Tag();

            tag->m_id = m_query->value( Tag::_id ).toInt();
            tag->m_cabinetId = m_query->value( Tag::_cabinetId ).toInt();
            tag->m_userId = m_query->value( Tag::_userId ).toInt();
            tag->m_category = m_query->value( Tag::_category ).toInt();
            tag->m_slot = m_query->value( Tag::_slot ).toInt();
            tag->m_currentSlot = m_query->value( Tag::_currentSlot ).toInt();
            tag->m_fob = m_query->value( Tag::_fob ).toInt();
            tag->m_mileage = 12345;                                                         // TODO: Use correct mileage (not in DB).
            tag->m_tagName = m_query->value( Tag::_keyName ).toString();
            tag->m_tagDescription = m_query->value( Tag::_keyDescription ).toString();
            tag->m_tagSerial = m_query->value( Tag::_keySerial ).toString();
            tag->m_dateAdded = m_query->value( Tag::_dateAdded ).toString();
            tag->m_startTime = m_query->value( Tag::_startTime ).toString();
            tag->m_endTime = m_query->value( Tag::_endTime ).toString();
            tag->m_location = "Location TODO";                                              // TODO: Use correct location (not in DB).
            tag->m_site = "Site TODO";                                                      // TODO: Use correct site (not in DB).
            tag->m_isStatic = m_query->value( Tag::_isStatic ).toBool();
            tag->m_isOut = m_query->value( Tag::_isOut ).toBool();
            tag->m_isWrongSlot = m_query->value( Tag::_isWrongSlot ).toBool();
            tag->m_booked = false;                                                          // TODO: Use correct booking (not in DB).
            tag->m_inBasket = false;

            // At the moment we're only interested in tags that are available.
            if ( !tag->m_isOut )
            {
                // Ensure the tag data is moved to the main GUI thread to
                // avoid access issues with QML getting data across threads.
                tag->moveToThread( QGuiApplication::instance()->thread() );

                // TODO: The tag / key category should be a foreign key in the
                // tag / key category table rather than residing in another database
                // table. As a result, we have to mock tag categories until
                int category = tag->m_category - 1;
                if ( category < categoryTags->count() )
                {
                    ( *categoryTags )[category].append( tag );
                }
            }
            else
            {
                delete tag;
            }

            ++count;
        }
    }

    // TODO: Reinstate when all DB calls are asyncronous.
    //closeDatabase();

    emit getCategoryTagsComplete( categoryTags );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Adds the given user tag / key permissons to the database.
///
/// \param  The user tag / key permissions to add.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addTagUserPermissions( const UserTagAccess& userTagAccess )
{
    qDebug() << "[QueryManager::addTagUserPermissions]";

    QStringList columns = { UserTagAccess::_userId,
                            UserTagAccess::_keyId,
                            UserTagAccess::_isAccessGranted };

    QString query = QueryManager::getInsertQuery( UserTagAccess::_userKeyAccessTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_userId ),          userTagAccess.m_userId );
    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_keyId ),           userTagAccess.m_keyId );
    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_isAccessGranted ), userTagAccess.m_isAccessGranted );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Removes the given user tag / key permissions from the database.
///
/// \param  The tag / key permissions to removed.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeTagUserPermissions( const UserTagAccess& userTagAccess )
{
    qDebug() << "[QueryManager::removeTagUserPermissions]";

    QString query = QueryManager::getDeleteQuery( UserTagAccess::_userKeyAccessTable, DatabaseControlConstants::_id );

    m_query->prepare( query );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ), userTagAccess.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Updates the user tag / key permissions in the database.
///
/// \param  The new user tag / key permissions to be updated data.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::updateTagUserPermissions( const UserTagAccess& userTagAccess )
{
    qDebug() << "[QueryManager::updateTagUserPermissions]";

    QStringList columns = { UserTagAccess::_userId,
                            UserTagAccess::_keyId,
                            UserTagAccess::_isAccessGranted };

    QString updateQuery = QueryManager::getUpdateQuery( UserTagAccess::_userKeyAccessTable, columns );
    QString query = QueryManager::appendWhereClause( updateQuery, DatabaseControlConstants::_id );

    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ),   userTagAccess.m_id );
    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_userId ),          userTagAccess.m_userId );
    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_keyId ),           userTagAccess.m_keyId );
    m_query->bindValue( QueryManager::getBinding( UserTagAccess::_isAccessGranted ), userTagAccess.m_isAccessGranted );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Adds the given user group tag / key permissons to the database.
///
/// \param  The user group tag / key permissions to add.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    qDebug() << "[QueryManager::addTagGroupPermissions]";

    QStringList columns = { GroupTagAccess::_userGroupId,
                            GroupTagAccess::_keyId,
                            GroupTagAccess::_isAccessGranted };

    QString query = QueryManager::getInsertQuery( GroupTagAccess::_userGroupKeyAccessTable, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_userGroupId ),     groupTagAccess.m_userGroupId );
    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_keyId ),           groupTagAccess.m_keyId );
    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_isAccessGranted ), groupTagAccess.m_isAccessGranted );

    // Get the result.
    return m_query->exec();

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Removes the given user group tag / key permissions from the database.
///
/// \param  The user group tag / key permissions to removed.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::removeTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    qDebug() << "[QueryManager::removeTagGroupPermissions]";

    QString query = QueryManager::getDeleteQuery( GroupTagAccess::_userGroupKeyAccessTable, DatabaseControlConstants::_id );

    m_query->prepare( query );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ), groupTagAccess.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Updates the user group tag / key permissions in the database.
///
/// \param  The new user group tag / key permissions to be updated data.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::updateTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    qDebug() << "[QueryManager::updateTagGroupPermissions]";

    QStringList columns = { GroupTagAccess::_userGroupId,
                            GroupTagAccess::_keyId,
                            GroupTagAccess::_isAccessGranted };

    QString updateQuery = QueryManager::getUpdateQuery( GroupTagAccess::_userGroupKeyAccessTable, columns );
    QString query = QueryManager::appendWhereClause( updateQuery, DatabaseControlConstants::_id );

    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ),    groupTagAccess.m_id );
    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_userGroupId ),     groupTagAccess.m_userGroupId );
    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_keyId ),           groupTagAccess.m_keyId );
    m_query->bindValue( QueryManager::getBinding( GroupTagAccess::_isAccessGranted ), groupTagAccess.m_isAccessGranted );

    // Get the result.
    return m_query->exec();

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserGroupTagAccess( const UserGroup& group )
///
/// \brief  Gets the tag access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserGroupTagAccess( const UserGroup& group )
{
    Q_UNUSED( group );

    qDebug() << "[QueryManager::getUserGroupTagAccess]";

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserTagAccess( const User& user  )
///
/// \brief  Gets the tag access for the given user.
///
/// \param  The user to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserTagAccess( const User& user )
{
    Q_UNUSED( user );

    qDebug() << "[QueryManager::getUserTagAccess]";

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addTagCategory( TagCategory* tagCategory )
///
/// \brief  Adds the given tag category.
///
/// \param  The tag category to add.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::addTagCategory( TagCategory* tagCategory )
{
    qDebug() << "[QueryManager::addTagCategory]";

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QStringList columns = { TagCategory::_tagCategoryType,
                            TagCategory::_tagCategoryIcon,
                            TagCategory::_tagCategoryName,
                            TagCategory::_tagCategoryShortName };

    QString query = QueryManager::getInsertQuery( TagCategory::_tagCategoryTable, columns );
    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryType ),      tagCategory->m_tagCategoryType );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryIcon ),      tagCategory->m_tagCategoryIcon );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryName ),      tagCategory->m_tagCategoryName );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryShortName ), tagCategory->m_tagCategoryShortName );

    bool success = m_query->exec();

    emit addTagCategoryComplete( tagCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::updateTagCategory( TagCategory* tagCategory )
///
/// \brief  Updates the given tag category.
///
/// \param  The tag category to update with the new data.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::updateTagCategory( TagCategory* tagCategory )
{
    qDebug() << "[QueryManager::updateTagCategory]";

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QStringList columns = { DatabaseControlConstants::_id,
                            TagCategory::_tagCategoryType,
                            TagCategory::_tagCategoryIcon,
                            TagCategory::_tagCategoryName,
                            TagCategory::_tagCategoryShortName };

    QString updateQuery = QueryManager::getUpdateQuery( TagCategory::_tagCategoryTable, columns );
    QString query = QueryManager::appendWhereClause( updateQuery, DatabaseControlConstants::_id );
    m_query->prepare( query );

    // TODO: Refactor to helper function.
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ),      tagCategory->m_tagCategoryId );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryType ),      tagCategory->m_tagCategoryType );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryIcon ),      tagCategory->m_tagCategoryIcon );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryName ),      tagCategory->m_tagCategoryName );
    m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategoryShortName ), tagCategory->m_tagCategoryShortName );

    bool success = m_query->exec();

    emit updateTagCategoryComplete( tagCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeTagCategory( TagCategory* tagCategory )
///
/// \brief  Removes the given tag category.
///
/// \param  The tag category to remove.
/// \param  The table to remove the category from.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::removeTagCategory( TagCategory* tagCategory , const QString& table )
{
    qDebug() << "[QueryManager::removeTagCategory]";

    if ( table.compare( TagCategory::_tagCategoryTable ) == 0 )
    {
        QString query = QueryManager::getDeleteQuery( table, DatabaseControlConstants::_id );
        m_query->prepare( query );
        m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_id ), tagCategory->m_tagCategoryId );
    }
    else
    {
        QString query = QueryManager::getDeleteQuery( table, TagCategory::_tagCategory );
        m_query->prepare( query );
        m_query->bindValue( QueryManager::getBinding( TagCategory::_tagCategory ), tagCategory->m_tagCategoryId );
    }

    bool success = m_query->exec();

    emit removeTagCategoryComplete( tagCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getTagCategories()
///
/// \brief  Executes a query to get all tag / key categories.
////////////////////////////////////////////////////////////////////////////////
void QueryManager::getTagCategories()
{
    qDebug() << QString( "[QueryManager::getTagCategories]" );

    QMutexLocker locker( &m_mutex );

    openDatabase();

    QString query = QueryManager::getSelectAllQuery( TagCategory::_tagCategoryTable );
    m_query->prepare( query );

    QList<TagCategory*>* tagCategories = new QList<TagCategory*> ();

    if ( m_query->exec() )
    {
        // Iterate through all tags / keys.
        while( m_query->next() )
        {
            // Get the tag and add it to the list.
            TagCategory* tagCategory = new TagCategory();

            tagCategory->m_tagCategoryId = m_query->value( DatabaseControlConstants::_id ).toInt();
            tagCategory->m_tagCategoryType = m_query->value( TagCategory::_tagCategoryType ).toString();
            tagCategory->m_tagCategoryIcon = m_query->value( TagCategory::_tagCategoryIcon ).toString();
            tagCategory->m_tagCategoryName = m_query->value( TagCategory::_tagCategoryName ).toString();
            tagCategory->m_tagCategoryShortName = m_query->value( TagCategory::_tagCategoryShortName ).toString();

            // Ensure the tag data is moved to the main GUI thread to
            // avoid access issues with QML getting data across threads.
            tagCategory->moveToThread( QGuiApplication::instance()->thread() );

            // When the tag / key group size is reached, insert the list.
            tagCategories->append( tagCategory );
        }
    }

    // TODO: Reinstate when all DB calls are asyncronous.
    //closeDatabase();

    emit getTagCategoriesComplete( tagCategories );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::performSqlQuery( const QString& sql )
///
/// \brief  Performs the given SQL query on the local database.
///
/// \param  The SQL query to execute.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::performSqlQuery( const QString& sql )
{
    qDebug() << "[QueryManager::performSqlQuery]";

    m_query->prepare( sql );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getNumRowsAffected()
///
/// \brief  Gets the number of rows affected by the most recent query.
///
/// \return The number of rows that were affected.
////////////////////////////////////////////////////////////////////////////////
int QueryManager::getNumRowsAffected()
{
    return m_query->numRowsAffected();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getValue( const QString& key )
///
/// \brief  Gets the value from the SQL query result for the given key.
///
/// \param  The requested key.
///
/// \return The corresponding value for the given key.
////////////////////////////////////////////////////////////////////////////////
QVariant QueryManager::getValue( const QString& key )
{
    QMutexLocker locker( &m_mutex );

    return m_query->value( key );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getNext()
///
/// \brief  Positions the query to the next result.
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getNext()
{
    QMutexLocker locker( &m_mutex );

    return m_query->next();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::addSystem( const System& system )
///
/// \brief  Writes the given system to the database.
///
/// \param  The system to add.
///
/// \return True if the database was written successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::addSystem( const System& system )
{
    qDebug() << "[QueryManager::addSystem]";

    QStringList columns = { DatabaseControlConstants::_systemName,
                            DatabaseControlConstants::_welcomeText,
                            DatabaseControlConstants::_systemTimezone,
                            DatabaseControlConstants::_registeredDate,
                            DatabaseControlConstants::_lastCom,
                            DatabaseControlConstants::_cardAuthentication,
                            DatabaseControlConstants::_biometricAuthentication,
                            DatabaseControlConstants::_compoundModule,
                            DatabaseControlConstants::_valetModule };

    QString query = QueryManager::getInsertQuery( DatabaseControlConstants::_system, columns );
    m_query->prepare( query );

    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_systemName ),              system.m_name );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_welcomeText ),             system.m_welcomeText );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_systemTimezone ),          system.m_timezone );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_registeredDate ),          system.m_registeredDate );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_lastCom ),                 system.m_lastCom );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_cardAuthentication ),      system.m_cardAuthentication );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_biometricAuthentication ), system.m_biometricAuthentication );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_compoundModule ),          system.m_compound );
    m_query->bindValue( QueryManager::getBinding( DatabaseControlConstants::_valetModule ),             system.m_valet );

    // Get the result.
    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserGroupSystemAccess( const UserGroup& group )
///
/// \brief  Gets the system access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserGroupSystemAccess( const UserGroup& group )
{
    qDebug() << "[QueryManager::getUserGroupSystemAccess]";

    QString selectAllQuery = getSelectAllQuery( GroupSystemAccess::_groupSystemAccessTable );
    QString query = appendWhereClause( selectAllQuery, GroupSystemAccess::_userGroup );

    m_query->prepare( query );
    m_query->bindValue( ":user_id", group.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUserSystemAccess( const User& user )
///
/// \brief  Gets the system access for the given user.
///
/// \param  The user to search for.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getUserSystemAccess( const User& user )
{
    qDebug() << "[QueryManager::getUserSystemAccess]";

    QString selectAllQuery = getSelectAllQuery( UserSystemAccess::_userSystemAccessTable );
    QString query = appendWhereClause( selectAllQuery, UserSystemAccess::_userId );

    m_query->prepare( query );
    m_query->bindValue( ":user_id", user.m_id );

    return m_query->exec();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getSystem()
///
/// \brief  Gets the system.
///
/// \return True if the database was read successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool QueryManager::getSystem()
{
    qDebug() << "[QueryManager::getSystem]";

    // Create a query.
    m_query->prepare( "SELECT * FROM `tbl_systems` WHERE `id`=1" );

    bool success = false;

    // Get the result.
    if ( m_query->exec() )
    {
        success = m_query->first();
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getTimestamp()
///
/// \brief  Returns the current timestamp. TODO: Move this to a utility class /
///         project as it will be used elsewhere.
///
/// \return The current timestamp.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getTimestamp()
{
    return QDateTime::currentDateTimeUtc().toString();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getDeleteQuery( const QString& table, const QString& column )
///
/// \brief  Returns a delete query for the given table and column.
///
/// \param  The table to delete from.
/// \param  The column to match.
///
/// \return The delete query.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getDeleteQuery( const QString& table, const QString& column )
{
    return QString( "DELETE FROM %1 WHERE `%2` = :%2" ).arg( table ).arg( column );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getInsertQuery(const QString& table, const QStringList& columns)
///
/// \brief  Returns an insert query for the given table and columns.
///
/// \param  The table to insert into.
/// \param  The columns that will be populated.
///
/// \return The insert query.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getInsertQuery( const QString& table, const QStringList& columns )
{
    QString values = "";
    QString bindings = "";

    for ( int i = 0; i < columns.length(); ++i )
    {
        values.append( QString( "`%1`" ).arg( columns[i] ) );
        values.append( i < columns.length() - 1 ? ", " : "" );

        bindings.append( QString( ":%1" ).arg( columns[i] ) );
        bindings.append( i < columns.length() - 1 ? ", " : "" );
    }

    QString query = QString( "INSERT INTO %1 (%2) VALUES (%3)" ).arg( table ).arg( values ).arg( bindings );

    return query;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getSelectQuery( const QString& table, const QStringList& columns )
///
/// \brief  Returns a select query for the given table and columns.
///
/// \param  The table to select from.
/// \param  The columns that will be selected.
///
/// \return The select query.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getSelectQuery(const QString& table, const QStringList& columns )
{
    QString values = "";

    for ( int i = 0; i < columns.length(); ++i )
    {
        values.append( QString( "`%1`" ).arg( columns[i] ) );
        values.append( i < columns.length() - 1 ? ", " : "" );
    }

    QString query = QString( "SELECT %1 FROM %2" ).arg( values ).arg( table );

    return query;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getSelectAllQuery(const QString& table )
///
/// \brief  Returns a select query for the given table and column.
///
/// \param  The table to select from.
///
/// \return The select query.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getSelectAllQuery(const QString& table )
{
    QString query = QString( "SELECT %1 FROM %2" ).arg( DatabaseControlConstants::_all ).arg( table );

    return query;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getUpdateQuery(const QString& table, const QStringList& columns)
///
/// \brief  Returns an update query for the given table and columns.
///
/// \param  The table to update from.
/// \param  The columns that will be updated.
///
/// \return The update query.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getUpdateQuery( const QString& table, const QStringList& columns )
{
    QString bindings = "";

    for ( int i = 0; i < columns.length(); ++i )
    {
        bindings.append( QString( "`%1` = :%1" ).arg( columns[i] ) );
        bindings.append( i < columns.length() - 1 ? ", " : "" );
    }

    QString query = QString( "UPDATE %1 SET %2" ).arg( table ).arg( bindings );

    return query;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::appendWhereClause( const QString& query, const QString& column )
///
/// \brief  Appends a where clause to the given query.
///
/// \param  The query to modify.
/// \param  The column the variable binding will be created for.
///
/// \return The given query with a modified where clause.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::appendWhereClause( const QString& query, const QString& column )
{
    QString whereQuery = query;

    return whereQuery.append( QString( " WHERE `%1` = :%2" ).arg( column, column ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::appendAndClause( const QString& query, const QString& column )
///
/// \brief  Appends an and clause to the given query.
///
/// \param  The query to modify.
/// \param  The column the variable binding will be created for.
///
/// \return The given query with a modified and clause.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::appendAndClause( const QString& query, const QString& column )
{
    QString andQuery = query;

    return andQuery.append( QString( " AND `%1` = :%2" ).arg( column, column ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::getBinding( const QString& name )
///
/// \brief  Returns a markd up binding that can be used with the SQL query bindings.
///
/// \param  The name to be used for the binding.
///
/// \return The marked up binding.
////////////////////////////////////////////////////////////////////////////////
QString QueryManager::getBinding( const QString& name )
{
    return QString( ":%1" ).arg( name );
}
