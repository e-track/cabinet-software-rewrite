// Qt.
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlApplicationEngine>
#include <QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

// Project.
#include "constants.h"
#include "databasecontrol.h"
#include "messagemanager.h"
#include "usergroup.h"
#include "userprofilepermissions.h"
#include "userprofiles.h"

// Statics.
DatabaseControl* _databaseControl = nullptr;

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::DatabaseControl()
///
/// \brief  Constructor.
///
/// \param  A query manager to handle local database queries.
/// \param  A message manager to handle remote MQTT messages.
////////////////////////////////////////////////////////////////////////////////
DatabaseControl::DatabaseControl( QueryManager* queryManager, MessageManager* messageManager )
    : QObject( nullptr )
    , m_queryManager( queryManager )
    , m_messageManager( messageManager )
    , m_selectedCabinet( nullptr )
    , m_registeredCabinet( nullptr )
    , m_tagGroupSize( 0 )
    , m_savedPin( "" )
    , m_savedCardId( "" )
    , m_savedFingerprintId( -1 )
    , m_thread( new QThread() )
{
    connect( m_messageManager, &MessageManager::payloadReceived, this, &DatabaseControl::onPayloadReceived );

    // Initialise message manager and connect to the MQTT broker.
    m_messageManager->connectToHost( "185.217.43.183", 1883 );

    // Threading.
    m_queryManager->moveToThread( m_thread );

    // To allow SQL queries to be asynchronous they must be invoked via a signal and handled in a slot.
    // The signals are prefixed with 'start' and the slots are prefixed with 'on'.

    // Cabinets.
    connect( this, &DatabaseControl::queryCabinets,       m_queryManager, &QueryManager::getCabinets,       m_queryManager->m_connectionType );

    // Tags / keys.
    connect( this, &DatabaseControl::querySearchTags,     m_queryManager, &QueryManager::searchTags,        m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::queryTagsForCabinet, m_queryManager, &QueryManager::getTagsForCabinet, m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::queryCategoryTags,   m_queryManager, &QueryManager::getCategoryTags,   m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::updateTag,           m_queryManager, &QueryManager::updateTag,         m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::eventLogUpdateTag,   m_queryManager, &QueryManager::eventLogUpdateTag, m_queryManager->m_connectionType );

    // Tag / key categories.
    connect( this, &DatabaseControl::addTagCategory,      m_queryManager, &QueryManager::addTagCategory,    m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::updateTagCategory,   m_queryManager, &QueryManager::updateTagCategory, m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::removeTagCategory,   m_queryManager, &QueryManager::removeTagCategory, m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::queryTagCategories,  m_queryManager, &QueryManager::getTagCategories,  m_queryManager->m_connectionType );

    // Database handling.
    connect( this, &DatabaseControl::initDatabase,        m_queryManager, &QueryManager::initDatabase,      m_queryManager->m_connectionType );
    connect( this, &DatabaseControl::closeDatabase,       m_queryManager, &QueryManager::closeDatabase,     m_queryManager->m_connectionType );

    // Cabinets.
    connect( m_queryManager, &QueryManager::getCabinetsComplete,       this, &DatabaseControl::onGetCabinetsComplete,       m_queryManager->m_connectionType );

    // Tags / keys.
    connect( m_queryManager, &QueryManager::searchTagsComplete,        this, &DatabaseControl::onSearchTagsComplete,        m_queryManager->m_connectionType );
    connect( m_queryManager, &QueryManager::getTagsForCabinetComplete, this, &DatabaseControl::onGetTagsForCabinetComplete, m_queryManager->m_connectionType );
    connect( m_queryManager, &QueryManager::getCategoryTagsComplete,   this, &DatabaseControl::onGetCategoryTagsComplete,   m_queryManager->m_connectionType );


    // Tag / key categories.
    connect( m_queryManager, &QueryManager::addTagCategoryComplete,    this, &DatabaseControl::onAddTagCategoryComplete,    m_queryManager->m_connectionType );
    connect( m_queryManager, &QueryManager::updateTagCategoryComplete, this, &DatabaseControl::onUpdateTagCategoryComplete, m_queryManager->m_connectionType );
    connect( m_queryManager, &QueryManager::removeTagCategoryComplete, this, &DatabaseControl::onRemoveTagCategoryComplete, m_queryManager->m_connectionType );
    connect( m_queryManager, &QueryManager::getTagCategoriesComplete,  this, &DatabaseControl::onGetTagCategoriesComplete,  m_queryManager->m_connectionType );

    // Database handling.
    connect( m_queryManager, &QueryManager::initDatabaseComplete,      this, &DatabaseControl::onInitDatabaseComplete,      m_queryManager->m_connectionType );

    m_thread->start();

    // Start initialisation.
    emit initDatabase();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::openDatabase()
///
/// \brief  Opens the database.
///         TODO: Remove this as QueryManager functions should call QueryManager::openDatabase.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::openDatabase()
{
    m_queryManager->openDatabase();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::setLoggedInUser( User user )
///
/// \brief  Sets the currently logged in user.
///
/// \param  The currently logged in user.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::setLoggedInUser( User user )
{
    qDebug() << QString( "[DatabaseControl::setLoggedInUser] User: %1" ).arg( user.m_id );

    if ( m_loggedInUser != user )
    {
        m_loggedInUser = user;

        emit loggedInUserChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::setAuthenticatedUser( User user )
///
/// \brief  Sets the currently authenticated user.
///
/// \param  The currently authenticated user.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::setAuthenticatedUser( User user )
{
    if ( m_authenticatedUser != user )
    {
        m_authenticatedUser = user;

        emit authenticatedUserChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::loginPin( QString pin )
///
/// \brief  Attempts to log-in using the given PIN.
///
/// \param  PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::loginPin( QString pin )
{
    openDatabase();

    if ( m_queryManager->loginPin( m_authenticatedUser, pin ) )
    {
        // Success.
        User user = User();
        user.m_id = m_queryManager->getValue(  DatabaseControlConstants::_id ).toInt();
        user.m_firstName = m_queryManager->getValue(  DatabaseControlConstants::_firstName ).toString();
        user.m_lastName = m_queryManager->getValue(  DatabaseControlConstants::_lastName ).toString();

        setLoggedInUser( user );

        // Create an event message.
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = user.m_id;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginSuccess;
        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        // Write the event to the database.
        m_queryManager->eventLogLoginSuccess( user );

        // Publish the event message.
        m_messageManager->publish( event );

        emit loginSuccess( user );
    }
    else
    {
        // Failure.
        setLoggedInUser( User() );

        // Create an event message.
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = m_authenticatedUser.m_id;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginFail;
        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        // Write the event to the database.
        m_queryManager->eventLogLoginFail( User() );

        // Publish the event message.
        m_messageManager->publish( event );

        emit loginFail();
    }

    // Logging in overrides authentication so we need to reset the authorisation
    // regardless of whether or not the login was successful.
    denyUser();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::loginTempPin( const QString& tempPin )
///
/// \brief  Attempts to log-in using the given temporary PIN.
///
/// \param  Temporary PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::loginTempPin( const QString& tempPin )
{
    openDatabase();

    if ( m_queryManager->loginTempPin( tempPin ) )
    {
        User user = User();
        user.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
        user.m_firstName = m_queryManager->getValue( DatabaseControlConstants::_firstName ).toString();
        user.m_lastName = m_queryManager->getValue( DatabaseControlConstants::_lastName ).toString();

        setLoggedInUser( user );

        // Create an event message.
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = user.m_id;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginSuccess;
        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        // Write the event to the database.
        openDatabase();

        m_queryManager->eventLogLoginSuccess( user );

        // Publish the event message.
        m_messageManager->publish( event );

        emit tempLoginSuccess( user );
    }
    else
    {
        // Failure.
        setLoggedInUser( User() );

        // Create an event message.
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = User().m_id;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginFail;
        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        // Write the event to the database.
        m_queryManager->eventLogLoginFail( User() );

        // Publish the event message.
        m_messageManager->publish( event );

        emit tempLoginFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::logout()
///
/// \brief  Attempts to log-in using the given temporary PIN.
///
/// \param  Temporary PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::logout()
{
    // Create an event message.
    QJsonObject jsonUser;
    jsonUser[DatabaseControlConstants::_userId] = m_loggedInUser.m_id;

    QJsonObject jsonEvent;
    jsonEvent[MessageManager::_event] = MessageManager::_logout;
    jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
    jsonEvent[MessageManager::_data] = jsonUser;

    QJsonDocument jsonDoc( jsonEvent );
    QString event = jsonDoc.toJson( QJsonDocument::Indented );

    // Write the event to the database.
    m_queryManager->eventLogLogout( m_loggedInUser );

    // Publish the event message.
    m_messageManager->publish( event );

    // Clear logged in user.
    setLoggedInUser( User() );

    emit logoutSuccess();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::savePin( const QString& pin )
///
/// \brief  Saves the PIN without writing the new PIN to the current logged in user.
///
/// \param  The new PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::savePin( const QString& pin )
{
    m_savedPin = pin;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::addFingerprint( const int userId, const int fingerprintId )
///
/// \brief  Writes the given fingerprint and user IDs to the database.
///
/// \param  The fingerprint ID.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::addFingerprint( const int fingerprintId )
{
    m_savedFingerprintId = fingerprintId;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::addCard( const int userId, const QString& cardId )
///
/// \brief  Writes the given card and user IDs to the database.
///
/// \param  The card ID.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::addCard( const QString& cardId )
{
    m_savedCardId = cardId;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::authenticateUser( int fingerprintId, QString cardId )
///
/// \brief  Uses the given user ID that has been authenticated via card
///         to get the user details.
///
/// \param  The authenticated fingerprint ID.
/// \param  The authenticated card ID.
////////////////////////////////////////////////////////////////////////////////
bool DatabaseControl::authenticateUser( int fingerprintId, QString cardId )
{
    openDatabase();

    bool access = false;

    if ( m_queryManager->getUserEntry( fingerprintId, cardId ) )
    {
        if ( m_queryManager->getNext() )
        {
            UserEntry entry;
            entry.m_userId = m_queryManager->getValue( UserEntry::_userId ).toInt();
            entry.m_entryType = m_queryManager->getValue( UserEntry::_entryType ).toInt();
            entry.m_entryValue = m_queryManager->getValue( UserEntry::_entryValue ).toString();

            User user = User();

            if ( m_queryManager->getUser( entry.m_userId ) )
            {
                if ( m_queryManager->getNext() )
                {
                    User user = User();
                    user.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
                    user.m_firstName = m_queryManager->getValue( DatabaseControlConstants::_firstName ).toString();
                    user.m_lastName = m_queryManager->getValue( DatabaseControlConstants::_lastName ).toString();

                    bool systemAccess = false;
                    bool cabinetAccess = false;

                    bool success = checkUserSystemPermissions( user, systemAccess ) &&
                                   checkUserCabinetPermissions( user, cabinetAccess );

                    if ( success && systemAccess && cabinetAccess )
                    {
                        access = true;

                        setAuthenticatedUser( user );
                    }
                    else
                    {
                        // Create an event message.
                        QJsonObject jsonUser;
                        jsonUser[DatabaseControlConstants::_userId] = user.m_id;

                        QJsonObject jsonEvent;
                        jsonEvent[MessageManager::_event] = MessageManager::_accessDenied;
                        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
                        jsonEvent[MessageManager::_data] = jsonUser;

                        QJsonDocument jsonDoc( jsonEvent );
                        QString event = jsonDoc.toJson( QJsonDocument::Indented );

                        // Write the event to the database.
                        m_queryManager->eventLogLoginFail( User() );

                        // Publish the event message.
                        m_messageManager->publish( event );
                    }
                }
            }
        }
    }

    if ( !access )
    {
        denyUser();

        emit accessDenied();
    }

    return access;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::denyUser()
///
/// \brief  Cancels authentication by setting the authenticated user to null.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::denyUser()
{
    setAuthenticatedUser( User() );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::registerClass()
{
    qmlRegisterSingletonType<DatabaseControl>( "Database",
                                               1,
                                               0,
                                               "Control",
                                               []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        return DatabaseControl::instance();
    });

    qmlRegisterType<Tag> ( "Database", 1, 0, "TagData" );

    // TODO: Use CabinetData in the same way as TagData.
    //qmlRegisterType<Cabinet> ( "Database", 1, 0, "CabinetData" );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::simulateLogin()
///
/// \brief  Simulates logging in. The database isn't queried and a simulated
///         user is created.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::simulateLogin()
{
    // Success.
    User user = User();
    user.m_id = 1;
    user.m_firstName = "Simulated";
    user.m_lastName = "Login";

    setLoggedInUser( user );

    // Create an event message.
    QJsonObject jsonUser;
    jsonUser[DatabaseControlConstants::_userId] = user.m_id;

    QJsonObject jsonEvent;
    jsonEvent[MessageManager::_event] = MessageManager::_loginSuccess;
    jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
    jsonEvent[MessageManager::_data] = jsonUser;

    QJsonDocument jsonDoc( jsonEvent );
    QString event = jsonDoc.toJson( QJsonDocument::Indented );

    // Publish the event message.
    m_messageManager->publish( event );

    emit loginSuccess( user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getLoggedInUser()
///
/// \brief  Gets the currently logged in user.
///
/// \return The currently logged in user.
////////////////////////////////////////////////////////////////////////////////
User DatabaseControl::getLoggedInUser() const
{
    return m_loggedInUser;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getLoggedInUser()
///
/// \brief  Gets the currently authenticated user.
///
/// \return The currently authenticated user.
////////////////////////////////////////////////////////////////////////////////
User DatabaseControl::getAuthenticatedUser() const
{
    return m_authenticatedUser;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getCabinets()
///
/// \brief  Gets a list of cabinets in a system.
///
/// \return The list of cabinets in the system.
////////////////////////////////////////////////////////////////////////////////
QList<Cabinet*> DatabaseControl::getCabinets()
{
    return m_cabinets;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::selectCabinet( int cabinetId )
///
/// \brief  Selects the cabinet with the given ID.
///
/// \param  The ID of the cabinet to select.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::selectCabinet( int cabinetId )
{
    for ( auto i : m_cabinets )
    {
        Cabinet* cabinet = static_cast<Cabinet*> ( i );

        if ( cabinet->m_id == cabinetId )
        {
            m_selectedCabinet = cabinet;

            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getSelectedCabinet()
///
/// \brief  Gets the selected cabinet.
///
/// \return The selected cabinet.
////////////////////////////////////////////////////////////////////////////////
Cabinet* DatabaseControl::getSelectedCabinet()
{
    return m_selectedCabinet;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getRegisteredCabinet()
///
/// \brief  Gets the registered cabinet.
///
/// \return The registered cabinet.
////////////////////////////////////////////////////////////////////////////////
Cabinet* DatabaseControl::getRegisteredCabinet()
{
    return m_registeredCabinet;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getSystem()
///
/// \brief  Gets the registered system.
///
/// \return The registered system.
////////////////////////////////////////////////////////////////////////////////
System DatabaseControl::getSystem()
{
    return m_system;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::createTagFromQuery()
///
/// \brief  Creates a tag from data in the current query record.
///
/// \return The tag that was created from the query record.
////////////////////////////////////////////////////////////////////////////////
Tag* DatabaseControl::createTagFromQuery()
{
    Tag* tag = new Tag( this );

    tag->m_id = m_queryManager->getValue( Tag::_id ).toInt();
    tag->m_cabinetId = m_queryManager->getValue( Tag::_cabinetId ).toInt();
    tag->m_userId = m_queryManager->getValue( Tag::_userId ).toInt();
    tag->m_category = m_queryManager->getValue( Tag::_category ).toInt();
    tag->m_slot = m_queryManager->getValue( Tag::_slot ).toInt();
    tag->m_currentSlot = m_queryManager->getValue( Tag::_currentSlot ).toInt();
    tag->m_fob = m_queryManager->getValue( Tag::_fob ).toInt();
    tag->m_mileage = 12345;                                                         // TODO: Use correct mileage (not in DB).
    tag->m_tagName = m_queryManager->getValue( Tag::_keyName ).toString();
    tag->m_tagDescription = m_queryManager->getValue( Tag::_keyDescription ).toString();
    tag->m_tagSerial = m_queryManager->getValue( Tag::_keySerial ).toString();
    tag->m_dateAdded = m_queryManager->getValue( Tag::_dateAdded ).toString();
    tag->m_startTime = m_queryManager->getValue( Tag::_startTime ).toString();
    tag->m_endTime = m_queryManager->getValue( Tag::_endTime ).toString();
    tag->m_location = "Location TODO";                                              // TODO: Use correct location (not in DB).
    tag->m_site = "Site TODO";                                                      // TODO: Use correct site (not in DB).
    tag->m_isStatic = m_queryManager->getValue( Tag::_isStatic ).toBool();
    tag->m_isOut = m_queryManager->getValue( Tag::_isOut ).toBool();
    tag->m_isWrongSlot = m_queryManager->getValue( Tag::_isWrongSlot ).toBool();
    tag->m_booked = false;                                                          // TODO: Use correct booking (not in DB).
    tag->m_inBasket = false;

    return tag;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::checkUserSystemPermissions( const User& user, bool& access )
///
/// \brief  Checks whether the given user has permission to access the current system.
///
/// \param  The user to check.
/// \param  Indicates whether the given user has system access.
///
/// \return True if the database could be successfully queried, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool DatabaseControl::checkUserSystemPermissions( const User& user, bool& access )
{
    access = false;
    bool success = false;

    // Get the user system access.
    if ( m_queryManager->getUserSystemAccess( user ) )
    {
        if ( m_queryManager->getNext() )
        {
            UserSystemAccess userSystemAccess;

            userSystemAccess.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
            userSystemAccess.m_system = m_queryManager->getValue( UserSystemAccess::_systemId ).toInt();
            userSystemAccess.m_user = m_queryManager->getValue( UserSystemAccess::_userId ).toInt();
            userSystemAccess.m_isAccessGranted = m_queryManager->getValue( UserSystemAccess::_isAccessGranted ).toBool();

            access = userSystemAccess.m_isAccessGranted;
            success = true;
        }
    }

    // If that fails, get the user group system access.
    if ( !success && m_queryManager->getUsersGroup( user ) )
    {
        if ( m_queryManager->getNext() )
        {
            int groupId = m_queryManager->getValue( UserGroup::_userGroup ).toInt();

            if ( m_queryManager->getUserGroup( groupId ) )
            {
                if ( m_queryManager->getNext() )
                {
                    UserGroup group;
                    group.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
                    group.m_groupName = m_queryManager->getValue( UserGroup::_groupName ).toString();
                    group.m_keyLimit = m_queryManager->getValue( UserGroup::_keyLimit ).toInt();

                    // Check system access for the user group.
                    if ( m_queryManager->getUserGroupSystemAccess( group ) )
                    {
                        if ( m_queryManager->getNext() )
                        {
                            GroupSystemAccess groupSystemAccess;

                            groupSystemAccess.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
                            groupSystemAccess.m_system = m_queryManager->getValue( GroupSystemAccess::_systemId ).toInt();
                            groupSystemAccess.m_group = m_queryManager->getValue( GroupSystemAccess::_userGroup ).toInt();
                            groupSystemAccess.m_isAccessGranted = m_queryManager->getValue( GroupSystemAccess::_isAccessGranted ).toBool();

                            access = groupSystemAccess.m_isAccessGranted;
                            success = true;
                        }
                    }
                }
            }
        }
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::checkUserCabinetPermissions( const User& user, bool& access )
///
/// \brief  Checks whether the given user has permission to access the current cabinet.
///
/// \param  The user to check.
/// \param  Indicates whether the given user has cabinet access.
///
/// \return True if the database could be successfully queried, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool DatabaseControl::checkUserCabinetPermissions( const User& user, bool& access )
{
    access = false;
    bool success = false;

    // Get the user cabinet permissions.
    if ( m_queryManager->getUserCabinetAccess( user ) )
    {
        if ( m_queryManager->getNext() )
        {
            UserCabinetAccess userCabinetAccess;

            userCabinetAccess.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
            userCabinetAccess.m_isAccessGranted = m_queryManager->getValue( UserCabinetAccess::_isAccessGranted ).toBool();

            access = userCabinetAccess.m_isAccessGranted;
            success = true;
        }
    }

    // If that fails, get the user group cabinet permissions.
    if ( !success && m_queryManager->getUsersGroup( user ) )
    {
        if ( m_queryManager->getNext() )
        {
            int groupId = m_queryManager->getValue( UserGroup::_userGroup ).toInt();

            if ( m_queryManager->getUserGroup( groupId ) )
            {
                if ( m_queryManager->getNext() )
                {
                    UserGroup group;
                    group.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
                    group.m_groupName = m_queryManager->getValue( UserGroup::_groupName ).toString();
                    group.m_keyLimit = m_queryManager->getValue( UserGroup::_keyLimit ).toInt();

                     // Check cabinet access for the user group.
                    if ( m_queryManager->getUserGroupCabinetAccess( group ) )
                    {
                        if ( m_queryManager->getNext() )
                        {
                            GroupCabinetAccess groupCabinetAccess;

                            groupCabinetAccess.m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
                            groupCabinetAccess.m_isAccessGranted = m_queryManager->getValue( GroupCabinetAccess::_isAccessGranted ).toBool();

                            access = groupCabinetAccess.m_isAccessGranted;
                            success = true;
                        }
                    }
                }
            }
        }
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::checkUserTagPermissions( const User& user, const int tagId, bool& access )
///
/// \brief  Checks whether the given user has permission to access the given tag / key.
///
/// \param  The user to check.
/// \param  The tag to check.
/// \param  Indicates whether the given user has cabinet access.
///
/// \return True if the database could be successfully queried, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool DatabaseControl::checkUserTagPermissions( const User& user, const int tagId, bool& access )
{
    Q_UNUSED( user );
    Q_UNUSED( tagId );
    Q_UNUSED( access );

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleHelloWorldPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a hello world command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleHelloWorldPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_systemName ) &&
                     data.contains( DatabaseControlConstants::_welcomeText ) &&
                     data.contains( DatabaseControlConstants::_systemTimezone ) &&
                     data.contains( DatabaseControlConstants::_registeredDate ) &&
                     data.contains( DatabaseControlConstants::_cardAuthentication ) &&
                     data.contains( DatabaseControlConstants::_biometricAuthentication ) &&
                     data.contains( DatabaseControlConstants::_compoundModule ) &&
                     data.contains( DatabaseControlConstants::_valetModule ) );

    if ( success )
    {
        // Create a system from the data.
        System system;
        system.m_name = data[DatabaseControlConstants::_systemName].toString();
        system.m_welcomeText = data[DatabaseControlConstants::_welcomeText].toString();
        system.m_timezone = data[DatabaseControlConstants::_systemTimezone].toString();
        system.m_registeredDate = data[DatabaseControlConstants::_registeredDate].toString();
        system.m_cardAuthentication = data[DatabaseControlConstants::_cardAuthentication].toBool();
        system.m_biometricAuthentication = data[DatabaseControlConstants::_biometricAuthentication].toBool();
        system.m_compound = data[DatabaseControlConstants::_compoundModule].toBool();
        system.m_valet = data[DatabaseControlConstants::_valetModule].toBool();

        // Add system to the database.
        success = m_queryManager->addSystem( system );
    }

    sendCommandResultMessage( MessageManager::_helloWorld, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleNewUserPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a new user command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleNewUserPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_username ) &&
                     data.contains( DatabaseControlConstants::_password ) &&
                     data.contains( DatabaseControlConstants::_firstName ) &&
                     data.contains( DatabaseControlConstants::_lastName ) &&
                     data.contains( DatabaseControlConstants::_isVisitor ) &&
                     data.contains( DatabaseControlConstants::_profileImage ) &&
                     data.contains( DatabaseControlConstants::_phoneNumber ) &&
                     data.contains( DatabaseControlConstants::_isActive ) &&
                     data.contains( DatabaseControlConstants::_activeFrom ) &&
                     data.contains( DatabaseControlConstants::_keyLimit ) &&
                     data.contains( DatabaseControlConstants::_isTemp ) &&
                     data.contains( DatabaseControlConstants::_isFirstLogin ) &&
                     data.contains( DatabaseControlConstants::_userProfile ) );

    if ( success )
    {
        // Create a user from the data.
        User user;
        user.m_keyLimit = data[DatabaseControlConstants::_keyLimit].toInt();
        user.m_roleId = data[DatabaseControlConstants::_userProfile].toInt();
        user.m_firstName = data[DatabaseControlConstants::_firstName].toString();
        user.m_lastName = data[DatabaseControlConstants::_lastName].toString();
        user.m_username = data[DatabaseControlConstants::_username].toString();
        user.m_password = data[DatabaseControlConstants::_password].toString();
        user.m_activeFrom = data[DatabaseControlConstants::_activeFrom].toString();
        user.m_profileImage = data[DatabaseControlConstants::_profileImage].toString();
        user.m_phoneNumber = data[DatabaseControlConstants::_phoneNumber].toString();
        user.m_isVisitor = data[DatabaseControlConstants::_isVisitor].toBool();
        user.m_isActive = data[DatabaseControlConstants::_isActive].toBool();
        user.m_isTemp = data[DatabaseControlConstants::_isTemp].toBool();
        user.m_isFirstLogin = data[DatabaseControlConstants::_isFirstLogin].toBool();

        // Add system to the database.
        success = m_queryManager->addUser( user );
    }

    sendCommandResultMessage( MessageManager::_newUser, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleUpdateUserPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an update user command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateUserPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     data.contains( DatabaseControlConstants::_username ) &&
                     data.contains( DatabaseControlConstants::_password ) &&
                     data.contains( DatabaseControlConstants::_firstName ) &&
                     data.contains( DatabaseControlConstants::_lastName ) &&
                     data.contains( DatabaseControlConstants::_isVisitor ) &&
                     data.contains( DatabaseControlConstants::_profileImage ) &&
                     data.contains( DatabaseControlConstants::_phoneNumber ) &&
                     data.contains( DatabaseControlConstants::_isActive ) &&
                     data.contains( DatabaseControlConstants::_activeFrom ) &&
                     data.contains( DatabaseControlConstants::_keyLimit ) &&
                     data.contains( DatabaseControlConstants::_isTemp ) &&
                     data.contains( DatabaseControlConstants::_isFirstLogin ) &&
                     data.contains( DatabaseControlConstants::_userProfile ) );

    if ( success )
    {
        // Create a user from the data.
        User user;
        user.m_id = data[DatabaseControlConstants::_id].toInt();
        user.m_keyLimit = data[DatabaseControlConstants::_keyLimit].toInt();
        user.m_roleId = data[DatabaseControlConstants::_userProfile].toInt();
        user.m_firstName = data[DatabaseControlConstants::_firstName].toString();
        user.m_lastName = data[DatabaseControlConstants::_lastName].toString();
        user.m_username = data[DatabaseControlConstants::_username].toString();
        user.m_password = data[DatabaseControlConstants::_password].toString();
        user.m_activeFrom = data[DatabaseControlConstants::_activeFrom].toString();
        user.m_profileImage = data[DatabaseControlConstants::_profileImage].toString();
        user.m_phoneNumber = data[DatabaseControlConstants::_phoneNumber].toString();
        user.m_isVisitor = data[DatabaseControlConstants::_isVisitor].toBool();
        user.m_isActive = data[DatabaseControlConstants::_isActive].toBool();
        user.m_isTemp = data[DatabaseControlConstants::_isTemp].toBool();
        user.m_isFirstLogin = data[DatabaseControlConstants::_isFirstLogin].toBool();

        // Add system to the database.
        success = m_queryManager->updateUser( user );
    }

    sendCommandResultMessage( MessageManager::_updateUser, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveUserPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove user command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveUserPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();
    QJsonObject trailingData = json[MessageManager::_trailingDataToRemove].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     trailingData.contains( MessageManager::_info ) &&
                     trailingData.contains( MessageManager::_tables ) );

    if ( success )
    {
        // Create a user from the data.
        User user;
        user.m_id = data[DatabaseControlConstants::_id].toInt();

        QJsonArray tables = trailingData[MessageManager::_tables].toArray();

        for ( auto table : tables)
        {
            success &= m_queryManager->removeUser( user, table.toString() );
        }

        success &= m_queryManager->removeUser( user, DatabaseControlConstants::_usersTable );
    }

    sendCommandResultMessage( MessageManager::_removeUser , success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleAddUserGroup( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an add user group command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleAddUserGroup( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( UserGroup::_groupName ) &&
                     data.contains( UserGroup::_keyLimit ) );

    if ( success )
    {
        // Create a user group from the data.
        UserGroup userGroup;
        userGroup.m_groupName = data[UserGroup::_groupName].toString();
        userGroup.m_keyLimit = data[UserGroup::_keyLimit].toInt();

        // Add system to the database.
        success = m_queryManager->addUserGroup( userGroup );
    }

    sendCommandResultMessage( MessageManager::_newUserGroup, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleAddUserGroup( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an add user group command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateUserGroup( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     data.contains( UserGroup::_groupName ) &&
                     data.contains( UserGroup::_keyLimit ) );

    if ( success )
    {
        // Create a user group from the data.
        UserGroup userGroup;
        userGroup.m_id = data[DatabaseControlConstants::_id].toInt();
        userGroup.m_groupName = data[UserGroup::_groupName].toString();
        userGroup.m_keyLimit = data[UserGroup::_keyLimit].toInt();

        // Add system to the database.
        success = m_queryManager->updateUserGroup( userGroup );
    }

    sendCommandResultMessage( MessageManager::_updateUserGroup, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveUserGroup( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove user group command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveUserGroup( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();
    QJsonObject trailingData = json[MessageManager::_trailingDataToRemove].toObject();

    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     trailingData.contains( MessageManager::_info ) &&
                     trailingData.contains( MessageManager::_tables ) );

    if ( success )
    {
        // Create a user group from the data.
        UserGroup userGroup;
        userGroup.m_id = data[DatabaseControlConstants::_id].toInt();

        QJsonArray tables = trailingData[MessageManager::_tables].toArray();

        for ( auto table : tables)
        {
            success &= m_queryManager->removeUserGroup( userGroup, table.toString() );
        }

        success &= m_queryManager->removeUserGroup( userGroup, UserGroup::_userGroupTable );
    }

    sendCommandResultMessage( MessageManager::_removeUserGroup, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleAddUserToGroup( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an add user to user group command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleAddUserToGroup( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( UserGroup::_userId ) &&
                     data.contains( UserGroup::_userGroup ) );

    if ( success )
    {
        // Create the user and user group IDs from the data.
        int userId = data[UserGroup::_userId].toInt();
        int userGroupId = data[UserGroup::_userGroup].toInt();

        // Add system to the database.
        success = m_queryManager->addUserToUserGroup( userId, userGroupId );
    }

    sendCommandResultMessage( MessageManager::_newUserToGroup, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleAddUserToGroup( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove user from user group command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveUserFromGroup( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    bool success = ( data.contains( UserGroup::_userId ) &&
                     data.contains( UserGroup::_userGroup ) );

    if ( success )
    {
        // Create the user and user group IDs from the data.
        int userId = data[UserGroup::_userId].toInt();
        int userGroupId = data[UserGroup::_userGroup].toInt();

        // Add system to the database.
        success = m_queryManager->removeUserFromUserGroup( userId, userGroupId );
    }

    sendCommandResultMessage( MessageManager::_removeUserFromGroup, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleNewKeyPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a new key command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleNewKeyPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( Tag::_cabinetId ) &&
                     data.contains( Tag::_category ) &&
                     data.contains( Tag::_slot ) &&
                     data.contains( Tag::_currentSlot ) &&
                     data.contains( Tag::_keyName ) &&
                     data.contains( Tag::_keyDescription ) &&
                     data.contains( Tag::_keySerial ) &&
                     data.contains( Tag::_fob ) &&
                     data.contains( Tag::_dateAdded ) &&
                     data.contains( Tag::_startTime ) &&
                     data.contains( Tag::_endTime ) &&
                     data.contains( Tag::_isStatic ) &&
                     data.contains( Tag::_isOut ) &&
                     data.contains( Tag::_isWrongSlot ) );

    if ( success )
    {
//        static int count = 1;

        // Create a tag from the data. TODO: Re-factor into helper function.
        Tag* tag = new Tag( this );
        tag->m_cabinetId =      data[Tag::_cabinetId].toInt();
        tag->m_category =       data[Tag::_category].toInt();
//        tag->m_slot =           count;
//        tag->m_currentSlot =    count;
        tag->m_slot =           data[Tag::_slot].toInt();
        tag->m_currentSlot =    data[Tag::_currentSlot].toInt();
        tag->m_fob =            data[Tag::_fob].toInt();
//        tag->m_tagName =        data[Tag::_keyName].toString().append( QString("_%1").arg(count));
//        tag->m_tagDescription = data[Tag::_keyDescription].toString().append( QString("_%1").arg(count));
//        tag->m_tagSerial =      data[Tag::_keySerial].toString().append( QString("_%1").arg(count));
        tag->m_tagName =        data[Tag::_keyName].toString();
        tag->m_tagDescription = data[Tag::_keyDescription].toString();
        tag->m_tagSerial =      data[Tag::_keySerial].toString();
        tag->m_dateAdded =      data[Tag::_dateAdded].toString();
        tag->m_startTime =      data[Tag::_startTime].toString();
        tag->m_endTime =        data[Tag::_endTime].toString();
        tag->m_isStatic =       data[Tag::_isStatic].toBool();
        tag->m_isOut =          data[Tag::_isOut].toBool();
        tag->m_isWrongSlot =    data[Tag::_isWrongSlot].toBool();

//        count++;

        // Add system to the database.
        success = m_queryManager->addTag( tag );
    }

    sendCommandResultMessage( MessageManager::_newKey, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveKeyPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove key command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveKeyPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();
    QJsonObject trailingData = json[MessageManager::_trailingDataToRemove].toObject();

    bool success = ( data.contains( Tag::_fob ) &&
                     trailingData.contains( MessageManager::_info ) &&
                     trailingData.contains( MessageManager::_tables ) );

    if ( success )
    {
        // Create a tag from the data.
        Tag* tag = new Tag( this );
        tag->m_fob = data[Tag::_fob].toInt();

        // First find the tag with the given fob ID.
        success &= m_queryManager->getTags( Tag::_fob, tag->m_fob );
        success &= m_queryManager->getNext();

        if ( success )
        {
            tag->m_id = m_queryManager->getValue( Tag::_id ).toUInt();

            QJsonArray tables = trailingData[MessageManager::_tables].toArray();

            for ( auto table : tables)
            {
                success &= m_queryManager->removeTag( tag, table.toString() );
            }

            success &= m_queryManager->removeTag( tag, Tag::_keysTable );
        }
    }

    sendCommandResultMessage( MessageManager::_removeKey, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleUpdateKeyPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an update key command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateKeyPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( Tag::_cabinetId ) &&
                     data.contains( Tag::_category ) &&
                     data.contains( Tag::_slot ) &&
                     data.contains( Tag::_currentSlot ) &&
                     data.contains( Tag::_keyName ) &&
                     data.contains( Tag::_keyDescription ) &&
                     data.contains( Tag::_keySerial ) &&
                     data.contains( Tag::_fob ) &&
                     data.contains( Tag::_dateAdded ) &&
                     data.contains( Tag::_startTime ) &&
                     data.contains( Tag::_endTime ) &&
                     data.contains( Tag::_isStatic ) &&
                     data.contains( Tag::_isOut ) &&
                     data.contains( Tag::_isWrongSlot ) );

    if ( success )
    {
        // Create a tag from the data. TODO: Re-factor into helper function.
        Tag* tag = new Tag( this );
        tag->m_cabinetId =      data[Tag::_cabinetId].toInt();
        tag->m_category =       data[Tag::_category].toInt();
        tag->m_slot =           data[Tag::_slot].toInt();
        tag->m_currentSlot =    data[Tag::_currentSlot].toInt();
        tag->m_fob =            data[Tag::_fob].toInt();
        tag->m_tagName =        data[Tag::_keyName].toString();
        tag->m_tagDescription = data[Tag::_keyDescription].toString();
        tag->m_tagSerial =      data[Tag::_keySerial].toString();
        tag->m_dateAdded =      data[Tag::_dateAdded].toString();
        tag->m_startTime =      data[Tag::_startTime].toString();
        tag->m_endTime =        data[Tag::_endTime].toString();
        tag->m_isStatic =       data[Tag::_isStatic].toBool();
        tag->m_isOut =          data[Tag::_isOut].toBool();
        tag->m_isWrongSlot =    data[Tag::_isWrongSlot].toBool();

        // First find the tag / key with the given fob ID.
        success &= m_queryManager->getTags( Tag::_fob, tag->m_fob );
        success &= m_queryManager->getNext();

        if ( success )
        {
            tag->m_id = m_queryManager->getValue( Tag::_id ).toUInt();

            connect( m_queryManager, &QueryManager::updateTagComplete, this, &DatabaseControl::onUpdateTagComplete, static_cast<Qt::ConnectionType> ( m_queryManager->m_connectionType | Qt::UniqueConnection ) );

            emit updateTag( tag );
        }
    }

    if ( !success )
    {
        sendCommandResultMessage( MessageManager::_updateKey, success );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleNewKeyCategoryPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a new key category command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleNewKeyCategoryPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( TagCategory::_tagCategoryType ) &&
                     data.contains( TagCategory::_tagCategoryIcon ) &&
                     data.contains( TagCategory::_tagCategoryName ) &&
                     data.contains( TagCategory::_tagCategoryShortName ) );

    if ( success )
    {
        // Create a tag from the data. TODO: Re-factor into helper function.
        TagCategory* tagCategory = new TagCategory();
        tagCategory->m_tagCategoryType =      data[TagCategory::_tagCategoryType].toString();
        tagCategory->m_tagCategoryIcon =      data[TagCategory::_tagCategoryIcon].toString();
        tagCategory->m_tagCategoryName =      data[TagCategory::_tagCategoryName].toString();
        tagCategory->m_tagCategoryShortName = data[TagCategory::_tagCategoryShortName].toString();

        // Add system to the database.
        emit addTagCategory( tagCategory );
    }

    sendCommandResultMessage( TagCategory::_newKeyCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleUpdateKeyCategoryPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an update key category command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateKeyCategoryPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( TagCategory::_tagCategoryId ) &&
                     data.contains( TagCategory::_tagCategoryType ) &&
                     data.contains( TagCategory::_tagCategoryIcon ) &&
                     data.contains( TagCategory::_tagCategoryName ) &&
                     data.contains( TagCategory::_tagCategoryShortName ) );

    if ( success )
    {
        // Create a tag from the data. TODO: Re-factor into helper function.
        TagCategory* tagCategory = new TagCategory();
        tagCategory->m_tagCategoryId =        data[TagCategory::_tagCategoryId].toInt();
        tagCategory->m_tagCategoryType =      data[TagCategory::_tagCategoryType].toString();
        tagCategory->m_tagCategoryIcon =      data[TagCategory::_tagCategoryIcon].toString();
        tagCategory->m_tagCategoryName =      data[TagCategory::_tagCategoryName].toString();
        tagCategory->m_tagCategoryShortName = data[TagCategory::_tagCategoryShortName].toString();

        // Add system to the database.
        emit updateTagCategory( tagCategory );
    }

    sendCommandResultMessage( TagCategory::_updateKeyCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveKeyCategoryPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove key category command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveKeyCategoryPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();
    QJsonObject trailingData = json[MessageManager::_trailingDataToRemove].toObject();

    bool success = ( data.contains( TagCategory::_tagCategoryId ) &&
                     trailingData.contains( MessageManager::_info ) &&
                     trailingData.contains( MessageManager::_tables ) );

    if ( success )
    {
        // Create a tag category from the data.
        TagCategory* tagCategory = new TagCategory();
        tagCategory->m_tagCategoryId = data[TagCategory::_tagCategoryId].toInt();

        QJsonArray tables = trailingData[MessageManager::_tables].toArray();

        for ( auto table : tables)
        {
            emit removeTagCategory( tagCategory, table.toString() );
        }

        emit removeTagCategory( tagCategory, TagCategory::_tagCategoryTable );
    }

    sendCommandResultMessage( TagCategory::_removeKeyCategory, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleNewKeyUserPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a user user key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleNewKeyUserPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( UserTagAccess::_userId ) &&
                     data.contains( UserTagAccess::_keyId ) &&
                     data.contains( UserTagAccess::_isAccessGranted ) );

    if ( success )
    {
        // Create user tag / key access from the data. TODO: Re-factor into helper function.
        UserTagAccess userTagAccess;
        userTagAccess.m_userId = data[UserTagAccess::_userId].toInt();
        userTagAccess.m_keyId = data[UserTagAccess::_keyId].toInt();
        userTagAccess.m_isAccessGranted = data[UserTagAccess::_isAccessGranted].toBool();

        success &= m_queryManager->addTagUserPermissions( userTagAccess );
    }

    sendCommandResultMessage( MessageManager::_newKeyUserPermission, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveKeyUserPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove user key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveKeyUserPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = data.contains( DatabaseControlConstants::_id );

    if ( success )
    {
        // Create user tag / key access from the data. TODO: Re-factor into helper function.
        UserTagAccess userTagAccess;
        userTagAccess.m_id = data[DatabaseControlConstants::_id].toInt();

        success &= m_queryManager->removeTagUserPermissions( userTagAccess );
        success &= ( m_queryManager->getNumRowsAffected() > 0 );
    }

    sendCommandResultMessage( MessageManager::_removeKeyUserPermission, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleUpdateKeyUserPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an update user key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateKeyUserPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     data.contains( UserTagAccess::_userId ) &&
                     data.contains( UserTagAccess::_keyId ) &&
                     data.contains( UserTagAccess::_isAccessGranted ) );

    if ( success )
    {
        // Create user tag / key access from the data. TODO: Re-factor into helper function.
        UserTagAccess userTagAccess;
        userTagAccess.m_id = data[DatabaseControlConstants::_id].toInt();
        userTagAccess.m_userId = data[UserTagAccess::_userId].toInt();
        userTagAccess.m_keyId = data[UserTagAccess::_keyId].toInt();
        userTagAccess.m_isAccessGranted = data[UserTagAccess::_isAccessGranted].toBool();

        success &= m_queryManager->updateTagUserPermissions( userTagAccess );
    }

    sendCommandResultMessage( MessageManager::_updateKeyUserPermission , success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleNewKeyGroupPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a new user group key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleNewKeyGroupPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( GroupTagAccess::_userGroupId ) &&
                     data.contains( GroupTagAccess::_keyId ) &&
                     data.contains( GroupTagAccess::_isAccessGranted ) );

    if ( success )
    {
        // Create user group tag / key access from the data. TODO: Re-factor into helper function.
        GroupTagAccess userGroupTagAccess;
        userGroupTagAccess.m_userGroupId = data[GroupTagAccess::_userGroupId].toInt();
        userGroupTagAccess.m_keyId = data[GroupTagAccess::_keyId].toInt();
        userGroupTagAccess.m_isAccessGranted = data[GroupTagAccess::_isAccessGranted].toBool();

        success &= m_queryManager->addTagGroupPermissions( userGroupTagAccess );
    }

    sendCommandResultMessage( MessageManager::_newKeyGroupPermission, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleRemoveKeyGroupPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a remove user group key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleRemoveKeyGroupPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = data.contains( DatabaseControlConstants::_id );

    if ( success )
    {
        // Create user group tag / key access from the data. TODO: Re-factor into helper function.
        GroupTagAccess userGroupTagAccess;
        userGroupTagAccess.m_id = data[DatabaseControlConstants::_id].toInt();

        success &= m_queryManager->removeTagGroupPermissions( userGroupTagAccess );
        success &= ( m_queryManager->getNumRowsAffected() > 0 );
    }

    sendCommandResultMessage( MessageManager::_removeKeyGroupPermission, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleUpdateKeyGroupPermissionsPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process an update user group key permissions command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleUpdateKeyGroupPermissionsPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( DatabaseControlConstants::_id ) &&
                     data.contains( GroupTagAccess::_userGroupId ) &&
                     data.contains( GroupTagAccess::_keyId ) &&
                     data.contains( GroupTagAccess::_isAccessGranted ) );

    if ( success )
    {
        // Create user tag / key access from the data. TODO: Re-factor into helper function.
        GroupTagAccess userGroupTagAccess;
        userGroupTagAccess.m_id = data[DatabaseControlConstants::_id].toInt();
        userGroupTagAccess.m_userGroupId = data[GroupTagAccess::_userGroupId].toInt();
        userGroupTagAccess.m_keyId = data[GroupTagAccess::_keyId].toInt();
        userGroupTagAccess.m_isAccessGranted = data[GroupTagAccess::_isAccessGranted].toBool();

        success &= m_queryManager->updateTagGroupPermissions( userGroupTagAccess );
    }

    sendCommandResultMessage( MessageManager::_updateKeyGroupPermission, success );
}


////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::handleResetUserPinPayload( QJsonObject& json )
///
/// \brief  Handles the JSON payload to process a reset PIN command.
///
/// \param  The JSON payload.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::handleResetUserPinPayload( QJsonObject& json )
{
    QJsonObject data = json[MessageManager::_data].toObject();

    // TODO: Re-factor into helper function.
    bool success = ( data.contains( UserEntry::_userId ) &&
                     data.contains( UserEntry::_entryValue ) );

    // NOTE: UserEntry::_entryType in the payload is actually redundant since the
    // command implies it is the PIN in 'tbl_user_entry_ids' that is being updated.

    if ( success )
    {
        // Create user and PIN from the payload. TODO: Re-factor into helper function.
        User user;
        user.m_id = data[UserEntry::_userId].toInt();

        QString pin = data[UserEntry::_entryValue].toString();

        success &= m_queryManager->updatePin( pin, user );
    }

    sendCommandResultMessage( MessageManager::_resetUserPin, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::sendCommandResultMessage( QJsonObject& json, bool success )
///
/// \brief  Sends the command result message.
///
/// \param  The command that was completed.
/// \param  Indicates whether the command was completed successfully.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::sendCommandResultMessage( const char* command, bool success )
{
    // Create a reply message.
    QJsonObject jsonReply;
    jsonReply[MessageManager::_reply] = command;
    jsonReply[MessageManager::_result] = ( success ? MessageManager::_success
                                                   : MessageManager::_fail );

    QJsonDocument jsonDoc( jsonReply );
    QString reply = jsonDoc.toJson( QJsonDocument::Indented );

    // Publish the reply message.
    m_messageManager->publish( reply );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::queryTagsForUser( const int userId )
///
/// \brief  Queries the database to update the tags (i.e. keys) in the system.
///
/// \param  ID of the user to get keys for.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::queryTagsForUser( const int userId )
{
    qDebug() << QString( "[DatabaseControl::queryTagsForUser] User ID: %1" ).arg( userId );

    m_userTags.clear();

    // TODO: Modify query to filter out unwanted results, possibly do this in a new function.
    if ( m_queryManager->getTags( Tag::_userId, userId ) )
    {
        while( m_queryManager->getNext() )
        {
            Tag* tag = createTagFromQuery();

            m_userTags.append( tag );
        }
    }
    else
    {

    }

    emit userTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::queryBookedTags()
///
/// \brief  Queries the database to update the tags (i.e. keys) available for booking.
///
/// \param  ID of the user to get keys for.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::queryBookedTags( const int userId )
{
    qDebug() << "[DatabaseControl::queryBookedTags]";

    openDatabase();

    m_bookedTags.clear();

    // TODO: Modify query to filter out unwanted results, possibly do this in a new function.
    if ( m_queryManager->getTags( Tag::_userId, userId ) )
    {
        while( m_queryManager->getNext() )
        {
            Tag* tag = createTagFromQuery();

            // Only add booked tags.
            if ( tag->m_booked )
            {
                m_bookedTags.append( tag );
            }
        }
    }
    else
    {

    }

    emit bookedTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::addTagToBasket( Tag* tag )
///
/// \brief  Adds a tag to the basket.
///
/// \param  The tag to be added.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::addTagToBasket( Tag* tag )
{
    m_basketTags.append( tag );

    emit basketTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::removeTagFromBasket( Tag* tag )
///
/// \brief  Removes a tag from the basket.
///
/// \param  The tag to be removed.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::removeTagFromBasket( Tag* tag )
{
    if ( m_basketTags.removeAll( tag ) )
    {
        emit basketTagsChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::clearBasketTags()
///
/// \brief  Clears all the tags / keys in the basket.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::clearBasketTags()
{
    m_basketTags.clear();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::addTagToReturning( Tag* tag )
///
/// \brief  Adds a tag to the returning list.
///
/// \param  The tag to be added.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::addTagToReturning( Tag* tag )
{
    m_returningTags.append( tag );

    emit returningTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::removeTagFromReturning( Tag* tag )
///
/// \brief  Removes a tag from the returning list.
///
/// \param  The tag to be removed.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::removeTagFromReturning( Tag* tag )
{
    if ( m_returningTags.removeAll( tag ) )
    {
        emit returningTagsChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getCabinetTags()
///
/// \brief  Gets the tags (i.e. keys) in the selected cabinet.
///
/// \return The tags (i.e. keys) in the selected cabinet. This is a 2D list
///         as the data is returned in pages.
////////////////////////////////////////////////////////////////////////////////
QList<QList<Tag*>> DatabaseControl::getCabinetTags()
{
    return m_cabinetTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getCategoryTags()
///
/// \brief  Gets the tags / keys grouped by tag / key category.
///
/// \return The tags (i.e. keys) grouped by tag / key category.
///         This is a 2D list as the data is returned in groups.
////////////////////////////////////////////////////////////////////////////////
QList<QList<Tag*> > DatabaseControl::getCategoryTags()
{
    return m_categoryTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getUserTags()
///
/// \brief  Gets the tags (i.e. keys) checked out by the user.
///
/// \return The tags (i.e. keys) checked out by the user.
////////////////////////////////////////////////////////////////////////////////
QList<Tag*> DatabaseControl::getUserTags()
{
    return m_userTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getBookedTags()
///
/// \brief  Gets the tags (i.e. keys) available for booking.
///
/// \return The tags (i.e. keys) available for booking.
////////////////////////////////////////////////////////////////////////////////
QList<Tag*> DatabaseControl::getBookedTags()
{
    return m_bookedTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getSearchTags()
///
/// \brief  Gets the tags (i.e. keys) found during the search.
///
/// \return The tags (i.e. keys) found during the search.
////////////////////////////////////////////////////////////////////////////////
QList<Tag*> DatabaseControl::getSearchTags()
{
    return m_searchTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getBasketTags()
///
/// \brief  Gets the tags (i.e. keys) in the basket ready to be checked out.
///
/// \return The tags (i.e. keys) in the basket ready to be checked out.
////////////////////////////////////////////////////////////////////////////////
QList<Tag*> DatabaseControl::getBasketTags()
{
    return m_basketTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getReturningTags()
///
/// \brief  Gets the tags / keys selected to be returned.
///
/// \return The tags / keys selected to be returned.
////////////////////////////////////////////////////////////////////////////////
QList<Tag*> DatabaseControl::getReturningTags()
{
    return m_returningTags;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::completeCheckoutBasket()
///
/// \brief  Complete the checkout tags / keys from the basket process by writing
///         the changes to the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::completeCheckoutBasket()
{
    QMutableListIterator<Tag*> i( m_basketTags );

    while ( i.hasNext() )
    {
        Tag* tag = i.next();
        tag->m_isOut = true;
        tag->m_userId = m_loggedInUser.m_id;

        connect( m_queryManager, &QueryManager::updateTagComplete, this, &DatabaseControl::onUpdateTagEventComplete, static_cast<Qt::ConnectionType> ( m_queryManager->m_connectionType | Qt::UniqueConnection ) );

        emit updateTag( tag );
    }

    m_basketTags.clear();
    emit basketTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::completeReturnTags()
///
/// \brief  Complete the return tags / keys process by writing the changes to the
///         database.
///
/// \return The tags / keys selected to be returned.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::completeReturnTags()
{
    QMutableListIterator<Tag*> i( m_returningTags );

    while ( i.hasNext() )
    {
        Tag* tag = i.next();
        tag->m_isOut = false;
        tag->m_userId = -1;

        connect( m_queryManager, &QueryManager::updateTagComplete, this, &DatabaseControl::onUpdateTagEventComplete, static_cast<Qt::ConnectionType> ( m_queryManager->m_connectionType | Qt::UniqueConnection ) );

        emit updateTag( tag );
    }

    m_returningTags.clear();
    emit returningTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::performSqlQuery( const QString& sql )
///
/// \brief  Performs the given SQL query on the local database.
///
/// \param  The SQL query to execute.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::performSqlQuery( const QString& sql )
{
    openDatabase();

    if ( m_queryManager->performSqlQuery( sql ) )
    {
        emit sqlSuccess();
    }
    else
    {
        emit sqlFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::saveAll()
///
/// \brief  Saves all changes for the current logged in user to the e local database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::saveAll()
{
    openDatabase();

    bool updatePinSuccess = m_queryManager->updatePin( m_savedPin, m_loggedInUser );
    bool resetTempPinSuccess = m_queryManager->resetTempPin( m_loggedInUser );
    bool addFingerprintSuccess = m_queryManager->addFingerprint( m_loggedInUser, m_savedFingerprintId );
    bool addCardSuccess = m_queryManager->addCard( m_loggedInUser, m_savedCardId );

    if ( updatePinSuccess &&
         resetTempPinSuccess &&
         addFingerprintSuccess &&
         addCardSuccess )
    {
        emit saveAllSuccess();
    }
    else
    {
        emit saveAllFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getTagGroupSize()
///
/// \brief  Gets the tags / keys group size. This is used to determine the size
///         of the groups that the tags / keys data will be split into.
///         For example, how many tags / keys will be shown on screen at once.
///
/// \return The tags / keys group size.
////////////////////////////////////////////////////////////////////////////////
unsigned int DatabaseControl::getTagGroupSize() const
{
    return m_tagGroupSize;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getTagGroupSize()
///
/// \brief  Gets the tags / keys group size.
///
/// \return The tags / keys group size.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::setTagGroupSize( unsigned int tagGroupSize )
{
    if ( m_tagGroupSize != tagGroupSize )
    {
        m_tagGroupSize = tagGroupSize;

        emit tagGroupSizeChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::getTagCategories()
///
/// \brief  Gets the tag / key categories.
///
/// \return The list of tag / key categories.
////////////////////////////////////////////////////////////////////////////////
QList<TagCategory*> DatabaseControl::getTagCategories()
{
    return m_tagCategories;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::instance()
///
/// \brief  Returns a static instance of DatabaseControl.
///
/// \return A DatabaseControl instance.
////////////////////////////////////////////////////////////////////////////////
DatabaseControl* DatabaseControl::instance()
{
    if ( !_databaseControl )
    {
        QueryManager* queryManager = new QueryManager();
        MessageManager* messageManager = MessageManager::instance();

        _databaseControl = new DatabaseControl( queryManager, messageManager );
    }

    return _databaseControl;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::instance( DatabaseControl* DatabaseControl, MessageManager* messageManager )
///
/// \brief  Returns a static instance of DatabaseControl with injected managers.
///
/// \param  An injected manager to handle local database queries.
/// \param  An injected manager to handle remote MQTT messages.
///
/// \return A DatabaseControl instance with injected managers.
////////////////////////////////////////////////////////////////////////////////
DatabaseControl* DatabaseControl::instance( QueryManager* queryManager, MessageManager* messageManager )
{
    if ( !_databaseControl )
    {
        _databaseControl = new DatabaseControl( queryManager, messageManager );
    }

    return _databaseControl;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::destroy()
///
/// \brief  Destroys the DatabaseControl instance.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::destroy()
{
    if ( _databaseControl )
    {
        delete _databaseControl;
        _databaseControl = nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onPayloadReceived( QString payload )
///
/// \brief  Handles payload received from the message manager.
///
/// \param  The payload that was received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onPayloadReceived( QString payload )
{
    QJsonObject json;

    if ( MessageManager::convertStringToJson( payload, json ) )
    {
        if      ( json[MessageManager::_command] == MessageManager::_helloWorld )               { handleHelloWorldPayload( json ); }

        // Users.
        else if ( json[MessageManager::_command] == MessageManager::_newUser )                  { handleNewUserPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_updateUser )               { handleUpdateUserPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeUser )               { handleRemoveUserPayload( json ); }

        // User groups.
        else if ( json[MessageManager::_command] == MessageManager::_newUserGroup )             { handleAddUserGroup( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_updateUserGroup )          { handleUpdateUserGroup( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeUserGroup )          { handleRemoveUserGroup( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_newUserToGroup )           { handleAddUserToGroup( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeUserFromGroup )      { handleRemoveUserFromGroup( json ); }

        // Tags / keys.
        else if ( json[MessageManager::_command] == MessageManager::_newKey )                   { handleNewKeyPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeKey )                { handleRemoveKeyPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_updateKey )                { handleUpdateKeyPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_newKeyUserPermission )     { handleNewKeyUserPermissionsPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeKeyUserPermission )  { handleRemoveKeyUserPermissionsPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_updateKeyUserPermission )  { handleUpdateKeyUserPermissionsPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_newKeyGroupPermission )    { handleNewKeyGroupPermissionsPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_removeKeyGroupPermission ) { handleRemoveKeyGroupPermissionsPayload( json ); }
        else if ( json[MessageManager::_command] == MessageManager::_updateKeyGroupPermission ) { handleUpdateKeyGroupPermissionsPayload( json ); }

        // PIN.
        else if ( json[MessageManager::_command] == MessageManager::_resetUserPin )             { handleResetUserPinPayload( json ); }

        // Tag // key categories.
        else if ( json[MessageManager::_command] == TagCategory::_newKeyCategory )    { handleNewKeyCategoryPayload( json ); }
        else if ( json[MessageManager::_command] == TagCategory::_updateKeyCategory ) { handleUpdateKeyCategoryPayload( json ); }
        else if ( json[MessageManager::_command] == TagCategory::_removeKeyCategory ) { handleRemoveKeyCategoryPayload( json ); }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onSearchTagsComplete( bool success )
///
/// \brief  If the search query was successful then the tags / keys are added to the
///         search tags / keys results.
///
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onSearchTagsComplete( bool success )
{
    qDebug() << QString( "[DatabaseControl::onSearchTagsComplete] %1" ).arg( success );

    m_searchTags.clear();

    // TODO: Modify query to filter out unwanted results, possibly do this in a new function.
    if ( success )
    {
        while( m_queryManager->getNext() )
        {
            Tag* tag = createTagFromQuery();

            m_searchTags.append( tag );
        }
    }

    emit searchTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onUpdateTagComplete( Tag* tag,  bool success )
///
/// \brief  Handles an update tag / key query completing that was invoked via
///         a message from the server.
///         The success of the query needs to be sent to the server.
///
/// \param  The tag that was updated.
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onUpdateTagComplete( Tag* tag, bool success )
{
    Q_UNUSED( tag );

    disconnect( m_queryManager, &QueryManager::updateTagComplete, this, &DatabaseControl::onUpdateTagComplete );

    sendCommandResultMessage( MessageManager::_updateKey, success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onUpdateTagComplete( Tag* tag, bool success )
///
/// \brief  Handles an update tag / key query completing that was invoked via
///         a cabinet event.
///         This event needs to be written to the database and sent to the server.
///
/// \param  The tag that was updated.
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onUpdateTagEventComplete( Tag* tag, bool success )
{
    disconnect( m_queryManager, &QueryManager::updateTagComplete, this, &DatabaseControl::onUpdateTagEventComplete );

    if ( success )
    {
        // Create an event message.
        QJsonObject jsonTag;
        jsonTag[Tag::_userId] = tag->m_userId;
        jsonTag[Tag::_cabinetId] = tag->m_cabinetId;
        jsonTag[Tag::_slot] = tag->m_slot;
        jsonTag[Tag::_currentSlot] = tag->m_currentSlot;
        jsonTag[Tag::_fob] = tag->m_fob;
        jsonTag[Tag::_isOut] = tag->m_isOut;
        jsonTag[Tag::_isWrongSlot] = tag->m_isWrongSlot;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] =  tag->m_isOut ? MessageManager::_keyOut : MessageManager::_keyIn;
        jsonEvent[MessageManager::_timestamp] = m_messageManager->getTimestamp();
        jsonEvent[MessageManager::_data] = jsonTag;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        // Publish the event message.
        m_messageManager->publish( event );

        connect( m_queryManager, &QueryManager::eventLogUpdateTagComplete, this, &DatabaseControl::onEventLogUpdateTagComplete, m_queryManager->m_connectionType );

        emit eventLogUpdateTag( tag, m_loggedInUser );
    }

    emit closeDatabase();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onEventLogUpdateTagComplete( Tag* tag, bool success )
///
/// \brief  Handles an event log update tag / key query completing.
///         This event needs to be written to the database and sent to the server.
///
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onEventLogUpdateTagComplete( Tag* tag,  bool success )
{
    Q_UNUSED( tag );
    Q_UNUSED( success );

    disconnect( m_queryManager, &QueryManager::eventLogUpdateTagComplete, this, &DatabaseControl::onEventLogUpdateTagComplete );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onGetTagsForCabinetComplete( QList<QList<Tag*>>* cabinetTags )
///
/// \brief  Updates the cabinet tags / keys when the query has completed.
///
/// \param  The tags / keys in the cabinet grouped in pages.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onGetTagsForCabinetComplete( QList<QList<Tag*>>* cabinetTags )
{
    qDebug() << "[DatabaseControl::onGetTagsForCabinetComplete]";

    m_cabinetTags = *cabinetTags;

    emit cabinetTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onGetCategoryTagsComplete( QList<QList<Tag*>>* categoryTags )
///
/// \brief  Updates the tags / keys when the query has completed.
///
/// \param  The tags / keys grouped by tag / key category.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onGetCategoryTagsComplete( QList<QList<Tag*> >* categoryTags )
{
    qDebug() << "[DatabaseControl::onGetCategoryTagsComplete]";

    m_categoryTags = *categoryTags;

    emit categoryTagsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onAddTagCategoryComplete( TagCategory* tagCategory, bool success )
///
/// \brief  Handles the new tag / key when the query has completed.
///
/// \param  The tag / key that was added.
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onAddTagCategoryComplete( TagCategory* tagCategory, bool success )
{
    Q_UNUSED( tagCategory );

    qDebug() << QString( "[DatabaseControl::onAddTagCategoryComplete] Success: %1" ).arg( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onUpdateTagCategoryComplete( TagCategory* tagCategory, bool success )
///
/// \brief  Handles the updated tag / key when the query has completed.
///
/// \param  The tag / key that was updated.
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onUpdateTagCategoryComplete( TagCategory* tagCategory, bool success )
{
    Q_UNUSED( tagCategory );

    qDebug() << QString( "[DatabaseControl::onUpdateTagCategoryComplete] Success: %1" ).arg( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onRemoveTagCategoryComplete( TagCategory* tagCategory, bool success )
///
/// \brief  Handles the removed tag / key when the query has completed.
///
/// \param  The tag / key that was removed.
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onRemoveTagCategoryComplete( TagCategory* tagCategory, bool success )
{
    Q_UNUSED( tagCategory );

    qDebug() << QString( "[DatabaseControl::onRemoveTagCategoryComplete] Success: %1" ).arg( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onGetTagCategoriesComplete( QList<TagCategory*>* tagCategories )
///
/// \brief  Handles the get tag / key categories when the query has completed.
///
/// \param  The list of tag / key categories.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onGetTagCategoriesComplete( QList<TagCategory*>* tagCategories )
{
    qDebug() << "[DatabaseControl::onGetTagCategoriesComplete]";

    m_tagCategories = *tagCategories;

    emit tagCategoriesChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onInitDatabaseComplete( bool success )
///
/// \brief  If the database initialisation was successful then the relevant
///         data can be initialised also.
///
/// \param  True if the query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onInitDatabaseComplete( bool success )
{
    if ( success )
    {
        // Initialise user profiles.
        initUserProfiles();
        initUserProfilePermissions();

        initSystem();
        initRegisteredCabinet();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::onGetCabinetsComplete( QList<Cabinet*>* cabinets )
///
/// \brief  If the cabinets query was successful then the cabinets need to be
///         added to the list of cabinets.
///
/// \param  The list of cabinets that was returned.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::onGetCabinetsComplete( QList<Cabinet*>* cabinets )
{
    qDebug() << QString( "[DatabaseControl::onGetCabinetsComplete] %1" ).arg( cabinets->count() );

    m_cabinets = *cabinets;

    emit cabinetsChanged();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::initUserProfiles()
///
/// \brief  Initialises the user profiles.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::initUserProfiles()
{
    UserProfiles::m_map.clear();

    openDatabase();

    if ( m_queryManager->getUserProfiles() )
    {
        while( m_queryManager->getNext() )
        {
            unsigned int id = m_queryManager->getValue( UserProfiles::_id ).toInt();
            QString name  = m_queryManager->getValue( UserProfiles::_profileName ).toString();

            UserProfiles::m_map[name] = id;
        }
    }
    else
    {
        qDebug() << QString( "[DatabaseControl::initUserProfiles] Fail." );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::initUserProfilePermissions()
///
/// \brief  Initialises the user profile permissions.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::initUserProfilePermissions()
{
    UserProfilePermissions::_map.clear();

    openDatabase();

    if ( m_queryManager->getUserProfilePermissions() )
    {
        while( m_queryManager->getNext() )
        {
            unsigned int id = m_queryManager->getValue( UserProfilePermissions::_id ).toInt();
            QString name  = m_queryManager->getValue( UserProfilePermissions::_permissionName ).toString();

            UserProfilePermissions::_map[name] = id;
        }
    }
    else
    {
        qDebug() << QString( "[DatabaseControl::initUserProfilePermissions] Fail." );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::initRegisteredCabinet()
///
/// \brief  Initialises the registered cabinet.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::initRegisteredCabinet()
{
    if ( m_registeredCabinet )
    {
        delete m_registeredCabinet;
    }

    m_registeredCabinet = new Cabinet();

    if ( m_queryManager->getRegisteredCabinet() )
    {
        if ( m_queryManager->getNext() )
        {
            m_registeredCabinet->m_id = m_queryManager->getValue( DatabaseControlConstants::_id ).toInt();
            m_registeredCabinet->m_slotCapacity = m_queryManager->getValue( DatabaseControlConstants::_slotCapacity ).toInt();
            m_registeredCabinet->m_startSlot = m_queryManager->getValue( DatabaseControlConstants::_startSlot ).toInt();
            m_registeredCabinet->m_endSlot = m_queryManager->getValue( DatabaseControlConstants::_endSlot ).toInt();
            m_registeredCabinet->m_slotWidth = m_queryManager->getValue( DatabaseControlConstants::_slotWidth ).toInt();
            m_registeredCabinet->m_noOfLocks = m_queryManager->getValue( DatabaseControlConstants::_noOfLocks ).toInt();
            m_registeredCabinet->m_cabinetName = m_queryManager->getValue( DatabaseControlConstants::_cabinetName ).toString();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControl::initSystem()
///
/// \brief  Initialises the system.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControl::initSystem()
{
    openDatabase();

    if ( m_queryManager->getSystem() )
    {
        m_system.m_name = m_queryManager->getValue( DatabaseControlConstants::_systemName ).toString();
        m_system.m_welcomeText = m_queryManager->getValue( DatabaseControlConstants::_welcomeText ).toString();
        m_system.m_timezone = m_queryManager->getValue( DatabaseControlConstants::_systemTimezone ).toString();
        m_system.m_registeredDate = m_queryManager->getValue( DatabaseControlConstants::_registeredDate ).toString();
        m_system.m_lastCom = m_queryManager->getValue( DatabaseControlConstants::_lastCom ).toString();
        m_system.m_cardAuthentication = m_queryManager->getValue( DatabaseControlConstants::_cardAuthentication ).toBool();
        m_system.m_biometricAuthentication = m_queryManager->getValue( DatabaseControlConstants::_biometricAuthentication ).toBool();
        m_system.m_compound = m_queryManager->getValue( DatabaseControlConstants::_compoundModule ).toBool();
        m_system.m_valet = m_queryManager->getValue( DatabaseControlConstants::_valetModule ).toBool();
    }
    else
    {
        m_system = System();

        qDebug() << QString( "[DatabaseControl::initSystem] Fail." );
    }
}
