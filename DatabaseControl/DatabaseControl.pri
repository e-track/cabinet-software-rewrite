################################################################################
# Config.
################################################################################
QT += mqtt quick sql

CONFIG += c++11

################################################################################
# Dependencies.
################################################################################

################################################################################
# Library.
################################################################################
if(!build_databasecontrol) {
    LIBS += -L$$(CABINETUI_INSTALLATION)/DatabaseControl -lDatabaseControl

    INCLUDEPATH += $$PWD/..
    DEPENDPATH += $$PWD/..
} else {
################################################################################
# Sources.
################################################################################
SOURCES += \
    $$PWD/cabinet.cpp \
    $$PWD/databasecontrol.cpp \
    $$PWD/messagemanager.cpp \
    $$PWD/querymanager.cpp \
    $$PWD/system.cpp \
    $$PWD/tag.cpp \
    $$PWD/tagcategory.cpp \
    $$PWD/user.cpp \
    $$PWD/usergroup.cpp \
    $$PWD/userprofilepermissions.cpp \
    $$PWD/userprofiles.cpp

################################################################################
# Headers.
################################################################################
HEADERS += \
    $$PWD/cabinet.h \
    $$PWD/constants.h \
    $$PWD/databasecontrol.h \
    $$PWD/databasecontrol_global.h \
    $$PWD/messagemanager.h \
    $$PWD/querymanager.h \
    $$PWD/system.h \
    $$PWD/tag.h \
    $$PWD/tagcategory.h \
    $$PWD/user.h \
    $$PWD/usergroup.h \
    $$PWD/userprofilepermissions.h \
    $$PWD/userprofiles.h
}
