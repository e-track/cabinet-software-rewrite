#ifndef USERPROFILES_H
#define USERPROFILES_H

// Qt.
#include <QMap>
#include <QString>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the user_profiles database table.
////////////////////////////////////////////////////////////////////////////////
struct DATABASECONTROL_EXPORT UserProfiles
{
    // Statics.
    DATABASECONTROL_EXPORT static const char* _id;
    DATABASECONTROL_EXPORT static const char* _profileName;

    DATABASECONTROL_EXPORT static const char* _administrator;
    DATABASECONTROL_EXPORT static const char* _standard;
    DATABASECONTROL_EXPORT static const char* _report;
    DATABASECONTROL_EXPORT static const char* _bookingOnly;
    DATABASECONTROL_EXPORT static const char* _engineer;

    DATABASECONTROL_EXPORT static QMap<QString, unsigned int> m_map;        ///< Maps the user profile name to ID.
};

#endif // USERPROFILES_H
