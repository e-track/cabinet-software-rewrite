// Project.
#include "cabinet.h"

const char* const Cabinet::_id = "id";
const char* const Cabinet::_slotCapacity = "slot_capacity";
const char* const Cabinet::_startSlot = "start_slot";
const char* const Cabinet::_endSlot = "end_slot";
const char* const Cabinet::_slotWidth = "slot_width";
const char* const Cabinet::_noOfLocks = "no_of_locks";
const char* const Cabinet::_cabinetName = "cabinet_name";

// GroupCabinetAccess constants.
const char* const GroupCabinetAccess::_groupCabinetAccessTable = "tbl_group_cabinet_access";
const char* const GroupCabinetAccess::_userGroup = "user_group";
const char* const GroupCabinetAccess::_cabinet = "cabinet";
const char* const GroupCabinetAccess::_isAccessGranted = "is_access_granted";

// UserCabinetAccess constants.
const char* const UserCabinetAccess::_userCabinetAccessTable = "tbl_user_cabinet_access";
const char* const UserCabinetAccess::_userId = "user_id";
const char* const UserCabinetAccess::_cabinet = "cabinet";
const char* const UserCabinetAccess::_isAccessGranted = "is_access_granted";

////////////////////////////////////////////////////////////////////////////////
/// \fn     Cabinet::Cabinet( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
Cabinet::Cabinet( QObject* parent )
    : QObject( parent )
    , m_id( -1 )
    , m_slotCapacity( -1 )
    , m_startSlot( -1 )
    , m_endSlot( -1 )
    , m_slotWidth( -1 )
    , m_noOfLocks( -1 )
    , m_cabinetName( "" )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Cabinet::operator==( const Cabinet& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool Cabinet::operator==( const Cabinet& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_slotCapacity == lhs.m_slotCapacity &&
             m_startSlot == lhs.m_startSlot &&
             m_endSlot  == lhs.m_endSlot &&
             m_slotWidth == lhs.m_slotWidth &&
             m_noOfLocks == lhs.m_noOfLocks &&
             m_cabinetName == lhs.m_cabinetName );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Cabinet::operator!=( const Cabinet& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool Cabinet::operator!=( const Cabinet& lhs ) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::GroupCabinetAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
GroupCabinetAccess::GroupCabinetAccess()
    : m_id( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::operator==( const GroupCabinetAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupCabinetAccess::operator==( const GroupCabinetAccess& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::operator!=( const GroupCabinetAccess& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupCabinetAccess::operator!=( const GroupCabinetAccess& lhs ) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::GroupCabinetAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UserCabinetAccess::UserCabinetAccess()
    : m_id( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::operator==( const GroupCabinetAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserCabinetAccess::operator==( const UserCabinetAccess& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupCabinetAccess::operator!=( const GroupCabinetAccess& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserCabinetAccess::operator!=( const UserCabinetAccess& lhs ) const
{
    return !( *this == lhs );
}
