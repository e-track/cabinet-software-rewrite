// Project.
#include "usergroup.h"

// Statics.
const char* const UserGroup::_userGroupTable = "tbl_user_group";
const char* const UserGroup::_userToGroupTable = "tbl_user_to_groups";
const char* const UserGroup::_userId = "user_id";
const char* const UserGroup::_userGroup = "user_group";
const char* const UserGroup::_groupName = "group_name";
const char* const UserGroup::_keyLimit= "key_limit";

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserGroup::UserGroup()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UserGroup::UserGroup()
    : m_id( -1 )
    , m_keyLimit( -1 )
    , m_groupName( "" )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserGroup::operator==( const UserGroup& lhs )
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserGroup::operator==( const UserGroup& lhs )
{
    return m_id == lhs.m_id &&
            m_keyLimit == lhs.m_keyLimit &&
            m_groupName == lhs.m_groupName;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserGroup::operator!=( const UserGroup& lhs )
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserGroup::operator!=( const UserGroup& lhs )
{
    return !( *this == lhs );
}
