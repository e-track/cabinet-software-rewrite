// Project.
#include "system.h"

// GroupSystemAccess constants.
const char* const GroupSystemAccess::_groupSystemAccessTable = "tbl_group_system_access";
const char* const GroupSystemAccess::_userGroup = "user_group";
const char* const GroupSystemAccess::_systemId = "system_id";
const char* const GroupSystemAccess::_isAccessGranted = "is_access_granted";

// UserSystemAccess constants.
const char* const UserSystemAccess::_userSystemAccessTable = "tbl_user_system_access";
const char* const UserSystemAccess::_userId = "user_id";
const char* const UserSystemAccess::_systemId = "system_id";
const char* const UserSystemAccess::_isAccessGranted = "is_access_granted";

////////////////////////////////////////////////////////////////////////////////
/// \fn     System::System()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
System::System()
    : m_name( "" )
    , m_welcomeText( "" )
    , m_timezone( "" )
    , m_registeredDate( "" )
    , m_lastCom( "" )
    , m_cardAuthentication( false )
    , m_biometricAuthentication( false )
    , m_compound( false )
    , m_valet( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     System::operator==( const System& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool System::operator==( const System& lhs ) const
{
    return ( m_name == lhs.m_name &&
             m_welcomeText == lhs.m_welcomeText &&
             m_timezone == lhs.m_timezone &&
             m_registeredDate == lhs.m_registeredDate &&
             m_lastCom == lhs.m_lastCom &&
             m_cardAuthentication == lhs.m_cardAuthentication &&
             m_biometricAuthentication == lhs.m_biometricAuthentication &&
             m_compound == lhs.m_compound &&
             m_valet == lhs.m_valet );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     System::operator!=( const System& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool System::operator!=( const System& lhs ) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupSystemAccess::GroupSystemAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
GroupSystemAccess::GroupSystemAccess()
    : m_id( -1 )
    , m_system( -1 )
    , m_group( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupSystemAccess::operator==( const GroupSystemAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupSystemAccess::operator==( const GroupSystemAccess& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_system == lhs.m_system &&
             m_group == lhs.m_group &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupSystemAccess::operator!=( const GroupSystemAccess& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupSystemAccess::operator!=( const GroupSystemAccess& lhs ) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserSystemAccess::UserSystemAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UserSystemAccess::UserSystemAccess()
    : m_id( -1 )
    , m_system( -1 )
    , m_user( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserSystemAccess::operator==( const UserSystemAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserSystemAccess::operator==( const UserSystemAccess& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_system == lhs.m_system &&
             m_user == lhs.m_user &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserSystemAccess::operator!=( const UserSystemAccess& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserSystemAccess::operator!=( const UserSystemAccess& lhs ) const
{
    return !( *this == lhs );
}
