// Project.
#include "userprofiles.h"

// Statics.
const char* UserProfiles::_id = "id";
const char* UserProfiles::_profileName = "profile_name";
const char* UserProfiles::_administrator = "Administrator";
const char* UserProfiles::_standard = "Standard";
const char* UserProfiles::_report = "Report";
const char* UserProfiles::_bookingOnly = "Booking Only";
const char* UserProfiles::_engineer = "Engineer";

QMap<QString, unsigned int> UserProfiles::m_map;
