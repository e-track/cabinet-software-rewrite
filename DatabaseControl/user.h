#ifndef USER_H
#define USER_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the 'users' database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT User
{
    Q_GADGET
    Q_PROPERTY( int id                 MEMBER m_id )
    Q_PROPERTY( int m_keyLimit         MEMBER m_keyLimit )
    Q_PROPERTY( int m_roleId           MEMBER m_roleId )
    Q_PROPERTY( QString firstName      MEMBER m_firstName )
    Q_PROPERTY( QString lastName       MEMBER m_lastName )
    Q_PROPERTY( QString m_username     MEMBER m_username )
    Q_PROPERTY( QString m_password     MEMBER m_password )
    Q_PROPERTY( QString m_activeFrom   MEMBER m_activeFrom )
    Q_PROPERTY( QString m_profileImage MEMBER m_profileImage )
    Q_PROPERTY( QString m_phoneNumber  MEMBER m_phoneNumber )
    Q_PROPERTY( bool m_isVisitor       MEMBER m_isVisitor )
    Q_PROPERTY( bool m_isActive        MEMBER m_isActive )
    Q_PROPERTY( bool m_isTemp          MEMBER m_isTemp )
    Q_PROPERTY( bool m_isFirstLogin    MEMBER m_isFirstLogin )

public:
    explicit User();
    ~User() = default;

    int m_id;
    int m_keyLimit;
    int m_roleId;

    QString m_firstName;
    QString m_lastName;
    QString m_username;
    QString m_password;
    QString m_activeFrom;
    QString m_profileImage;
    QString m_phoneNumber;

    bool m_isVisitor;
    bool m_isActive;
    bool m_isTemp;
    bool m_isFirstLogin;

    bool operator==( const User& lhs ) const;
    bool operator!=( const User& lhs ) const;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the 'user_entry_ids' database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT UserEntry
{
    Q_GADGET
    Q_PROPERTY( int userId         MEMBER m_userId )
    Q_PROPERTY( int entryType      MEMBER m_entryType )
    Q_PROPERTY( QString entryValue MEMBER m_entryValue )

public:
    explicit UserEntry();
    ~UserEntry() = default;

    int m_userId;
    int m_entryType;
    QString m_entryValue;

    bool operator==( const UserEntry& lhs ) const;
    bool operator!=( const UserEntry& lhs ) const;

    // User entry IDs.
    DATABASECONTROL_EXPORT static const char* const _userEntryIdsTable;
    DATABASECONTROL_EXPORT static const char* const _userId;
    DATABASECONTROL_EXPORT static const char* const _entryType;
    DATABASECONTROL_EXPORT static const char* const _entryValue;
};

#endif // USER_H
