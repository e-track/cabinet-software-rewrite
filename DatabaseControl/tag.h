#ifndef TAG_H
#define TAG_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_keys database table.
///         Database parameters are given in parenthesis.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT Tag : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int tagId              MEMBER m_id             NOTIFY tagIdChanged )
    Q_PROPERTY( int cabinet            MEMBER m_cabinetId      NOTIFY cabinetChanged )
    Q_PROPERTY( int userId             MEMBER m_userId         NOTIFY userIdChanged )
    Q_PROPERTY( int slot               MEMBER m_slot           NOTIFY slotChanged )
    Q_PROPERTY( int currentSlot        MEMBER m_currentSlot    NOTIFY currentSlotChanged )
    Q_PROPERTY( int fob                MEMBER m_fob            NOTIFY fobChanged )
    Q_PROPERTY( int mileage            MEMBER m_mileage        NOTIFY mileageChanged )
    Q_PROPERTY( int category           MEMBER m_category       NOTIFY categoryChanged )
    Q_PROPERTY( QString name           MEMBER m_tagName        NOTIFY nameChanged )           // TODO: Rename property to tagName and update QML.
    Q_PROPERTY( QString tagDescription MEMBER m_tagDescription NOTIFY tagDescriptionChanged )
    Q_PROPERTY( QString serial         MEMBER m_tagSerial      NOTIFY serialChanged )         // TODO: Rename property to tagSerial and update QML.
    Q_PROPERTY( QString dateAdded      MEMBER m_dateAdded      NOTIFY dateAddedChanged )
    Q_PROPERTY( QString startTime      MEMBER m_startTime      NOTIFY startTimeChanged )
    Q_PROPERTY( QString endTime        MEMBER m_endTime        NOTIFY endTimeChanged )
    Q_PROPERTY( QString location       MEMBER m_location       NOTIFY locationChanged )
    Q_PROPERTY( QString site           MEMBER m_site           NOTIFY siteChanged )
    Q_PROPERTY( bool isStatic          MEMBER m_isStatic       NOTIFY isStaticChanged )
    Q_PROPERTY( bool isOut             MEMBER m_isOut          NOTIFY isOutChanged )
    Q_PROPERTY( bool isWrongSlot       MEMBER m_isWrongSlot    NOTIFY isWrongSlotChanged )
    Q_PROPERTY( bool booked            MEMBER m_booked         NOTIFY bookedChanged )
    Q_PROPERTY( bool inBasket          MEMBER m_inBasket       NOTIFY inBasketChanged )
    Q_PROPERTY( bool isReturning       MEMBER m_isReturning    NOTIFY isReturningChanged )

public:
    explicit Tag( QObject* parent = nullptr );
    virtual ~Tag() = default;

    int m_id;                   ///< ID.
    int m_cabinetId;            ///< Cabinet ID.
    int m_userId;               ///< User key is checked out to.
    int m_category;             ///< Tag / key category.
    int m_slot;                 ///< Slot.
    int m_currentSlot;          ///< Current slot.
    int m_fob;                  ///< Fob reference.
    int m_mileage;              ///< TODO: Temporary, indicates mileage (not in DB).

    QString m_tagName;          ///< Tag name.
    QString m_tagDescription;   ///< Tag description.
    QString m_tagSerial;        ///< Tag serial number.
    QString m_dateAdded;        ///< Date added (TODO: Use QDate when this is implemented fully).
    QString m_startTime;        ///< Start time (TODO: Use QTime when this is implemented fully).
    QString m_endTime;          ///< End time (TODO: Use QTime when this is implemented fully).
    QString m_location;         ///< TODO: Temporary, indicates location (not in DB).
    QString m_site;             ///< TODO: Temporary, indicates site (not in DB).

    bool m_isStatic;            ///< Static state.
    bool m_isOut;               ///< Taken out state.
    bool m_isWrongSlot;         ///< Incorrect slot state.
    bool m_booked;              ///< TODO: Temporary, indicates booking availability (not in DB).
    bool m_inBasket;            ///< Tag is in basket ready to be checked out.
    bool m_isReturning;         ///< Tag has been selected to be returned.

    bool operator==( const Tag& lhs ) const;
    bool operator!=( const Tag& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _keysTable;
    DATABASECONTROL_EXPORT static const char* const _keyId;
    DATABASECONTROL_EXPORT static const char* const _id;
    DATABASECONTROL_EXPORT static const char* const _cabinetId;
    DATABASECONTROL_EXPORT static const char* const _userId;
    DATABASECONTROL_EXPORT static const char* const _category;
    DATABASECONTROL_EXPORT static const char* const _keyName;
    DATABASECONTROL_EXPORT static const char* const _keyDescription;
    DATABASECONTROL_EXPORT static const char* const _keySerial;
    DATABASECONTROL_EXPORT static const char* const _slot;
    DATABASECONTROL_EXPORT static const char* const _currentSlot;
    DATABASECONTROL_EXPORT static const char* const _fob;
    DATABASECONTROL_EXPORT static const char* const _isStatic;
    DATABASECONTROL_EXPORT static const char* const _dateAdded;
    DATABASECONTROL_EXPORT static const char* const _startTime;
    DATABASECONTROL_EXPORT static const char* const _endTime;
    DATABASECONTROL_EXPORT static const char* const _isOut;
    DATABASECONTROL_EXPORT static const char* const _isWrongSlot;
    DATABASECONTROL_EXPORT static const char* const _additionalFields;

signals:
    void tagIdChanged();
    void cabinetChanged();
    void userIdChanged();
    void slotChanged();
    void currentSlotChanged();
    void fobChanged();
    void mileageChanged();
    void categoryChanged();
    void nameChanged();
    void tagDescriptionChanged();
    void serialChanged();
    void dateAddedChanged();
    void startTimeChanged();
    void endTimeChanged();
    void locationChanged();
    void siteChanged();
    void isStaticChanged();
    void isOutChanged();
    void isWrongSlotChanged();
    void bookedChanged();
    void inBasketChanged();
    void isReturningChanged();
};

Q_DECLARE_METATYPE(Tag);

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_user_to_key_access database table.
///         TODO: Consider whether to have a single TagAccess class for both
///         user and user group tag / key access since the properties are similar.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT UserTagAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( int userId           MEMBER m_userId )
    Q_PROPERTY( int keyId            MEMBER m_keyId )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit UserTagAccess();

    int m_id;
    int m_userId;
    int m_keyId;
    bool m_isAccessGranted;

    bool operator==( const UserTagAccess& lhs ) const;
    bool operator!=( const UserTagAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _userKeyAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userId;
    DATABASECONTROL_EXPORT static const char* const _keyId;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_group_to_key_access database table.
///         TODO: Consider whether to have a single TagAccess class for both
///         user and user group tag / key access since the properties are similar.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT GroupTagAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( int userGroupId      MEMBER m_userGroupId )
    Q_PROPERTY( int keyId            MEMBER m_keyId )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit GroupTagAccess();

    int m_id;
    int m_userGroupId;
    int m_keyId;
    bool m_isAccessGranted;

    bool operator==( const GroupTagAccess& lhs ) const;
    bool operator!=( const GroupTagAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _userGroupKeyAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userGroupId;
    DATABASECONTROL_EXPORT static const char* const _keyId;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

#endif // TAG_H
