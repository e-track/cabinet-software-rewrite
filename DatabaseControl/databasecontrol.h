#ifndef DATABASECONTROL_H
#define DATABASECONTROL_H

// Qt.
#include <QObject>
#include <QThread>
#include <QVariantList>

// Project.
#include "cabinet.h"
#include "databasecontrol_global.h"
#include "querymanager.h"
#include "system.h"
#include "tag.h"
#include "tagcategory.h"
#include "user.h"

class MessageManager;
class QSqlDatabase;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Database controller.
///         TODO: It would be a good idea to have separate controllers for
///         different types of query (cabinets, tags / keys, users etc).
///         These can then be registered and imported individually which would
///         simplify this class and be optimal.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT DatabaseControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY( User loggedInUser                 READ getLoggedInUser                            NOTIFY loggedInUserChanged )
    Q_PROPERTY( User authenticatedUser            READ getAuthenticatedUser                       NOTIFY authenticatedUserChanged )
    Q_PROPERTY( Cabinet* selectedCabinet          READ getSelectedCabinet                         NOTIFY selectedCabinetChanged )
    Q_PROPERTY( QList<Cabinet*> cabinets          READ getCabinets                                NOTIFY cabinetsChanged )
    Q_PROPERTY( QList<QList<Tag*>> cabinetTags    READ getCabinetTags                             NOTIFY cabinetTagsChanged )
    Q_PROPERTY( QList<QList<Tag*>> categoryTags   READ getCategoryTags                            NOTIFY categoryTagsChanged )
    Q_PROPERTY( QList<Tag*> userTags              READ getUserTags                                NOTIFY userTagsChanged )
    Q_PROPERTY( QList<Tag*> bookedTags            READ getBookedTags                              NOTIFY bookedTagsChanged )
    Q_PROPERTY( QList<Tag*> searchTags            READ getSearchTags                              NOTIFY searchTagsChanged )
    Q_PROPERTY( QList<Tag*> basketTags            READ getBasketTags                              NOTIFY basketTagsChanged )
    Q_PROPERTY( QList<Tag*> returningTags         READ getReturningTags                           NOTIFY returningTagsChanged )
    Q_PROPERTY( QList<TagCategory*> tagCategories READ getTagCategories                           NOTIFY tagCategoriesChanged )
    Q_PROPERTY( unsigned int tagGroupSize         READ getTagGroupSize      WRITE setTagGroupSize NOTIFY tagGroupSizeChanged )

public:
    virtual ~DatabaseControl() = default;

    // Login.
    Q_INVOKABLE void loginPin( QString pin );
    Q_INVOKABLE void loginTempPin( const QString& tempPin );
    Q_INVOKABLE void logout();

    // PIN.
    Q_INVOKABLE void savePin( const QString& pin );

    // Setup.
    DATABASECONTROL_EXPORT static void registerClass();

    // Simulation.
    Q_INVOKABLE void simulateLogin();

    // User.
    User getLoggedInUser() const;
    User getAuthenticatedUser() const;

    Q_INVOKABLE void addFingerprint( const int fingerprintId );
    Q_INVOKABLE void addCard( const QString& cardId );

    bool authenticateUser( int fingerprintId, QString cardId );
    void denyUser();

    // Cabinets.
    QList<Cabinet*> getCabinets();

    Q_INVOKABLE void selectCabinet( int cabinetId );
    Cabinet* getSelectedCabinet();
    Cabinet* getRegisteredCabinet();

    // Systems.
    System getSystem();

    // Tags / keys.
    Q_INVOKABLE void queryTagsForUser( const int userId );
    Q_INVOKABLE void queryBookedTags( const int userId );

    Q_INVOKABLE void addTagToBasket( Tag* tag );
    Q_INVOKABLE void removeTagFromBasket( Tag* tag );
    Q_INVOKABLE void clearBasketTags();

    Q_INVOKABLE void addTagToReturning( Tag* tag );
    Q_INVOKABLE void removeTagFromReturning( Tag* tag );

    Q_INVOKABLE void completeCheckoutBasket();
    Q_INVOKABLE void completeReturnTags();

    QList<QList<Tag*>> getCabinetTags();
    QList<QList<Tag*>> getCategoryTags();
    QList<Tag*> getUserTags();
    QList<Tag*> getBookedTags();
    QList<Tag*> getSearchTags();
    QList<Tag*> getBasketTags();
    QList<Tag*> getReturningTags();

    unsigned int getTagGroupSize() const;
    void setTagGroupSize( unsigned int tagGroupSize );

    // Tag / key categories.
    QList<TagCategory*> getTagCategories();

    // General SQL queries.
    virtual void performSqlQuery( const QString& sql );

    // Save.
    Q_INVOKABLE void saveAll();

    // Statics.
    DATABASECONTROL_EXPORT static DatabaseControl* instance();
    DATABASECONTROL_EXPORT static DatabaseControl* instance( QueryManager* queryManager, MessageManager* messageManager );
    DATABASECONTROL_EXPORT static void destroy();

    // Friendship.
    friend class DatabaseControlTest;

signals:
    // Initialisation.
    void initDatabase();

    // Database access.
    void closeDatabase();

    // Login.
    void loginSuccess( const User& user );
    void loginFail();
    void logoutSuccess();

    // Temp login.
    void tempLoginSuccess( const User& user );
    void tempLoginFail();

    // User.
    void loggedInUserChanged();
    void authenticatedUserChanged();

    // Cabinets.
    void queryCabinets();
    void cabinetsChanged();
    void selectedCabinetChanged();

    // Tags.
    void querySearchTags( QString text );
    void queryTagsForCabinet( const int cabinetId, const int groupSize );
    void queryCategoryTags( const int groupSize );
    void updateTag( Tag* tag );

    void cabinetTagsChanged();
    void categoryTagsChanged();
    void userTagsChanged();
    void bookedTagsChanged();
    void searchTagsChanged();
    void basketTagsChanged();
    void returningTagsChanged();

    void tagGroupSizeChanged();

    // Tag / key categories.
    void addTagCategory( TagCategory* tagCategory );
    void updateTagCategory( TagCategory* tagCategory );
    void removeTagCategory( TagCategory* tagCategory, const QString& table  );

    void queryTagCategories();
    void tagCategoriesChanged();

    // General SQL queries.
    void sqlSuccess();
    void sqlFail();

    // Save.
    void saveAllSuccess();
    void saveAllFail();

    // Permissions.
    void accessDenied();

    // Events.
    void eventLogUpdateTag( Tag* tag, const User& user );

private slots:
    // MQTT payloads.
    void onPayloadReceived( QString payload );

    // Initialisation.
    void onInitDatabaseComplete( bool success );

    // Cabinets.
    void onGetCabinetsComplete( QList<Cabinet*>* cabinets );

    // Tags / keys.
    void onSearchTagsComplete( bool success );
    void onUpdateTagComplete( Tag* tag, bool success );
    void onUpdateTagEventComplete( Tag* tag, bool success );
    void onEventLogUpdateTagComplete( Tag* tag, bool success );
    void onGetTagsForCabinetComplete( QList<QList<Tag*>>* cabinetTags );
    void onGetCategoryTagsComplete( QList<QList<Tag*>>* categoryTags );

    // Tag / key categories.
    void onAddTagCategoryComplete( TagCategory* tagCategory, bool success );
    void onUpdateTagCategoryComplete( TagCategory* tagCategory, bool success );
    void onRemoveTagCategoryComplete( TagCategory* tagCategory, bool success );
    void onGetTagCategoriesComplete( QList<TagCategory*>* tagCategories );

private:
    explicit DatabaseControl( QueryManager* queryManager, MessageManager* messageManager );

    // Database access.
    void openDatabase();

    // User.
    void setLoggedInUser( User user );
    void setAuthenticatedUser( User user );

    void initUserProfiles();
    void initUserProfilePermissions();

    // Cabinets.
    void initRegisteredCabinet();

    // System.
    void initSystem();

    // Tags.
    Tag* createTagFromQuery();

    // Permissions.
    bool checkUserSystemPermissions( const User& user, bool& access );
    bool checkUserCabinetPermissions( const User& user, bool& access );
    bool checkUserTagPermissions( const User& user, const int tagId, bool& access );

    // Payloads.
    void handleHelloWorldPayload( QJsonObject& json );
    void handleNewUserPayload( QJsonObject& json );
    void handleUpdateUserPayload( QJsonObject& json );
    void handleRemoveUserPayload( QJsonObject& json );
    void handleAddUserGroup( QJsonObject& json );
    void handleUpdateUserGroup( QJsonObject& json );
    void handleRemoveUserGroup( QJsonObject& json );
    void handleAddUserToGroup( QJsonObject& json );
    void handleRemoveUserFromGroup( QJsonObject& json );
    void handleNewKeyPayload( QJsonObject& json );
    void handleRemoveKeyPayload( QJsonObject& json );
    void handleUpdateKeyPayload( QJsonObject& json );
    void handleNewKeyCategoryPayload( QJsonObject& json );
    void handleUpdateKeyCategoryPayload( QJsonObject& json );
    void handleRemoveKeyCategoryPayload( QJsonObject& json );
    void handleNewKeyUserPermissionsPayload( QJsonObject& json );
    void handleRemoveKeyUserPermissionsPayload( QJsonObject& json );
    void handleUpdateKeyUserPermissionsPayload( QJsonObject& json );
    void handleNewKeyGroupPermissionsPayload( QJsonObject& json );
    void handleRemoveKeyGroupPermissionsPayload( QJsonObject& json );
    void handleUpdateKeyGroupPermissionsPayload( QJsonObject& json );
    void handleResetUserPinPayload( QJsonObject& json );

    // Message.
    void sendCommandResultMessage( const char* command, bool success );

    // Managers.
    QueryManager* m_queryManager;           ///< Handles local database queries.
    MessageManager* m_messageManager;       ///< Handles remote MQTT messages.

    // Users.
    User m_loggedInUser;                    ///< Logged in user.
    User m_authenticatedUser;               ///< User that has been authenticated.

    // Cabinets.
    Cabinet* m_selectedCabinet;             ///< The currently selected / opened cabinet.
    QList<Cabinet*> m_cabinets;             ///< The list of cabinets in the system.
    Cabinet* m_registeredCabinet;           ///< The registered cabinet.

    // Systems.
    System m_system;                        ///< The currently registered system.

    // Tags / keys.
    QList<QList<Tag*>> m_cabinetTags;       ///< The list of tags / keys in the current cabinet.
    QList<QList<Tag*>> m_categoryTags;      ///< The list of tags / keys grouped by category.
    QList<Tag*> m_userTags;                 ///< The list of tags / keys checked out by the current user.
    QList<Tag*> m_bookedTags;               ///< The list of tags / keys available for booking.
    QList<Tag*> m_searchTags;               ///< The list of tags / keys found during a search.
    QList<Tag*> m_basketTags;               ///< The list of tags / keys in the basket ready to be checked out.
    QList<Tag*> m_returningTags;            ///< The list of tags / keys that have been selected to be returned to the cabinet.
    unsigned int m_tagGroupSize;            ///< The size of the tag / key groups.

    // Tag / key categories.
    QList<TagCategory*> m_tagCategories;    ///< The list of tag / key categories.

    // Identification (TODO: Move to User struct).
    QString m_savedPin;                     ///< Saved PIN.
    QString m_savedCardId;                  ///< Saved card ID.
    int m_savedFingerprintId;               ///< Saved fingerprint ID.

    QThread* m_thread;
};

#endif // DATABASECONTROL_H
