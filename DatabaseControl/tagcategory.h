#ifndef TAGCATEGORY_H
#define TAGCATEGORY_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the tbl_key_categories database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT TagCategory : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int tagCategoryId            MEMBER m_tagCategoryId        NOTIFY tagCategoryIdChanged        );
    Q_PROPERTY( QString tagCategoryType      MEMBER m_tagCategoryType      NOTIFY tagCategoryTypeChanged      );
    Q_PROPERTY( QString tagCategoryIcon      MEMBER m_tagCategoryIcon      NOTIFY tagCategoryIconChanged      );
    Q_PROPERTY( QString tagCategoryName      MEMBER m_tagCategoryName      NOTIFY tagCategoryNameChanged      );
    Q_PROPERTY( QString tagCategoryShortName MEMBER m_tagCategoryShortName NOTIFY tagCategoryShortNameChanged );

public:
    explicit TagCategory( QObject* parent = nullptr );
    virtual ~TagCategory() = default;

    int m_tagCategoryId;                ///< ID (primary key).
    QString m_tagCategoryType;          ///< Key category type.
    QString m_tagCategoryIcon;          ///< Key category icon (taken from pre-defined list).
    QString m_tagCategoryName;          ///< Key category long name.
    QString m_tagCategoryShortName;     ///< Key category short name.

    bool operator==( const TagCategory& lhs ) const;
    bool operator!=( const TagCategory& lhs ) const;

    // Tag category database tables.
    DATABASECONTROL_EXPORT static const char* const _tagCategoryTable;
    DATABASECONTROL_EXPORT static const char* const _groupToTagCategoryAccessTable;
    DATABASECONTROL_EXPORT static const char* const _tagToCategoriesTable;
    DATABASECONTROL_EXPORT static const char* const _userToTagCategoryAccessTable;

    // Tag category database fields.
    DATABASECONTROL_EXPORT static const char* const _tagCategory;
    DATABASECONTROL_EXPORT static const char* const _tagCategoryId;
    DATABASECONTROL_EXPORT static const char* const _tagCategoryType;
    DATABASECONTROL_EXPORT static const char* const _tagCategoryIcon;
    DATABASECONTROL_EXPORT static const char* const _tagCategoryName;
    DATABASECONTROL_EXPORT static const char* const _tagCategoryShortName;

    // Tag category commands.
    DATABASECONTROL_EXPORT static const char* const _newKeyCategory;
    DATABASECONTROL_EXPORT static const char* const _updateKeyCategory;
    DATABASECONTROL_EXPORT static const char* const _removeKeyCategory;

signals:
    void tagCategoryIdChanged();
    void tagCategoryTypeChanged();
    void tagCategoryIconChanged();
    void tagCategoryNameChanged();
    void tagCategoryShortNameChanged();
};

Q_DECLARE_METATYPE(TagCategory);

#endif // TAGCATEGORY_H
