#ifndef CONSTANTS_H
#define CONSTANTS_H

// Project.
#include "databasecontrol_global.h"

// TODO: I've going off the idea of having constants for the database and messaging all in one file.
// This should only contain general constants like 'id' or '*', the rest should be defined in the
// classes that they represent.
// For example, system.h defines the system class that represents the tbl_system table in the database
// so all the systems constants should be taken from there instead.
namespace DatabaseControlConstants
{
    // General.
    const char* const _id = "id";
    const char* const _all = "*";

    // Systems.
    const char* const _systemsTable = "tbl_systems";
    const char* const _system = "system";
    const char* const _systemName = "system_name";
    const char* const _welcomeText = "welcome_text";
    const char* const _systemTimezone = "system_timezone";
    const char* const _cardAuthentication = "card_authentication";
    const char* const _biometricAuthentication = "biometric_authentication";
    const char* const _registeredDate = "registered_date";
    const char* const _lastCom = "last_com";
    const char* const _compoundModule = "compound_module";
    const char* const _valetModule = "valet_module";

    // Cabinets.
    const char* const _cabinetsTable = "tbl_cabinets";
    const char* const _cabinetName = "cabinet_name";
    const char* const _slotCapacity = "slot_capacity";
    const char* const _startSlot = "start_slot";
    const char* const _endSlot = "end_slot";
    const char* const _slotWidth = "slot_width";
    const char* const _noOfLocks = "no_of_locks";

    // Users.
    const char* const _usersTable = "tbl_users";
    const char* const _userId = "user_id";
    const char* const _username = "username";
    const char* const _password = "password";
    const char* const _firstName = "first_name";
    const char* const _lastName = "last_name";
    const char* const _isVisitor = "is_visitor";
    const char* const _profileImage = "profile_image";
    const char* const _phoneNumber = "phone_number";
    const char* const _isActive = "is_active";
    const char* const _activeFrom = "active_from";
    const char* const _keyLimit = "key_limit";
    const char* const _isTemp = "is_temp";
    const char* const _isFirstLogin = "is_first_login";
    const char* const _userProfile = "user_profile";
    const char* const _roleId = "role_id";                                  // TODO: Check this is still used.

    // User profiles.
    const char* const _userProfilesTable = "tbl_user_profiles";
    const char* const _userProfilePermissionsTable = "tbl_user_profile_permissions";
    const char* const _profileName = "profile_name";
    const char* const _profileDescription = "profile_description";

    // User profile permissions.
    const char* const _permissionName = "permission_name";

    // Access.
    const char* const _isAccessGranted = "is_access_granted";

    // Events.
    const char* const _eventsLogTable = "tbl_events_log";
    const char* const _eventAction = "event_action";
    const char* const _eventMessage = "event_message";
    const char* const _parentSystem = "parent_system";
    const char* const _eventDate = "event_date";
}

#endif // CONSTANTS_H
