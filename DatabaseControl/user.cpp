// Project.
#include "user.h"

const char* const UserEntry::_userEntryIdsTable = "tbl_user_entry_ids";
const char* const UserEntry::_userId = "user_id";
const char* const UserEntry::_entryType = "entry_type";
const char* const UserEntry::_entryValue = "entry_value";

////////////////////////////////////////////////////////////////////////////////
/// \fn     User::User()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
User::User()
    : m_id( -1 )
    , m_keyLimit( -1 )
    , m_roleId( -1 )
    , m_firstName( "" )
    , m_lastName( "" )
    , m_username( "" )
    , m_password( "" )
    , m_activeFrom( "" )
    , m_profileImage( "" )
    , m_phoneNumber( "" )
    , m_isVisitor( false )
    , m_isActive( false )
    , m_isTemp( false )
    , m_isFirstLogin( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     User::operator==( const User& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool User::operator==( const User& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_keyLimit == lhs.m_keyLimit &&
             m_roleId == lhs.m_roleId &&
             m_firstName == lhs.m_firstName &&
             m_lastName == lhs.m_lastName &&
             m_username == lhs.m_username &&
             m_password == lhs.m_password &&
             m_activeFrom == lhs.m_activeFrom &&
             m_profileImage == lhs.m_profileImage &&
             m_phoneNumber == lhs.m_phoneNumber &&
             m_isVisitor == lhs.m_isVisitor &&
             m_isActive == lhs.m_isActive &&
             m_isTemp == lhs.m_isTemp &&
             m_isFirstLogin == lhs.m_isFirstLogin );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     User::operator!=( const User& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool User::operator!=( const User& lhs ) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserEntry::UserEntry()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UserEntry::UserEntry()
    : m_userId( -1 )
    , m_entryType( -1 )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserEntry::operator==( const UserEntry& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserEntry::operator==( const UserEntry& lhs ) const
{
    return ( m_userId == lhs.m_userId &&
             m_entryType == lhs.m_entryType &&
             m_entryValue == lhs.m_entryValue );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserEntry::operator!=( const UserEntry& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserEntry::operator!=( const UserEntry& lhs ) const
{
    return !( *this == lhs );
}
