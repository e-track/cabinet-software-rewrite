// Project.
#include "tag.h"

// Tag statics.
const char* const Tag::_keysTable = "tbl_keys";
const char* const Tag::_keyId = "key_id";
const char* const Tag::_id = "id";
const char* const Tag::_cabinetId = "cabinet";
const char* const Tag::_userId = "user_id";
const char* const Tag::_category = "category";
const char* const Tag::_keyName = "key_name";
const char* const Tag::_keyDescription = "key_description";
const char* const Tag::_keySerial = "key_serial";
const char* const Tag::_slot = "slot";
const char* const Tag::_currentSlot = "current_slot";
const char* const Tag::_fob = "fob";
const char* const Tag::_isStatic = "is_static";
const char* const Tag::_dateAdded = "date_added";
const char* const Tag::_startTime = "start_time";
const char* const Tag::_endTime = "end_time";
const char* const Tag::_isOut = "is_out";
const char* const Tag::_isWrongSlot = "is_wrong_slot";
const char* const Tag::_additionalFields = "additional_fields";

// User tag statics.
const char* const UserTagAccess::_userKeyAccessTable = "tbl_user_to_key_access";
const char* const UserTagAccess::_userId = "user_id";
const char* const UserTagAccess::_keyId = "key_id";
const char* const UserTagAccess::_isAccessGranted = "is_access_granted";

// Group tag statics.
const char* const GroupTagAccess::_userGroupKeyAccessTable = "tbl_group_to_key_access";
const char* const GroupTagAccess::_userGroupId = "user_group";
const char* const GroupTagAccess::_keyId = "key_id";
const char* const GroupTagAccess::_isAccessGranted = "is_access_granted";

////////////////////////////////////////////////////////////////////////////////
/// \fn     Tag::Tag( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
Tag::Tag( QObject* parent )
    : QObject( parent )
    , m_id( -1 )
    , m_cabinetId( -1 )
    , m_userId( -1 )
    , m_category( -1 )
    , m_slot( -1 )
    , m_currentSlot( -1 )
    , m_fob( -1 )
    , m_mileage( -1 )
    , m_isStatic( false )
    , m_isOut( false )
    , m_isWrongSlot( false )
    , m_booked( false )
    , m_inBasket( false )
    , m_isReturning( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Tag::operator==( const Tag& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool Tag::operator==( const Tag& lhs ) const
{
    return ( m_id == lhs.m_id &&
             m_cabinetId == lhs.m_cabinetId &&
             m_userId == lhs.m_userId &&
             m_category == lhs.m_category &&
             m_slot == lhs.m_slot &&
             m_currentSlot == lhs.m_currentSlot &&
             m_fob  == lhs.m_fob &&
             m_mileage == lhs.m_mileage &&
             m_tagName  == lhs.m_tagName &&
             m_tagDescription  == lhs.m_tagDescription &&
             m_tagSerial  == lhs.m_tagSerial &&
             m_dateAdded == lhs.m_dateAdded &&
             m_startTime == lhs.m_startTime &&
             m_endTime == lhs.m_endTime &&
             m_location == lhs.m_location &&
             m_site == lhs.m_site &&
             m_isStatic == lhs.m_isStatic &&
             m_isOut == lhs.m_isOut &&
             m_isWrongSlot == lhs.m_isWrongSlot &&
             m_booked == lhs.m_booked &&
             m_inBasket == lhs.m_inBasket &&
             m_isReturning == lhs.m_isReturning );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Tag::operator!=( Tag User& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool Tag::operator!=(const Tag& lhs) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupTagAccess::GroupTagAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
GroupTagAccess::GroupTagAccess()
    : m_id( -1 )
    , m_userGroupId( -1 )
    , m_keyId( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupTagAccess::operator==( const GroupTagAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupTagAccess::operator==(const GroupTagAccess& lhs) const
{
    return ( m_id == lhs.m_id &&
             m_userGroupId == lhs.m_userGroupId &&
             m_keyId == lhs.m_keyId &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     GroupTagAccess::operator!=( GroupTagAccess User& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool GroupTagAccess::operator!=(const GroupTagAccess& lhs) const
{
    return !( *this == lhs );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserTagAccess::UserTagAccess()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UserTagAccess::UserTagAccess()
    : m_id( -1 )
    , m_userId( -1 )
    , m_keyId( -1 )
    , m_isAccessGranted( false )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserTagAccess::operator==( const UserTagAccess& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserTagAccess::operator==(const UserTagAccess& lhs) const
{
    return ( m_id == lhs.m_id &&
             m_userId == lhs.m_userId &&
             m_keyId == lhs.m_keyId &&
             m_isAccessGranted == lhs.m_isAccessGranted );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserTagAccess::operator!=( UserTagAccess User& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool UserTagAccess::operator!=(const UserTagAccess& lhs) const
{
    return !( *this == lhs );
}
