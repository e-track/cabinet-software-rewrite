#ifndef QUERYMANAGER_H
#define QUERYMANAGER_H

// Qt.
#include <QMutex>
#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>

// Project.
#include "cabinet.h"
#include "databasecontrol_global.h"
#include "system.h"

// Forward declarations.
class GroupTagAccess;
class QSqlQuery;
class Tag;
class TagCategory;
class User;
class UserEntry;
class UserGroup;
class UserTagAccess;

////////////////////////////////////////////////////////////////////////////////
/// \brief  A wrapper for SQL queries to facilitate unit tests.
///         TODO: This could be simplified so the controllers pass in the data
///         they need to perform the different types of query (insert, update,
///         remove, select etc).
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT QueryManager : public QObject
{
    Q_OBJECT

public:
    explicit QueryManager( QObject* parent = nullptr );
    ~QueryManager() = default;

    // Initialisation.
    virtual void initDatabase();

    // Login.
    virtual bool loginTempPin( const QString& tempPin );
    virtual bool loginPin( const User& user, const QString& pin );

    // Events.
    virtual bool eventLogLoginSuccess( const User& user );
    virtual bool eventLogLoginFail( const User& user );
    virtual bool eventLogLogout( const User& user );
    virtual bool eventLogAccessDenied( const User& user );
    virtual void eventLogUpdateTag( Tag* tag, const User& user );

    // PIN.
    virtual bool updatePin( const QString& pin, const User& user );
    virtual bool resetTempPin( const User& user );

    // Authentication.
    virtual bool addFingerprint( const User& user, const int fingerprintId );
    virtual bool addCard( const User& user, const QString& cardId );

    // Users.
    virtual bool addUser( const User& user );
    virtual bool updateUser( const User& user );
    virtual bool removeUser( const User& user, const QString& table );
    virtual bool getUser( const int userId );
    virtual bool getUserEntry( const int fingerprintId, const QString& cardId );
    virtual bool getUserProfiles();
    virtual bool getUserProfilePermissions();

    // User groups.
    virtual bool getUsersGroup( const User& user );
    virtual bool getUserGroup( const int id );
    virtual bool addUserGroup( const UserGroup& userGroup );
    virtual bool removeUserGroup( const UserGroup& userGroup, const QString& table );
    virtual bool updateUserGroup( const UserGroup& userGroup );
    virtual bool addUserToUserGroup( const int& userId, const int& userGroupId );
    virtual bool removeUserFromUserGroup( const int& userId, const int& userGroupId );

    // Cabinets.
    virtual bool getRegisteredCabinet();
    virtual bool getUserGroupCabinetAccess( const UserGroup& group );
    virtual bool getUserCabinetAccess( const User& user );

    // Tags / keys.
    virtual bool getTags( const QString& column, const QVariant& value );
    virtual bool addTag( Tag* tag );
    virtual bool removeTag( Tag* tag, const QString& table );

    virtual bool addTagUserPermissions( const UserTagAccess& userTagAccess );
    virtual bool removeTagUserPermissions( const UserTagAccess& userTagAccess );
    virtual bool updateTagUserPermissions( const UserTagAccess& userTagAccess );

    virtual bool addTagGroupPermissions( const GroupTagAccess& groupTagAccess );
    virtual bool removeTagGroupPermissions( const GroupTagAccess& groupTagAccess );
    virtual bool updateTagGroupPermissions( const GroupTagAccess& groupTagAccess );

    virtual bool getUserGroupTagAccess( const UserGroup& user );
    virtual bool getUserTagAccess( const User& user );

    // General SQL queries.
    virtual bool performSqlQuery( const QString& sql );
    virtual int getNumRowsAffected();

    // Value.
    virtual QVariant getValue( const QString& key );
    virtual bool getNext();

    // Systems.
    virtual bool addSystem( const System& system );
    virtual bool getUserGroupSystemAccess( const UserGroup& group );
    virtual bool getUserSystemAccess( const User& user );
    virtual bool getSystem();

    // Timestamp. TODO: Move to utility class / project.
    virtual QString getTimestamp();

    // Database statics.
    DATABASECONTROL_EXPORT static const char* _defaultDatabaseName;

    // Statics for 'entry' table.
    DATABASECONTROL_EXPORT static const int _entryUsePin;
    DATABASECONTROL_EXPORT static const int _entryUseFingerprint;
    DATABASECONTROL_EXPORT static const int _entryUseCard;

    // Statics for 'event_action' table.
    DATABASECONTROL_EXPORT static const int _eventActionLogin;
    DATABASECONTROL_EXPORT static const int _eventActionLoginFail;
    DATABASECONTROL_EXPORT static const int _eventActionLogout;
    DATABASECONTROL_EXPORT static const int _eventActionAccessDenied;
    DATABASECONTROL_EXPORT static const int _eventActionKeyIn;
    DATABASECONTROL_EXPORT static const int _eventActionKeyOut;

    // Factory methods.
    DATABASECONTROL_EXPORT static QString getDeleteQuery( const QString& table, const QString& column );
    DATABASECONTROL_EXPORT static QString getInsertQuery( const QString& table, const QStringList& columns );
    DATABASECONTROL_EXPORT static QString getSelectQuery( const QString& table, const QStringList& columns );
    DATABASECONTROL_EXPORT static QString getSelectAllQuery( const QString& table );
    DATABASECONTROL_EXPORT static QString getUpdateQuery( const QString& table, const QStringList& columns );
    DATABASECONTROL_EXPORT static QString appendWhereClause( const QString& query, const QString& column );
    DATABASECONTROL_EXPORT static QString appendAndClause( const QString& query, const QString& column );

    DATABASECONTROL_EXPORT static QString getBinding( const QString& name );

    // Friendship.
    friend class QueryManagerTest;

    Qt::ConnectionType m_connectionType;

signals:
    // Initialisation.
    void initDatabaseComplete( bool success );

    // Cabinets.
    void getCabinetsComplete( QList<Cabinet*>* cabinets );

    // Tags / keys.
    void searchTagsComplete( bool success );
    void updateTagComplete( Tag* tag, bool success );
    void getTagsForCabinetComplete( QList<QList<Tag*>>* cabinetTags  );
    void getCategoryTagsComplete( QList<QList<Tag*>>* categoryTags  );

    // Tag / key categories.
    void addTagCategoryComplete( TagCategory* tagCategory, bool success );
    void updateTagCategoryComplete( TagCategory* tagCategory, bool success );
    void removeTagCategoryComplete( TagCategory* tagCategory, bool success );
    void getTagCategoriesComplete( QList<TagCategory*>* tagCategories );

    // Events.
    void eventLogUpdateTagComplete( Tag* tag, bool success );

public slots:
    // Database access.
    virtual void openDatabase();
    virtual void closeDatabase();

    // Cabinets.
    virtual void getCabinets();

    // Tags / keys.
    virtual void searchTags( const QString& text );
    virtual void updateTag( Tag* tag );
    virtual void getTagsForCabinet( const int cabinetId, const int groupSize );
    virtual void getCategoryTags( const int groupSize );

    // Tag / key categories.
    virtual void addTagCategory( TagCategory* tagCategory );
    virtual void updateTagCategory( TagCategory* tagCategory );
    virtual void removeTagCategory( TagCategory* tagCategory, const QString& table );
    virtual void getTagCategories();

protected:
    QSqlQuery* m_query;
    QMutex m_mutex;
};

#endif // QUERYMANAGER_H
