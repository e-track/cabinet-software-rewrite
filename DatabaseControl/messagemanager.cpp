// Qt.
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMqttClient>
#include <QMqttSubscription>
#include <QQmlApplicationEngine>

// Project.
#include "constants.h"
#include "databasecontrol.h"
#include "messagemanager.h"

// Topic statics.
const char* const MessageManager::_topicQuery = "query";
const char* const MessageManager::_topic = "e-track/cabinet-1";
const char* const MessageManager::_debugTopic = "cpp/debugging";

// General statics.
const char* const MessageManager::_command = "command";
const char* const MessageManager::_reply = "reply";
const char* const MessageManager::_event = "event";
const char* const MessageManager::_data = "data";
const char* const MessageManager::_packetId = "packet_id";
const char* const MessageManager::_timestamp = "timestamp";
const char* const MessageManager::_result = "result";
const char* const MessageManager::_success = "success";
const char* const MessageManager::_fail = "fail";
const char* const MessageManager::_info = "info";
const char* const MessageManager::_tables = "tables";
const char* const MessageManager::_trailingDataToRemove = "trailing_data_to_remove";

// Command statics.
const char* const MessageManager::_helloWorld = "hello_world";
const char* const MessageManager::_newUser = "new_user";
const char* const MessageManager::_updateUser = "update_user";
const char* const MessageManager::_removeUser = "remove_user";
const char* const MessageManager::_newUserGroup = "new_user_group";
const char* const MessageManager::_updateUserGroup = "update_user_group";
const char* const MessageManager::_removeUserGroup = "remove_user_group";
const char* const MessageManager::_newUserToGroup = "new_user_to_group";
const char* const MessageManager::_removeUserFromGroup = "remove_user_from_group";

const char* const MessageManager::_newKey = "new_key";
const char* const MessageManager::_removeKey = "remove_key";
const char* const MessageManager::_updateKey = "update_key";

const char* const MessageManager::_newKeyUserPermission = "new_key_user_access";
const char* const MessageManager::_removeKeyUserPermission = "remove_key_user_access";
const char* const MessageManager::_updateKeyUserPermission = "update_key_user_access";
const char* const MessageManager::_newKeyGroupPermission = "new_key_usergroup_access";
const char* const MessageManager::_removeKeyGroupPermission = "remove_key_usergroup_access";
const char* const MessageManager::_updateKeyGroupPermission = "update_key_usergroup_access";
const char* const MessageManager::_resetUserPin = "reset_user_pin";

// Event statics.
const char* const MessageManager::_loginSuccess = "login_success";
const char* const MessageManager::_loginFail = "login_failed";
const char* const MessageManager::_logout = "logout";
const char* const MessageManager::_accessDenied = "access_denied";
const char* const MessageManager::_keyIn = "key_in";
const char* const MessageManager::_keyOut = "key_out";

// Statics.
MessageManager* _messageManager = nullptr;

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::MessageManager()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
MessageManager::MessageManager( QObject* parent )
    : QObject( parent )
    , m_client( new QMqttClient( this ) )
    , m_subscription( nullptr )
{
    connect( m_client, &QMqttClient::connected, this, &MessageManager::onConnected );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::MessageManager()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::registerClass()
{
    qmlRegisterSingletonType<MessageManager>( "Message",
                                              1,
                                              0,
                                              "Manager",
                                              []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        return MessageManager::instance();
    });
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::connectToHost( const QString& address, int port )
///
/// \brief  Connects to the given host.
///
/// \param  The hostname of the broker.
/// \param  The port to listen to.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::connectToHost( const QString& address, int port )
{
    qDebug() << QString( "[MessageManager::connectToHost] Attempting to connect to %1, port %2" ).arg( address ).arg( port );

    m_client->setHostname( address );
    m_client->setPort( port );
    m_client->setCleanSession( false );
    m_client->setUsername( "admin" );
    m_client->setPassword( "Keytracker1" );

    m_client->connectToHost();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::disconnectFromHost()
///
/// \brief  Disconnects from the host.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::disconnectFromHost()
{
    m_client->disconnectFromHost();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::subscribe( const QString& topic )
///
/// \brief  Subscribes to the given topic.
///
/// \param  The name of the topic to be subscribed to.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::subscribe( const QString& topic )
{
    if ( !m_subscription )
    {
        m_subscription = m_client->subscribe( topic, 1 );

        if ( m_subscription  )
        {
            qDebug() << QString( "[MessageManager::subscribe] Subscription state: %1" ).arg( m_subscription->state() );

            connect( m_subscription, &QMqttSubscription::stateChanged, this, &MessageManager::onSubscriptionStateChanged );
        }
    }
    else
    {
        qWarning() << QString( "[MessageManager::subscribe] Attemping to subscribe when the subscription already exists." );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::publish( const QString& topic, const QString& message )
///
/// \brief  Publishes the given message with its corresponding topic.
///
/// \param  The message to be published.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::publish( const QString& message )
{
    qDebug() << QString( "[MessageManager::publish] Attempting to publish: %1" ).arg( message );

    m_client->publish( QMqttTopicName( _debugTopic ), message.toLatin1(), 1, true );

    qDebug() << QString( "[MessageManager::publish] Error: %1" ).arg( m_client->error() );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::simulateEvent( const QString& event )
///
/// \brief  Publishes the selected event with a test message body.
///
/// \param  The event to be simulated.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::simulateEvent( const QString& event )
{
    // TODO: Re-factor JSON message construction into separate functions for each event type.

    if ( event == MessageManager::_loginSuccess )
    {
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = 1;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginSuccess;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
    else if ( event == MessageManager::_loginFail )
    {
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = 1;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_loginFail;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
    else if ( event == MessageManager::_logout )
    {
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = 1;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_logout;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
    else if ( event == MessageManager::_accessDenied )
    {
        QJsonObject jsonUser;
        jsonUser[DatabaseControlConstants::_userId] = 1;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] = MessageManager::_accessDenied;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonUser;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
    else if ( event == MessageManager::_keyIn )
    {
        QJsonObject jsonTag;
        jsonTag[Tag::_userId] = 1;
        jsonTag[Tag::_cabinetId] = 1;
        jsonTag[Tag::_slot] = 10;
        jsonTag[Tag::_currentSlot] = 10;
        jsonTag[Tag::_fob] = 1234567890;
        jsonTag[Tag::_isOut] = false;
        jsonTag[Tag::_isWrongSlot] = false;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] =  MessageManager::_keyIn;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonTag;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
    else if ( event == MessageManager::_keyOut )
    {
        QJsonObject jsonTag;
        jsonTag[Tag::_userId] = 1;
        jsonTag[Tag::_cabinetId] = 1;
        jsonTag[Tag::_slot] = 20;
        jsonTag[Tag::_currentSlot] = 20;
        jsonTag[Tag::_fob] = 1234567890;
        jsonTag[Tag::_isOut] = false;
        jsonTag[Tag::_isWrongSlot] = false;

        QJsonObject jsonEvent;
        jsonEvent[MessageManager::_event] =  MessageManager::_keyOut;
        jsonEvent[MessageManager::_timestamp] = getTimestamp();
        jsonEvent[MessageManager::_data] = jsonTag;

        QJsonDocument jsonDoc( jsonEvent );
        QString event = jsonDoc.toJson( QJsonDocument::Indented );

        publish( event );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::onMessageReceived( QMqttMessage message )
///
/// \brief  Handles message received from a subscription.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::onMessageReceived( QMqttMessage message )
{
    qDebug() << QString( "[MessageManager::onMessageReceived] Message received: %1" ).arg( message.payload() );
    qDebug() << QString( "[MessageManager::onMessageReceived] Message ID: %1" ).arg( message.id() );

    emit payloadReceived( message.payload() );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::onConnected()
///
/// \brief  Handles successful connection.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::onConnected()
{
    qDebug() << QString( "[MessageManager::onConnected] Connected to %1, port %2" ).arg( m_client->hostname() ).arg( m_client->port() );

    emit connected();

    subscribe( _debugTopic );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::onConnected()
///
/// \brief  Handles successful connection.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::onSubscriptionStateChanged()
{
    QMqttSubscription::SubscriptionState state = m_subscription->state();

    qDebug() << QString( "[MessageManager::onSubscriptionStateChanged] %1" ).arg( state );

    switch ( state )
    {
        case QMqttSubscription::Subscribed:
        {
            connect( m_subscription, &QMqttSubscription::messageReceived, this, &MessageManager::onMessageReceived );
            break;
        }

        default:
        {
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::stringToJson( const QString& json, QJsonObject& jsonObject )
///
/// \brief  Converts the given string to a JSON object.
///
/// \param  The string to be converted to JSON.
/// \param  The JSON object represented by the string.
///
/// \return True if the conversion was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MessageManager::convertStringToJson( const QString& json, QJsonObject& jsonObject )
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson( json.toUtf8() );

    bool success = false;

    if( !jsonDoc.isNull() )
    {
        if( jsonDoc.isObject() )
        {
            jsonObject = jsonDoc.object();

            success = true;
        }
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::instance()
///
/// \brief  Returns a static instance of MessageManager.
///
/// \return A MessageManager instance.
////////////////////////////////////////////////////////////////////////////////
MessageManager* MessageManager::instance()
{
    if ( !_messageManager )
    {
        _messageManager = new MessageManager();
    }

    return _messageManager;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::destroy()
///
/// \brief  Destroys the MessageManager instance.
////////////////////////////////////////////////////////////////////////////////
void MessageManager::destroy()
{
    if ( _messageManager )
    {
        delete _messageManager;
        _messageManager = nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManager::getTimestamp()
///
/// \brief  Returns the current timestamp. TODO: Move this to a utility class /
///         project as it will be used elsewhere.
///
/// \return The current timestamp.
////////////////////////////////////////////////////////////////////////////////
QString MessageManager::getTimestamp()
{
    return QDateTime::currentDateTimeUtc().toString();
}
