#ifndef USERGROUP_H
#define USERGROUP_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the user_group database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT UserGroup
{
    Q_GADGET
    Q_PROPERTY( int id            MEMBER m_id )
    Q_PROPERTY( int keyLimit      MEMBER m_keyLimit )
    Q_PROPERTY( QString groupName MEMBER m_groupName )

public:
    explicit UserGroup();
    ~UserGroup() = default;

    int m_id;
    int m_keyLimit;
    QString m_groupName;

    bool operator==( const UserGroup& lhs );
    bool operator!=( const UserGroup& lhs );

    // Contants.
    static const char* const _userGroupTable;
    static const char* const _userToGroupTable;
    static const char* const _userId;
    static const char* const _userGroup;
    static const char* const _groupName;
    static const char* const _keyLimit;
};

#endif // USERGROUP_H
