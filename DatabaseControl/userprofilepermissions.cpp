// Project.
#include "userprofilepermissions.h"

// Statics.
const char* UserProfilePermissions::_id = "id";
const char* UserProfilePermissions::_permissionName = "permission_name";
const char* UserProfilePermissions::_accessUsers = "accessUsers";
const char* UserProfilePermissions::_manageUsers = "manageUsers";
const char* UserProfilePermissions::_accessUserGroups = "accessUserGroups";
const char* UserProfilePermissions::_manageUserGroups = "manageUserGroups";
const char* UserProfilePermissions::_accessKeys = "accessKeys";
const char* UserProfilePermissions::_manageKeys = "manageKeys";
const char* UserProfilePermissions::_accessSettings = "accessSettings";
const char* UserProfilePermissions::_accessReports = "accessReports";
const char* UserProfilePermissions::_accessSystems = "accessSystems";
const char* UserProfilePermissions::_accessBookings = "accessBookings";
const char* UserProfilePermissions::_makeBooking = "makeBooking";
const char* UserProfilePermissions::_manageBooking = "manageBooking";
const char* UserProfilePermissions::_isHandover = "isHandover";
const char* UserProfilePermissions::_autoApprove = "autoApprove";
const char* UserProfilePermissions::_hasGlobalSearch = "hasGlobalSearch";
const char* UserProfilePermissions::_webLogin = "webLogin";
const char* UserProfilePermissions::_manageRoles = "manageRoles";

QMap<QString, unsigned int> UserProfilePermissions::_map;
