-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 07, 2022 at 09:28 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cabinets`
--

DROP TABLE IF EXISTS `tbl_cabinets`;
CREATE TABLE IF NOT EXISTS `tbl_cabinets` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_cabinets',
  `parent_system` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `cabinet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `com_port` int(11) NOT NULL,
  `slot_capacity` int(11) NOT NULL,
  `start_slot` int(11) NOT NULL,
  `end_slot` int(11) NOT NULL,
  `slot_width` int(11) NOT NULL,
  `no_of_locks` int(11) NOT NULL,
  `multi_factor` tinyint(1) NOT NULL,
  `card_reader` tinyint(1) NOT NULL,
  `biometric` tinyint(1) NOT NULL,
  `is_dynamic` tinyint(1) NOT NULL,
  `peg_width` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_871077AF865ED24D` (`parent_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_cabinets`
--

INSERT INTO `tbl_cabinets` (`id`, `parent_system`, `cabinet_name`, `com_port`, `slot_capacity`, `start_slot`, `end_slot`, `slot_width`, `no_of_locks`, `multi_factor`, `card_reader`, `biometric`, `is_dynamic`, `peg_width`) VALUES
(1, 1, 'First Cabinet', 334, 60, 1, 60, 20, 2, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entry_types`
--

DROP TABLE IF EXISTS `tbl_entry_types`;
CREATE TABLE IF NOT EXISTS `tbl_entry_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_entry_types',
  `entry_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_entry_types`
--

INSERT INTO `tbl_entry_types` (`id`, `entry_type`) VALUES
(1, 'usePin'),
(2, 'useFingerprint'),
(3, 'useCard');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_log`
--

DROP TABLE IF EXISTS `tbl_events_log`;
CREATE TABLE IF NOT EXISTS `tbl_events_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_events_log',
  `event_action` int(11) DEFAULT NULL COMMENT 'tbl_event_actions',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `event_message` int(11) DEFAULT NULL COMMENT 'tbl_event_messages',
  `parent_system` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `event_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B0EDB5007555FBC0` (`event_action`),
  KEY `IDX_B0EDB500A76ED395` (`user_id`),
  KEY `IDX_B0EDB50033EA99D0` (`event_message`),
  KEY `IDX_B0EDB500865ED24D` (`parent_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_actions`
--

DROP TABLE IF EXISTS `tbl_event_actions`;
CREATE TABLE IF NOT EXISTS `tbl_event_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_event_actions',
  `action_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_event_actions`
--

INSERT INTO `tbl_event_actions` (`id`, `action_type`) VALUES
(1, 'login_success'),
(2, 'login_failed'),
(3, 'logout'),
(4, 'fob_removed'),
(5, 'fob_returned'),
(6, 'key_in'),
(7, 'key_out'),
(8, 'door_opened'),
(9, 'door_closed'),
(10, 'door_timeout'),
(11, 'link_eFob'),
(12, 'parking_bay_update'),
(13, 'item_in'),
(14, 'item_out'),
(15, 'controller_manual_override'),
(16, 'key_forced'),
(17, 'unauthorised_deposit'),
(18, 'access_denied');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_messages`
--

DROP TABLE IF EXISTS `tbl_event_messages`;
CREATE TABLE IF NOT EXISTS `tbl_event_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_event_messages',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `handover_user` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `entry_type` int(11) DEFAULT NULL COMMENT 'tbl_entry_types',
  `key_position` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `incorrect_access_value` tinyint(1) NOT NULL DEFAULT '0',
  `invalid_access_time` tinyint(1) NOT NULL DEFAULT '0',
  `wrong_slot_detected` tinyint(1) NOT NULL DEFAULT '0',
  `removed_from_wrong_slot` tinyint(1) NOT NULL DEFAULT '0',
  `unknown_key_detected` tinyint(1) NOT NULL DEFAULT '0',
  `key_in_unassigned_slot` tinyint(1) NOT NULL DEFAULT '0',
  `key_out_of_unassigned_slot` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_A25BCABD145533` (`key_id`),
  KEY `IDX_A25BCAB52C83E47` (`handover_user`),
  KEY `IDX_A25BCAB28FC2F3A` (`entry_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_cabinet_access`
--

DROP TABLE IF EXISTS `tbl_group_cabinet_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_cabinet_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_cabinet_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CA00992B8F02BF9D` (`user_group`),
  KEY `IDX_CA00992B4CED05B0` (`cabinet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_group_cabinet_access`
--

INSERT INTO `tbl_group_cabinet_access` (`id`, `user_group`, `cabinet`, `is_access_granted`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_location_access`
--

DROP TABLE IF EXISTS `tbl_group_location_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_location_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_location_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A3203208F02BF9D` (`user_group`),
  KEY `IDX_4A3203205E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_group_location_access`
--

INSERT INTO `tbl_group_location_access` (`id`, `user_group`, `location`, `is_access_granted`) VALUES
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_site_access`
--

DROP TABLE IF EXISTS `tbl_group_site_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_site_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_site_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ED7E54588F02BF9D` (`user_group`),
  KEY `IDX_ED7E5458694309E4` (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_group_site_access`
--

INSERT INTO `tbl_group_site_access` (`id`, `user_group`, `site`, `is_access_granted`) VALUES
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_system_access`
--

DROP TABLE IF EXISTS `tbl_group_system_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_system_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_system_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `system_id` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76A5E9F98F02BF9D` (`user_group`),
  KEY `IDX_76A5E9F9D0952FA5` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_group_system_access`
--

INSERT INTO `tbl_group_system_access` (`id`, `user_group`, `system_id`, `is_access_granted`) VALUES
(2, 1, 1, 1),
(3, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_to_key_access`
--

DROP TABLE IF EXISTS `tbl_group_to_key_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_to_key_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_to_key_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `is_access_granted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A215F198F02BF9D` (`user_group`),
  KEY `IDX_4A215F19D145533` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_group_to_key_access`
--

INSERT INTO `tbl_group_to_key_access` (`id`, `user_group`, `key_id`, `is_access_granted`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_to_key_category_access`
--

DROP TABLE IF EXISTS `tbl_group_to_key_category_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_to_key_category_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_to_key_category_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1386B1C8F02BF9D` (`user_group`),
  KEY `IDX_1386B1CC74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keys`
--

DROP TABLE IF EXISTS `tbl_keys`;
CREATE TABLE IF NOT EXISTS `tbl_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_keys',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `key_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_description` longtext COLLATE utf8mb4_unicode_ci,
  `key_serial` longtext COLLATE utf8mb4_unicode_ci,
  `slot` int(11) NOT NULL,
  `current_slot` int(11) DEFAULT NULL,
  `fob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_static` tinyint(1) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `is_out` tinyint(1) NOT NULL,
  `is_wrong_slot` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1AE11044CED05B0` (`cabinet`),
  KEY `IDX_1AE1104A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_keys`
--

INSERT INTO `tbl_keys` (`id`, `cabinet`, `user_id`, `category`, `key_name`, `key_description`, `key_serial`, `slot`, `current_slot`, `fob`, `is_static`, `date_added`, `start_time`, `end_time`, `is_out`, `is_wrong_slot`) VALUES
(1,   1, 1,    1, 'Key 001', 'KEY_DESCRIPTION_001', 'KEY_SERIAL_001',   1,   1,   1, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(2,   1, 1,    1, 'Key 002', 'KEY_DESCRIPTION_002', 'KEY_SERIAL_002',   2,   2,   2, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(3,   1, 1,    1, 'Key 003', 'KEY_DESCRIPTION_003', 'KEY_SERIAL_003',   3,   3,   3, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(4,   1, 1,    1, 'Key 004', 'KEY_DESCRIPTION_004', 'KEY_SERIAL_004',   4,   4,   4, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(5,   1, 1,    1, 'Key 005', 'KEY_DESCRIPTION_005', 'KEY_SERIAL_005',   5,   5,   5, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(6,   1, 1,    1, 'Key 006', 'KEY_DESCRIPTION_006', 'KEY_SERIAL_006',   6,   6,   6, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(7,   1, 1,    1, 'Key 007', 'KEY_DESCRIPTION_007', 'KEY_SERIAL_007',   7,   7,   7, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 1, 0 ),
(8,   1, NULL, 1, 'Key 008', 'KEY_DESCRIPTION_008', 'KEY_SERIAL_008',   8,   8,   8, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 1 ),
(9,   1, NULL, 1, 'Key 009', 'KEY_DESCRIPTION_009', 'KEY_SERIAL_009',   9,   9,   9, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 1 ),
(10,  1, NULL, 1, 'Key 010', 'KEY_DESCRIPTION_010', 'KEY_SERIAL_010',  10,  10,  10, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(11,  1, NULL, 1, 'Key 011', 'KEY_DESCRIPTION_011', 'KEY_SERIAL_011',  11,  11,  11, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(12,  1, NULL, 1, 'Key 012', 'KEY_DESCRIPTION_012', 'KEY_SERIAL_012',  12,  12,  12, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(13,  1, NULL, 1, 'Key 013', 'KEY_DESCRIPTION_013', 'KEY_SERIAL_013',  13,  13,  13, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(14,  1, NULL, 1, 'Key 014', 'KEY_DESCRIPTION_014', 'KEY_SERIAL_014',  14,  14,  14, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(15,  1, NULL, 1, 'Key 015', 'KEY_DESCRIPTION_015', 'KEY_SERIAL_015',  15,  15,  15, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(16,  1, NULL, 1, 'Key 016', 'KEY_DESCRIPTION_016', 'KEY_SERIAL_016',  16,  16,  16, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(17,  1, NULL, 1, 'Key 017', 'KEY_DESCRIPTION_017', 'KEY_SERIAL_017',  17,  17,  17, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(18,  1, NULL, 1, 'Key 018', 'KEY_DESCRIPTION_018', 'KEY_SERIAL_018',  18,  18,  18, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(19,  1, NULL, 1, 'Key 019', 'KEY_DESCRIPTION_019', 'KEY_SERIAL_019',  19,  19,  19, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(20,  1, NULL, 1, 'Key 020', 'KEY_DESCRIPTION_020', 'KEY_SERIAL_020',  20,  20,  20, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(21,  1, NULL, 1, 'Key 021', 'KEY_DESCRIPTION_021', 'KEY_SERIAL_021',  21,  21,  21, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(22,  1, NULL, 1, 'Key 022', 'KEY_DESCRIPTION_022', 'KEY_SERIAL_022',  22,  22,  22, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(23,  1, NULL, 1, 'Key 023', 'KEY_DESCRIPTION_023', 'KEY_SERIAL_023',  23,  23,  23, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(24,  1, NULL, 1, 'Key 024', 'KEY_DESCRIPTION_024', 'KEY_SERIAL_024',  24,  24,  24, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(25,  1, NULL, 1, 'Key 025', 'KEY_DESCRIPTION_025', 'KEY_SERIAL_025',  25,  25,  25, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(26,  1, NULL, 1, 'Key 026', 'KEY_DESCRIPTION_026', 'KEY_SERIAL_026',  26,  26,  26, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(27,  1, NULL, 2, 'Key 027', 'KEY_DESCRIPTION_027', 'KEY_SERIAL_027',  27,  27,  27, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(28,  1, NULL, 2, 'Key 028', 'KEY_DESCRIPTION_028', 'KEY_SERIAL_028',  28,  28,  28, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(29,  1, NULL, 2, 'Key 029', 'KEY_DESCRIPTION_029', 'KEY_SERIAL_029',  29,  29,  29, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(30,  1, NULL, 2, 'Key 030', 'KEY_DESCRIPTION_030', 'KEY_SERIAL_030',  30,  30,  30, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(31,  1, NULL, 2, 'Key 031', 'KEY_DESCRIPTION_031', 'KEY_SERIAL_031',  31,  31,  31, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(32,  1, NULL, 2, 'Key 032', 'KEY_DESCRIPTION_032', 'KEY_SERIAL_032',  32,  32,  32, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(33,  1, NULL, 2, 'Key 033', 'KEY_DESCRIPTION_033', 'KEY_SERIAL_033',  33,  33,  33, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(34,  1, NULL, 2, 'Key 034', 'KEY_DESCRIPTION_034', 'KEY_SERIAL_034',  34,  34,  34, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(35,  1, NULL, 2, 'Key 035', 'KEY_DESCRIPTION_035', 'KEY_SERIAL_035',  35,  35,  35, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(36,  1, NULL, 2, 'Key 036', 'KEY_DESCRIPTION_036', 'KEY_SERIAL_036',  36,  36,  36, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(37,  1, NULL, 2, 'Key 037', 'KEY_DESCRIPTION_037', 'KEY_SERIAL_037',  37,  37,  37, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(38,  1, NULL, 2, 'Key 038', 'KEY_DESCRIPTION_038', 'KEY_SERIAL_038',  38,  38,  38, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(39,  1, NULL, 2, 'Key 039', 'KEY_DESCRIPTION_039', 'KEY_SERIAL_039',  39,  39,  39, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(40,  1, NULL, 2, 'Key 040', 'KEY_DESCRIPTION_040', 'KEY_SERIAL_040',  40,  40,  40, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(41,  1, NULL, 2, 'Key 041', 'KEY_DESCRIPTION_041', 'KEY_SERIAL_041',  41,  41,  41, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(42,  1, NULL, 2, 'Key 042', 'KEY_DESCRIPTION_042', 'KEY_SERIAL_042',  42,  42,  42, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(43,  1, NULL, 2, 'Key 043', 'KEY_DESCRIPTION_043', 'KEY_SERIAL_043',  43,  43,  43, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(44,  1, NULL, 2, 'Key 044', 'KEY_DESCRIPTION_044', 'KEY_SERIAL_044',  44,  44,  44, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(45,  1, NULL, 2, 'Key 045', 'KEY_DESCRIPTION_045', 'KEY_SERIAL_045',  45,  45,  45, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(46,  1, NULL, 2, 'Key 046', 'KEY_DESCRIPTION_046', 'KEY_SERIAL_046',  46,  46,  46, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(47,  1, NULL, 2, 'Key 047', 'KEY_DESCRIPTION_047', 'KEY_SERIAL_047',  47,  47,  47, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(48,  1, NULL, 2, 'Key 048', 'KEY_DESCRIPTION_048', 'KEY_SERIAL_048',  48,  48,  48, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(49,  1, NULL, 2, 'Key 049', 'KEY_DESCRIPTION_049', 'KEY_SERIAL_049',  49,  49,  49, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(50,  1, NULL, 2, 'Key 050', 'KEY_DESCRIPTION_050', 'KEY_SERIAL_050',  50,  50,  50, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(51,  1, NULL, 2, 'Key 051', 'KEY_DESCRIPTION_051', 'KEY_SERIAL_051',  51,  51,  51, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(52,  1, NULL, 2, 'Key 052', 'KEY_DESCRIPTION_052', 'KEY_SERIAL_052',  52,  52,  52, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(53,  1, NULL, 2, 'Key 053', 'KEY_DESCRIPTION_053', 'KEY_SERIAL_053',  53,  53,  53, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(54,  1, NULL, 2, 'Key 054', 'KEY_DESCRIPTION_054', 'KEY_SERIAL_054',  54,  54,  54, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(55,  1, NULL, 2, 'Key 055', 'KEY_DESCRIPTION_055', 'KEY_SERIAL_055',  55,  55,  55, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(56,  1, NULL, 2, 'Key 056', 'KEY_DESCRIPTION_056', 'KEY_SERIAL_056',  56,  56,  56, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(57,  1, NULL, 2, 'Key 057', 'KEY_DESCRIPTION_057', 'KEY_SERIAL_057',  57,  57,  57, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(58,  1, NULL, 3, 'Key 058', 'KEY_DESCRIPTION_058', 'KEY_SERIAL_058',  58,  58,  58, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(59,  1, NULL, 3, 'Key 059', 'KEY_DESCRIPTION_059', 'KEY_SERIAL_059',  59,  59,  59, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(60,  1, NULL, 3, 'Key 060', 'KEY_DESCRIPTION_060', 'KEY_SERIAL_060',  60,  60,  60, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(61,  1, NULL, 3, 'Key 061', 'KEY_DESCRIPTION_061', 'KEY_SERIAL_061',  61,  61,  61, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(62,  1, NULL, 3, 'Key 062', 'KEY_DESCRIPTION_062', 'KEY_SERIAL_062',  62,  62,  62, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(63,  1, NULL, 3, 'Key 063', 'KEY_DESCRIPTION_063', 'KEY_SERIAL_063',  63,  63,  63, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(64,  1, NULL, 3, 'Key 064', 'KEY_DESCRIPTION_064', 'KEY_SERIAL_064',  64,  64,  64, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(65,  1, NULL, 3, 'Key 065', 'KEY_DESCRIPTION_065', 'KEY_SERIAL_065',  65,  65,  65, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(66,  1, NULL, 3, 'Key 066', 'KEY_DESCRIPTION_066', 'KEY_SERIAL_066',  66,  66,  66, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(67,  1, NULL, 3, 'Key 067', 'KEY_DESCRIPTION_067', 'KEY_SERIAL_067',  67,  67,  67, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(68,  1, NULL, 3, 'Key 068', 'KEY_DESCRIPTION_068', 'KEY_SERIAL_068',  68,  68,  68, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(69,  1, NULL, 3, 'Key 069', 'KEY_DESCRIPTION_069', 'KEY_SERIAL_069',  69,  69,  69, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(70,  1, NULL, 3, 'Key 070', 'KEY_DESCRIPTION_070', 'KEY_SERIAL_070',  70,  70,  70, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(71,  1, NULL, 3, 'Key 071', 'KEY_DESCRIPTION_071', 'KEY_SERIAL_071',  71,  71,  71, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(72,  1, NULL, 3, 'Key 072', 'KEY_DESCRIPTION_072', 'KEY_SERIAL_072',  72,  72,  72, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(73,  1, NULL, 3, 'Key 073', 'KEY_DESCRIPTION_073', 'KEY_SERIAL_073',  73,  73,  73, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(74,  1, NULL, 3, 'Key 074', 'KEY_DESCRIPTION_074', 'KEY_SERIAL_074',  74,  74,  74, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(75,  1, NULL, 3, 'Key 075', 'KEY_DESCRIPTION_075', 'KEY_SERIAL_075',  75,  75,  75, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(76,  1, NULL, 3, 'Key 076', 'KEY_DESCRIPTION_076', 'KEY_SERIAL_076',  76,  76,  76, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(77,  1, NULL, 3, 'Key 077', 'KEY_DESCRIPTION_077', 'KEY_SERIAL_077',  77,  77,  77, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(78,  1, NULL, 3, 'Key 078', 'KEY_DESCRIPTION_078', 'KEY_SERIAL_078',  78,  78,  78, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(79,  1, NULL, 3, 'Key 079', 'KEY_DESCRIPTION_079', 'KEY_SERIAL_079',  79,  79,  79, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(80,  1, NULL, 3, 'Key 080', 'KEY_DESCRIPTION_080', 'KEY_SERIAL_080',  80,  80,  80, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(81,  1, NULL, 3, 'Key 081', 'KEY_DESCRIPTION_081', 'KEY_SERIAL_081',  81,  81,  81, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(82,  1, NULL, 3, 'Key 082', 'KEY_DESCRIPTION_082', 'KEY_SERIAL_082',  82,  82,  82, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(83,  1, NULL, 3, 'Key 083', 'KEY_DESCRIPTION_083', 'KEY_SERIAL_083',  83,  83,  83, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(84,  1, NULL, 3, 'Key 084', 'KEY_DESCRIPTION_084', 'KEY_SERIAL_084',  84,  84,  84, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(85,  1, NULL, 3, 'Key 085', 'KEY_DESCRIPTION_085', 'KEY_SERIAL_085',  85,  85,  85, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(86,  1, NULL, 4, 'Key 086', 'KEY_DESCRIPTION_086', 'KEY_SERIAL_086',  86,  86,  86, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(87,  1, NULL, 4, 'Key 087', 'KEY_DESCRIPTION_087', 'KEY_SERIAL_087',  87,  87,  87, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(88,  1, NULL, 4, 'Key 088', 'KEY_DESCRIPTION_088', 'KEY_SERIAL_088',  88,  88,  88, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(89,  1, NULL, 4, 'Key 089', 'KEY_DESCRIPTION_089', 'KEY_SERIAL_089',  89,  89,  89, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(90,  1, NULL, 4, 'Key 090', 'KEY_DESCRIPTION_090', 'KEY_SERIAL_090',  90,  90,  90, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(91,  1, NULL, 4, 'Key 091', 'KEY_DESCRIPTION_091', 'KEY_SERIAL_091',  91,  91,  91, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(92,  1, NULL, 4, 'Key 092', 'KEY_DESCRIPTION_092', 'KEY_SERIAL_092',  92,  92,  92, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(93,  1, NULL, 4, 'Key 093', 'KEY_DESCRIPTION_093', 'KEY_SERIAL_093',  93,  93,  93, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(94,  1, NULL, 4, 'Key 094', 'KEY_DESCRIPTION_094', 'KEY_SERIAL_094',  94,  94,  94, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(95,  1, NULL, 4, 'Key 095', 'KEY_DESCRIPTION_095', 'KEY_SERIAL_095',  95,  95,  95, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(96,  1, NULL, 4, 'Key 096', 'KEY_DESCRIPTION_096', 'KEY_SERIAL_096',  96,  96,  96, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(97,  1, NULL, 4, 'Key 097', 'KEY_DESCRIPTION_097', 'KEY_SERIAL_097',  97,  97,  97, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(98,  1, NULL, 4, 'Key 098', 'KEY_DESCRIPTION_098', 'KEY_SERIAL_098',  98,  98,  98, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(99,  1, NULL, 4, 'Key 099', 'KEY_DESCRIPTION_099', 'KEY_SERIAL_099',  99,  99,  99, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(100, 1, NULL, 4, 'Key 100', 'KEY_DESCRIPTION_100', 'KEY_SERIAL_100', 100, 100, 100, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(101, 1, NULL, 4, 'Key 101', 'KEY_DESCRIPTION_101', 'KEY_SERIAL_101',   1,   1, 101, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(102, 1, NULL, 4, 'Key 102', 'KEY_DESCRIPTION_102', 'KEY_SERIAL_102',   2,   2, 102, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(103, 1, NULL, 4, 'Key 103', 'KEY_DESCRIPTION_103', 'KEY_SERIAL_103',   3,   3, 103, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(104, 1, NULL, 4, 'Key 104', 'KEY_DESCRIPTION_104', 'KEY_SERIAL_104',   4,   4, 104, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(105, 1, NULL, 4, 'Key 105', 'KEY_DESCRIPTION_105', 'KEY_SERIAL_105',   5,   5, 105, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(106, 1, NULL, 4, 'Key 106', 'KEY_DESCRIPTION_106', 'KEY_SERIAL_106',   6,   6, 106, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(107, 1, NULL, 4, 'Key 107', 'KEY_DESCRIPTION_107', 'KEY_SERIAL_107',   7,   7, 107, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(108, 1, NULL, 4, 'Key 108', 'KEY_DESCRIPTION_108', 'KEY_SERIAL_108',   8,   8, 108, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(109, 1, NULL, 4, 'Key 109', 'KEY_DESCRIPTION_109', 'KEY_SERIAL_109',   9,   9, 109, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(110, 1, NULL, 4, 'Key 110', 'KEY_DESCRIPTION_110', 'KEY_SERIAL_110',  10,  10, 110, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(111, 1, NULL, 4, 'Key 111', 'KEY_DESCRIPTION_111', 'KEY_SERIAL_111',  11,  11, 111, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(112, 1, NULL, 4, 'Key 112', 'KEY_DESCRIPTION_112', 'KEY_SERIAL_112',  12,  12, 112, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(113, 1, NULL, 4, 'Key 113', 'KEY_DESCRIPTION_113', 'KEY_SERIAL_113',  13,  13, 113, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(114, 1, NULL, 4, 'Key 114', 'KEY_DESCRIPTION_114', 'KEY_SERIAL_114',  14,  14, 114, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(115, 1, NULL, 4, 'Key 115', 'KEY_DESCRIPTION_115', 'KEY_SERIAL_115',  15,  15, 115, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(116, 1, NULL, 4, 'Key 116', 'KEY_DESCRIPTION_116', 'KEY_SERIAL_116',  16,  16, 116, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(117, 1, NULL, 4, 'Key 117', 'KEY_DESCRIPTION_117', 'KEY_SERIAL_117',  17,  17, 117, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(118, 1, NULL, 4, 'Key 118', 'KEY_DESCRIPTION_118', 'KEY_SERIAL_118',  18,  18, 118, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(119, 1, NULL, 4, 'Key 119', 'KEY_DESCRIPTION_119', 'KEY_SERIAL_119',  19,  19, 119, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(120, 1, NULL, 4, 'Key 120', 'KEY_DESCRIPTION_120', 'KEY_SERIAL_120',  20,  20, 120, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(121, 1, NULL, 4, 'Key 121', 'KEY_DESCRIPTION_121', 'KEY_SERIAL_121',  21,  21, 121, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(122, 1, NULL, 4, 'Key 122', 'KEY_DESCRIPTION_122', 'KEY_SERIAL_122',  22,  22, 122, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(123, 1, NULL, 4, 'Key 123', 'KEY_DESCRIPTION_123', 'KEY_SERIAL_123',  23,  23, 123, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(124, 1, NULL, 4, 'Key 124', 'KEY_DESCRIPTION_124', 'KEY_SERIAL_124',  24,  24, 124, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(125, 1, NULL, 4, 'Key 125', 'KEY_DESCRIPTION_125', 'KEY_SERIAL_125',  25,  25, 125, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(126, 1, NULL, 4, 'Key 126', 'KEY_DESCRIPTION_126', 'KEY_SERIAL_126',  26,  26, 126, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(127, 1, NULL, 4, 'Key 127', 'KEY_DESCRIPTION_127', 'KEY_SERIAL_127',  27,  27, 127, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(128, 1, NULL, 4, 'Key 128', 'KEY_DESCRIPTION_128', 'KEY_SERIAL_128',  28,  28, 128, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(129, 1, NULL, 4, 'Key 129', 'KEY_DESCRIPTION_129', 'KEY_SERIAL_129',  29,  29, 129, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(130, 1, NULL, 4, 'Key 130', 'KEY_DESCRIPTION_130', 'KEY_SERIAL_130',  30,  30, 130, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(131, 1, NULL, 4, 'Key 131', 'KEY_DESCRIPTION_131', 'KEY_SERIAL_131',  31,  31, 131, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(132, 1, NULL, 4, 'Key 132', 'KEY_DESCRIPTION_132', 'KEY_SERIAL_132',  32,  32, 132, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(133, 1, NULL, 4, 'Key 133', 'KEY_DESCRIPTION_133', 'KEY_SERIAL_133',  33,  33, 133, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(134, 1, NULL, 4, 'Key 134', 'KEY_DESCRIPTION_134', 'KEY_SERIAL_134',  34,  34, 134, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(135, 1, NULL, 4, 'Key 135', 'KEY_DESCRIPTION_135', 'KEY_SERIAL_135',  35,  35, 135, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(136, 1, NULL, 4, 'Key 136', 'KEY_DESCRIPTION_136', 'KEY_SERIAL_136',  36,  36, 136, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(137, 1, NULL, 4, 'Key 137', 'KEY_DESCRIPTION_137', 'KEY_SERIAL_137',  37,  37, 137, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(138, 1, NULL, 4, 'Key 138', 'KEY_DESCRIPTION_138', 'KEY_SERIAL_138',  38,  38, 138, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(139, 1, NULL, 4, 'Key 139', 'KEY_DESCRIPTION_139', 'KEY_SERIAL_139',  39,  39, 139, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(140, 1, NULL, 4, 'Key 140', 'KEY_DESCRIPTION_140', 'KEY_SERIAL_140',  40,  40, 140, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(141, 1, NULL, 4, 'Key 141', 'KEY_DESCRIPTION_141', 'KEY_SERIAL_141',  41,  41, 141, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(142, 1, NULL, 4, 'Key 142', 'KEY_DESCRIPTION_142', 'KEY_SERIAL_142',  42,  42, 142, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(143, 1, NULL, 4, 'Key 143', 'KEY_DESCRIPTION_143', 'KEY_SERIAL_143',  43,  43, 143, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(144, 1, NULL, 4, 'Key 144', 'KEY_DESCRIPTION_144', 'KEY_SERIAL_144',  44,  44, 144, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(145, 1, NULL, 5, 'Key 145', 'KEY_DESCRIPTION_145', 'KEY_SERIAL_145',  45,  45, 145, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(146, 1, NULL, 5, 'Key 146', 'KEY_DESCRIPTION_146', 'KEY_SERIAL_146',  46,  46, 146, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(147, 1, NULL, 5, 'Key 147', 'KEY_DESCRIPTION_147', 'KEY_SERIAL_147',  47,  47, 147, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(148, 1, NULL, 5, 'Key 148', 'KEY_DESCRIPTION_148', 'KEY_SERIAL_148',  48,  48, 148, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(149, 1, NULL, 5, 'Key 149', 'KEY_DESCRIPTION_149', 'KEY_SERIAL_149',  49,  49, 149, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(150, 1, NULL, 5, 'Key 150', 'KEY_DESCRIPTION_150', 'KEY_SERIAL_150',  50,  50, 150, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(151, 1, NULL, 5, 'Key 151', 'KEY_DESCRIPTION_151', 'KEY_SERIAL_151',  51,  51, 151, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(152, 1, NULL, 5, 'Key 152', 'KEY_DESCRIPTION_152', 'KEY_SERIAL_152',  52,  52, 152, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(153, 1, NULL, 5, 'Key 153', 'KEY_DESCRIPTION_153', 'KEY_SERIAL_153',  53,  53, 153, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(154, 1, NULL, 5, 'Key 154', 'KEY_DESCRIPTION_154', 'KEY_SERIAL_154',  54,  54, 154, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(155, 1, NULL, 5, 'Key 155', 'KEY_DESCRIPTION_155', 'KEY_SERIAL_155',  55,  55, 155, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(156, 1, NULL, 5, 'Key 156', 'KEY_DESCRIPTION_156', 'KEY_SERIAL_156',  56,  56, 156, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(157, 1, NULL, 5, 'Key 157', 'KEY_DESCRIPTION_157', 'KEY_SERIAL_157',  57,  57, 157, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(158, 1, NULL, 5, 'Key 158', 'KEY_DESCRIPTION_158', 'KEY_SERIAL_158',  58,  58, 158, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(159, 1, NULL, 5, 'Key 159', 'KEY_DESCRIPTION_159', 'KEY_SERIAL_159',  59,  59, 159, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(160, 1, NULL, 5, 'Key 160', 'KEY_DESCRIPTION_160', 'KEY_SERIAL_160',  60,  60, 160, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(161, 1, NULL, 5, 'Key 161', 'KEY_DESCRIPTION_161', 'KEY_SERIAL_161',  61,  61, 161, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(162, 1, NULL, 5, 'Key 162', 'KEY_DESCRIPTION_162', 'KEY_SERIAL_162',  62,  62, 162, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(163, 1, NULL, 5, 'Key 163', 'KEY_DESCRIPTION_163', 'KEY_SERIAL_163',  63,  63, 163, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(164, 1, NULL, 5, 'Key 164', 'KEY_DESCRIPTION_164', 'KEY_SERIAL_164',  64,  64, 164, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(165, 1, NULL, 5, 'Key 165', 'KEY_DESCRIPTION_165', 'KEY_SERIAL_165',  65,  65, 165, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(166, 1, NULL, 5, 'Key 166', 'KEY_DESCRIPTION_166', 'KEY_SERIAL_166',  66,  66, 166, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(167, 1, NULL, 5, 'Key 167', 'KEY_DESCRIPTION_167', 'KEY_SERIAL_167',  67,  67, 167, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(168, 1, NULL, 5, 'Key 168', 'KEY_DESCRIPTION_168', 'KEY_SERIAL_168',  68,  68, 168, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(169, 1, NULL, 5, 'Key 169', 'KEY_DESCRIPTION_169', 'KEY_SERIAL_169',  69,  69, 169, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(170, 1, NULL, 5, 'Key 170', 'KEY_DESCRIPTION_170', 'KEY_SERIAL_170',  70,  70, 170, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(171, 1, NULL, 5, 'Key 171', 'KEY_DESCRIPTION_171', 'KEY_SERIAL_171',  71,  71, 171, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(172, 1, NULL, 5, 'Key 172', 'KEY_DESCRIPTION_172', 'KEY_SERIAL_172',  72,  72, 172, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(173, 1, NULL, 5, 'Key 173', 'KEY_DESCRIPTION_173', 'KEY_SERIAL_173',  73,  73, 173, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(174, 1, NULL, 5, 'Key 174', 'KEY_DESCRIPTION_174', 'KEY_SERIAL_174',  74,  74, 174, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(175, 1, NULL, 5, 'Key 175', 'KEY_DESCRIPTION_175', 'KEY_SERIAL_175',  75,  75, 175, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(176, 1, NULL, 5, 'Key 176', 'KEY_DESCRIPTION_176', 'KEY_SERIAL_176',  76,  76, 176, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(177, 1, NULL, 5, 'Key 177', 'KEY_DESCRIPTION_177', 'KEY_SERIAL_177',  77,  77, 177, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(178, 1, NULL, 5, 'Key 178', 'KEY_DESCRIPTION_178', 'KEY_SERIAL_178',  78,  78, 178, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(179, 1, NULL, 5, 'Key 179', 'KEY_DESCRIPTION_179', 'KEY_SERIAL_179',  79,  79, 179, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(180, 1, NULL, 5, 'Key 180', 'KEY_DESCRIPTION_180', 'KEY_SERIAL_180',  80,  80, 180, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(181, 1, NULL, 5, 'Key 181', 'KEY_DESCRIPTION_181', 'KEY_SERIAL_181',  81,  81, 181, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(182, 1, NULL, 5, 'Key 182', 'KEY_DESCRIPTION_182', 'KEY_SERIAL_182',  82,  82, 182, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(183, 1, NULL, 5, 'Key 183', 'KEY_DESCRIPTION_183', 'KEY_SERIAL_183',  83,  83, 183, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(184, 1, NULL, 5, 'Key 184', 'KEY_DESCRIPTION_184', 'KEY_SERIAL_184',  84,  84, 184, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(185, 1, NULL, 5, 'Key 185', 'KEY_DESCRIPTION_185', 'KEY_SERIAL_185',  85,  85, 185, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(186, 1, NULL, 5, 'Key 186', 'KEY_DESCRIPTION_186', 'KEY_SERIAL_186',  86,  86, 186, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(187, 1, NULL, 5, 'Key 187', 'KEY_DESCRIPTION_187', 'KEY_SERIAL_187',  87,  87, 187, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(188, 1, NULL, 5, 'Key 188', 'KEY_DESCRIPTION_188', 'KEY_SERIAL_188',  88,  88, 188, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(189, 1, NULL, 5, 'Key 189', 'KEY_DESCRIPTION_189', 'KEY_SERIAL_189',  89,  89, 189, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(190, 1, NULL, 5, 'Key 190', 'KEY_DESCRIPTION_190', 'KEY_SERIAL_190',  90,  90, 190, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(191, 1, NULL, 5, 'Key 191', 'KEY_DESCRIPTION_191', 'KEY_SERIAL_191',  91,  91, 191, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(192, 1, NULL, 5, 'Key 192', 'KEY_DESCRIPTION_192', 'KEY_SERIAL_192',  92,  92, 192, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(193, 1, NULL, 5, 'Key 193', 'KEY_DESCRIPTION_193', 'KEY_SERIAL_193',  93,  93, 193, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(194, 1, NULL, 5, 'Key 194', 'KEY_DESCRIPTION_194', 'KEY_SERIAL_194',  94,  94, 194, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(195, 1, NULL, 5, 'Key 195', 'KEY_DESCRIPTION_195', 'KEY_SERIAL_195',  95,  95, 195, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(196, 1, NULL, 5, 'Key 196', 'KEY_DESCRIPTION_196', 'KEY_SERIAL_196',  96,  96, 196, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(197, 1, NULL, 5, 'Key 197', 'KEY_DESCRIPTION_197', 'KEY_SERIAL_197',  97,  97, 197, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(198, 1, NULL, 5, 'Key 198', 'KEY_DESCRIPTION_198', 'KEY_SERIAL_198',  98,  98, 198, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(199, 1, NULL, 5, 'Key 199', 'KEY_DESCRIPTION_199', 'KEY_SERIAL_199',  99,  99, 199, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(200, 1, NULL, 5, 'Key 200', 'KEY_DESCRIPTION_100', 'KEY_SERIAL_100', 100, 100, 200, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(201, 1, NULL, 5, 'Key 201', 'KEY_DESCRIPTION_201', 'KEY_SERIAL_201',   1,   1, 201, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(202, 1, NULL, 5, 'Key 202', 'KEY_DESCRIPTION_202', 'KEY_SERIAL_202',   2,   2, 202, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(203, 1, NULL, 5, 'Key 203', 'KEY_DESCRIPTION_203', 'KEY_SERIAL_203',   3,   3, 203, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(204, 1, NULL, 5, 'Key 204', 'KEY_DESCRIPTION_204', 'KEY_SERIAL_204',   4,   4, 204, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(205, 1, NULL, 5, 'Key 205', 'KEY_DESCRIPTION_205', 'KEY_SERIAL_205',   5,   5, 205, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(206, 1, NULL, 6, 'Key 206', 'KEY_DESCRIPTION_206', 'KEY_SERIAL_206',   6,   6, 206, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(207, 1, NULL, 6, 'Key 207', 'KEY_DESCRIPTION_207', 'KEY_SERIAL_207',   7,   7, 207, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(208, 1, NULL, 6, 'Key 208', 'KEY_DESCRIPTION_208', 'KEY_SERIAL_208',   8,   8, 208, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(209, 1, NULL, 6, 'Key 209', 'KEY_DESCRIPTION_209', 'KEY_SERIAL_209',   9,   9, 209, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(210, 1, NULL, 6, 'Key 210', 'KEY_DESCRIPTION_210', 'KEY_SERIAL_210',  10,  10, 210, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(211, 1, NULL, 6, 'Key 211', 'KEY_DESCRIPTION_211', 'KEY_SERIAL_211',  11,  11, 211, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(212, 1, NULL, 6, 'Key 212', 'KEY_DESCRIPTION_212', 'KEY_SERIAL_212',  12,  12, 212, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(213, 1, NULL, 6, 'Key 213', 'KEY_DESCRIPTION_213', 'KEY_SERIAL_213',  13,  13, 213, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(214, 1, NULL, 6, 'Key 214', 'KEY_DESCRIPTION_214', 'KEY_SERIAL_214',  14,  14, 214, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(215, 1, NULL, 6, 'Key 215', 'KEY_DESCRIPTION_215', 'KEY_SERIAL_215',  15,  15, 215, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(216, 1, NULL, 6, 'Key 216', 'KEY_DESCRIPTION_216', 'KEY_SERIAL_216',  16,  16, 216, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(217, 1, NULL, 6, 'Key 217', 'KEY_DESCRIPTION_217', 'KEY_SERIAL_217',  17,  17, 217, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(218, 1, NULL, 6, 'Key 218', 'KEY_DESCRIPTION_218', 'KEY_SERIAL_218',  18,  18, 218, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(219, 1, NULL, 6, 'Key 219', 'KEY_DESCRIPTION_219', 'KEY_SERIAL_219',  19,  19, 219, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(220, 1, NULL, 6, 'Key 220', 'KEY_DESCRIPTION_220', 'KEY_SERIAL_220',  20,  20, 220, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(221, 1, NULL, 6, 'Key 221', 'KEY_DESCRIPTION_221', 'KEY_SERIAL_221',  21,  21, 221, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(222, 1, NULL, 6, 'Key 222', 'KEY_DESCRIPTION_222', 'KEY_SERIAL_222',  22,  22, 222, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(223, 1, NULL, 6, 'Key 223', 'KEY_DESCRIPTION_223', 'KEY_SERIAL_223',  23,  23, 223, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(224, 1, NULL, 6, 'Key 224', 'KEY_DESCRIPTION_224', 'KEY_SERIAL_224',  24,  24, 224, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(225, 1, NULL, 6, 'Key 225', 'KEY_DESCRIPTION_225', 'KEY_SERIAL_225',  25,  25, 225, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(226, 1, NULL, 6, 'Key 226', 'KEY_DESCRIPTION_226', 'KEY_SERIAL_226',  26,  26, 226, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(227, 1, NULL, 6, 'Key 227', 'KEY_DESCRIPTION_227', 'KEY_SERIAL_227',  27,  27, 227, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(228, 1, NULL, 6, 'Key 228', 'KEY_DESCRIPTION_228', 'KEY_SERIAL_228',  28,  28, 228, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(229, 1, NULL, 6, 'Key 229', 'KEY_DESCRIPTION_229', 'KEY_SERIAL_229',  29,  29, 229, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(230, 1, NULL, 6, 'Key 230', 'KEY_DESCRIPTION_230', 'KEY_SERIAL_230',  30,  30, 230, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(231, 1, NULL, 6, 'Key 231', 'KEY_DESCRIPTION_231', 'KEY_SERIAL_231',  31,  31, 231, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(232, 1, NULL, 6, 'Key 232', 'KEY_DESCRIPTION_232', 'KEY_SERIAL_232',  32,  32, 232, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(233, 1, NULL, 6, 'Key 233', 'KEY_DESCRIPTION_233', 'KEY_SERIAL_233',  33,  33, 233, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(234, 1, NULL, 6, 'Key 234', 'KEY_DESCRIPTION_234', 'KEY_SERIAL_234',  34,  34, 234, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(235, 1, NULL, 6, 'Key 235', 'KEY_DESCRIPTION_235', 'KEY_SERIAL_235',  35,  35, 235, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(236, 1, NULL, 6, 'Key 236', 'KEY_DESCRIPTION_236', 'KEY_SERIAL_236',  36,  36, 236, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(237, 1, NULL, 6, 'Key 237', 'KEY_DESCRIPTION_237', 'KEY_SERIAL_237',  37,  37, 237, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(238, 1, NULL, 6, 'Key 238', 'KEY_DESCRIPTION_238', 'KEY_SERIAL_238',  38,  38, 238, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(239, 1, NULL, 6, 'Key 239', 'KEY_DESCRIPTION_239', 'KEY_SERIAL_239',  39,  39, 239, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(240, 1, NULL, 6, 'Key 240', 'KEY_DESCRIPTION_240', 'KEY_SERIAL_240',  40,  40, 240, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(241, 1, NULL, 6, 'Key 241', 'KEY_DESCRIPTION_241', 'KEY_SERIAL_241',  41,  41, 241, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(242, 1, NULL, 6, 'Key 242', 'KEY_DESCRIPTION_242', 'KEY_SERIAL_242',  42,  42, 242, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(243, 1, NULL, 6, 'Key 243', 'KEY_DESCRIPTION_243', 'KEY_SERIAL_243',  43,  43, 243, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(244, 1, NULL, 6, 'Key 244', 'KEY_DESCRIPTION_244', 'KEY_SERIAL_244',  44,  44, 244, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(245, 1, NULL, 6, 'Key 245', 'KEY_DESCRIPTION_245', 'KEY_SERIAL_245',  45,  45, 245, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(246, 1, NULL, 6, 'Key 246', 'KEY_DESCRIPTION_246', 'KEY_SERIAL_246',  46,  46, 246, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(247, 1, NULL, 6, 'Key 247', 'KEY_DESCRIPTION_247', 'KEY_SERIAL_247',  47,  47, 247, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(248, 1, NULL, 6, 'Key 248', 'KEY_DESCRIPTION_248', 'KEY_SERIAL_248',  48,  48, 248, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(249, 1, NULL, 6, 'Key 249', 'KEY_DESCRIPTION_249', 'KEY_SERIAL_249',  49,  49, 249, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 ),
(250, 1, NULL, 6, 'Key 250', 'KEY_DESCRIPTION_250', 'KEY_SERIAL_250',  50,  50, 250, 1, '2021-09-24 12:14:38', '08:00:00', '20:00:00', 0, 0 );

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_additional_fields`
--

DROP TABLE IF EXISTS `tbl_key_additional_fields`;
CREATE TABLE IF NOT EXISTS `tbl_key_additional_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_additional_fields',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `help_text` longtext COLLATE utf8mb4_unicode_ci,
  `error_text` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_key_additional_fields`
--

INSERT INTO `tbl_key_additional_fields` (`id`, `field_name`, `display_name`, `required`, `help_text`, `error_text`) VALUES
(1, 'newfield1', 'The New Fieldd', 1, 'Some help text here', 'Some error text here');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_categories`
--

DROP TABLE IF EXISTS `tbl_key_categories`;
CREATE TABLE IF NOT EXISTS `tbl_key_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_categories',
  `category_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_short_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_key_categories`
--

INSERT INTO `tbl_key_categories` (`id`, `category_type`, `category_icon`, `category_name`, `category_short_name`) VALUES
(1, 'type_irv', 'car_crash', 'Incident Response Vehicle', 'IRV'),
(2, 'type_arv', 'shield', 'Armed Response Vehicle', 'ARV'),
(3, 'type_rpu', 'road', 'Roads Policing Unit', 'RPU'),
(4, 'type_area_cars', 'car', 'Area Cars', 'Area Cars'),
(5, 'type_motorcycles', 'motorcycle', 'Motorcycles', 'Motorcycles'),
(6, 'type_vans', 'bus', 'Vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_to_additional_field_values`
--

DROP TABLE IF EXISTS `tbl_key_to_additional_field_values`;
CREATE TABLE IF NOT EXISTS `tbl_key_to_additional_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_to_additional_field_values',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `additional_field` int(11) DEFAULT NULL COMMENT 'tbl_key_additional_fields',
  `field_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_AB7E7ED1D145533` (`key_id`),
  KEY `IDX_AB7E7ED127073E69` (`additional_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_key_to_additional_field_values`
--

INSERT INTO `tbl_key_to_additional_field_values` (`id`, `key_id`, `additional_field`, `field_value`) VALUES
(2, 2, 1, 'test value');
-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_to_categories`
--

DROP TABLE IF EXISTS `tbl_key_to_categories`;
CREATE TABLE IF NOT EXISTS `tbl_key_to_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_to_categories',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  PRIMARY KEY (`id`),
  KEY `IDX_E19C7C95D145533` (`key_id`),
  KEY `IDX_E19C7C95C74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_labels`
--

DROP TABLE IF EXISTS `tbl_labels`;
CREATE TABLE IF NOT EXISTS `tbl_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_labels',
  `label_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `label_language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_38776BAE921BEBCA` (`label_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locations`
--

DROP TABLE IF EXISTS `tbl_locations`;
CREATE TABLE IF NOT EXISTS `tbl_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_locations',
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` longtext COLLATE utf8mb4_unicode_ci,
  `latitude` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_locations`
--

INSERT INTO `tbl_locations` (`id`, `location_name`, `longitude`, `latitude`) VALUES
(1, 'London', '51.52531761747996', '-0.12153787219587886');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_password_reset_requests`
--

DROP TABLE IF EXISTS `tbl_password_reset_requests`;
CREATE TABLE IF NOT EXISTS `tbl_password_reset_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_password_reset_requests',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E182F2C5A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requests_log`
--

DROP TABLE IF EXISTS `tbl_requests_log`;
CREATE TABLE IF NOT EXISTS `tbl_requests_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_requests_log',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `requested_route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_body` longtext COLLATE utf8mb4_unicode_ci,
  `status_code` int(11) NOT NULL,
  `request_error` longtext COLLATE utf8mb4_unicode_ci,
  `request_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4791EE56A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_requests_log`
--

INSERT INTO `tbl_requests_log` (`id`, `user_id`, `requested_route`, `request_type`, `request_body`, `status_code`, `request_error`, `request_date`) VALUES
(1, NULL, '/database/make/defaults', 'POST', 'null', 200, NULL, '2022-01-07 09:28:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_to_permission`
--

DROP TABLE IF EXISTS `tbl_role_to_permission`;
CREATE TABLE IF NOT EXISTS `tbl_role_to_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_role_to_permission',
  `account_profile` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles',
  `profile_permission` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles_permissions',
  PRIMARY KEY (`id`),
  KEY `IDX_56344BE2487CBE47` (`account_profile`),
  KEY `IDX_56344BE22722A5F7` (`profile_permission`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_role_to_permission`
--

INSERT INTO `tbl_role_to_permission` (`id`, `account_profile`, `profile_permission`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 2, 5),
(19, 3, 5),
(20, 3, 8),
(21, 4, 10),
(22, 4, 11),
(23, 5, 1),
(24, 5, 2),
(25, 5, 3),
(26, 5, 4),
(27, 5, 5),
(28, 5, 6),
(29, 5, 7),
(30, 5, 8),
(31, 5, 9),
(32, 5, 10),
(33, 5, 11),
(34, 5, 12),
(35, 5, 13),
(36, 5, 14),
(37, 5, 15),
(38, 5, 16),
(39, 5, 17);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

DROP TABLE IF EXISTS `tbl_settings`;
CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_settings',
  `setting_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'AnalyticsModule', 'true'),
(2, 'AutomotiveModule', 'true'),
(3, 'BookingModule', 'true'),
(4, 'CompoundModule', 'true'),
(5, 'HandoverModule', 'true'),
(6, 'ValetModule', 'true'),
(7, 'VisitorsModule', 'true'),
(8, 'ShiftsModule', 'true'),
(9, 'useFingerprint', 'true'),
(10, 'useCard', 'true'),
(11, 'cardReaderPort', '333'),
(12, 'accessedOutOfHoursUsers', NULL),
(13, 'bookingAddedUsers', NULL),
(14, 'bookingOverdueUsers', NULL),
(15, 'reportsEmailTimes', '17:00,22:00'),
(16, 'reportsKeysInEmailUsers', NULL),
(17, 'reportsKeysOutEmailUsers', NULL),
(18, 'overdueKeyEmailUsers', NULL),
(19, 'shiftEmailTime', '0.5'),
(20, 'shiftEmailUsers', NULL),
(21, 'addFobButton', 'true'),
(22, 'allKeyAccess', 'true'),
(23, 'deleteFobButton', 'true'),
(24, 'bookingLimitRange', '12:00,14:00;16:00,18:00'),
(25, 'SMTPServer', NULL),
(26, 'SMTPAccount', NULL),
(27, 'SMTPPassword', NULL),
(28, 'SMTPEncryption', NULL),
(29, 'SMTPPort', NULL),
(30, 'doorTimeout', '20000'),
(31, 'fingerPrintMaker', ''),
(32, 'peripherals', ''),
(33, 'capitalise', 'true'),
(34, 'cronEmails', 'false'),
(35, 'department', 'true'),
(36, 'shorthandNames', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sites`
--

DROP TABLE IF EXISTS `tbl_sites`;
CREATE TABLE IF NOT EXISTS `tbl_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_sites',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `site_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` longtext COLLATE utf8mb4_unicode_ci,
  `latitude` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_1264E07C5E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sites`
--

INSERT INTO `tbl_sites` (`id`, `location`, `site_name`, `longitude`, `latitude`) VALUES
(1, 1, 'The Mothership', '51.52531761747996', '-0.12153787219587886');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_systems`
--

DROP TABLE IF EXISTS `tbl_systems`;
CREATE TABLE IF NOT EXISTS `tbl_systems` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_systems',
  `parent_site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `system_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `welcome_text` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_host` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_user` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_password` longtext COLLATE utf8mb4_unicode_ci,
  `ip` longtext COLLATE utf8mb4_unicode_ci,
  `mac` longtext COLLATE utf8mb4_unicode_ci,
  `subnet` longtext COLLATE utf8mb4_unicode_ci,
  `gateway` longtext COLLATE utf8mb4_unicode_ci,
  `dns_1` longtext COLLATE utf8mb4_unicode_ci,
  `dns_2` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_port` int(11) DEFAULT NULL,
  `mqtt_topic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mqtt topic name to be used by the system',
  `system_timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_authentication` tinyint(1) NOT NULL,
  `biometric_authentication` tinyint(1) NOT NULL,
  `registered_date` datetime DEFAULT NULL,
  `last_com` datetime DEFAULT NULL,
  `touch_software_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compound_module` tinyint(1) DEFAULT NULL,
  `valet_module` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3AFED0C654A03F50` (`parent_site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_systems`
--

INSERT INTO `tbl_systems` (`id`, `parent_site`, `system_name`, `welcome_text`, `mqtt_host`, `mqtt_user`, `mqtt_password`, `ip`, `mac`, `subnet`, `gateway`, `dns_1`, `dns_2`, `mqtt_port`, `mqtt_topic`, `system_timezone`, `card_authentication`, `biometric_authentication`, `registered_date`, `last_com`, `touch_software_version`, `compound_module`, `valet_module`) VALUES
(1, 1, 'The system to rule all systems', 'Welcome to The system to rule all systems!', '185.217.43.183', 'admin', 'Keytracker1', NULL, NULL, NULL, NULL, NULL, NULL, 1883, 'e-track/cabinet-1', 'Europe/London', 1, 1, NULL, NULL, '1.0', 1, 1),
(2, 1, 'The system to rule all systems', 'Welcome to The system to rule all systems!', '185.217.43.183', 'admin', 'Keytracker1', NULL, NULL, NULL, NULL, NULL, NULL, 1883, 'e-track/cabinet-1', 'Europe/London', 1, 1, NULL, NULL, '1.0', 1, 1),
(3, 1, 'The system to rule all systems', 'Welcome to The system to rule all systems!', '185.217.43.183', 'admin', 'Keytracker1', NULL, NULL, NULL, NULL, NULL, NULL, 1883, 'e-track/cabinet-1', 'Europe/London', 1, 1, NULL, NULL, '1.0', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_users',
  `user_profile` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_visitor` tinyint(1) NOT NULL DEFAULT '0',
  `profile_image` longtext COLLATE utf8mb4_unicode_ci,
  `phone_number` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `active_from` datetime DEFAULT NULL,
  `key_limit` int(11) NOT NULL,
  `is_temp` tinyint(1) NOT NULL,
  `is_first_login` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BAE7EFF6F85E0677` (`username`),
  KEY `IDX_BAE7EFF6D95AB405` (`user_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_profile`, `username`, `password`, `first_name`, `last_name`, `is_visitor`, `profile_image`, `phone_number`, `is_active`, `active_from`, `key_limit`, `is_temp`, `is_first_login`) VALUES
(1, 5, 'first@e-tracksystems.com', '548693e5d533f7db2d5b2ad98aeb592ffe11d9575e9b1671b9bcdf004224c1d6a9ae5606f9a8a9557a3067cbf9c83d34ed89bc3c1af162bc520f6655944b963e', 'First', 'Engineer', 0, NULL, NULL, 1, '2021-12-09 15:25:37', 0, 1, 0),
(2, 1, 'second@e-tracksystems.com', 'fdc979bb4d7bfdf4bb37a6eac2f79f029d40d265f8097ab0458981df66cb5210df5a485cb68974b54f4d68a700b6b403a7c3b1ebfd8211a75baa947f94f8d3d6', 'Second', 'Admin', 0, NULL, NULL, 1, '2021-12-09 16:32:06', 10, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_cabinet_access`
--

DROP TABLE IF EXISTS `tbl_user_cabinet_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_cabinet_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_cabinet_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DA4A9058A76ED395` (`user_id`),
  KEY `IDX_DA4A90584CED05B0` (`cabinet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_cabinet_access`
--

INSERT INTO `tbl_user_cabinet_access` (`id`, `user_id`, `cabinet`, `is_access_granted`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_defined_fields`
--

DROP TABLE IF EXISTS `tbl_user_defined_fields`;
CREATE TABLE IF NOT EXISTS `tbl_user_defined_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_defined_fields',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `help_text` longtext COLLATE utf8mb4_unicode_ci,
  `error_text` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_defined_fields`
--

INSERT INTO `tbl_user_defined_fields` (`id`, `field_name`, `display_name`, `required`, `help_text`, `error_text`) VALUES
(1, 'field1', 'Position', 0, NULL, NULL),
(2, 'field2', 'Department', 0, NULL, NULL),
(3, 'field3', 'Company', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_entry_ids`
--

DROP TABLE IF EXISTS `tbl_user_entry_ids`;
CREATE TABLE IF NOT EXISTS `tbl_user_entry_ids` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_entry_ids',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `entry_type` int(11) DEFAULT NULL COMMENT 'tbl_entry_types',
  `entry_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_144D1FF7A76ED395` (`user_id`),
  KEY `IDX_144D1FF728FC2F3A` (`entry_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_entry_ids`
--

INSERT INTO `tbl_user_entry_ids` (`id`, `user_id`, `entry_type`, `entry_value`) VALUES
(1, 1, 1, '1111'),
(2, 2, 1, '2222');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_group`
--

DROP TABLE IF EXISTS `tbl_user_group`;
CREATE TABLE IF NOT EXISTS `tbl_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_group',
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_limit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_group`
--

INSERT INTO `tbl_user_group` (`id`, `group_name`, `key_limit`) VALUES
(1, 'Group 1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_location_access`
--

DROP TABLE IF EXISTS `tbl_user_location_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_location_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_location_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_832E69AFA76ED395` (`user_id`),
  KEY `IDX_832E69AF5E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `tbl_user_location_access` (`id`, `user_id`, `location`, `is_access_granted`) VALUES
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profiles`
--

DROP TABLE IF EXISTS `tbl_user_profiles`;
CREATE TABLE IF NOT EXISTS `tbl_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_profiles',
  `profile_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_722F043BE8EBF192` (`profile_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_profiles`
--

INSERT INTO `tbl_user_profiles` (`id`, `profile_name`, `profile_description`) VALUES
(1, 'Administrator', NULL),
(2, 'Standard', NULL),
(3, 'Report', NULL),
(4, 'Booking Only', NULL),
(5, 'Engineer', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profile_permissions`
--

DROP TABLE IF EXISTS `tbl_user_profile_permissions`;
CREATE TABLE IF NOT EXISTS `tbl_user_profile_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_profiles_permissions',
  `permission_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_profile_permissions`
--

INSERT INTO `tbl_user_profile_permissions` (`id`, `permission_name`) VALUES
(1, 'accessUsers'),
(2, 'manageUsers'),
(3, 'accessUserGroups'),
(4, 'manageUserGroups'),
(5, 'accessKeys'),
(6, 'manageKeys'),
(7, 'accessSettings'),
(8, 'accessReports'),
(9, 'accessSystems'),
(10, 'accessBookings'),
(11, 'makeBooking'),
(12, 'manageBooking'),
(13, 'isHandover'),
(14, 'autoApprove'),
(15, 'hasGlobalSearch'),
(16, 'webLogin'),
(17, 'manageRoles');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_site_access`
--

DROP TABLE IF EXISTS `tbl_user_site_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_site_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_site_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D502CEECA76ED395` (`user_id`),
  KEY `IDX_D502CEEC694309E4` (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_site_access`
--

INSERT INTO `tbl_user_site_access` (`id`, `user_id`, `site`, `is_access_granted`) VALUES
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_system_access`
--

DROP TABLE IF EXISTS `tbl_user_system_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_system_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_system_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `system_id` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6D6C063A76ED395` (`user_id`),
  KEY `IDX_E6D6C063D0952FA5` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_system_access`
--

INSERT INTO `tbl_user_system_access` (`id`, `user_id`, `system_id`, `is_access_granted`) VALUES
(3, 1, 1, 1),
(4, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_defined_field_values`
--

DROP TABLE IF EXISTS `tbl_user_to_defined_field_values`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_defined_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_defined_field_values',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `custom_field` int(11) DEFAULT NULL COMMENT 'tbl_user_defined_fields',
  `field_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_E4EDAF39A76ED395` (`user_id`),
  KEY `IDX_E4EDAF3998F8BD31` (`custom_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_groups`
--

DROP TABLE IF EXISTS `tbl_user_to_groups`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_groups',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  PRIMARY KEY (`id`),
  KEY `IDX_4194F3E4A76ED395` (`user_id`),
  KEY `IDX_4194F3E48F02BF9D` (`user_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_key_access`
--

DROP TABLE IF EXISTS `tbl_user_to_key_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_key_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_key_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `is_access_granted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DA527683A76ED395` (`user_id`),
  KEY `IDX_DA527683D145533` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_to_key_access`
--

INSERT INTO `tbl_user_to_key_access` (`id`, `user_id`, `key_id`, `is_access_granted`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_key_category_access`
--

DROP TABLE IF EXISTS `tbl_user_to_key_category_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_key_category_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_key_category_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_785C0634A76ED395` (`user_id`),
  KEY `IDX_785C0634C74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cabinets`
--
ALTER TABLE `tbl_cabinets`
  ADD CONSTRAINT `FK_871077AF865ED24D` FOREIGN KEY (`parent_system`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_events_log`
--
ALTER TABLE `tbl_events_log`
  ADD CONSTRAINT `FK_B0EDB50033EA99D0` FOREIGN KEY (`event_message`) REFERENCES `tbl_event_messages` (`id`),
  ADD CONSTRAINT `FK_B0EDB5007555FBC0` FOREIGN KEY (`event_action`) REFERENCES `tbl_event_actions` (`id`),
  ADD CONSTRAINT `FK_B0EDB500865ED24D` FOREIGN KEY (`parent_system`) REFERENCES `tbl_systems` (`id`),
  ADD CONSTRAINT `FK_B0EDB500A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_event_messages`
--
ALTER TABLE `tbl_event_messages`
  ADD CONSTRAINT `FK_A25BCAB28FC2F3A` FOREIGN KEY (`entry_type`) REFERENCES `tbl_entry_types` (`id`),
  ADD CONSTRAINT `FK_A25BCAB52C83E47` FOREIGN KEY (`handover_user`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_A25BCABD145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_group_cabinet_access`
--
ALTER TABLE `tbl_group_cabinet_access`
  ADD CONSTRAINT `FK_CA00992B4CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_CA00992B8F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_location_access`
--
ALTER TABLE `tbl_group_location_access`
  ADD CONSTRAINT `FK_4A3203205E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`),
  ADD CONSTRAINT `FK_4A3203208F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_site_access`
--
ALTER TABLE `tbl_group_site_access`
  ADD CONSTRAINT `FK_ED7E5458694309E4` FOREIGN KEY (`site`) REFERENCES `tbl_sites` (`id`),
  ADD CONSTRAINT `FK_ED7E54588F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_system_access`
--
ALTER TABLE `tbl_group_system_access`
  ADD CONSTRAINT `FK_76A5E9F98F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_76A5E9F9D0952FA5` FOREIGN KEY (`system_id`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_group_to_key_access`
--
ALTER TABLE `tbl_group_to_key_access`
  ADD CONSTRAINT `FK_4A215F198F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_4A215F19D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_group_to_key_category_access`
--
ALTER TABLE `tbl_group_to_key_category_access`
  ADD CONSTRAINT `FK_1386B1C8F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_1386B1CC74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`);

--
-- Constraints for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  ADD CONSTRAINT `FK_1AE11044CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_1AE1104A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_1AE1104A76ED396` FOREIGN KEY (`category`) REFERENCES `tbl_key_categories` (`id`);

--
-- Constraints for table `tbl_key_to_additional_field_values`
--
ALTER TABLE `tbl_key_to_additional_field_values`
  ADD CONSTRAINT `FK_AB7E7ED127073E69` FOREIGN KEY (`additional_field`) REFERENCES `tbl_key_additional_fields` (`id`),
  ADD CONSTRAINT `FK_AB7E7ED1D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_key_to_categories`
--
ALTER TABLE `tbl_key_to_categories`
  ADD CONSTRAINT `FK_E19C7C95C74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`),
  ADD CONSTRAINT `FK_E19C7C95D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_password_reset_requests`
--
ALTER TABLE `tbl_password_reset_requests`
  ADD CONSTRAINT `FK_E182F2C5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_requests_log`
--
ALTER TABLE `tbl_requests_log`
  ADD CONSTRAINT `FK_4791EE56A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_role_to_permission`
--
ALTER TABLE `tbl_role_to_permission`
  ADD CONSTRAINT `FK_56344BE22722A5F7` FOREIGN KEY (`profile_permission`) REFERENCES `tbl_user_profile_permissions` (`id`),
  ADD CONSTRAINT `FK_56344BE2487CBE47` FOREIGN KEY (`account_profile`) REFERENCES `tbl_user_profiles` (`id`);

--
-- Constraints for table `tbl_sites`
--
ALTER TABLE `tbl_sites`
  ADD CONSTRAINT `FK_1264E07C5E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`);

--
-- Constraints for table `tbl_systems`
--
ALTER TABLE `tbl_systems`
  ADD CONSTRAINT `FK_3AFED0C654A03F50` FOREIGN KEY (`parent_site`) REFERENCES `tbl_sites` (`id`);

--
-- Constraints for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD CONSTRAINT `FK_BAE7EFF6D95AB405` FOREIGN KEY (`user_profile`) REFERENCES `tbl_user_profiles` (`id`);

--
-- Constraints for table `tbl_user_cabinet_access`
--
ALTER TABLE `tbl_user_cabinet_access`
  ADD CONSTRAINT `FK_DA4A90584CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_DA4A9058A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_entry_ids`
--
ALTER TABLE `tbl_user_entry_ids`
  ADD CONSTRAINT `FK_144D1FF728FC2F3A` FOREIGN KEY (`entry_type`) REFERENCES `tbl_entry_types` (`id`),
  ADD CONSTRAINT `FK_144D1FF7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_location_access`
--
ALTER TABLE `tbl_user_location_access`
  ADD CONSTRAINT `FK_832E69AF5E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`),
  ADD CONSTRAINT `FK_832E69AFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_site_access`
--
ALTER TABLE `tbl_user_site_access`
  ADD CONSTRAINT `FK_D502CEEC694309E4` FOREIGN KEY (`site`) REFERENCES `tbl_sites` (`id`),
  ADD CONSTRAINT `FK_D502CEECA76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_system_access`
--
ALTER TABLE `tbl_user_system_access`
  ADD CONSTRAINT `FK_E6D6C063A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_E6D6C063D0952FA5` FOREIGN KEY (`system_id`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_user_to_defined_field_values`
--
ALTER TABLE `tbl_user_to_defined_field_values`
  ADD CONSTRAINT `FK_E4EDAF3998F8BD31` FOREIGN KEY (`custom_field`) REFERENCES `tbl_user_defined_fields` (`id`),
  ADD CONSTRAINT `FK_E4EDAF39A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_to_groups`
--
ALTER TABLE `tbl_user_to_groups`
  ADD CONSTRAINT `FK_4194F3E48F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_4194F3E4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_to_key_access`
--
ALTER TABLE `tbl_user_to_key_access`
  ADD CONSTRAINT `FK_DA527683A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_DA527683D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_user_to_key_category_access`
--
ALTER TABLE `tbl_user_to_key_category_access`
  ADD CONSTRAINT `FK_785C0634A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_785C0634C74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
