-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 07, 2022 at 09:28 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cabinets`
--

DROP TABLE IF EXISTS `tbl_cabinets`;
CREATE TABLE IF NOT EXISTS `tbl_cabinets` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_cabinets',
  `parent_system` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `cabinet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `com_port` int(11) NOT NULL,
  `slot_capacity` int(11) NOT NULL,
  `start_slot` int(11) NOT NULL,
  `end_slot` int(11) NOT NULL,
  `slot_width` int(11) NOT NULL,
  `no_of_locks` int(11) NOT NULL,
  `multi_factor` tinyint(1) NOT NULL,
  `card_reader` tinyint(1) NOT NULL,
  `biometric` tinyint(1) NOT NULL,
  `is_dynamic` tinyint(1) NOT NULL,
  `peg_width` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_871077AF865ED24D` (`parent_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entry_types`
--

DROP TABLE IF EXISTS `tbl_entry_types`;
CREATE TABLE IF NOT EXISTS `tbl_entry_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_entry_types',
  `entry_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_entry_types`
--

INSERT INTO `tbl_entry_types` (`id`, `entry_type`) VALUES
(1, 'usePin'),
(2, 'useFingerprint'),
(3, 'useCard');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_log`
--

DROP TABLE IF EXISTS `tbl_events_log`;
CREATE TABLE IF NOT EXISTS `tbl_events_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_events_log',
  `event_action` int(11) DEFAULT NULL COMMENT 'tbl_event_actions',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `event_message` int(11) DEFAULT NULL COMMENT 'tbl_event_messages',
  `parent_system` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `event_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B0EDB5007555FBC0` (`event_action`),
  KEY `IDX_B0EDB500A76ED395` (`user_id`),
  KEY `IDX_B0EDB50033EA99D0` (`event_message`),
  KEY `IDX_B0EDB500865ED24D` (`parent_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_actions`
--

DROP TABLE IF EXISTS `tbl_event_actions`;
CREATE TABLE IF NOT EXISTS `tbl_event_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_event_actions',
  `action_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_event_actions`
--

INSERT INTO `tbl_event_actions` (`id`, `action_type`) VALUES
(1, 'login_success'),
(2, 'login_failed'),
(3, 'logout'),
(4, 'fob_removed'),
(5, 'fob_returned'),
(6, 'key_in'),
(7, 'key_out'),
(8, 'door_opened'),
(9, 'door_closed'),
(10, 'door_timeout'),
(11, 'link_eFob'),
(12, 'parking_bay_update'),
(13, 'item_in'),
(14, 'item_out'),
(15, 'controller_manual_override'),
(16, 'key_forced'),
(17, 'unauthorised_deposit'),
(18, 'access_denied');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_messages`
--

DROP TABLE IF EXISTS `tbl_event_messages`;
CREATE TABLE IF NOT EXISTS `tbl_event_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_event_messages',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `handover_user` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `entry_type` int(11) DEFAULT NULL COMMENT 'tbl_entry_types',
  `key_position` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `incorrect_access_value` tinyint(1) NOT NULL DEFAULT '0',
  `invalid_access_time` tinyint(1) NOT NULL DEFAULT '0',
  `wrong_slot_detected` tinyint(1) NOT NULL DEFAULT '0',
  `removed_from_wrong_slot` tinyint(1) NOT NULL DEFAULT '0',
  `unknown_key_detected` tinyint(1) NOT NULL DEFAULT '0',
  `key_in_unassigned_slot` tinyint(1) NOT NULL DEFAULT '0',
  `key_out_of_unassigned_slot` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_A25BCABD145533` (`key_id`),
  KEY `IDX_A25BCAB52C83E47` (`handover_user`),
  KEY `IDX_A25BCAB28FC2F3A` (`entry_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_cabinet_access`
--

DROP TABLE IF EXISTS `tbl_group_cabinet_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_cabinet_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_cabinet_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CA00992B8F02BF9D` (`user_group`),
  KEY `IDX_CA00992B4CED05B0` (`cabinet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_location_access`
--

DROP TABLE IF EXISTS `tbl_group_location_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_location_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_location_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A3203208F02BF9D` (`user_group`),
  KEY `IDX_4A3203205E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_site_access`
--

DROP TABLE IF EXISTS `tbl_group_site_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_site_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_site_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ED7E54588F02BF9D` (`user_group`),
  KEY `IDX_ED7E5458694309E4` (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_system_access`
--

DROP TABLE IF EXISTS `tbl_group_system_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_system_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_system_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `system_id` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76A5E9F98F02BF9D` (`user_group`),
  KEY `IDX_76A5E9F9D0952FA5` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_to_key_access`
--

DROP TABLE IF EXISTS `tbl_group_to_key_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_to_key_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_to_key_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `is_access_granted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A215F198F02BF9D` (`user_group`),
  KEY `IDX_4A215F19D145533` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_to_key_category_access`
--

DROP TABLE IF EXISTS `tbl_group_to_key_category_access`;
CREATE TABLE IF NOT EXISTS `tbl_group_to_key_category_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_group_to_key_category_access',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1386B1C8F02BF9D` (`user_group`),
  KEY `IDX_1386B1CC74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keys`
--

DROP TABLE IF EXISTS `tbl_keys`;
CREATE TABLE IF NOT EXISTS `tbl_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_keys',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `key_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_description` longtext COLLATE utf8mb4_unicode_ci,
  `key_serial` longtext COLLATE utf8mb4_unicode_ci,
  `slot` int(11) NOT NULL,
  `current_slot` int(11) DEFAULT NULL,
  `fob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_static` tinyint(1) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `is_out` tinyint(1) NOT NULL,
  `is_wrong_slot` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1AE11044CED05B0` (`cabinet`),
  KEY `IDX_1AE1104A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_additional_fields`
--

DROP TABLE IF EXISTS `tbl_key_additional_fields`;
CREATE TABLE IF NOT EXISTS `tbl_key_additional_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_additional_fields',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `help_text` longtext COLLATE utf8mb4_unicode_ci,
  `error_text` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_categories`
--

DROP TABLE IF EXISTS `tbl_key_categories`;
CREATE TABLE IF NOT EXISTS `tbl_key_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_categories',
  `category_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_short_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_to_additional_field_values`
--

DROP TABLE IF EXISTS `tbl_key_to_additional_field_values`;
CREATE TABLE IF NOT EXISTS `tbl_key_to_additional_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_to_additional_field_values',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `additional_field` int(11) DEFAULT NULL COMMENT 'tbl_key_additional_fields',
  `field_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_AB7E7ED1D145533` (`key_id`),
  KEY `IDX_AB7E7ED127073E69` (`additional_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_key_to_categories`
--

DROP TABLE IF EXISTS `tbl_key_to_categories`;
CREATE TABLE IF NOT EXISTS `tbl_key_to_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_key_to_categories',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  PRIMARY KEY (`id`),
  KEY `IDX_E19C7C95D145533` (`key_id`),
  KEY `IDX_E19C7C95C74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_labels`
--

DROP TABLE IF EXISTS `tbl_labels`;
CREATE TABLE IF NOT EXISTS `tbl_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_labels',
  `label_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `label_language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_38776BAE921BEBCA` (`label_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locations`
--

DROP TABLE IF EXISTS `tbl_locations`;
CREATE TABLE IF NOT EXISTS `tbl_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_locations',
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` longtext COLLATE utf8mb4_unicode_ci,
  `latitude` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_password_reset_requests`
--

DROP TABLE IF EXISTS `tbl_password_reset_requests`;
CREATE TABLE IF NOT EXISTS `tbl_password_reset_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_password_reset_requests',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E182F2C5A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requests_log`
--

DROP TABLE IF EXISTS `tbl_requests_log`;
CREATE TABLE IF NOT EXISTS `tbl_requests_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_requests_log',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `requested_route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_body` longtext COLLATE utf8mb4_unicode_ci,
  `status_code` int(11) NOT NULL,
  `request_error` longtext COLLATE utf8mb4_unicode_ci,
  `request_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4791EE56A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_requests_log`
--

INSERT INTO `tbl_requests_log` (`id`, `user_id`, `requested_route`, `request_type`, `request_body`, `status_code`, `request_error`, `request_date`) VALUES
(1, NULL, '/database/make/defaults', 'POST', 'null', 200, NULL, '2022-01-07 09:28:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_to_permission`
--

DROP TABLE IF EXISTS `tbl_role_to_permission`;
CREATE TABLE IF NOT EXISTS `tbl_role_to_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_role_to_permission',
  `account_profile` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles',
  `profile_permission` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles_permissions',
  PRIMARY KEY (`id`),
  KEY `IDX_56344BE2487CBE47` (`account_profile`),
  KEY `IDX_56344BE22722A5F7` (`profile_permission`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_role_to_permission`
--

INSERT INTO `tbl_role_to_permission` (`id`, `account_profile`, `profile_permission`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 2, 5),
(19, 3, 5),
(20, 3, 8),
(21, 4, 10),
(22, 4, 11),
(23, 5, 1),
(24, 5, 2),
(25, 5, 3),
(26, 5, 4),
(27, 5, 5),
(28, 5, 6),
(29, 5, 7),
(30, 5, 8),
(31, 5, 9),
(32, 5, 10),
(33, 5, 11),
(34, 5, 12),
(35, 5, 13),
(36, 5, 14),
(37, 5, 15),
(38, 5, 16),
(39, 5, 17);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

DROP TABLE IF EXISTS `tbl_settings`;
CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_settings',
  `setting_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'AnalyticsModule', 'true'),
(2, 'AutomotiveModule', 'true'),
(3, 'BookingModule', 'true'),
(4, 'CompoundModule', 'true'),
(5, 'HandoverModule', 'true'),
(6, 'ValetModule', 'true'),
(7, 'VisitorsModule', 'true'),
(8, 'ShiftsModule', 'true'),
(9, 'useFingerprint', 'true'),
(10, 'useCard', 'true'),
(11, 'cardReaderPort', '333'),
(12, 'accessedOutOfHoursUsers', NULL),
(13, 'bookingAddedUsers', NULL),
(14, 'bookingOverdueUsers', NULL),
(15, 'reportsEmailTimes', '17:00,22:00'),
(16, 'reportsKeysInEmailUsers', NULL),
(17, 'reportsKeysOutEmailUsers', NULL),
(18, 'overdueKeyEmailUsers', NULL),
(19, 'shiftEmailTime', '0.5'),
(20, 'shiftEmailUsers', NULL),
(21, 'addFobButton', 'true'),
(22, 'allKeyAccess', 'true'),
(23, 'deleteFobButton', 'true'),
(24, 'bookingLimitRange', '12:00,14:00;16:00,18:00'),
(25, 'SMTPServer', NULL),
(26, 'SMTPAccount', NULL),
(27, 'SMTPPassword', NULL),
(28, 'SMTPEncryption', NULL),
(29, 'SMTPPort', NULL),
(30, 'doorTimeout', '20000'),
(31, 'fingerPrintMaker', ''),
(32, 'peripherals', ''),
(33, 'capitalise', 'true'),
(34, 'cronEmails', 'false'),
(35, 'department', 'true'),
(36, 'shorthandNames', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sites`
--

DROP TABLE IF EXISTS `tbl_sites`;
CREATE TABLE IF NOT EXISTS `tbl_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_sites',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `site_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` longtext COLLATE utf8mb4_unicode_ci,
  `latitude` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_1264E07C5E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_systems`
--

DROP TABLE IF EXISTS `tbl_systems`;
CREATE TABLE IF NOT EXISTS `tbl_systems` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_systems',
  `parent_site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `system_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `welcome_text` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_host` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_user` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_password` longtext COLLATE utf8mb4_unicode_ci,
  `ip` longtext COLLATE utf8mb4_unicode_ci,
  `mac` longtext COLLATE utf8mb4_unicode_ci,
  `subnet` longtext COLLATE utf8mb4_unicode_ci,
  `gateway` longtext COLLATE utf8mb4_unicode_ci,
  `dns_1` longtext COLLATE utf8mb4_unicode_ci,
  `dns_2` longtext COLLATE utf8mb4_unicode_ci,
  `mqtt_port` int(11) DEFAULT NULL,
  `mqtt_topic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mqtt topic name to be used by the system',
  `system_timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_authentication` tinyint(1) NOT NULL,
  `biometric_authentication` tinyint(1) NOT NULL,
  `registered_date` datetime DEFAULT NULL,
  `last_com` datetime DEFAULT NULL,
  `touch_software_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compound_module` tinyint(1) DEFAULT NULL,
  `valet_module` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3AFED0C654A03F50` (`parent_site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_users',
  `user_profile` int(11) DEFAULT NULL COMMENT 'tbl_user_profiles',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_visitor` tinyint(1) NOT NULL DEFAULT '0',
  `profile_image` longtext COLLATE utf8mb4_unicode_ci,
  `phone_number` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `active_from` datetime DEFAULT NULL,
  `key_limit` int(11) NOT NULL,
  `is_temp` tinyint(1) NOT NULL,
  `is_first_login` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BAE7EFF6F85E0677` (`username`),
  KEY `IDX_BAE7EFF6D95AB405` (`user_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_profile`, `username`, `password`, `first_name`, `last_name`, `is_visitor`, `profile_image`, `phone_number`, `is_active`, `active_from`, `key_limit`, `is_temp`, `is_first_login`) VALUES
(1, 5, 'stefan.ciobanu@e-tracksystems.com', '548693e5d533f7db2d5b2ad98aeb592ffe11d9575e9b1671b9bcdf004224c1d6a9ae5606f9a8a9557a3067cbf9c83d34ed89bc3c1af162bc520f6655944b963e', 'eTrack', 'Engineer', 0, NULL, NULL, 1, '2022-01-07 09:28:14', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_cabinet_access`
--

DROP TABLE IF EXISTS `tbl_user_cabinet_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_cabinet_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_cabinet_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `cabinet` int(11) DEFAULT NULL COMMENT 'tbl_cabinets',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DA4A9058A76ED395` (`user_id`),
  KEY `IDX_DA4A90584CED05B0` (`cabinet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_defined_fields`
--

DROP TABLE IF EXISTS `tbl_user_defined_fields`;
CREATE TABLE IF NOT EXISTS `tbl_user_defined_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_defined_fields',
  `field_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `help_text` longtext COLLATE utf8mb4_unicode_ci,
  `error_text` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_defined_fields`
--

INSERT INTO `tbl_user_defined_fields` (`id`, `field_name`, `display_name`, `required`, `help_text`, `error_text`) VALUES
(1, 'field1', 'Position', 0, NULL, NULL),
(2, 'field2', 'Department', 0, NULL, NULL),
(3, 'field3', 'Company', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_entry_ids`
--

DROP TABLE IF EXISTS `tbl_user_entry_ids`;
CREATE TABLE IF NOT EXISTS `tbl_user_entry_ids` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_entry_ids',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `entry_type` int(11) DEFAULT NULL COMMENT 'tbl_entry_types',
  `entry_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_144D1FF7A76ED395` (`user_id`),
  KEY `IDX_144D1FF728FC2F3A` (`entry_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_entry_ids`
--

INSERT INTO `tbl_user_entry_ids` (`id`, `user_id`, `entry_type`, `entry_value`) VALUES
(1, 1, 1, '3218');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_group`
--

DROP TABLE IF EXISTS `tbl_user_group`;
CREATE TABLE IF NOT EXISTS `tbl_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_group',
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_limit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_location_access`
--

DROP TABLE IF EXISTS `tbl_user_location_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_location_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_location_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `location` int(11) DEFAULT NULL COMMENT 'tbl_locations',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_832E69AFA76ED395` (`user_id`),
  KEY `IDX_832E69AF5E9E89CB` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profiles`
--

DROP TABLE IF EXISTS `tbl_user_profiles`;
CREATE TABLE IF NOT EXISTS `tbl_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_profiles',
  `profile_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_722F043BE8EBF192` (`profile_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_profiles`
--

INSERT INTO `tbl_user_profiles` (`id`, `profile_name`, `profile_description`) VALUES
(1, 'Administrator', NULL),
(2, 'Standard', NULL),
(3, 'Report', NULL),
(4, 'Booking Only', NULL),
(5, 'Engineer', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profile_permissions`
--

DROP TABLE IF EXISTS `tbl_user_profile_permissions`;
CREATE TABLE IF NOT EXISTS `tbl_user_profile_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_profiles_permissions',
  `permission_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_profile_permissions`
--

INSERT INTO `tbl_user_profile_permissions` (`id`, `permission_name`) VALUES
(1, 'accessUsers'),
(2, 'manageUsers'),
(3, 'accessUserGroups'),
(4, 'manageUserGroups'),
(5, 'accessKeys'),
(6, 'manageKeys'),
(7, 'accessSettings'),
(8, 'accessReports'),
(9, 'accessSystems'),
(10, 'accessBookings'),
(11, 'makeBooking'),
(12, 'manageBooking'),
(13, 'isHandover'),
(14, 'autoApprove'),
(15, 'hasGlobalSearch'),
(16, 'webLogin'),
(17, 'manageRoles');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_site_access`
--

DROP TABLE IF EXISTS `tbl_user_site_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_site_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_site_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `site` int(11) DEFAULT NULL COMMENT 'tbl_sites',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D502CEECA76ED395` (`user_id`),
  KEY `IDX_D502CEEC694309E4` (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_system_access`
--

DROP TABLE IF EXISTS `tbl_user_system_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_system_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_system_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `system_id` int(11) DEFAULT NULL COMMENT 'tbl_systems',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6D6C063A76ED395` (`user_id`),
  KEY `IDX_E6D6C063D0952FA5` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_defined_field_values`
--

DROP TABLE IF EXISTS `tbl_user_to_defined_field_values`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_defined_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_defined_field_values',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `custom_field` int(11) DEFAULT NULL COMMENT 'tbl_user_defined_fields',
  `field_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_E4EDAF39A76ED395` (`user_id`),
  KEY `IDX_E4EDAF3998F8BD31` (`custom_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_groups`
--

DROP TABLE IF EXISTS `tbl_user_to_groups`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_groups',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `user_group` int(11) DEFAULT NULL COMMENT 'tbl_user_group',
  PRIMARY KEY (`id`),
  KEY `IDX_4194F3E4A76ED395` (`user_id`),
  KEY `IDX_4194F3E48F02BF9D` (`user_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_key_access`
--

DROP TABLE IF EXISTS `tbl_user_to_key_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_key_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_key_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `key_id` int(11) DEFAULT NULL COMMENT 'tbl_keys',
  `is_access_granted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DA527683A76ED395` (`user_id`),
  KEY `IDX_DA527683D145533` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_to_key_category_access`
--

DROP TABLE IF EXISTS `tbl_user_to_key_category_access`;
CREATE TABLE IF NOT EXISTS `tbl_user_to_key_category_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tbl_user_to_key_category_access',
  `user_id` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `key_category` int(11) DEFAULT NULL COMMENT 'tbl_key_categories',
  `is_access_granted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_785C0634A76ED395` (`user_id`),
  KEY `IDX_785C0634C74FD24E` (`key_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cabinets`
--
ALTER TABLE `tbl_cabinets`
  ADD CONSTRAINT `FK_871077AF865ED24D` FOREIGN KEY (`parent_system`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_events_log`
--
ALTER TABLE `tbl_events_log`
  ADD CONSTRAINT `FK_B0EDB50033EA99D0` FOREIGN KEY (`event_message`) REFERENCES `tbl_event_messages` (`id`),
  ADD CONSTRAINT `FK_B0EDB5007555FBC0` FOREIGN KEY (`event_action`) REFERENCES `tbl_event_actions` (`id`),
  ADD CONSTRAINT `FK_B0EDB500865ED24D` FOREIGN KEY (`parent_system`) REFERENCES `tbl_systems` (`id`),
  ADD CONSTRAINT `FK_B0EDB500A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_event_messages`
--
ALTER TABLE `tbl_event_messages`
  ADD CONSTRAINT `FK_A25BCAB28FC2F3A` FOREIGN KEY (`entry_type`) REFERENCES `tbl_entry_types` (`id`),
  ADD CONSTRAINT `FK_A25BCAB52C83E47` FOREIGN KEY (`handover_user`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_A25BCABD145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_group_cabinet_access`
--
ALTER TABLE `tbl_group_cabinet_access`
  ADD CONSTRAINT `FK_CA00992B4CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_CA00992B8F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_location_access`
--
ALTER TABLE `tbl_group_location_access`
  ADD CONSTRAINT `FK_4A3203205E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`),
  ADD CONSTRAINT `FK_4A3203208F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_site_access`
--
ALTER TABLE `tbl_group_site_access`
  ADD CONSTRAINT `FK_ED7E5458694309E4` FOREIGN KEY (`site`) REFERENCES `tbl_sites` (`id`),
  ADD CONSTRAINT `FK_ED7E54588F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`);

--
-- Constraints for table `tbl_group_system_access`
--
ALTER TABLE `tbl_group_system_access`
  ADD CONSTRAINT `FK_76A5E9F98F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_76A5E9F9D0952FA5` FOREIGN KEY (`system_id`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_group_to_key_access`
--
ALTER TABLE `tbl_group_to_key_access`
  ADD CONSTRAINT `FK_4A215F198F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_4A215F19D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_group_to_key_category_access`
--
ALTER TABLE `tbl_group_to_key_category_access`
  ADD CONSTRAINT `FK_1386B1C8F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_1386B1CC74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`);

--
-- Constraints for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  ADD CONSTRAINT `FK_1AE11044CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_1AE1104A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_1AE1104A76ED396` FOREIGN KEY (`category`) REFERENCES `tbl_key_categories` (`id`);

--
-- Constraints for table `tbl_key_to_additional_field_values`
--
ALTER TABLE `tbl_key_to_additional_field_values`
  ADD CONSTRAINT `FK_AB7E7ED127073E69` FOREIGN KEY (`additional_field`) REFERENCES `tbl_key_additional_fields` (`id`),
  ADD CONSTRAINT `FK_AB7E7ED1D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_key_to_categories`
--
ALTER TABLE `tbl_key_to_categories`
  ADD CONSTRAINT `FK_E19C7C95C74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`),
  ADD CONSTRAINT `FK_E19C7C95D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_password_reset_requests`
--
ALTER TABLE `tbl_password_reset_requests`
  ADD CONSTRAINT `FK_E182F2C5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_requests_log`
--
ALTER TABLE `tbl_requests_log`
  ADD CONSTRAINT `FK_4791EE56A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_role_to_permission`
--
ALTER TABLE `tbl_role_to_permission`
  ADD CONSTRAINT `FK_56344BE22722A5F7` FOREIGN KEY (`profile_permission`) REFERENCES `tbl_user_profile_permissions` (`id`),
  ADD CONSTRAINT `FK_56344BE2487CBE47` FOREIGN KEY (`account_profile`) REFERENCES `tbl_user_profiles` (`id`);

--
-- Constraints for table `tbl_sites`
--
ALTER TABLE `tbl_sites`
  ADD CONSTRAINT `FK_1264E07C5E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`);

--
-- Constraints for table `tbl_systems`
--
ALTER TABLE `tbl_systems`
  ADD CONSTRAINT `FK_3AFED0C654A03F50` FOREIGN KEY (`parent_site`) REFERENCES `tbl_sites` (`id`);

--
-- Constraints for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD CONSTRAINT `FK_BAE7EFF6D95AB405` FOREIGN KEY (`user_profile`) REFERENCES `tbl_user_profiles` (`id`);

--
-- Constraints for table `tbl_user_cabinet_access`
--
ALTER TABLE `tbl_user_cabinet_access`
  ADD CONSTRAINT `FK_DA4A90584CED05B0` FOREIGN KEY (`cabinet`) REFERENCES `tbl_cabinets` (`id`),
  ADD CONSTRAINT `FK_DA4A9058A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_entry_ids`
--
ALTER TABLE `tbl_user_entry_ids`
  ADD CONSTRAINT `FK_144D1FF728FC2F3A` FOREIGN KEY (`entry_type`) REFERENCES `tbl_entry_types` (`id`),
  ADD CONSTRAINT `FK_144D1FF7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_location_access`
--
ALTER TABLE `tbl_user_location_access`
  ADD CONSTRAINT `FK_832E69AF5E9E89CB` FOREIGN KEY (`location`) REFERENCES `tbl_locations` (`id`),
  ADD CONSTRAINT `FK_832E69AFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_site_access`
--
ALTER TABLE `tbl_user_site_access`
  ADD CONSTRAINT `FK_D502CEEC694309E4` FOREIGN KEY (`site`) REFERENCES `tbl_sites` (`id`),
  ADD CONSTRAINT `FK_D502CEECA76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_system_access`
--
ALTER TABLE `tbl_user_system_access`
  ADD CONSTRAINT `FK_E6D6C063A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_E6D6C063D0952FA5` FOREIGN KEY (`system_id`) REFERENCES `tbl_systems` (`id`);

--
-- Constraints for table `tbl_user_to_defined_field_values`
--
ALTER TABLE `tbl_user_to_defined_field_values`
  ADD CONSTRAINT `FK_E4EDAF3998F8BD31` FOREIGN KEY (`custom_field`) REFERENCES `tbl_user_defined_fields` (`id`),
  ADD CONSTRAINT `FK_E4EDAF39A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_to_groups`
--
ALTER TABLE `tbl_user_to_groups`
  ADD CONSTRAINT `FK_4194F3E48F02BF9D` FOREIGN KEY (`user_group`) REFERENCES `tbl_user_group` (`id`),
  ADD CONSTRAINT `FK_4194F3E4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_user_to_key_access`
--
ALTER TABLE `tbl_user_to_key_access`
  ADD CONSTRAINT `FK_DA527683A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_DA527683D145533` FOREIGN KEY (`key_id`) REFERENCES `tbl_keys` (`id`);

--
-- Constraints for table `tbl_user_to_key_category_access`
--
ALTER TABLE `tbl_user_to_key_category_access`
  ADD CONSTRAINT `FK_785C0634A76ED395` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `FK_785C0634C74FD24E` FOREIGN KEY (`key_category`) REFERENCES `tbl_key_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
