// Project.
#include "tagcategory.h"

// Tag category database tables.
const char* const TagCategory::_tagCategoryTable = "tbl_key_categories";
const char* const TagCategory::_groupToTagCategoryAccessTable = "tbl_group_to_key_category_access";
const char* const TagCategory::_tagToCategoriesTable = "tbl_key_to_categories";
const char* const TagCategory::_userToTagCategoryAccessTable = "tbl_user_to_key_category_access";

// Tag category database fields.
const char* const TagCategory::_tagCategory = "key_category";
const char* const TagCategory::_tagCategoryId = "category_id";
const char* const TagCategory::_tagCategoryType = "category_type";
const char* const TagCategory::_tagCategoryIcon = "category_icon";
const char* const TagCategory::_tagCategoryName = "category_name";
const char* const TagCategory::_tagCategoryShortName = "category_short_name";

// Tag category commands.
const char* const TagCategory::_newKeyCategory = "new_key_category";
const char* const TagCategory::_updateKeyCategory = "update_key_category";
const char* const TagCategory::_removeKeyCategory = "remove_key_category";

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategory::TagCategory( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
TagCategory::TagCategory( QObject* parent )
    : QObject( parent )
    , m_tagCategoryId( -1 )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategory::operator==( const TagCategory& lhs ) const
///
/// \brief  Equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool TagCategory::operator==( const TagCategory& lhs ) const
{
    return ( m_tagCategoryId == lhs.m_tagCategoryId &&
             m_tagCategoryType == lhs.m_tagCategoryType &&
             m_tagCategoryIcon == lhs.m_tagCategoryIcon &&
             m_tagCategoryName == lhs.m_tagCategoryName &&
             m_tagCategoryShortName == lhs.m_tagCategoryShortName );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategory::operator!=( TagCategory User& lhs ) const
///
/// \brief  Not equal comparison operator.
////////////////////////////////////////////////////////////////////////////////
bool TagCategory::operator!=(const TagCategory& lhs) const
{
    return !( *this == lhs );
}
