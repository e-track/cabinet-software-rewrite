#ifndef CABINET_H
#define CABINET_H

// Qt.
#include <QObject>

// Project.
#include "databasecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the cabinets database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT Cabinet : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int cabinetId       MEMBER m_id           NOTIFY cabinetIdChanged    )
    Q_PROPERTY( int slotCapacity    MEMBER m_slotCapacity NOTIFY slotCapacityChanged )
    Q_PROPERTY( int startSlot       MEMBER m_startSlot    NOTIFY startSlotChanged    )
    Q_PROPERTY( int endSlot         MEMBER m_endSlot      NOTIFY endSlotChanged      )
    Q_PROPERTY( int slotWidth       MEMBER m_slotWidth    NOTIFY slotWidthChanged    )
    Q_PROPERTY( int noOfLocks       MEMBER m_noOfLocks    NOTIFY noOfLocksChanged    )
    Q_PROPERTY( QString cabinetName MEMBER m_cabinetName  NOTIFY cabinetNameChanged  )

public:
    explicit Cabinet( QObject* parent = nullptr );
    virtual ~Cabinet() = default;

    int m_id;
    int m_slotCapacity;
    int m_startSlot;
    int m_endSlot;
    int m_slotWidth;
    int m_noOfLocks;

    QString m_cabinetName;

    bool operator==( const Cabinet& lhs ) const;
    bool operator!=( const Cabinet& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _id;
    DATABASECONTROL_EXPORT static const char* const _slotCapacity;
    DATABASECONTROL_EXPORT static const char* const _startSlot;
    DATABASECONTROL_EXPORT static const char* const _endSlot;
    DATABASECONTROL_EXPORT static const char* const _slotWidth;
    DATABASECONTROL_EXPORT static const char* const _noOfLocks;
    DATABASECONTROL_EXPORT static const char* const _cabinetName;

signals:
    void cabinetIdChanged();
    void slotCapacityChanged();
    void startSlotChanged();
    void endSlotChanged();
    void slotWidthChanged();
    void noOfLocksChanged();
    void cabinetNameChanged();
};

Q_DECLARE_METATYPE(Cabinet);

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the group_cabinet_access database table.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT GroupCabinetAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit GroupCabinetAccess();

    int m_id;
    bool m_isAccessGranted;

    bool operator==( const GroupCabinetAccess& lhs ) const;
    bool operator!=( const GroupCabinetAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _groupCabinetAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userGroup;
    DATABASECONTROL_EXPORT static const char* const _cabinet;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief  Representation of an entry in the user_cabinet_access database table.
///         TODO: Consider whether to have a single CabinetAccess class for both
///         user and user group cabinet access since the properties are identical.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROL_EXPORT UserCabinetAccess
{
    Q_GADGET
    Q_PROPERTY( int id               MEMBER m_id )
    Q_PROPERTY( bool isAccessGranted MEMBER m_isAccessGranted )

public:
    explicit UserCabinetAccess();

    int m_id;
    bool m_isAccessGranted;

    bool operator==( const UserCabinetAccess& lhs ) const;
    bool operator!=( const UserCabinetAccess& lhs ) const;

    DATABASECONTROL_EXPORT static const char* const _userCabinetAccessTable;
    DATABASECONTROL_EXPORT static const char* const _userId;
    DATABASECONTROL_EXPORT static const char* const _cabinet;
    DATABASECONTROL_EXPORT static const char* const _isAccessGranted;
};

#endif // CABINET_H
