// Project.
#include "mockmessagemanager.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockMessageManager::MockMessageManager( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
MockMessageManager::MockMessageManager( QObject* parent )
    : MessageManager( parent )
    , m_mockPublishedPayload( "" )
    , m_mockTimestamp( "" )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockMessageManager::doReceivePayload( QString payload )
///
/// \brief  Mocks the payload receive signal.
///
/// \param  The mock payload that was received.
////////////////////////////////////////////////////////////////////////////////
void MockMessageManager::doReceivePayload( QString payload )
{
    emit payloadReceived( payload );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockMessageManager::publish( const QString& message )
///
/// \brief  Mocks publishing the given message with its corresponding topic.
///
/// \param  The mock message to be published.
////////////////////////////////////////////////////////////////////////////////
void MockMessageManager::publish( const QString& message )
{
    m_mockPublishedPayload = message;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockMessageManager::getTimestamp()
///
/// \brief  Mocks the current timestamp.
///
/// \return The mocked current timestamp.
////////////////////////////////////////////////////////////////////////////////
QString MockMessageManager::getTimestamp()
{
    return m_mockTimestamp;
}
