#ifndef MOCKQUERYMANAGER_H
#define MOCKQUERYMANAGER_H

// Qt.
#include <QMap>

// Library.
#include <DatabaseControl/querymanager.h>
#include <DatabaseControl/tag.h>
#include <DatabaseControl/user.h>
#include <DatabaseControl/usergroup.h>

////////////////////////////////////////////////////////////////////////////////
/// \brief  Mock implementation of QueryManager.
////////////////////////////////////////////////////////////////////////////////
class MockQueryManager : public QueryManager
{
    Q_OBJECT

public:
    explicit MockQueryManager( QObject* parent = nullptr );
    ~MockQueryManager() = default;

    // Initialisation.
    void doInitDatabaseComplete( bool success );
    virtual void initDatabase();

    // Database access.
    virtual void openDatabase();
    virtual void closeDatabase();

    // Login.
    virtual bool loginTempPin( const QString& tempPin );
    virtual bool loginPin( const User& user, const QString& pin );

    // Events.
    virtual bool eventLogLoginSuccess( const User& user );
    virtual bool eventLogLoginFail( const User& user );
    virtual bool eventLogLogout( const User& user );
    virtual bool eventLogAccessDenied( const User& user );
    virtual void eventLogUpdateTag( Tag* tag, const User& user );

    // PIN.
    virtual bool updatePin( const QString& pin, const User& user );
    virtual bool resetTempPin( const User& user );

    // Save.
    virtual void saveAll();

    // General SQL queries.
    virtual bool performSqlQuery( const QString& sql );
    virtual int getNumRowsAffected();

    // Authentication.
    virtual bool addFingerprint( const User& user, const int fingerprintId );
    virtual bool addCard( const User& user, const QString& cardId );

    // Users.
    virtual bool addUser( const User& user );
    virtual bool updateUser( const User& user );
    virtual bool removeUser( const User& user, const QString& table );
    virtual bool getUser( const int userId );
    virtual bool getUserEntry( const int fingerprintId, const QString& cardId );
    virtual bool getUserProfiles();
    virtual bool getUserProfilePermissions();

    // User groups.
    virtual bool getUsersGroup( const User& user );
    virtual bool getUserGroup( const int id );
    virtual bool addUserGroup( const UserGroup& userGroup );
    virtual bool removeUserGroup( const UserGroup& userGroup, const QString& table );
    virtual bool updateUserGroup( const UserGroup& userGroup );
    virtual bool addUserToUserGroup( const int& userId, const int& userGroupId );
    virtual bool removeUserFromUserGroup( const int& userId, const int& userGroupId );

    // Cabinets.
    virtual bool getRegisteredCabinet();
    virtual bool getUserGroupCabinetAccess( const UserGroup& group );
    virtual bool getUserCabinetAccess( const User& user );

    // Tags / keys.
    void doSearchTagsComplete( bool success );

    virtual bool getTags( const QString& column, const QVariant& value );
    virtual bool addTag( Tag* tag );
    virtual bool removeTag( Tag* tag, const QString& table );

    virtual bool addTagUserPermissions( const UserTagAccess& userTagAccess );
    virtual bool removeTagUserPermissions( const UserTagAccess& userTagAccess );
    virtual bool updateTagUserPermissions( const UserTagAccess& userTagAccess );

    virtual bool addTagGroupPermissions( const GroupTagAccess& groupTagAccess );
    virtual bool removeTagGroupPermissions( const GroupTagAccess& groupTagAccess );
    virtual bool updateTagGroupPermissions( const GroupTagAccess& groupTagAccess );

    virtual bool getUserGroupTagAccess( const UserGroup& user );
    virtual bool getUserTagAccess( const User& user );

    // Value.
    virtual QVariant getValue( const QString& key );
    virtual bool getNext();

    // Systems.
    virtual bool addSystem( const System& system );
    virtual bool getUserGroupSystemAccess( const UserGroup& group );
    virtual bool getUserSystemAccess( const User& user );
    virtual bool getSystem();

    // Queries.
    bool m_sqlSuccess;                                      ///< Mocks whether SQL queries will be successful.
    bool m_loginPinSuccess;                                 ///< Mocks whether a login with PIN will be successful.
    bool m_loginTempPinSuccess;                             ///< Mocks whether a login with temporary PIN will be successful.
    bool m_updatePinSuccess;                                ///< Mocks whether PIN update will be successful.
    bool m_resetTempPinSuccess;                             ///< Mocks whether reset temporary PIN will be successful.
    bool m_addFingerprintSuccess;                           ///< Mocks whether adding a fingerprint will be successful.
    bool m_addCardSuccess;                                  ///< Mocks whether adding a card will be successful.
    bool m_addSystemSuccess;                                ///< Mocks whether adding a system will be successful.
    bool m_getUserProfilesSuccess;                          ///< Mocks whether getting user profiles will be successful.
    bool m_getUserProfilePermissionsSuccess;                ///< Mocks whether getting user profile permissions will be successful.
    bool m_getSystemSuccess;                                ///< Mocks whether getting the system will be successful.

    UserEntry m_mockUserEntry;                              ///< Mock user entry.

    // User queries.
    bool m_newUserSuccess;                                  ///< Mocks whether adding new users is successful.
    bool m_updateUserSuccess;                               ///< Mocks whether updating users is successful.
    bool m_removeUserSuccess;                               ///< Mocks whether removing users is successful.
    bool m_getUserSucess;                                   ///< Mocks whether getting user is successful.
    bool m_getUserEntrySucess;                              ///< Mocks whether getting user entry ID is successful.

    User m_mockNewUser;                                     ///< Mock new user that was added.
    User m_mockUpdatedUser;                                 ///< Mock user that was updated.
    User m_mockRemovedUser;                                 ///< Mock user that was removed.

    // User group queries.
    bool m_getUsersGroupSuccess;                            ///< Mocks whether getting the user's group is successful.
    bool m_getUserGroupSuccess;                             ///< Mocks whether getting the user group is successful.
    bool m_addUserGroupSuccess;                             ///< Mocks whether adding a new user group is successful.
    bool m_addUserToGroupSuccess;                           ///< Mocks whether adding a user to a user group is successful.
    bool m_updateUserGroupSuccess;                          ///< Mocks whether updating a new user group is successful.
    bool m_updateUserToGroupSuccess;                        ///< Mocks whether updating a user to a user group is successful.
    bool m_removeUserGroupSuccess;                          ///< Mocks whether removing a new user group is successful.
    bool m_removeUserToGroupSuccess;                        ///< Mocks whether removing a user to a user group is successful.
    bool m_userGroupQuerySuccess;                           ///< Mocks whether a user group query is successful (TODO: This should replace all the above).

    UserGroup m_mockUserGroup;                              ///< Mock user group.
    int m_mockUserId;                                       ///< Mock user ID that was added to mock user group ID.
    int m_mockUserGroupId;                                  ///< Mock user group ID that mock user ID was added to.

    // System access mocks.
    bool m_getUserGroupSystemAccessSuccess;                 ///< Mocks user group system access success.
    bool m_getUserSystemAccessSuccess;                      ///< Mocks user system access success.
    bool m_mockUserGroupSystemAccess;                       ///< Mocks the user group system access.
    bool m_mockUserSystemAccess;                            ///< Mocks the user system access.

    System m_newSystem;                                     ///< Mock system that was added.

    // Cabinet access mocks.
    bool m_getUserGroupCabinetAccessSuccess;                ///< Mocks user group cabinet access success.
    bool m_getUserCabinetAccessSuccess;                     ///< Mocks user cabinet access success.
    bool m_mockUserGroupCabinetAccess;                      ///< Mocks the user group cabinet access.
    bool m_mockUserCabinetAccess;                           ///< Mocks the user cabinet access.
    bool m_getRegisteredCabinetSuccess;                     ///< Mocks whether getting the registered cabinet is successful.

    // Tags / keys.
    bool m_tagQuerySuccess;                                 ///< Mocks whether tag / key related queries will be successful.
    bool m_getUserGroupTagAccessSuccess;                    ///< Mocks user group tag / key access success.
    bool m_getUserTagAccessSuccess;                         ///< Mocks user tag / key access success.

    Tag* m_mockTag;                                         ///< Mock tag that was added, edited or removed.
    GroupTagAccess m_mockUserGroupTagAccess;                ///< Mocks the user group tag / key access.
    UserTagAccess m_mockUserTagAccess;                      ///< Mocks the user tag / key access.

    // Events.
    bool m_eventLoginSuccess;                               ///< Mock login success event query.
    bool m_eventLoginFail;                                  ///< Mock login fail event query.
    bool m_eventLogout;                                     ///< Mock logout event query.
    bool m_eventAccessDenied;                               ///< Mock access denied event query.

    // Values.
    QMap<QString, QVariant> m_mockValues;                   ///< Mock values to be returned.
    int m_next;                                             ///< Mocks the number of next query results.
    int m_nextCount;                                        ///< Counts the number of next query results.
    int m_mockRowsAffected;                                 ///< Mocks the number of rows affected.

    // General statics.
    static const int _testId;                               ///< Test ID.

    // User statics.
    static const char* _testFirstName;                      ///< Test first name.
    static const char* _testLastName;                       ///< Test last name.
    static const char* _testTempPin;                        ///< Test temp PIN.

    // User group statics.
    static const int _testUserGroupId;                      ///< Test user group ID.
    static const int _testUserGroupKeyLimit;                ///< Test user group key limit.
    static const char* _testUserGroupName;                  ///< Test user group name.

    // Cabinet statics.
    static const int _testSlotCapacity;                     ///< Test slot capacity.
    static const int _testStartSlot;                        ///< Test start slot.
    static const int _testEndSlot;                          ///< Test end slot.
    static const int _testSlotWidth;                        ///< Test slot width.
    static const int _testNoOfLocks;                        ///< Test number of locks.
    static const char* _testCabinetName;                    ///< Test cabinet name.

    // System access statics.
    static const int _testSystem;                           ///< Test system ID.
    static const int _testUserGroup;                        ///< Test user group ID.
    static const int _testUser;                             ///< Test user ID.

    // User profiles statics.
    static const int _testUserProfilesId;                   ///< Test user profile ID.
    static const char* _testUserProfilesName;               ///< Test user profile name.

    // User profile permissions statics.
    static const int _testUserProfilePermissionsId;         ///< Test user profile permissions ID.
    static const char* _testUserProfilePermissionsName;     ///< Test user profile permissions name.

    // Tag statics.
    static const int _testTagId;                            ///< Test tag ID for tag queries.

public slots:
    // Tags / keys.
    virtual void updateTag( Tag* tag );

    // Cabinets.
    virtual void getCabinets();
};

#endif // MOCKQUERYMANAGER_H
