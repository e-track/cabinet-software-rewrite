#ifndef MOCKMESSAGEMANAGER_H
#define MOCKMESSAGEMANAGER_H

// Library.
#include <DatabaseControl/messagemanager.h>

////////////////////////////////////////////////////////////////////////////////
/// \brief  Mock implementation of MessageManager.
////////////////////////////////////////////////////////////////////////////////
class MockMessageManager : public MessageManager
{
    Q_OBJECT

public:
    explicit MockMessageManager( QObject* parent = nullptr );
    ~MockMessageManager() = default;

    // Mock signals.
    void doReceivePayload( QString payload );

    // Publish.
    virtual void publish( const QString& message );

    // Timestamp.
    virtual QString getTimestamp();

    QString m_mockPublishedPayload;     ///< Mock published payload.
    QString m_mockTimestamp;            ///< Mock timestamp.
};

#endif // MOCKMESSAGEMANAGER_H
