// Project.
#include "mockquerymanager.h"

// Library.
#include <DatabaseControl/constants.h>
#include <DatabaseControl/usergroup.h>
#include <DatabaseControl/userprofilepermissions.h>
#include <DatabaseControl/userprofiles.h>

// General statics.
const int MockQueryManager::_testId = 11;

// User statics.
const char* MockQueryManager::_testFirstName = "TEST_FIRST_NAME";
const char* MockQueryManager::_testLastName = "TEST_LAST_NAME";
const char* MockQueryManager::_testTempPin = "TEST_TEMP_PIN";

// User group statics.
const int MockQueryManager::_testUserGroupId = 11;
const int MockQueryManager::_testUserGroupKeyLimit = 22;
const char* MockQueryManager::_testUserGroupName = "TEST_USER_GROUP_NAME";

// Cabinet statics.
const int MockQueryManager::_testSlotCapacity = 22;
const int MockQueryManager::_testStartSlot = 33;
const int MockQueryManager::_testEndSlot = 44;
const int MockQueryManager::_testSlotWidth = 55;
const int MockQueryManager::_testNoOfLocks = 66;
const char* MockQueryManager::_testCabinetName = "TEST_CABINET";

// System access statics.
const int MockQueryManager::_testSystem = 11;
const int MockQueryManager::_testUserGroup = 22;
const int MockQueryManager::_testUser = 33;

// User profiles statics.
const int MockQueryManager::_testUserProfilesId = 4;
const char* MockQueryManager::_testUserProfilesName = "TEST_USER_PROFILE_NAME";

// User profile permissions statics.
const int MockQueryManager::_testUserProfilePermissionsId = 8;
const char* MockQueryManager::_testUserProfilePermissionsName = "TEST_USER_PROFILE_PERMISSIONS_NAME";

// Tag statics.
const int MockQueryManager::_testTagId = 22;

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::MockQueryManager( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
MockQueryManager::MockQueryManager( QObject* parent )
    : QueryManager( parent )
    , m_sqlSuccess( false )
    , m_loginPinSuccess( false )
    , m_loginTempPinSuccess( false )
    , m_updatePinSuccess( false )
    , m_resetTempPinSuccess( false )
    , m_addFingerprintSuccess( false )
    , m_addCardSuccess( false )
    , m_addSystemSuccess( false )
    , m_getUserProfilesSuccess( true )
    , m_getUserProfilePermissionsSuccess( true )
    , m_getSystemSuccess( false )
    , m_newUserSuccess( false )
    , m_updateUserSuccess( false )
    , m_removeUserSuccess( false )
    , m_getUserSucess( false )
    , m_getUserEntrySucess( false )
    , m_getUsersGroupSuccess( false )
    , m_getUserGroupSuccess( false )
    , m_addUserGroupSuccess( false )
    , m_addUserToGroupSuccess( false )
    , m_updateUserGroupSuccess( false )
    , m_updateUserToGroupSuccess( false )
    , m_removeUserGroupSuccess( false )
    , m_removeUserToGroupSuccess( false )
    , m_userGroupQuerySuccess( false )
    , m_mockUserId( -1 )
    , m_mockUserGroupId( -1 )
    , m_getUserGroupSystemAccessSuccess( false )
    , m_getUserSystemAccessSuccess( false )
    , m_mockUserGroupSystemAccess( false )
    , m_mockUserSystemAccess( false )
    , m_getUserGroupCabinetAccessSuccess( false )
    , m_getUserCabinetAccessSuccess( false )
    , m_mockUserGroupCabinetAccess( false )
    , m_mockUserCabinetAccess( false )
    , m_getRegisteredCabinetSuccess( false )
    , m_tagQuerySuccess( false )
    , m_getUserGroupTagAccessSuccess( false )
    , m_getUserTagAccessSuccess( false )
    , m_mockTag( nullptr )
    , m_eventLoginSuccess( false )
    , m_eventLoginFail( false )
    , m_eventLogout( false )
    , m_eventAccessDenied( false )
    , m_next( 1 )
    , m_nextCount( 0 )
    , m_mockRowsAffected( 1 )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::doInitDatabaseComplete( bool success )
///
/// \brief  Mocks initDatabaseComplete() signal.
///
/// \param  True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::doInitDatabaseComplete( bool success )
{
    emit initDatabaseComplete( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::initDatabase()
///
/// \brief  Mocks initialising the database.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::initDatabase()
{
    // Create query for the new database.
    m_query = new QSqlQuery( QSqlDatabase::database( QueryManager::_defaultDatabaseName ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::openDatabase()
///
/// \brief  Mocks opening the database.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::openDatabase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::closeDatabase()
///
/// \brief  Mocks closing the database.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::closeDatabase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::loginTempPin( const QString& tempPin )
///
/// \brief  Mocks login with the given temporary PIN.
///
/// \param  Temporary PIN.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::loginTempPin( const QString& tempPin )
{
    Q_UNUSED( tempPin );

    if ( m_loginTempPinSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_firstName] = _testFirstName;
        m_mockValues[DatabaseControlConstants::_lastName] = _testLastName;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_firstName] = QVariant();
        m_mockValues[DatabaseControlConstants::_lastName] = QVariant();
    }

    return m_loginTempPinSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::loginPin( const QString& pin )
///
/// \brief  Mocks login with the given PIN.
///
/// \param  User.
/// \param  PIN.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::loginPin( const User& user, const QString& pin )
{
    Q_UNUSED( user )
    Q_UNUSED( pin )

    if ( m_loginPinSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_firstName] = _testFirstName;
        m_mockValues[DatabaseControlConstants::_lastName] = _testLastName;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_firstName] = QVariant();
        m_mockValues[DatabaseControlConstants::_lastName] = QVariant();
    }

    return m_loginPinSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::eventLogLoginSuccess( const User& user )
///
/// \brief  Mocks adding a successful login event to the database.
///
/// \param  Authenticated user that logged in.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::eventLogLoginSuccess( const User& user )
{
    Q_UNUSED( user );

    m_eventLoginSuccess = true;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::eventLogLoginFail( const User& user )
///
/// \brief  Mocks adding a failed login event to the database.
///
/// \param  Authenticated user that failed to log in.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::eventLogLoginFail( const User& user )
{
    Q_UNUSED( user );

    m_eventLoginFail = true;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::eventLogLogout( const User& user )
///
/// \brief  Mocks adding a successful logout event to the database.
///
/// \param  Authenticated user that logged out.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::eventLogLogout( const User& user )
{
    Q_UNUSED( user );

    m_eventLogout = true;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::eventLogAccessDenied( const User& user )
///
/// \brief  Mocks adding an access denied event to the database.
///
/// \param  User that denied access.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::eventLogAccessDenied( const User& user )
{
    Q_UNUSED( user );

    m_eventAccessDenied = true;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::eventLogUpdateTag( Tag* tag, const User& user  )
///
/// \brief  Mocks adding a tag / key updated event to the database.
///
/// \param  Tag that was updated.
/// \param  Authenticated user that logged in.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::eventLogUpdateTag( Tag* tag, const User& user )
{
    Q_UNUSED( tag );
    Q_UNUSED( user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updatePin( const QString& pin, const User& user )
///
/// \brief  Mocks PIN update.
///
/// \param  PIN.
/// \param  User to update.
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::updatePin( const QString& pin, const User& user )
{
    Q_UNUSED( pin )
    Q_UNUSED( user )

    m_mockUserEntry.m_userId = user.m_id;
    m_mockUserEntry.m_entryType = _entryUsePin;
    m_mockUserEntry.m_entryValue = pin;

    return m_updatePinSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::saveAll()
///
/// \brief  Mocks saving all pending changed to the database.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::saveAll()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::resetTempPin( const User& user )
///
/// \brief  Mocks temporary PIN reset.
///
/// \param  User to update.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::resetTempPin( const User& user )
{
    Q_UNUSED( user );

    return m_resetTempPinSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::performSqlQuery( const QString& sql )
///
/// \brief  Mocks general SQL query.
///
/// \param  The SQL query.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::performSqlQuery( const QString& sql )
{
    Q_UNUSED( sql );

    return m_sqlSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getNumRowsAffected()
///
/// \brief  Mocks getting the number of rows affected by the most recent query.
///
/// \return The number of rows that were affected.
////////////////////////////////////////////////////////////////////////////////
int MockQueryManager::getNumRowsAffected()
{
    return m_mockRowsAffected;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addFingerprint( const int userId, const int fingerprintId )
///
/// \brief  Mocks adding fingerprint ID.
///
/// \param  The user to update.
/// \param  The fingerprint ID.
///
/// \return True if the mock write was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addFingerprint( const User& user, const int fingerprintId )
{
    Q_UNUSED( user );
    Q_UNUSED( fingerprintId );

    return m_addFingerprintSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addCard( const int userId, const int cardId )
///
/// \brief  Mocks adding card ID.
///
/// \param  The user to update.
/// \param  The card ID.
///
/// \return True if the mock write was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addCard( const User& user, const QString& cardId )
{
    Q_UNUSED( user );
    Q_UNUSED( cardId );

    return m_addCardSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addUser( const User& user )
///
/// \brief  Mocks writing the given user to the database.
///
/// \param  The user to add.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addUser( const User& user )
{
    m_mockNewUser = user;

    return m_newUserSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updateUser( const User& user )
///
/// \brief  Mocks updating the given user to the database.
///
/// \param  The user to update.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::updateUser( const User& user )
{
    m_mockUpdatedUser = user;

    return m_updateUserSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::removeUser( const User& user, const QString& table )
///
/// \brief  Mocks removing a user from a table in the database.
///
/// \param  The user to remove.
/// \param  The table to remove the user from.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeUser( const User& user, const QString& table )
{
    Q_UNUSED( table );

    m_mockRemovedUser = user;

    return m_removeUserSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUser( const int userId )
///
/// \brief  Mocks getting the user that corresponds to the given user ID.
///
/// \param  The user ID to match.
///
/// \return True to mock succes, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUser( const int userId )
{
    Q_UNUSED( userId );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserSucess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_firstName] = _testFirstName;
        m_mockValues[DatabaseControlConstants::_lastName] = _testLastName;
    }

    return m_getUserSucess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserEntry( const int fingerprintId, const int cardId )
///
/// \brief  Mocks getting the user entry that corresponds to the given fingerprint ID.
///
/// \param  The fingerprint ID to match.
/// \param  The fingerprint ID to match.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserEntry( const int fingerprintId, const QString& cardId )
{
    Q_UNUSED( fingerprintId );
    Q_UNUSED( cardId );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserEntrySucess )
    {
        m_mockValues[UserEntry::_userId] = 11;
        m_mockValues[UserEntry::_entryType] = 22;
        m_mockValues[UserEntry::_entryValue] = "3344";
    }

    return m_getUserEntrySucess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUsersGroup( UserEntry& userEntry )
///
/// \brief  Mocks getting the group ID for the given user.
///
/// \param  Unused.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUsersGroup(const User& user )
{
    Q_UNUSED( user );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUsersGroupSuccess )
    {
        m_mockValues[UserGroup::_userGroup] = _testUserGroupId;
    }

    return m_getUsersGroupSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserGroup( const int id )
///
/// \brief  Mocks getting the user group for the given user group ID.
///
/// \param  Unused.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserGroup( const int id )
{
    Q_UNUSED( id );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserGroupSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testUserGroupId;
        m_mockValues[UserGroup::_groupName] = _testUserGroupName;
        m_mockValues[UserGroup::_keyLimit] =_testUserGroupKeyLimit;
    }

    return m_getUserGroupSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addUserGroup( const UserGroup& userGroup )
///
/// \brief  Mocks adding a user group.
///
/// \param  The user group to be added.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addUserGroup( const UserGroup& userGroup )
{
    // Reset mock next count.
    m_nextCount = 0;

    if ( m_addUserGroupSuccess )
    {
        m_mockUserGroup.m_id = _testUserGroupId;
        m_mockUserGroup.m_groupName = userGroup.m_groupName;
        m_mockUserGroup.m_keyLimit = userGroup.m_keyLimit;
    }

    return m_addUserGroupSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::removeUserGroup( const UserGroup& userGroup )
///
/// \brief  Mocks removing a user group from a table in the database.
///
/// \param  The mock user group to remove.
/// \param  The table to remove the user group from.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeUserGroup( const UserGroup& userGroup, const QString& table )
{
    Q_UNUSED( table );

    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserGroup = userGroup;

    return m_removeUserGroupSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updateUserGroup( const UserGroup& userGroup )
///
/// \brief  Mocks updating a user group.
///
/// \param  The mock user group to be updated.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::updateUserGroup( const UserGroup& userGroup )
{
    // Reset mock next count.
    m_nextCount = 0;

    if ( m_updateUserGroupSuccess )
    {
        m_mockUserGroup = userGroup;
    }

    return m_updateUserGroupSuccess;
}

/// ////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addUserToUserGroup( const int& userId, const int& userGroupId )
///
/// \brief  Mocks adding a user ID to a user group ID.
///
/// \param  The user ID to be added.
/// \param  The user group ID to add the user to.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addUserToUserGroup( const int& userId, const int& userGroupId )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserId = userId;
    m_mockUserGroupId = userGroupId;

    return m_addUserToGroupSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManager::removeUserFromUserGroup( const int& user, const int& userGroupId )
///
/// \brief  Mocks removing a user from a user group.
///
/// \param  The user ID to be remove.
/// \param  The user group ID to remove ther user from.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeUserFromUserGroup( const int& userId, const int& userGroupId )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserId = userId;
    m_mockUserGroupId = userGroupId;

    return m_userGroupQuerySuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserProfiles()
///
/// \brief  Mocks getting the user profiles from the database.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserProfiles()
{
    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserProfilesSuccess )
    {
        m_mockValues[UserProfiles::_id] = _testUserProfilesId;
        m_mockValues[UserProfiles::_profileName] = _testUserProfilesName;
    }

    return m_getUserProfilesSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserProfilePermissions()
///
/// \brief  Mocks getting the user profile permissions from the database.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserProfilePermissions()
{
    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserProfilePermissionsSuccess )
    {
        m_mockValues[UserProfilePermissions::_id] = _testUserProfilePermissionsId;
        m_mockValues[UserProfilePermissions::_permissionName] = _testUserProfilePermissionsName;
    }

    return m_getUserProfilePermissionsSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getCabinets()
///
/// \brief  Mocks getting the cabinets.
///
/// \return True to mock a successful database read, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::getCabinets()
{
    // Reset mock next count.
    m_nextCount = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getRegisteredCabinet()
///
/// \brief  Executes a query to get the registered cabinet in the database.
///         Presently, all cabinets are obtained and it is assumed that the
///         first cabinet is the registered one.
///         TODO: Update this following real cabinet registration.
///
/// \return True if query was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getRegisteredCabinet()
{
    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getRegisteredCabinetSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_slotCapacity] = _testSlotCapacity;
        m_mockValues[DatabaseControlConstants::_startSlot] = _testStartSlot;
        m_mockValues[DatabaseControlConstants::_endSlot] = _testEndSlot;
        m_mockValues[DatabaseControlConstants::_slotWidth] = _testSlotWidth;
        m_mockValues[DatabaseControlConstants::_noOfLocks] = _testNoOfLocks;
        m_mockValues[DatabaseControlConstants::_cabinetName] = _testCabinetName;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_slotCapacity] = QVariant();
        m_mockValues[DatabaseControlConstants::_startSlot] = QVariant();
        m_mockValues[DatabaseControlConstants::_endSlot] = QVariant();
        m_mockValues[DatabaseControlConstants::_slotWidth] = QVariant();
        m_mockValues[DatabaseControlConstants::_noOfLocks] = QVariant();
        m_mockValues[DatabaseControlConstants::_cabinetName] = QVariant();
    }

    return m_getRegisteredCabinetSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserGroupCabinetAccess( const UserGroup& group )
///
/// \brief  Mocks getting the cabinet access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True to mock a successful database read, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserGroupCabinetAccess( const UserGroup& group )
{
    Q_UNUSED( group );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserGroupCabinetAccessSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = m_mockUserGroupCabinetAccess;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = QVariant();
    }

    return m_getUserGroupCabinetAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserCabinetAccess( const User& user )
///
/// \brief  Gets the cabinet access for the given user.
///
/// \param  The user to search for.
///
/// \return True to mock a successful database read, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserCabinetAccess( const User& user )
{
    Q_UNUSED( user );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserCabinetAccessSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = m_mockUserCabinetAccess;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = QVariant();
    }

    return m_getUserCabinetAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::doSearchTagsComplete( bool success )
///
/// \brief  Mocks searchTagsComplete() signal.
///
/// \param  True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::doSearchTagsComplete( bool success )
{
    emit searchTagsComplete( success );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getTags( const QString& column, const QVariant& value )
///
/// \brief  Mocks getting all tags that match the value to the given column.
///
/// \param  The column to be matched.
/// \param  The value to match.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getTags( const QString& column, const QVariant& value )
{
    Q_UNUSED( column );
    Q_UNUSED( value );

    // Reset mock next count.
    m_nextCount = 0;

    m_mockValues[DatabaseControlConstants::_id] = _testTagId;

    return m_tagQuerySuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addTag( Tag* tag )
///
/// \brief  Mocks writing the given tag / key to the database.
///
/// \param  The tag / key to add.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addTag( Tag* tag )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockTag = tag;

    return m_tagQuerySuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::removeTag( Tag* tag, const QString& table )
///
/// \brief  Mocks removing the given tag / key from the database.
/// \param  The table to remove the tag / key from.
///
/// \param  The tag / key to removed.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeTag( Tag* tag, const QString& table )
{
    Q_UNUSED( table );

    // Reset mock next count.
    m_nextCount = 0;

    m_mockTag = tag;

    return m_tagQuerySuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updateTag( Tag* tag )
///
/// \brief  Mocks updating the tag in the database using the fob as the identifier.
///
/// \param  The tag containing the updated data.
////////////////////////////////////////////////////////////////////////////////
void MockQueryManager::updateTag( Tag* tag )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockTag = tag;

    emit updateTagComplete( tag, m_tagQuerySuccess );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Mocks adding the given user tag / key permissons to the database.
///
/// \param  The user tag / key permissions to add.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addTagUserPermissions( const UserTagAccess& userTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserTagAccess = userTagAccess;

    return m_getUserTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::removeTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Mocks removing the given user tag / key permissions from the database.
///
/// \param  The tag / key permissions to removed.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeTagUserPermissions( const UserTagAccess& userTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserTagAccess = userTagAccess;

    return m_getUserTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updateTagUserPermissions( const UserTagAccess& userTagAccess )
///
/// \brief  Mocks updating the user tag / key permissions in the database.
///
/// \param  The new user tag / key permissions to be updated data.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::updateTagUserPermissions( const UserTagAccess& userTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserTagAccess = userTagAccess;

    return m_getUserTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Mocks adding the given user group tag / key permissons to the database.
///
/// \param  The user group tag / key permissions to add.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserGroupTagAccess = groupTagAccess;

    return m_getUserGroupTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::removeTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Mocks removing the given user group tag / key permissions from the database.
///
/// \param  The user group tag / key permissions to removed.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::removeTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserGroupTagAccess = groupTagAccess;

    return m_getUserGroupTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::updateTagGroupPermissions( const GroupTagAccess& groupTagAccess )
///
/// \brief  Mocks updating the user group tag / key permissions in the database.
///
/// \param  The new user group tag / key permissions to be updated data.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::updateTagGroupPermissions( const GroupTagAccess& groupTagAccess )
{
    // Reset mock next count.
    m_nextCount = 0;

    m_mockUserGroupTagAccess = groupTagAccess;

    return m_getUserGroupTagAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserGroupTagAccess( const UserGroup& group )
///
/// \brief  Mocks getting the tag / key access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserGroupTagAccess( const UserGroup& group )
{
    Q_UNUSED( group );

    return false;
}


////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserTagAccess( const User& user  )
///
/// \brief  Mocks getting the tag / key access for the given user.
///
/// \param  The user to search for.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserTagAccess( const User& user )
{
    Q_UNUSED( user );

    return false;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getValue( const QString& key )
///
/// \brief  Mocks the value to be returned from the SQL query result for the given key.
///
/// \param  The requested key.
///
/// \return The mocked value for the given key.
////////////////////////////////////////////////////////////////////////////////
QVariant MockQueryManager::getValue( const QString& key )
{
    return m_mockValues[key];
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getNext()
///
/// \brief  Mocks positioning the query to the next result.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getNext()
{
    bool success = ( m_nextCount < m_next );

    if ( m_nextCount < m_next )
    {
        m_nextCount++;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::addSystem( const System& system )
///
/// \brief  Mocks adding the given system to the database.
///
/// \param  The system to add.
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::addSystem( const System& system )
{
    m_newSystem = system;

    return m_addSystemSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserGroupSystemAccess( const UserGroup& group )
///
/// \brief  Mocks getting the system access for the given user group.
///
/// \param  The user group to search for.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserGroupSystemAccess( const UserGroup& group )
{
    Q_UNUSED( group );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserGroupSystemAccessSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_system] = _testSystem;
        m_mockValues[UserGroup::_userGroup] = _testUserGroup;
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = m_mockUserGroupSystemAccess;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_system] = QVariant();
        m_mockValues[UserGroup::_userGroup] = QVariant();
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = QVariant();
    }

    return m_getUserGroupSystemAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserSystemAccess( const User& user )
///
/// \brief  Mocks getting the system access for the given user.
///
/// \param  The user to search for.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserSystemAccess( const User& user )
{
    Q_UNUSED( user );

    // Reset mock next count.
    m_nextCount = 0;

    if ( m_getUserSystemAccessSuccess )
    {
        m_mockValues[DatabaseControlConstants::_id] = _testId;
        m_mockValues[DatabaseControlConstants::_system] = _testSystem;
        m_mockValues[DatabaseControlConstants::_userId] = _testUser;
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = m_mockUserSystemAccess;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_id] = QVariant();
        m_mockValues[DatabaseControlConstants::_system] = QVariant();
        m_mockValues[DatabaseControlConstants::_userId] = QVariant();
        m_mockValues[DatabaseControlConstants::_isAccessGranted] = QVariant();
    }

    return m_getUserSystemAccessSuccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getSystem()
///
/// \brief  Mocks getting the system.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getSystem()
{
    if ( m_getSystemSuccess )
    {
        m_mockValues[DatabaseControlConstants::_systemName] = "TEST_SYSTEM";
        m_mockValues[DatabaseControlConstants::_welcomeText] = "TEST_WELCOME_TEXT";
        m_mockValues[DatabaseControlConstants::_systemTimezone] = "TEST_TIMEZONE";
        m_mockValues[DatabaseControlConstants::_registeredDate] = "TEST_REGISTERED_DATE";
        m_mockValues[DatabaseControlConstants::_lastCom] = "TEST_LAST_COM";
        m_mockValues[DatabaseControlConstants::_cardAuthentication] = true;
        m_mockValues[DatabaseControlConstants::_biometricAuthentication] = true;
        m_mockValues[DatabaseControlConstants::_compoundModule] = true;
        m_mockValues[DatabaseControlConstants::_valetModule] = true;
    }
    else
    {
        m_mockValues[DatabaseControlConstants::_systemName] = "";
        m_mockValues[DatabaseControlConstants::_welcomeText] = "";
        m_mockValues[DatabaseControlConstants::_systemTimezone] = "";
        m_mockValues[DatabaseControlConstants::_registeredDate] = "";
        m_mockValues[DatabaseControlConstants::_lastCom] = "";
        m_mockValues[DatabaseControlConstants::_cardAuthentication] = false;
        m_mockValues[DatabaseControlConstants::_biometricAuthentication] = false;
        m_mockValues[DatabaseControlConstants::_compoundModule] = false;
        m_mockValues[DatabaseControlConstants::_valetModule] = false;
    }

    return m_getSystemSuccess;
}
