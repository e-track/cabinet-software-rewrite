// External.
#include <string.h>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/userprofilepermissions.h>

// Project.
#include "tst_userprofilepermissions.h"

// Statics.
static const char* _testId = "id";
static const char* _testName = "permission_name";
static const char* _testAccessUsers = "accessUsers";
static const char* _testManageUsers = "manageUsers";
static const char* _testAccessUserGroups = "accessUserGroups";
static const char* _testManageUserGroups = "manageUserGroups";
static const char* _testAccessKeys = "accessKeys";
static const char* _testManageKeys = "manageKeys";
static const char* _testAccessSettings = "accessSettings";
static const char* _testAccessReports = "accessReports";
static const char* _testAccessSystems = "accessSystems";
static const char* _testAccessBookings = "accessBookings";
static const char* _testMakeBooking = "makeBooking";
static const char* _testManageBooking = "manageBooking";
static const char* _testIsHandover = "isHandover";
static const char* _testAutoApprove = "autoApprove";
static const char* _testHasGlobalSearch = "hasGlobalSearch";
static const char* _testWebLogin = "webLogin";
static const char* _testManageRoles = "manageRoles";

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::UserProfilePermissionsTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
UserProfilePermissionsTest::UserProfilePermissionsTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void UserProfilePermissionsTest::init()
{
    // Create class under test.
    m_userProfilePermissions = new UserProfilePermissions();
    QVERIFY( m_userProfilePermissions );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void UserProfilePermissionsTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::testConstants()
///
/// \brief  Tests permissions constants.
////////////////////////////////////////////////////////////////////////////////
void UserProfilePermissionsTest::testConstants()
{
    QVERIFY( strcmp( UserProfilePermissions::_id,               _testId ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_permissionName,   _testName ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessUsers,      _testAccessUsers ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_manageUsers,      _testManageUsers ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessUserGroups, _testAccessUserGroups ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_manageUserGroups, _testManageUserGroups ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessKeys,       _testAccessKeys ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_manageKeys,       _testManageKeys ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessSettings,   _testAccessSettings ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessReports,    _testAccessReports ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessSystems,    _testAccessSystems ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_accessBookings,   _testAccessBookings ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_makeBooking,      _testMakeBooking ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_manageBooking,    _testManageBooking ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_isHandover,       _testIsHandover ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_autoApprove,      _testAutoApprove ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_hasGlobalSearch,  _testHasGlobalSearch ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_webLogin,         _testWebLogin ) == 0 );
    QVERIFY( strcmp( UserProfilePermissions::_manageRoles,      _testManageRoles ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void UserProfilePermissionsTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilePermissionsTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void UserProfilePermissionsTest::cleanupTestCase()
{

}
