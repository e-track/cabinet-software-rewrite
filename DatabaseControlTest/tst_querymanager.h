#ifndef TST_QUERYMANAGER_H
#define TST_QUERYMANAGER_H

// Library.
#include <DatabaseControl/tag.h>
#include <DatabaseControl/user.h>
#include <DatabaseControl/usergroup.h>
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

class QueryManager;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the Query class.
///         TODO: These tests can be improved if the QueryManager had an injected
///         wrapper for the Qt SQL calls. At the moment, the tests just verify
///         that the query functions exist and haven't been changed.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT QueryManagerTest : public UnitTest
{
    Q_OBJECT

public:
    explicit QueryManagerTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Constants.
    void testConstants();

    // Login.
    void testLoginTempPin();
    void testLoginPin();

    // PIN.
    void testUpdatePin();

    // Events.
    void testLoginEvent();
    void testLogoutEvent();
    void testAccessDeniedEvent();

    // Users.
    void testUserCommands();
    void testUserProfilePermissions();
    void testUserProfiles();

    // User groups.
    void testUserGroups();

    // Systems.
    void testUserGroupSystemAccess();
    void testUserSystemAccess();

    // Cabinets.
    void testUserGroupCabinetAccess();
    void testUserCabinetAccess();

    // Tags / keys.
    void testTagCommands();

    // General SQL query.
    void testSqlQuery();

    // Factory.
    void testQueryFactoryDelete();
    void testQueryFactoryInsert();
    void testQueryFactorySelect();
    void testQueryFactorySelectAll();
    void testQueryFactoryUpdate();
    void testQueryAppendWhere();
    void testQueryAppendAnd();
    void testBindingsFactory();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    QueryManager* m_queryManager;       ///< Class under test.
    User m_user;                        ///< Test user.
    UserGroup m_userGroup;              ///< Test user group.
    Tag* m_tag;                         ///< Test tag / key.
};

#endif // TST_QUERYMANAGER_H
