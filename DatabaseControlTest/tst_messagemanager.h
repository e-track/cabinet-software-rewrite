#ifndef MESSAGEMANAGERTEST_H
#define MESSAGEMANAGERTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

class MessageManager;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the MessageManager class.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT MessageManagerTest : public UnitTest
{
    Q_OBJECT

public:
    explicit MessageManagerTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();

    // QML.
    void testInvokables();

    // Data.
    void testConstants();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    MessageManager* m_messageManager;       ///< Class under test.
};

#endif // MESSAGEMANAGERTEST_H
