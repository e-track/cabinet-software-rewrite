#ifndef CONSTANTSTEST_H
#define CONSTANTSTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the constants header.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT ConstantsTest : public UnitTest
{
    Q_OBJECT

public:
    explicit ConstantsTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Data.
    void testGeneralConstants();
    void testTagConstants();
    void testUserConstants();
    void testUserGroupConstants();
    void testSystemConstants();
    void tetsCabinetConstants();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();
};

#endif // CONSTANTSTEST_H
