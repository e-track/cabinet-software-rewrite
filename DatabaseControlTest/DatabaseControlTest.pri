################################################################################
# Dependencies.
################################################################################
include( $$PWD/../DatabaseControl/DatabaseControl.pri )
include( $$PWD/../UnitTestSuite/UnitTestSuite.pri )

################################################################################
# Library.
################################################################################
if(!build_databasecontroltest) {
    LIBS += -L$$(CABINETUI_INSTALLATION)/DatabaseControlTest -lDatabaseControlTest

    INCLUDEPATH += $$PWD/..
    DEPENDPATH += $$PWD/..
} else {
################################################################################
# Sources.
################################################################################
SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mocks/mockmessagemanager.cpp \
    $$PWD/mocks/mockquerymanager.cpp \
    $$PWD/tst_constants.cpp \
    $$PWD/tst_databasecontrol.cpp \
    $$PWD/tst_messagemanager.cpp \
    $$PWD/tst_querymanager.cpp \
    $$PWD/tst_tagcategory.cpp \
    $$PWD/tst_userprofilepermissions.cpp \
    $$PWD/tst_userprofiles.cpp

################################################################################
# Headers.
################################################################################
HEADERS += \
    $$PWD/databasecontroltest_global.h \
    $$PWD/mocks/mockmessagemanager.h \
    $$PWD/mocks/mockquerymanager.h \
    $$PWD/tst_constants.h \
    $$PWD/tst_databasecontrol.h \
    $$PWD/tst_messagemanager.h \
    $$PWD/tst_querymanager.h \
    $$PWD/tst_tagcategory.h \
    $$PWD/tst_userprofilepermissions.h \
    $$PWD/tst_userprofiles.h
}
