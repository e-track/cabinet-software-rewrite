// External.
#include <string.h>

// Qt.
#include <QMetaObject>
#include <QQmlEngine>
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/constants.h>
#include <DatabaseControl/databasecontrol.h>
#include <DatabaseControl/system.h>
#include <DatabaseControl/usergroup.h>
#include <DatabaseControl/userprofilepermissions.h>
#include <DatabaseControl/userprofiles.h>

// Project.
#include "mocks/mockmessagemanager.h"
#include "mocks/mockquerymanager.h"
#include "tst_databasecontrol.h"

// Constants.
static const char* _pin = "TEST_PIN";
static const char* _tempPin = "TEST_TEMP_PIN";
static const char* _firstName = "TEST_FIRST_NAME";
static const char* _lastName = "TEST_LAST_NAME";
static const char* _testTimestamp = "TEST_TIMESTAMP";
static const int _userId = 11;

static const bool _successFail[] = { true, false, true, false };

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::DatabaseControlTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
DatabaseControlTest::DatabaseControlTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::init()
{
    // Create query manager.
    m_mockQueryManager = new MockQueryManager();
    QVERIFY( m_mockQueryManager );
    m_mockQueryManager->m_connectionType = Qt::DirectConnection ;

    // Create messag manager.
    m_mockMessageManager = new MockMessageManager();
    QVERIFY( m_mockMessageManager );

    // Create class under test.
    m_databaseControl = DatabaseControl::instance( m_mockQueryManager, m_mockMessageManager );
    QVERIFY( m_databaseControl );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRegisterClass()
{
    // Register the class.
    DatabaseControl::registerClass();

    // Expected class and module names.
    QString expectedModuleName = "Database";
    QString expectedClassName = "Control";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLoggedInUser()
///
/// \brief  Tests access to the logged in user.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testLoggedInUser()
{
    // Default logged in user.
    User defaultUser = User();
    User user = m_databaseControl->getLoggedInUser();

    QVERIFY( defaultUser == user );

    // Logged in user only changes with successful login and this is covered
    // by testLoginPin() and testLoginTempPin.
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLoggedInUserProperties()
///
/// \brief  Tests logged in user properties are declared correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testLoggedInUserProperties()
{
    QString expectedUser = "";

    // Logged in user is readable.
    QVariant loggedInUser = m_databaseControl->property( "loggedInUser" );
    QVERIFY( loggedInUser.isValid() );

    // Logged in user matches the expected logged in user.
    QVERIFY( expectedUser == loggedInUser.toString() );

    // Logged in user is not writeable.
    QVERIFY( !m_databaseControl->setProperty( "loggedInUser", "TEST_USER" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLoggedInUserProperties()
///
/// \brief  Tests logged in user properties are declared correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testAuthenticateUser()
{
    User defaultAuthenticatedUser = User();
    User authenticatedUser = m_databaseControl->getAuthenticatedUser();

    QVERIFY( authenticatedUser == defaultAuthenticatedUser );

    // Mock authenticated user.
    User mockAuthenticatedUser;
    mockAuthenticatedUser.m_id = MockQueryManager::_testId;
    mockAuthenticatedUser.m_firstName = MockQueryManager::_testFirstName;
    mockAuthenticatedUser.m_lastName = MockQueryManager::_testLastName;

    // Create the expected access denied event message.
    QJsonObject userDenied;
    userDenied[DatabaseControlConstants::_userId] = mockAuthenticatedUser.m_id;

    QJsonObject eventDenied;
    eventDenied[MessageManager::_event] = MessageManager::_accessDenied;
    eventDenied[MessageManager::_timestamp] = _testTimestamp;
    eventDenied[MessageManager::_data] = userDenied;

    QJsonDocument jsonDocDenied( eventDenied );

    for ( bool getUserSuccess : _successFail )
    {
        // Mock user entry success.
        m_mockQueryManager->m_getUserSucess = getUserSuccess;
        m_mockQueryManager->m_getUserEntrySucess = getUserSuccess;

        // Test no cabinet or system access.
        m_mockQueryManager->m_mockUserCabinetAccess = false;
        m_mockQueryManager->m_getUserCabinetAccessSuccess = false;
        m_mockQueryManager->m_mockUserSystemAccess = false;
        m_mockQueryManager->m_getUserSystemAccessSuccess = false;

        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Authenticate user and get the result.
        m_databaseControl->authenticateUser( -1, "" );
        authenticatedUser = m_databaseControl->getAuthenticatedUser();

        // The authenticated user should be null as no access was granted.
        QVERIFY( authenticatedUser == defaultAuthenticatedUser );

        // The access denied message should be published if the user was found.
        QString expectedJson = getUserSuccess ? jsonDocDenied.toJson( QJsonDocument::Indented ) : "";
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );

        // Test cabinet access but no system access.
        m_mockQueryManager->m_mockUserCabinetAccess = true;
        m_mockQueryManager->m_getUserCabinetAccessSuccess = true;
        m_mockQueryManager->m_mockUserSystemAccess = false;
        m_mockQueryManager->m_getUserSystemAccessSuccess = false;

        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Authenticate user and get the result.
        m_databaseControl->authenticateUser( -1, "" );
        authenticatedUser = m_databaseControl->getAuthenticatedUser();

        // The authenticated user should be null as it doesn't have full access.
        QVERIFY( authenticatedUser == defaultAuthenticatedUser );

        // The access denied message should be published if the user was found.
        expectedJson = getUserSuccess ? jsonDocDenied.toJson( QJsonDocument::Indented ) : "";
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );

        // Test no cabinet access but system access.
        m_mockQueryManager->m_mockUserCabinetAccess = false;
        m_mockQueryManager->m_getUserCabinetAccessSuccess = false;
        m_mockQueryManager->m_mockUserSystemAccess = true;
        m_mockQueryManager->m_getUserSystemAccessSuccess = true;

        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Authenticate user and get the result.
        m_databaseControl->authenticateUser( -1, "" );
        authenticatedUser = m_databaseControl->getAuthenticatedUser();

        // The authenticated user should be null as it doesn't have full access.
        QVERIFY( authenticatedUser == defaultAuthenticatedUser );

        // The access denied message should be published if the user was found.
        expectedJson = getUserSuccess ? jsonDocDenied.toJson( QJsonDocument::Indented ) : "";
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );

        // Test cabinet and system access.
        m_mockQueryManager->m_mockUserCabinetAccess = true;
        m_mockQueryManager->m_getUserCabinetAccessSuccess = true;
        m_mockQueryManager->m_mockUserSystemAccess = true;
        m_mockQueryManager->m_getUserSystemAccessSuccess = true;

        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Authenticate user and get the result.
        m_databaseControl->authenticateUser( -1, "" );
        authenticatedUser = m_databaseControl->getAuthenticatedUser();

        // The authenticated user should be the mock user if the user was found.
        QVERIFY( authenticatedUser == ( getUserSuccess ? mockAuthenticatedUser : defaultAuthenticatedUser ) );

        // The access denied message should never be published as either the user wasn't found
        // or the user has access.
        expectedJson = "";
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLoginPin()
///
/// \brief  Tests login using PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testLoginPin()
{
    // Signal spies.
    QSignalSpy successSpy( m_databaseControl, &DatabaseControl::loginSuccess );
    QSignalSpy failSpy( m_databaseControl, &DatabaseControl::loginFail );
    QSignalSpy userSpy( m_databaseControl, &DatabaseControl::loggedInUserChanged );

    // Mock authenticated user.
    User mockAuthenticatedUser;
    mockAuthenticatedUser.m_id = MockQueryManager::_testId;
    mockAuthenticatedUser.m_firstName = MockQueryManager::_testFirstName;
    mockAuthenticatedUser.m_lastName = MockQueryManager::_testLastName;

    for ( bool success : _successFail )
    {
        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Mock logged in success and event query.
        m_mockQueryManager->m_loginPinSuccess = success;
        m_mockQueryManager->m_eventLoginSuccess = false;
        m_mockQueryManager->m_eventLoginFail = false;

        // Log in with temporary PIN.
        m_databaseControl->loginPin( _pin );

        // The logged in user ID should be obtained from the query manager.
        User loggedInUser = m_databaseControl->getLoggedInUser();

        // The expected logged in user ID will be the mock set above or an empty string.
        User expectedLoggedInUser = ( success ? mockAuthenticatedUser : User() );
        QVERIFY( loggedInUser ==  expectedLoggedInUser );

        // Create the expected success event message.
        QJsonObject userSuccess;
        userSuccess[DatabaseControlConstants::_userId] = expectedLoggedInUser.m_id;

        QJsonObject eventSuccess;
        eventSuccess[MessageManager::_event] = MessageManager::_loginSuccess;
        eventSuccess[MessageManager::_timestamp] = _testTimestamp;
        eventSuccess[MessageManager::_data] = userSuccess;

        QJsonDocument jsonDocSuccess( eventSuccess );

        // Create the expected failed event message.
        QJsonObject userFail;
        userFail[DatabaseControlConstants::_userId] = User().m_id;

        QJsonObject eventFail;
        eventFail[MessageManager::_event] = MessageManager::_loginFail;
        eventFail[MessageManager::_timestamp] = _testTimestamp;
        eventFail[MessageManager::_data] = userFail;

        QJsonDocument jsonDocFail( eventFail );

        QString expectedJson = success ? jsonDocSuccess.toJson( QJsonDocument::Indented )
                                       : jsonDocFail.toJson( QJsonDocument::Indented );

        // Test the expected database query function has been called.
        if ( success )
        {
            QVERIFY( m_mockQueryManager->m_eventLoginSuccess && !m_mockQueryManager->m_eventLoginFail );
        }
        else
        {
            QVERIFY( !m_mockQueryManager->m_eventLoginSuccess && m_mockQueryManager->m_eventLoginFail );
        }

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }

    QVERIFY( successSpy.count() == 2 );
    QVERIFY( failSpy.count() == 2 );
    QVERIFY( userSpy.count() == 4 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLoginTempPin()
///
/// \brief  Tests login using temp PIN.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testLoginTempPin()
{
    // Signal spies.
    QSignalSpy successSpy( m_databaseControl, &DatabaseControl::tempLoginSuccess );
    QSignalSpy failSpy( m_databaseControl, &DatabaseControl::tempLoginFail );

    // Mock logged in user.
    User mockLoggedInUser;
    mockLoggedInUser.m_id = MockQueryManager::_testId;
    mockLoggedInUser.m_firstName = MockQueryManager::_testFirstName;
    mockLoggedInUser.m_lastName = MockQueryManager::_testLastName;

    for ( bool loginSuccess : _successFail )
    {
        // Reset publish payload.
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockMessageManager->m_mockTimestamp = _testTimestamp;

        // Mock logged in success (i.e. PIN match).
        m_mockQueryManager->m_loginTempPinSuccess = loginSuccess;

        // Mock user group success (i.e. user belongs to a group).
        m_mockQueryManager->m_getUsersGroupSuccess = loginSuccess;

        // Mock events being called.
        m_mockQueryManager->m_eventLoginSuccess = false;
        m_mockQueryManager->m_eventLoginFail = false;
        m_mockQueryManager->m_eventAccessDenied = false;

        // Log in with temporary PIN.
        m_databaseControl->loginTempPin( _tempPin );

        // The logged in user ID should be obtained from the query manager.
        User loggedInUser = m_databaseControl->getLoggedInUser();

        // The expected logged in user ID will be the mock set above or an empty string.
        User expectedLoggedInUser = ( loginSuccess ? mockLoggedInUser
                                                   : User() );

        QVERIFY( loggedInUser ==  expectedLoggedInUser );

        // Create the expected success event message.
        QJsonObject user;
        user[DatabaseControlConstants::_userId] = expectedLoggedInUser.m_id;

        QJsonObject eventSuccess;
        eventSuccess[MessageManager::_event] = MessageManager::_loginSuccess;
        eventSuccess[MessageManager::_timestamp] = _testTimestamp;
        eventSuccess[MessageManager::_data] = user;

        QJsonDocument jsonDocSuccess( eventSuccess );

        // Create the expected failed event message.
        QJsonObject userFail;
        userFail[DatabaseControlConstants::_userId] = User().m_id;

        QJsonObject eventFail;
        eventFail[MessageManager::_event] = MessageManager::_loginFail;
        eventFail[MessageManager::_timestamp] = _testTimestamp;
        eventFail[MessageManager::_data] = userFail;

        QJsonDocument jsonDocFail( eventFail );

        // Expected JSON message.
        QString jsonSuccess = jsonDocSuccess.toJson( QJsonDocument::Indented );
        QString jsonFail = jsonDocFail.toJson( QJsonDocument::Indented );

        // Test the expected database query function has been called.
        if ( loginSuccess )
        {
            QVERIFY( m_mockQueryManager->m_eventLoginSuccess &&
                     !m_mockQueryManager->m_eventLoginFail &&
                     !m_mockQueryManager->m_eventAccessDenied );

            // Once processed, the reply should be passed into MessageManager::publish().
            QVERIFY( m_mockMessageManager->m_mockPublishedPayload == jsonSuccess );
        }
        else
        {
            QVERIFY( !m_mockQueryManager->m_eventLoginSuccess &&
                     m_mockQueryManager->m_eventLoginFail &&
                     !m_mockQueryManager->m_eventAccessDenied );

            // Once processed, the reply should be passed into MessageManager::publish().
            QVERIFY( m_mockMessageManager->m_mockPublishedPayload == jsonFail );
        }
    }

    QVERIFY( successSpy.count() == 2 );
    QVERIFY( failSpy.count() == 2 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testLogout()
///
/// \brief  Tests logging out.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testLogout()
{
    // Signal spy.
    QSignalSpy logoutSpy( m_databaseControl, &DatabaseControl::logoutSuccess );

    // Reset publish payload.
    m_mockMessageManager->m_mockPublishedPayload = "";
    m_mockMessageManager->m_mockTimestamp = _testTimestamp;

    // Logout.
    m_databaseControl->logout();

    // The logged in user ID should be obtained from the query manager.
    User loggedInUser = m_databaseControl->getLoggedInUser();

    // The expected logged in user should be null following a logout.
    User expectedLoggedInUser = User();

    QVERIFY( loggedInUser ==  expectedLoggedInUser );

    // Create the expected success event message.
    QJsonObject user;
    user[DatabaseControlConstants::_userId] = User().m_id;

    QJsonObject event;
    event[MessageManager::_event] = MessageManager::_logout;
    event[MessageManager::_timestamp] = _testTimestamp;
    event[MessageManager::_data] = user;

    QJsonDocument jsonDoc( event );
    QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

    // Test the expected database query function has been called.
    QVERIFY( m_mockQueryManager->m_eventLogout = true );

    // Once processed, the reply should be passed into MessageManager::publish().
    QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );

    QVERIFY( logoutSpy.count() == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testInvokables()
///
/// \brief  Tests invokable functions.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testInvokables()
{
    // Login.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "loginPin", Q_ARG( QString, "TEST_PIN" ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "loginTempPin", Q_ARG( QString, "TEST_PIN" ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "logout" ) );

    // PIN.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "savePin", Q_ARG( QString, "TEST_PIN" ) ) );

    // Simulation.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "simulateLogin" ) );

    // User.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "addFingerprint", Q_ARG( const int, 11 ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "addCard", Q_ARG( QString, "TEST_CARD_ID" ) ) );

    // Cabinets.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "selectCabinet", Q_ARG( const int, 22 ) ) );

    // Tags / keys.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "queryTagsForUser", Q_ARG( const int, 44 ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "queryBookedTags", Q_ARG( const int, 55 ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "addTagToBasket", Q_ARG( Tag*, new Tag( this ) ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "removeTagFromBasket", Q_ARG( Tag*, new Tag( this ) ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "clearBasketTags" ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "addTagToReturning", Q_ARG( Tag*, new Tag( this ) ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "removeTagFromReturning", Q_ARG( Tag*, new Tag( this ) ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "completeCheckoutBasket" ) );
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "completeReturnTags" ) );

    // Save.
    QVERIFY( QMetaObject::invokeMethod( m_databaseControl, "saveAll" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSaveAll()
///
/// \brief  Tests all changes can be saved to the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSaveAll()
{
    // Signal spies.
    QSignalSpy sqlSuccessSpy( m_databaseControl, &DatabaseControl::sqlSuccess );
    QSignalSpy sqlFailSpy( m_databaseControl, &DatabaseControl::sqlFail );
    QSignalSpy saveAllSuccessSpy( m_databaseControl, &DatabaseControl::saveAllSuccess );
    QSignalSpy saveAllFailSpy( m_databaseControl, &DatabaseControl::saveAllFail );

    for ( bool updatePinSuccess : _successFail )
    {
        // Mock update PIN success state.
        m_mockQueryManager->m_updatePinSuccess = updatePinSuccess;

        for ( bool resetTempPinSuccess : _successFail )
        {
            // Mock reset temporary PIN success state.
            m_mockQueryManager->m_resetTempPinSuccess = resetTempPinSuccess;

            for ( bool addFingerprintSuccess : _successFail )
            {
                // Mock add fingerprint success state.
                m_mockQueryManager->m_addFingerprintSuccess = addFingerprintSuccess;

                for ( bool addCardSuccess : _successFail )
                {
                    // Mock add card success.
                    m_mockQueryManager->m_addCardSuccess = addCardSuccess;

                    // Save the PIN.
                    m_databaseControl->saveAll();
                }
            }
        }
    }

    QVERIFY( sqlSuccessSpy.count() == 0 );
    QVERIFY( sqlFailSpy.count() == 0 );
    QVERIFY( saveAllSuccessSpy.count() == 16 );
    QVERIFY( saveAllFailSpy.count() == 240 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSavePin()
///
/// \brief  Tests PIN can be saved.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSavePin()
{
    // Signal spies.
    QSignalSpy sqlSuccessSpy( m_databaseControl, &DatabaseControl::sqlSuccess );
    QSignalSpy sqlFailSpy( m_databaseControl, &DatabaseControl::sqlFail );
    QSignalSpy saveAllSuccessSpy( m_databaseControl, &DatabaseControl::saveAllSuccess );
    QSignalSpy saveAllFailSpy( m_databaseControl, &DatabaseControl::saveAllFail );

    for ( bool savePinSuccess : _successFail )
    {
        // Mock update PIN success state.
        m_mockQueryManager->m_sqlSuccess = savePinSuccess;
        m_mockQueryManager->m_updatePinSuccess = savePinSuccess;
        m_mockQueryManager->m_resetTempPinSuccess = savePinSuccess;

        // Save the PIN.
        m_databaseControl->savePin( _pin );
    }

    // Nothing should be updated until saveAll() is called therefore no signals should be emitted.
    QVERIFY( sqlSuccessSpy.count() == 0 );
    QVERIFY( sqlFailSpy.count() == 0 );
    QVERIFY( saveAllSuccessSpy.count() == 0 );
    QVERIFY( saveAllFailSpy.count() == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdatePinFromValidPayload()
///
/// \brief  Tests PIN can be updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdatePinFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"reset_user_pin\","
      "\"info\" : \"TEST_INFO\","
      "\"data\" : {"
        "\"user_id\" : 11,"
        "\"entry_type\" : 22,"
        "\"entry_value\" : \"3344\""
      "}"
    "}";

    for ( bool updatePinSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updatePinSuccess = updatePinSuccess;
        m_mockQueryManager->m_mockUserEntry = UserEntry();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user entry is based on the above payload.
        UserEntry expectedUserEntry;
        expectedUserEntry.m_userId = 11;
        expectedUserEntry.m_entryType = QueryManager::_entryUsePin;
        expectedUserEntry.m_entryValue = "3344";

        // If the command is 'reset_user_pin' then the user entry should be updated in QueryManager::updatePin().
        QVERIFY( m_mockQueryManager->m_mockUserEntry == expectedUserEntry );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_resetUserPin;
        reply[MessageManager::_result] = ( updatePinSuccess ? MessageManager::_success
                                                            : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdatePinFromInvalidPayload()
///
/// \brief  Tests PIN isn't updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdatePinFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"reset_user_pin\","
      "\"info_wrong\" : \"TEST_INFO\","
      "\"data_wrong\" : {"
        "\"user_id_wrong\" : 11,"
        "\"entry_type_wrong\" : 22,"
        "\"entry_value_wrong\" : \"3344\""
      "}"
    "}";

    for ( bool updatePinSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updatePinSuccess = updatePinSuccess;
        m_mockQueryManager->m_mockUserEntry = UserEntry();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user entry is default.
        UserEntry expectedUserEntry;

        // If the command is 'reset_user_pin' then the user entry should not be updated in QueryManager::updatePin().
        QVERIFY( m_mockQueryManager->m_mockUserEntry == expectedUserEntry );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_resetUserPin;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSqlQuery()
///
/// \brief  Test generic SQL query can be executed.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSqlQuery()
{
    // Signal spies.
    QSignalSpy successSpy( m_databaseControl, &DatabaseControl::sqlSuccess );
    QSignalSpy failSpy( m_databaseControl, &DatabaseControl::sqlFail );

    for ( bool success : _successFail )
    {
        // Mock SQL query success state.
        m_mockQueryManager->m_sqlSuccess = success;

        // Perform SQL query.
        m_databaseControl->performSqlQuery( "" );
    }

    QVERIFY( successSpy.count() == 2 );
    QVERIFY( failSpy.count() == 2 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testCabinets()
///
/// \brief  Tests access to the cabinets.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testCabinets()
{
    // Test default cabinets.
    int expectedCabinets = 0;

    // Test cabinets can be obtained using the get function.
    QList<Cabinet*> cabinets = m_databaseControl->getCabinets();

    QVERIFY( cabinets.count() == expectedCabinets );

    // Test cabinets can be obtained using the property.
    // TODO: Cast this properly but we're only comparing the size so it
    // doesn't matter too much at the moment.
    QVariantList cabinetsList = m_databaseControl->property( "cabinets" ).toList();

    QVERIFY( cabinetsList.count() == expectedCabinets );

    // TODO: Use mocks to check QVariantList is returned correctly.
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testCabinetsProperties()
///
/// \brief  Tests cabinets properties are declared correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testCabinetsProperties()
{
    // Cabinets is readable.
    QVariant cabinets = m_databaseControl->property( "cabinets" );
    QVERIFY( cabinets.isValid() );

    // Cabinets is not writeable.
    QVERIFY( !m_databaseControl->setProperty( "cabinets", 10 ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testCabinetPermissions()
///
/// \brief  Tests cabinet permissions are accessed correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testCabinetPermissions()
{
    // Test user.
    User user;
    user.m_id = _userId;
    user.m_firstName = _firstName;
    user.m_lastName = _lastName;

    // Assume we can always get the user group.
    m_mockQueryManager->m_getUserGroupSuccess = true;

    for ( bool userGroupCabinetAccess : _successFail )
    {
        // Mock the user group cabinet access.
        m_mockQueryManager->m_mockUserGroupCabinetAccess = userGroupCabinetAccess;

        for ( bool userCabinetAccess : _successFail )
        {
            // Mock the user cabinet access.
            m_mockQueryManager->m_mockUserCabinetAccess = userCabinetAccess;

            for ( bool userGroupSuccess : _successFail )
            {
                // Mock getting the user group.
                m_mockQueryManager->m_getUsersGroupSuccess = userGroupSuccess;

                bool access = false;

                // The user and user group do not have specified permissions,
                // Access should always be denied as this indicates the
                // database could not be queried so the actual access is unknown.
                m_mockQueryManager->m_getUserCabinetAccessSuccess = false;
                m_mockQueryManager->m_getUserGroupCabinetAccessSuccess = false;

                m_databaseControl->checkUserCabinetPermissions( user, access );

                QVERIFY( !access );

                // Test user permissions are specified without group permissions.
                // Access should match the user cabinet access regardless of
                // whether the user's group could be obtained.
                m_mockQueryManager->m_getUserCabinetAccessSuccess = true;
                m_mockQueryManager->m_getUserGroupCabinetAccessSuccess = false;

                m_databaseControl->checkUserCabinetPermissions( user, access );

                QVERIFY( access == userCabinetAccess );

                // Test user group permissions are specified without user permissions.
                // Access should match the user group cabinet access only if the
                // the user's group could be obtained.
                m_mockQueryManager->m_getUserCabinetAccessSuccess = false;
                m_mockQueryManager->m_getUserGroupCabinetAccessSuccess = true;

                m_databaseControl->checkUserCabinetPermissions( user, access );

                QVERIFY( access == ( userGroupCabinetAccess && userGroupSuccess ) );

                // Test both user and user group permissions are specified.
                // Access should match the user cabinet access since that takes
                // precedence over user group cabinet access.
                m_mockQueryManager->m_getUserCabinetAccessSuccess = true;
                m_mockQueryManager->m_getUserGroupCabinetAccessSuccess = true;

                m_databaseControl->checkUserCabinetPermissions( user, access );

                QVERIFY( access == userCabinetAccess );
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRegisteredCabinet()
///
/// \brief  Tests registered cabinet can be initialised from the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRegisteredCabinet()
{
    // Mock initialisation success.
    m_mockQueryManager->doInitDatabaseComplete( true );

    // Default cabinet.
    Cabinet defaultCabinet;
    defaultCabinet.m_id = -1;
    defaultCabinet.m_slotCapacity = -1;
    defaultCabinet.m_startSlot = -1;
    defaultCabinet.m_endSlot = -1;
    defaultCabinet.m_slotWidth = -1;
    defaultCabinet.m_noOfLocks = -1;
    defaultCabinet.m_cabinetName = "";

    Cabinet* registeredCabinet = m_databaseControl->getRegisteredCabinet();
    QVERIFY( *registeredCabinet == defaultCabinet );

    for ( bool success : _successFail )
    {
        // Mock user profile success state.
        m_mockQueryManager->m_getRegisteredCabinetSuccess = success;

        // Initialise the registered cabinet.
        m_databaseControl->initRegisteredCabinet();

        // Expected cabinet is based on the mocked data.
        Cabinet expectedCabinet;
        expectedCabinet.m_id = MockQueryManager::_testId;
        expectedCabinet.m_slotCapacity = MockQueryManager::_testSlotCapacity;
        expectedCabinet.m_startSlot = MockQueryManager::_testStartSlot;
        expectedCabinet.m_endSlot = MockQueryManager::_testEndSlot;
        expectedCabinet.m_slotWidth = MockQueryManager::_testSlotWidth;
        expectedCabinet.m_noOfLocks = MockQueryManager::_testNoOfLocks;
        expectedCabinet.m_cabinetName = MockQueryManager::_testCabinetName;

        registeredCabinet = m_databaseControl->getRegisteredCabinet();
        QVERIFY( *registeredCabinet == ( success ? expectedCabinet : defaultCabinet ) );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSystemsFromValidPayload()
///
/// \brief  Tests systems are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSystemsFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload = "{\"command\" : \"hello_world\",\"data\" : {\"system_name\" : \"TEST_NAME\",\"welcome_text\" : \"TEST_WELCOME\",\"system_timezone\" : \"TEST_TIMEZONE\",\"card_authentication\" : true,\"biometric_authentication\" : true,\"registered_date\" : \"TEST_DATE\",\"last_com\" : \"TEST_LASTCOM\",\"compound_module\" : true,\"valet_module\" : true}}";

    for ( bool success : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addSystemSuccess = success;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected system is based on the above payload.
        System expectedSystem;
        expectedSystem.m_name = "TEST_NAME";
        expectedSystem.m_welcomeText = "TEST_WELCOME";
        expectedSystem.m_timezone = "TEST_TIMEZONE";
        expectedSystem.m_registeredDate = "TEST_DATE";
        expectedSystem.m_cardAuthentication = true;
        expectedSystem.m_biometricAuthentication = true;
        expectedSystem.m_compound = true;
        expectedSystem.m_valet = true;

        // If the command is 'hello_world' then a system should be created in QueryManager::addSystem().
        QVERIFY( m_mockQueryManager->m_newSystem == expectedSystem );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_helloWorld;
        reply[MessageManager::_result] = ( success ? MessageManager::_success
                                                   : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSystemsFromInvalidPayload()
///
/// \brief  Tests systems are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSystemsFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload = "{\"command\" : \"hello_world\",\"data\" : {\"name_wrong\" : \"TEST_NAME\",\"welcome_text_wrong\" : \"TEST_WELCOME\",\"timezone_wrong\" : \"TEST_TIMEZONE\",\"card_authentication_wrong\" : true,\"biometric_authentication_wrong\" : true,\"registered_date_wrong\" : \"TEST_DATE\",\"last_com_wrong\" : \"TEST_LASTCOM\",\"compound_wrong\" : true,\"valet_wrong\" : true}}";

    for ( bool success : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addSystemSuccess = success;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected system should be the default system.
        System expectedSystem;

        // If the command is 'hello_world' then a system should not be created in QueryManager::addSystem()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_newSystem == expectedSystem );

        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_helloWorld;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSystem()
///
/// \brief  Tests system can be initialised from the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSystem()
{
    // Default system.
    System defaultSystem;
    defaultSystem.m_name = "";
    defaultSystem.m_welcomeText = "";
    defaultSystem.m_timezone = "";
    defaultSystem.m_registeredDate = "";
    defaultSystem.m_lastCom = "";
    defaultSystem.m_cardAuthentication = false;
    defaultSystem.m_biometricAuthentication = false;
    defaultSystem.m_compound = false;
    defaultSystem.m_valet = false;

    System system = m_databaseControl->getSystem();
    QVERIFY( system == defaultSystem );

    for ( bool success : _successFail )
    {
        // Mock user profile success state.
        m_mockQueryManager->m_getSystemSuccess = success;

        // Initialise the system.
        m_databaseControl->initSystem();

        // Expected system is based on the mocked data.
        System expectedSystem;
        expectedSystem.m_name = "TEST_SYSTEM";
        expectedSystem.m_welcomeText = "TEST_WELCOME_TEXT";
        expectedSystem.m_timezone = "TEST_TIMEZONE";
        expectedSystem.m_registeredDate = "TEST_REGISTERED_DATE";
        expectedSystem.m_lastCom = "TEST_LAST_COM";
        expectedSystem.m_cardAuthentication = true;
        expectedSystem.m_biometricAuthentication = true;
        expectedSystem.m_compound = true;
        expectedSystem.m_valet = true;

        system = m_databaseControl->getSystem();
        QVERIFY( system == ( success ? expectedSystem : defaultSystem ) );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testSystemPermissions()
///
/// \brief  Tests system permissions are accessed correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testSystemPermissions()
{
    // Test user.
    User user;
    user.m_id = _userId;
    user.m_firstName = _firstName;
    user.m_lastName = _lastName;

    // Assume we can always get the user group.
    m_mockQueryManager->m_getUserGroupSuccess = true;

    for ( bool userGroupSystemAccess : _successFail )
    {
        // Mock the user group system access.
        m_mockQueryManager->m_mockUserGroupSystemAccess = userGroupSystemAccess;

        for ( bool userSystemAccess : _successFail )
        {
            // Mock the user system access.
            m_mockQueryManager->m_mockUserSystemAccess = userSystemAccess;

            for ( bool userGroupSuccess : _successFail )
            {
                // Mock getting the user group.
                m_mockQueryManager->m_getUsersGroupSuccess = userGroupSuccess;

                bool access = false;

                // The user and user group do not have specified permissions,
                // Access should always be denied as this indicates the
                // database could not be queried so the actual access is unknown.
                m_mockQueryManager->m_getUserSystemAccessSuccess = false;
                m_mockQueryManager->m_getUserGroupSystemAccessSuccess = false;

                m_databaseControl->checkUserSystemPermissions( user, access );

                QVERIFY( !access );

                // Test user permissions are specified without group permissions.
                // Access should match the user system access regardless of
                // whether the user's group could be obtained.
                m_mockQueryManager->m_getUserSystemAccessSuccess = true;
                m_mockQueryManager->m_getUserGroupSystemAccessSuccess = false;

                m_databaseControl->checkUserSystemPermissions( user, access );

                QVERIFY( access == userSystemAccess );

                // Test user group permissions are specified without user permissions.
                // Access should match the user group system access only if the
                // the user's group could be obtained.
                m_mockQueryManager->m_getUserSystemAccessSuccess = false;
                m_mockQueryManager->m_getUserGroupSystemAccessSuccess = true;

                m_databaseControl->checkUserSystemPermissions( user, access );

                QVERIFY( access == ( userGroupSystemAccess && userGroupSuccess ) );

                // Test both user and user group permissions are specified.
                // Access should match the user system access since that takes
                // precedence over user group system access.
                m_mockQueryManager->m_getUserSystemAccessSuccess = true;
                m_mockQueryManager->m_getUserGroupSystemAccessSuccess = true;

                m_databaseControl->checkUserSystemPermissions( user, access );

                QVERIFY( access == userSystemAccess );
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUserProfiles()
///
/// \brief  Tests user profiles can be initialised from the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUserProfiles()
{
    // Mock initialisation success.
    m_mockQueryManager->doInitDatabaseComplete( true );

    // The user profiles should be initialised with the database controller.
    QVERIFY( UserProfiles::m_map.count() == 1 );

    for ( bool success : _successFail )
    {
        // Mock user profile success state.
        m_mockQueryManager->m_getUserProfilesSuccess = success;

        // Initialise the user profiles.
        m_databaseControl->initUserProfiles();

        int expectedCount = ( success ? 1 : 0 );
        QVERIFY( UserProfiles::m_map.count() == expectedCount );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUserProfilePermissions()
///
/// \brief  Tests user profile permissions can be initialised from the database.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUserProfilePermissions()
{
    // The user profile permissions should be initialised with the database controller.
    QVERIFY( UserProfilePermissions::_map.count() == 1 );

    for ( bool success : _successFail )
    {
        // Mock user profile permissions success state.
        m_mockQueryManager->m_getUserProfilePermissionsSuccess = success;

        // Initialise the user profile permissions.
        m_databaseControl->initUserProfilePermissions();

        int expectedCount = ( success ? 1 : 0 );
        QVERIFY( UserProfilePermissions::_map.count() == expectedCount );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewUserFromValidPayload()
///
/// \brief  Tests new users are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewUserFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user\","
        "\"data\" : {"
            "\"username\" : \"TEST_USERNAME\","
            "\"password\" : \"TEST_PASSWORD\","
            "\"first_name\" : \"TEST_FIRST_NAME\","
            "\"last_name\" : \"TEST_LAST_NAME\","
            "\"is_visitor\" : false,"
            "\"profile_image\" : null,"
            "\"phone_number\" : null,"
            "\"is_active\" : true,"
            "\"active_from\" : \"TEST_ACTIVE_FROM\","
            "\"key_limit\" : 11,"
            "\"is_temp\" : true,"
            "\"is_first_login\" : true,"
            "\"user_profile\" : 22"
        "}"
    "}";

    for ( bool newUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_newUserSuccess = newUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user is based on the above payload.
        User expectedUser;
        expectedUser.m_username = "TEST_USERNAME";
        expectedUser.m_password = "TEST_PASSWORD";
        expectedUser.m_firstName = "TEST_FIRST_NAME";
        expectedUser.m_lastName = "TEST_LAST_NAME";
        expectedUser.m_isVisitor = false;
        expectedUser.m_profileImage = "";
        expectedUser.m_phoneNumber = "";
        expectedUser.m_isActive = true;
        expectedUser.m_activeFrom = "TEST_ACTIVE_FROM";
        expectedUser.m_keyLimit = 11;
        expectedUser.m_isTemp = true;
        expectedUser.m_isFirstLogin = true;
        expectedUser.m_roleId = 22;

        // If the command is 'new_user' then a user should be created in QueryManager::addUser().
        QVERIFY( m_mockQueryManager->m_mockNewUser == expectedUser );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUser;
        reply[MessageManager::_result] = ( newUserSuccess ? MessageManager::_success
                                                          : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewUserFromInvalidPayload()
///
/// \brief  Tests new users are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewUserFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user\","
        "\"data\" : {"
            "\"username_wrong\" : \"TEST_USERNAME\","
            "\"password_wrong\" : \"TEST_PASSWORD\","
            "\"first_name_wrong\" : \"TEST_FIRST_NAME\","
            "\"last_name_wrong\" : \"TEST_LAST_NAME\","
            "\"is_visitor_wrong\" : false,"
            "\"profile_image_wrong\" : null,"
            "\"phone_number_wrong\" : null,"
            "\"is_active_wrong\" : true,"
            "\"active_from_wrong\" : \"TEST_ACTIVE_FROM\","
            "\"key_limit_wrong\" : 11,"
            "\"is_temp_wrong\" : true,"
            "\"is_first_login_wrong\" : true,"
            "\"user_profile_wrong\" : 22"
        "}"
    "}";

    for ( bool newUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_newUserSuccess = newUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user should be the default user.
        User expectedUser;

        // If the command is 'new_user' then a user should not be created in QueryManager::addUser()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockNewUser == expectedUser );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUser;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateUserFromValidPayload()
///
/// \brief  Tests users are updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateUserFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"update_user\","
        "\"data\" : {"
            "\"id\" : 11,"
            "\"username\" : \"TEST_USERNAME\","
            "\"password\" : \"TEST_PASSWORD\","
            "\"first_name\" : \"TEST_FIRST_NAME\","
            "\"last_name\" : \"TEST_LAST_NAME\","
            "\"is_visitor\" : false,"
            "\"profile_image\" : null,"
            "\"phone_number\" : null,"
            "\"is_active\" : true,"
            "\"active_from\" : \"TEST_ACTIVE_FROM\","
            "\"key_limit\" : 22,"
            "\"is_temp\" : true,"
            "\"is_first_login\" : true,"
            "\"user_profile\" : 33"
        "}"
    "}";

    for ( bool updateUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updateUserSuccess = updateUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user is based on the above payload.
        User expectedUser;
        expectedUser.m_id = 11;
        expectedUser.m_username = "TEST_USERNAME";
        expectedUser.m_password = "TEST_PASSWORD";
        expectedUser.m_firstName = "TEST_FIRST_NAME";
        expectedUser.m_lastName = "TEST_LAST_NAME";
        expectedUser.m_isVisitor = false;
        expectedUser.m_profileImage = "";
        expectedUser.m_phoneNumber = "";
        expectedUser.m_isActive = true;
        expectedUser.m_activeFrom = "TEST_ACTIVE_FROM";
        expectedUser.m_keyLimit = 22;
        expectedUser.m_isTemp = true;
        expectedUser.m_isFirstLogin = true;
        expectedUser.m_roleId = 33;

        // If the command is 'update_user' then a user should be updated in QueryManager::updateUser().
        QVERIFY( m_mockQueryManager->m_mockUpdatedUser == expectedUser );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateUser;
        reply[MessageManager::_result] = ( updateUserSuccess ? MessageManager::_success
                                                             : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateUserFromInvalidPayload()
///
/// \brief  Tests users are not updated when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateUserFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"update_user\","
        "\"data\" : {"
            "\"id_wrong\" : \"11\","
            "\"username_wrong\" : \"TEST_USERNAME\","
            "\"password_wrong\" : \"TEST_PASSWORD\","
            "\"first_name_wrong\" : \"TEST_FIRST_NAME\","
            "\"last_name_wrong\" : \"TEST_LAST_NAME\","
            "\"is_visitor_wrong\" : false,"
            "\"profile_image_wrong\" : null,"
            "\"phone_number_wrong\" : null,"
            "\"is_active_wrong\" : true,"
            "\"active_from_wrong\" : \"TEST_ACTIVE_FROM\","
            "\"key_limit_wrong\" : 22,"
            "\"is_temp_wrong\" : true,"
            "\"is_first_login_wrong\" : true,"
            "\"user_profile_wrong\" : 33"
        "}"
    "}";

    for ( bool updateUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updateUserSuccess = updateUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user should be the default user.
        User expectedUser;

        // If the command is 'update_user' then a user should not be updated in QueryManager::updateUser()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUpdatedUser == expectedUser );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateUser;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserFromValidPayload()
///
/// \brief  Tests users are removed when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user\","
        "\"data\" : {"
            "\"id\" : 11"
        "},"
        "\"trailing_data_to_remove\" : {"
            "\"info\" : \"To avoid orphaned data please, before removing the user remove all entries from the specified tables below where user = (3)\","
            "\"tables\" : [ \"tbl_user_entry_ids\", \"tbl_user_site_access\", \"tbl_user_location_access\", \"tbl_user_system_access\", \"tbl_user_cabinet_access\", \"tbl_user_to_key_access\", \"tbl_user_to_key_category_access\", \"tbl_user_to_groups\" ]"
        "}"
    "}";

    for ( bool removeUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_removeUserSuccess = removeUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user is based on the above payload.
        User expectedUser;
        expectedUser.m_id = 11;

        // If the command is 'remove_user' then a user should be removed in QueryManager::removeUser().
        QVERIFY( m_mockQueryManager->m_mockRemovedUser == expectedUser );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUser;
        reply[MessageManager::_result] = ( removeUserSuccess ? MessageManager::_success
                                                             : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserFromInvalidPayload()
///
/// \brief  Tests users are not removed when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user\","
        "\"data\" : {"
            "\"id_wrong\" : 11"
        "},"
        "\"trailing_data_to_remove\" : {"
            "\"info_wrong\" : \"To avoid orphaned data please, before removing the user remove all entries from the specified tables below where user = (3)\","
            "\"tables_wrong\" : [ \"tbl_user_entry_ids\", \"tbl_user_site_access\", \"tbl_user_location_access\", \"tbl_user_system_access\", \"tbl_user_cabinet_access\", \"tbl_user_to_key_access\", \"tbl_user_to_key_category_access\", \"tbl_user_to_groups\" ]"
        "}"
    "}";

    for ( bool removeUserSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_removeUserSuccess = removeUserSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user should be the default user.
        User expectedUser;

        // If the command is 'remove_user' then a user should not be updated in QueryManager::removeUser()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUpdatedUser == expectedUser );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUser;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testAddUserGroupFromValidPayload()
///
/// \brief  Tests user groups are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testAddUserGroupFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user_group\","
        "\"data\" : {"
            "\"group_name\" : \"TEST_USER_GROUP_NAME\","
            "\"key_limit\" : 22"
        "}"
    "}";

    for ( bool addUserGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addUserGroupSuccess = addUserGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group is based on the above payload.
        UserGroup expectedUserGroup;
        expectedUserGroup.m_id = MockQueryManager::_testUserGroupId;
        expectedUserGroup.m_groupName = "TEST_USER_GROUP_NAME";
        expectedUserGroup.m_keyLimit = 22;

        // If the command is 'new_userGroup' then a user group should be added in QueryManager::addUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUserGroup;
        reply[MessageManager::_result] = ( addUserGroupSuccess ? MessageManager::_success
                                                               : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testAddUserGroupFromInvalidPayload()
///
/// \brief  Tests user groups are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testAddUserGroupFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user_group\","
        "\"data\" : {"
            "\"name_wrong\" : \"TEST_USER_GROUP_NAME\","
            "\"key_limit_wrong\" : 22"
        "}"
    "}";

    for ( bool addUserGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addUserGroupSuccess = addUserGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockQueryManager->m_mockUserGroup = UserGroup();

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group should not change.
        UserGroup expectedUserGroup;

        // If the command is 'new_userGroup' then a user group should not be added in QueryManager::addUserGroup()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUserGroup;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testAddUserToUserGroupFromValidPayload()
///
/// \brief  Tests users are added to user groups when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testAddUserToUserGroupFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user_to_group\","
        "\"data\" : {"
            "\"user_id\" : 11,"
            "\"user_group\" : 22"
        "}"
    "}";

    for ( bool addUserToGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addUserToGroupSuccess = addUserToGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockQueryManager->m_mockUserId = -1;
        m_mockQueryManager->m_mockUserGroupId = -1;

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user ID and user group ID are based on the above payload.
        int expectedUserId = 11;
        int expectedUserGroupId = 22;

        // If the command is 'new_userToGroup' then a user should be added to a group in QueryManager::addUserToUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserId == expectedUserId );
        QVERIFY( m_mockQueryManager->m_mockUserGroupId == expectedUserGroupId );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUserToGroup;
        reply[MessageManager::_result] = ( addUserToGroupSuccess ? MessageManager::_success
                                                                 : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testAddUserToUserGroupFromInvalidPayload()
///
/// \brief  Tests users are not added to user groups when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testAddUserToUserGroupFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"new_user_to_group\","
        "\"data\" : {"
            "\"user_wrong\" : 11,"
            "\"user_group_wrong\" : 22"
        "}"
    "}";

    for ( bool addUserToGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_addUserToGroupSuccess = addUserToGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockQueryManager->m_mockUserId = -1;
        m_mockQueryManager->m_mockUserGroupId = -1;

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user ID and user group ID should not change.
        int expectedUserId = -1;
        int expectedUserGroupId = -1;

        // If the command is 'new_userToGroup' then a user should not be added to a group in QueryManager::addUserToUserGroup()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUserId == expectedUserId );
        QVERIFY( m_mockQueryManager->m_mockUserGroupId == expectedUserGroupId );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newUserToGroup;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserFromUserGroupFromValidPayload()
///
/// \brief  Tests users are removed from user groups when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserFromUserGroupFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user_from_group\","
        "\"data\" : {"
            "\"user_id\" : 33,"
            "\"user_group\" : 44"
        "}"
    "}";

    for ( bool removeUserFromGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_userGroupQuerySuccess = removeUserFromGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockQueryManager->m_mockUserId = -1;
        m_mockQueryManager->m_mockUserGroupId = -1;

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user ID and user group ID are based on the above payload.
        int expectedUserId = 33;
        int expectedUserGroupId = 44;

        // If the command is 'remove_userFromGroup' then a user should be removed from a group in QueryManager::removeUserFromUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserId == expectedUserId );
        QVERIFY( m_mockQueryManager->m_mockUserGroupId == expectedUserGroupId );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUserFromGroup;
        reply[MessageManager::_result] = ( removeUserFromGroupSuccess ? MessageManager::_success
                                                                      : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserFromUserGroupFromInvalidPayload()
///
/// \brief  Tests users are not removed from user groups when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserFromUserGroupFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user_from_group\","
        "\"data\" : {"
            "\"user_wrong\" : 11,"
            "\"user_group_wrong\" : 22"
        "}"
    "}";

    for ( bool removeUserFromGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_userGroupQuerySuccess = removeUserFromGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";
        m_mockQueryManager->m_mockUserId = -1;
        m_mockQueryManager->m_mockUserGroupId = -1;

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user ID and user group ID should not change.
        int expectedUserId = -1;
        int expectedUserGroupId = -1;

        // If the command is 'new_userToGroup' then a user should not be added to a group in QueryManager::addUserToUserGroup()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUserId == expectedUserId );
        QVERIFY( m_mockQueryManager->m_mockUserGroupId == expectedUserGroupId );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUserFromGroup;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateUserGroupFromValidPayload()
///
/// \brief  Tests user groups are updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateUserGroupFromValidPayload()
{
    // Test payload.
    QString testPayload =
    "{"
        "\"command\" : \"update_user_group\","
        "\"data\" : {"
            "\"id\" : 11,"
            "\"group_name\" : \"TEST_USER_GROUP_NAME_UPDATED\","
            "\"key_limit\" : 33"
        "}"
    "}";

    for ( bool updateUserGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updateUserGroupSuccess = updateUserGroupSuccess;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group is based on the above payload.
        UserGroup expectedUserGroup;
        expectedUserGroup.m_id = MockQueryManager::_testUserGroupId;
        expectedUserGroup.m_groupName = "TEST_USER_GROUP_NAME_UPDATED";
        expectedUserGroup.m_keyLimit = 33;

        // If the command is 'update_userGroup' then a user group should be updated in QueryManager::updateUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateUserGroup;
        reply[MessageManager::_result] = ( updateUserGroupSuccess ? MessageManager::_success
                                                                  : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateUserGroupFromInvalidPayload()
///
/// \brief  Tests user groups are not updated when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateUserGroupFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
        "\"command\" : \"update_user_group\","
        "\"data\" : {"
            "\"id_wrong\" : 11,"
            "\"group_name_wrong\" : \"TEST_USER_GROUP_NAME_UPDATED\","
            "\"key_limit_wrong\" : 33"
        "}"
    "}";

    for ( bool updateUserToGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_updateUserToGroupSuccess = updateUserToGroupSuccess;
        m_mockQueryManager->m_mockUserGroup = UserGroup();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group should be the default user.
        UserGroup expectedUserGroup;

        // If the command is 'remove_userGroup' then a user group should not be updated in QueryManager::removeUserGroup()
        // if the full JSON cannot be parsed correctly.
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected failed reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateUserGroup;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserGroupFromValidPayload()
///
/// \brief  Tests user groups are removed when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserGroupFromValidPayload()
{
    // Test payload.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user_group\","
        "\"data\" : {"
            "\"id\" : 44"
        "},"
        "\"trailing_data_to_remove\" : {"
            "\"info\" : \"To avoid orphaned data please, before removing the user remove all entries from the specified tables below where user = (3)\","
            "\"tables\" : [ \"tbl_user_to_groups\", \"tbl_group_cabinet_access\", \"tbl_group_location_access\", \"tbl_group_site_access\", \"tbl_group_system_access\", \"tbl_group_to_key_category_access\", \"tbl_group_to_key_access\", \"tbl_group_to_key_category_access\" ]"
        "}"
    "}";

    for ( bool removeUserGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_removeUserGroupSuccess = removeUserGroupSuccess;
        m_mockQueryManager->m_mockUserGroup = UserGroup();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group should be based on the above payload
        // (to show that the correct function was called).
        UserGroup expectedUserGroup;
        expectedUserGroup.m_id = 44;

        // If the command is 'remove_userGroup' then a user group should be updated in QueryManager::removeUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUserGroup;
        reply[MessageManager::_result] = ( removeUserGroupSuccess ? MessageManager::_success
                                                                  : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveUserGroupFromInvalidPayload()
///
/// \brief  Tests user groups are not removed when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveUserGroupFromInvalidPayload()
{
    // Test payload.
    QString testPayload =
    "{"
        "\"command\" : \"remove_user_group\","
        "\"data_wrong\" : {"
            "\"id_wrong\" : 44"
        "},"
        "\"trailing_data_to_remove_wrong\" : {"
            "\"info_wrong\" : \"To avoid orphaned data please, before removing the user remove all entries from the specified tables below where user = (3)\","
            "\"tables_wrong\" : [ \"tbl_user_to_groups\", \"tbl_group_cabinet_access\", \"tbl_group_location_access\", \"tbl_group_site_access\", \"tbl_group_system_access\", \"tbl_group_to_key_category_access\", \"tbl_group_to_key_access\", \"tbl_group_to_key_category_access\" ]"
        "}"
    "}";

    for ( bool removeUserGroupSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_removeUserGroupSuccess = removeUserGroupSuccess;
        m_mockQueryManager->m_mockUserGroup = UserGroup();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group is based on the above payload.
        UserGroup expectedUserGroup;

        // If the command is 'update_userGroup' then a user group should be updated in QueryManager::updateUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeUserGroup;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyFromValidPayload()
///
/// \brief  Tests tags are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyFromValidPayload()
{
    // Test payload.
    QString testPayload =
    "{"
      "\"command\" : \"new_key\","
      "\"data\" : {"
        "\"cabinet\" : 11,"
        "\"category\" : 22,"
        "\"slot\" : 33,"
        "\"current_slot\" : 44,"
        "\"fob\" : 55,"
        "\"key_name\" : \"TEST_KEY_NAME\","
        "\"key_description\" : \"TEST_KEY_DESCRIPTION\","
        "\"key_serial\" : \"TEST_KEY_SERIAL\","
        "\"date_added\" : \"TEST_DATE_ADDED\","
        "\"start_time\" : \"TEST_START_TIME\","
        "\"end_time\" : \"TEST_END_TIME\","
        "\"is_static\" : true,"
        "\"is_out\" : true,"
        "\"is_wrong_slot\" : true,"
        "\"additionalFields\" : \"TEST_ADDITIONAL_FIELDS\""
      "}"
    "}";

    for ( bool newKeySuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = newKeySuccess;
        m_mockQueryManager->m_mockTag = new Tag( this );
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group should be based on the above payload
        // (to show that the correct function was called).
        Tag* expectedTag = new Tag( this );
        expectedTag->m_cabinetId = 11;
        expectedTag->m_category = 22;
        expectedTag->m_slot = 33;
        expectedTag->m_currentSlot = 44;
        expectedTag->m_fob = 55;
        expectedTag->m_tagName = "TEST_KEY_NAME";
        expectedTag->m_tagDescription = "TEST_KEY_DESCRIPTION";
        expectedTag->m_tagSerial = "TEST_KEY_SERIAL";
        expectedTag->m_dateAdded = "TEST_DATE_ADDED";
        expectedTag->m_startTime = "TEST_START_TIME";
        expectedTag->m_endTime = "TEST_END_TIME";
        expectedTag->m_isStatic = true;
        expectedTag->m_isOut = true;
        expectedTag->m_isWrongSlot = true;

        // If the command is 'new_key' then a new tag / key should be added in QueryManager::addTag().
        QVERIFY( *m_mockQueryManager->m_mockTag == *expectedTag );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKey;
        reply[MessageManager::_result] = ( newKeySuccess ? MessageManager::_success
                                                         : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyFromInvalidPayload()
///
/// \brief  Tests tags are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyFromInvalidPayload()
{
    // Test payload.
    QString testPayload =
    "{"
      "\"command\" : \"new_key\","
      "\"data_wrong\" : {"
        "\"cabinet_wrong\" : 11,"
        "\"category_wrong\" : 22,"
        "\"slot_wrong\" : 33,"
        "\"current_slot_wrong\" : 44,"
        "\"key_name_wrong\" : \"TEST_KEY_NAME\","
        "\"key_description_wrong\" : \"TEST_KEY_DESCRIPTION\","
        "\"key_serial_wrong\" : \"TEST_KEY_SERIAL\","
        "\"fob_wrong\" : 55,"
        "\"date_added_wrong\" : \"TEST_DATE_ADDED\","
        "\"start_time_wrong\" : \"TEST_START_TIME\","
        "\"end_time_wrong\" : \"TEST_END_TIME\","
        "\"is_static_wrong\" : true,"
        "\"is_out_wrong\" : true,"
        "\"is_wrong_slot_wrong\" : true,"
        "\"additionalFields_wrong\" : \"TEST_ADDITIONAL_FIELDS\""
      "}"
    "}";

    for ( bool newKeySuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = newKeySuccess;
        m_mockQueryManager->m_mockTag = new Tag( this );
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user group isn't changed.
        UserGroup expectedUserGroup;

        // If the command is 'update_userGroup' then a user group should be updated in QueryManager::updateUserGroup().
        QVERIFY( m_mockQueryManager->m_mockUserGroup == expectedUserGroup );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKey;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyFromValidPayload()
///
/// \brief  Tests tags are removed when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key\","
      "\"data\" : {"
        "\"fob\" : 1234567890"
      "},"
      "\"trailing_data_to_remove\" : {"
        "\"info\" : \"To avoid orphaned data please, before removing the key remove all entries from the specified tables below where key_id = to whatever id the key has in system/cabinet\","
        "\"tables\" : [ \"tbl_group_to_key_access\", \"tbl_key_to_additional_field_values\", \"tbl_key_to_categories\", \"tbl_user_to_key_access\" ]"
      "}"
    "}";

    for ( bool removeKeySuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = removeKeySuccess;
        m_mockQueryManager->m_mockTag = new Tag( this );
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user is based on the above payload.
        Tag* expectedTag = new Tag( this );
        expectedTag->m_id = ( removeKeySuccess ? MockQueryManager::_testTagId : -1 );
        expectedTag->m_fob = ( removeKeySuccess ? 1234567890 : -1 );

        // If the command is 'remove_key' then a user should only be removed in QueryManager::removeTag()
        // if the tag queries are successful.
        QVERIFY( *m_mockQueryManager->m_mockTag == *expectedTag );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKey;
        reply[MessageManager::_result] = ( removeKeySuccess ? MessageManager::_success
                                                            : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyFromInvalidPayload()
///
/// \brief  Tests tags are not removed when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key\","
      "\"data_wrong\" : {"
        "\"fob_wrong\" : 1234567890"
      "},"
      "\"trailing_data_to_remove\" : {"
        "\"info_wrong\" : \"To avoid orphaned data please, before removing the key remove all entries from the specified tables below where key_id = to whatever id the key has in system/cabinet\","
        "\"tables_wrong\" : [ \"tbl_group_to_key_access\", \"tbl_key_to_additional_field_values\", \"tbl_key_to_categories\", \"tbl_user_to_key_access\" ]"
      "}"
    "}";

    for ( bool removeKeySuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = removeKeySuccess;
        m_mockQueryManager->m_mockTag = new Tag( this );
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected user is default.
        Tag* expectedTag = new Tag( this );

        // If the command is 'remove_key' then a user should not be removed in QueryManager::removeTag().
        QVERIFY( *m_mockQueryManager->m_mockTag == *expectedTag );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKey;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyFromValidPayload()
///
/// \brief  Tests tags are updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key\","
      "\"data\" : {"
        "\"cabinet\" : 11,"
        "\"category\" : 22,"
        "\"slot\" : 33,"
        "\"current_slot\" : 44,"
        "\"fob\" : 55,"
        "\"key_name\" : \"TEST_KEY_NAME\","
        "\"key_description\" : \"TEST_KEY_DESCRIPTION\","
        "\"key_serial\" : \"TEST_KEY_SERIAL\","
        "\"date_added\" : \"TEST_DATE_ADDED\","
        "\"start_time\" : \"TEST_START_TIME\","
        "\"end_time\" : \"TEST_END_TIME\","
        "\"is_static\" : true,"
        "\"is_out\" : true,"
        "\"is_wrong_slot\" : true,"
        "\"additionalFields\" : \"TEST_ADDITIONAL_FIELDS\""
      "}"
    "}";

    for ( bool updateKeySuccess : _successFail )
    {
        Tag* defaultTag = new Tag( this );

        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = updateKeySuccess;
        m_mockQueryManager->m_mockTag = defaultTag;
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        Tag* payloadTag = new Tag( this );
        payloadTag->m_id = MockQueryManager::_testTagId;
        payloadTag->m_cabinetId = 11;
        payloadTag->m_category = 22;
        payloadTag->m_slot = 33;
        payloadTag->m_currentSlot = 44;
        payloadTag->m_fob = 55;
        payloadTag->m_tagName = "TEST_KEY_NAME";
        payloadTag->m_tagDescription = "TEST_KEY_DESCRIPTION";
        payloadTag->m_tagSerial = "TEST_KEY_SERIAL";
        payloadTag->m_dateAdded = "TEST_DATE_ADDED";
        payloadTag->m_startTime = "TEST_START_TIME";
        payloadTag->m_endTime = "TEST_END_TIME";
        payloadTag->m_isStatic = true;
        payloadTag->m_isOut = true;
        payloadTag->m_isWrongSlot = true;

        Tag* expectedTag = ( updateKeySuccess ? payloadTag : defaultTag );

        // If the command is 'update_key' then a user should only be removed in QueryManager::updateTag()
        // if the tag queries are successful.
        QVERIFY( *m_mockQueryManager->m_mockTag == *expectedTag );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKey;
        reply[MessageManager::_result] = ( updateKeySuccess ? MessageManager::_success
                                                            : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyFromInvalidPayload()
///
/// \brief  Tests tags are not updated when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key\","
      "\"data_wrong\" : {"
        "\"cabinet_wrong\" : 11,"
        "\"slot_wrong\" : 22,"
        "\"current_slot_wrong\" : 33,"
        "\"fob_wrong\" : 44,"
        "\"key_name_wrong\" : \"TEST_KEY_NAME\","
        "\"key_description_wrong\" : \"TEST_KEY_DESCRIPTION\","
        "\"key_serial_wrong\" : \"TEST_KEY_SERIAL\","
        "\"date_added_wrong\" : \"TEST_DATE_ADDED\","
        "\"start_time_wrong\" : \"TEST_START_TIME\","
        "\"end_time_wrong\" : \"TEST_END_TIME\","
        "\"is_static_wrong\" : true,"
        "\"is_out_wrong\" : true,"
        "\"is_wrong_slot_wrong\" : true,"
        "\"additionalFields_wrong\" : \"TEST_ADDITIONAL_FIELDS\""
      "}"
    "}";

    for ( bool updateKeySuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_tagQuerySuccess = updateKeySuccess;
        m_mockQueryManager->m_mockTag = new Tag( this );
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        Tag* expectedTag = new Tag( this );

        // If the command is 'remove_key' then a user should not be removed in QueryManager::removeTag().
        QVERIFY( *m_mockQueryManager->m_mockTag == *expectedTag );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKey;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyUserPermissionFromValidPayload()
///
/// \brief  Tests tag / key user permissions are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyUserPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"new_key_user_access\","
      "\"data\" : {"
        "\"user_id\" : 11,"
        "\"key_id\" : 22,"
        "\"is_access_granted\" : true"
      "}"
    "}";

    for ( bool newKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = newKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        UserTagAccess payloadTagAccess;
        payloadTagAccess.m_userId = 11;
        payloadTagAccess.m_keyId = 22;
        payloadTagAccess.m_isAccessGranted = true;

        UserTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'new_key_user_access' then a user should be added in QueryManager::addTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKeyUserPermission;
        reply[MessageManager::_result] = ( newKeyUserPermissionSuccess ? MessageManager::_success
                                                                       : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyUserPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key user permissions are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyUserPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"new_key_user_access\","
      "\"data_wrong\" : {"
        "\"user_id_wrong\" : 11,"
        "\"key_id_wrong\" : 22,"
        "\"is_access_granted_wrong\" : true"
      "}"
    "}";

    for ( bool newKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = newKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        UserTagAccess expectedTagAccess;

        // If the command is 'new_key_user_access' then a user tag / key permission should not be added in QueryManager::addTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKeyUserPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyUserPermissionFromValidPayload()
///
/// \brief  Tests tag / key user permissions are removed when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyUserPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key_user_access\","
      "\"data\" : {"
        "\"id\" : 11"
      "}"
    "}";

    for ( bool removeKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = removeKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        UserTagAccess payloadTagAccess;
        payloadTagAccess.m_id = 11;

        UserTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'remove_key_user_access' then a user should be removed in QueryManager::removeTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKeyUserPermission;
        reply[MessageManager::_result] = ( removeKeyUserPermissionSuccess ? MessageManager::_success
                                                                          : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyUserPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key user permissions are not removed when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyUserPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key_user_access\","
      "\"data_wrong\" : {"
        "\"user_id_wrong\" : 11,"
        "\"key_id_wrong\" : 22"
      "}"
    "}";

    for ( bool removeKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = removeKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        UserTagAccess expectedTagAccess;

        // If the command is 'new_key_user_access' then a user tag / key permission should not be added in QueryManager::addTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKeyUserPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyUserPermissionFromValidPayload()
///
/// \brief  Tests tag / key user permissions are updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyUserPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key_user_access\","
      "\"data\" : {"
        "\"id\" : 11,"
        "\"user_id\" : 22,"
        "\"key_id\" : 33,"
        "\"is_access_granted\" : false"
      "}"
    "}";

    for ( bool updateKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = updateKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        UserTagAccess payloadTagAccess;
        payloadTagAccess.m_id = 11;
        payloadTagAccess.m_userId = 22;
        payloadTagAccess.m_keyId = 33;

        UserTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'update_key_user_access' then a user should be removed in QueryManager::updateTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKeyUserPermission;
        reply[MessageManager::_result] = ( updateKeyUserPermissionSuccess ? MessageManager::_success
                                                                          : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyUserPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key user permissions are not updated when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyUserPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key_user_access\","
      "\"data_wrong\" : {"
        "\"user_id_wrong\" : 11,"
        "\"key_id_wrong\" : 22"
      "}"
    "}";

    for ( bool updateKeyUserPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = updateKeyUserPermissionSuccess;
        m_mockQueryManager->m_mockUserTagAccess = UserTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        UserTagAccess expectedTagAccess;

        // If the command is 'update_key_user_access' then a user tag / key permission should not be added in QueryManager::updateTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKeyUserPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyGroupPermissionFromValidPayload()
///
/// \brief  Tests tag / key group permissions are added when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyGroupPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"new_key_usergroup_access\","
      "\"data\" : {"
        "\"user_group\" : 11,"
        "\"key_id\" : 22,"
        "\"is_access_granted\" : true"
      "}"
    "}";

    for ( bool newKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserGroupTagAccessSuccess = newKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        GroupTagAccess payloadTagAccess;
        payloadTagAccess.m_userGroupId = 11;
        payloadTagAccess.m_keyId = 22;
        payloadTagAccess.m_isAccessGranted = true;

        GroupTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'new_key_usergroup_access' then a user group should be added in QueryManager::addTagGroupPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKeyGroupPermission;
        reply[MessageManager::_result] = ( newKeyUserGroupPermissionSuccess ? MessageManager::_success
                                                                            : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testNewKeyGroupPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key group permissions are not added when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testNewKeyGroupPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"new_key_usergroup_access\","
      "\"data_wrong\" : {"
        "\"user_group_wrong\" : 11,"
        "\"key_id_wrong\" : 22,"
        "\"is_access_granted_wrong\" : true"
      "}"
    "}";

    for ( bool newKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserGroupTagAccessSuccess = newKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        GroupTagAccess expectedTagAccess;

        // If the command is 'new_key_usergroup_access' then a user tag / key permission should not be added in QueryManager::addTagGroupPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_newKeyGroupPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyGroupPermissionFromValidPayload()
///
/// \brief  Tests tag / key group permissions are removed when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyGroupPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key_usergroup_access\","
      "\"data\" : {"
        "\"id\" : 11"
      "}"
    "}";

    for ( bool removeKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserGroupTagAccessSuccess = removeKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        GroupTagAccess payloadTagAccess;
        payloadTagAccess.m_id = 11;

        GroupTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'remove_key_usergroup_access' then a user should be removed in QueryManager::removeTagGroupPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKeyGroupPermission;
        reply[MessageManager::_result] = ( removeKeyUserGroupPermissionSuccess ? MessageManager::_success
                                                                               : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testRemoveKeyGroupPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key group permissions are not removed when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testRemoveKeyGroupPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"remove_key_usergroup_access\","
      "\"data_wrong\" : {"
        "\"user_id_wrong\" : 11,"
        "\"key_id_wrong\" : 22"
      "}"
    "}";

    for ( bool removeKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserGroupTagAccessSuccess = removeKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        GroupTagAccess expectedTagAccess;

        // If the command is 'new_key_usergroup_access' then a user tag / key permission should not be added in QueryManager::addTagGroupPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_removeKeyGroupPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyGroupPermissionFromValidPayload()
///
/// \brief  Tests tag / key group permissions are updated when a valid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyGroupPermissionFromValidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key_usergroup_access\","
      "\"data\" : {"
        "\"id\" : 11,"
        "\"user_group\" : 22,"
        "\"key_id\" : 33,"
        "\"is_access_granted\" : false"
      "}"
    "}";

    for ( bool updateKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserGroupTagAccessSuccess = updateKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is based on the above payload.
        GroupTagAccess payloadTagAccess;
        payloadTagAccess.m_id = 11;
        payloadTagAccess.m_userGroupId = 22;
        payloadTagAccess.m_keyId = 33;

        GroupTagAccess expectedTagAccess = payloadTagAccess;

        // If the command is 'update_key_user_access' then a user should be removed in QueryManager::updateTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKeyGroupPermission;
        reply[MessageManager::_result] = ( updateKeyUserGroupPermissionSuccess ? MessageManager::_success
                                                                               : MessageManager::_fail );

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testUpdateKeyGroupPermissionFromInvalidPayload()
///
/// \brief  Tests tag / key group permissions are not updated when an invalid payload is received.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testUpdateKeyGroupPermissionFromInvalidPayload()
{
    // Test payload.
    // TODO: Read params from constants.
    QString testPayload =
    "{"
      "\"command\" : \"update_key_usergroup_access\","
      "\"data_wrong\" : {"
        "\"user_group_wrong\" : 11,"
        "\"key_id_wrong\" : 22"
      "}"
    "}";

    for ( bool updateKeyUserGroupPermissionSuccess : _successFail )
    {
        // Toggle database write success and reset publish payload.
        m_mockQueryManager->m_getUserTagAccessSuccess = updateKeyUserGroupPermissionSuccess;
        m_mockQueryManager->m_mockUserGroupTagAccess = GroupTagAccess();
        m_mockMessageManager->m_mockPublishedPayload = "";

        // Emit the given payload.
        m_mockMessageManager->doReceivePayload( testPayload );

        // Expected tag / key is default.
        GroupTagAccess expectedTagAccess;

        // If the command is 'update_key_user_access' then a user tag / key permission should not be added in QueryManager::updateTagUserPermissions().
        QVERIFY( m_mockQueryManager->m_mockUserGroupTagAccess == expectedTagAccess );

        // Create the expected reply message.
        QJsonObject reply;
        reply[MessageManager::_reply] = MessageManager::_updateKeyGroupPermission;
        reply[MessageManager::_result] = MessageManager::_fail;

        QJsonDocument jsonDoc( reply );
        QString expectedJson = jsonDoc.toJson( QJsonDocument::Indented );

        // Once processed, the reply should be passed into MessageManager::publish().
        QVERIFY( m_mockMessageManager->m_mockPublishedPayload == expectedJson );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testKeyPermissions()
///
/// \brief  Tests key permissions are accessed correctly.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testKeyPermissions()
{
    //
//    // Test user.
//    User user;
//    user.m_id = _userId;
//    user.m_firstName = _firstName;
//    user.m_lastName = _lastName;

//    // Assume we can always get the user group.
//    m_mockQueryManager->m_getUserGroupSuccess = true;

//    for ( bool userGroupSystemAccess : _successFail )
//    {
//        // Mock the user group system access.
//        m_mockQueryManager->m_mockUserGroupTagAccess = userGroupSystemAccess;

//        for ( bool userSystemAccess : _successFail )
//        {
//            // Mock the user system access.
//            m_mockQueryManager->m_mockUserTagAccess = userSystemAccess;

//            for ( bool userGroupSuccess : _successFail )
//            {
//                // Mock getting the user group.
//                m_mockQueryManager->m_getUsersGroupSuccess = userGroupSuccess;

//                bool access = false;

//                // The user and user group do not have specified permissions,
//                // Access should always be denied as this indicates the
//                // database could not be queried so the actual access is unknown.
//                m_mockQueryManager->m_getUserTagAccessSuccess = false;
//                m_mockQueryManager->m_getUserGroupTagAccessSuccess = false;

//                m_databaseControl->checkUserTagPermissions( user, access );

//                QVERIFY( !access );

//                // Test user permissions are specified without group permissions.
//                // Access should match the user tag / key access regardless of
//                // whether the user's group could be obtained.
//                m_mockQueryManager->m_getUserTagAccessSuccess = true;
//                m_mockQueryManager->m_getUserGroupTagAccessSuccess = false;

//                m_databaseControl->checkUserTagPermissions( user, access );

//                QVERIFY( access == userSystemAccess );

//                // Test user group permissions are specified without user permissions.
//                // Access should match the user group tag / key access only if the
//                // the user's group could be obtained.
//                m_mockQueryManager->m_getUserTagAccessSuccess = false;
//                m_mockQueryManager->m_getUserGroupTagAccessSuccess = true;

//                m_databaseControl->checkUserTagPermissions( user, access );

//                QVERIFY( access == ( userGroupSystemAccess && userGroupSuccess ) );

//                // Test both user and user group permissions are specified.
//                // Access should match the user tag / key access since that takes
//                // precedence over user group tag / key access.
//                m_mockQueryManager->m_getUserTagAccessSuccess = true;
//                m_mockQueryManager->m_getUserGroupTagAccessSuccess = true;

//                m_databaseControl->checkUserTagPermissions( user, access );

//                QVERIFY( access == userSystemAccess );
//            }
//        }
    //    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testKeyDisplayGroupSize()
///
/// \brief  Tests access to tag / key data group size.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::testKeyDisplayGroupSize()
{
    QSignalSpy spy( m_databaseControl, &DatabaseControl::tagGroupSizeChanged );

    // Test default tag / key group size.
    unsigned int defaultTagGroupSize = 0;

    // Test tag / key group size can be access using the get function.
    unsigned int tagGroupSize = m_databaseControl->getTagGroupSize();

    QVERIFY( tagGroupSize == defaultTagGroupSize );

    // Test tag / key group size can be set correctly.
    unsigned int expectedTagGroupSize = 123;

    // Test setting the same value twice doesn't result in a changed signal.
    m_databaseControl->setTagGroupSize( expectedTagGroupSize );
    m_databaseControl->setTagGroupSize( expectedTagGroupSize );
    tagGroupSize = m_databaseControl->getTagGroupSize();

    QVERIFY( tagGroupSize == expectedTagGroupSize );
    QVERIFY( spy.count() == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::testKeyDisplayGroupSize()
///
/// \brief  Tests a property exists for the tag / key data group size.
////////////////////////////////////////////////////////////////////////////////}
void DatabaseControlTest::testKeyDisplayGroupSizeProperties()
{
    QSignalSpy spy( m_databaseControl, &DatabaseControl::tagGroupSizeChanged );

    // Test default tag / key group size.
    unsigned int defaultTagGroupSize = 0;

    // Test tag / key group size can be accessed via the property.
    QVariant tagGroupSize = m_databaseControl->property( "tagGroupSize" );

    QVERIFY( tagGroupSize.isValid() );
    QVERIFY( tagGroupSize.toUInt() == defaultTagGroupSize );

    // Test tag / key group size can be set via the property.
    unsigned int expectedTagGroupSize = 123;

    // Test setting the same value twice doesn't result in a changed signal.
    m_databaseControl->setProperty( "tagGroupSize", expectedTagGroupSize );
    m_databaseControl->setProperty( "tagGroupSize", expectedTagGroupSize );
    tagGroupSize = m_databaseControl->property( "tagGroupSize" );

    QVERIFY( tagGroupSize.toUInt() == expectedTagGroupSize );
    QVERIFY( spy.count() == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::cleanup()
{
    DatabaseControl::destroy();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     DatabaseControlTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void DatabaseControlTest::cleanupTestCase()
{

}
