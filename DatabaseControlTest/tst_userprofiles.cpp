// External.
#include <string.h>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/userprofiles.h>

// Project.
#include "tst_userprofiles.h"

// Statics.
static const char* _testId = "id";
static const char* _testName = "profile_name";
static const char* _testAdministrator = "Administrator";
static const char* _testStandard = "Standard";
static const char* _testReport = "Report";
static const char* _testBookingOnly = "Booking Only";
static const char* _testEngineer = "Engineer";

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::UserProfilesTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
UserProfilesTest::UserProfilesTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void UserProfilesTest::init()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void UserProfilesTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::testConstants()
///
/// \brief  Tests permissions constants.
////////////////////////////////////////////////////////////////////////////////
void UserProfilesTest::testConstants()
{
    QVERIFY( strcmp( UserProfiles::_id,            _testId ) == 0 );
    QVERIFY( strcmp( UserProfiles::_profileName,   _testName ) == 0 );
    QVERIFY( strcmp( UserProfiles::_administrator, _testAdministrator ) == 0 );
    QVERIFY( strcmp( UserProfiles::_standard,      _testStandard ) == 0 );
    QVERIFY( strcmp( UserProfiles::_report,        _testReport ) == 0 );
    QVERIFY( strcmp( UserProfiles::_bookingOnly,   _testBookingOnly ) == 0 );
    QVERIFY( strcmp( UserProfiles::_engineer,      _testEngineer ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void UserProfilesTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UserProfilesTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void UserProfilesTest::cleanupTestCase()
{

}
