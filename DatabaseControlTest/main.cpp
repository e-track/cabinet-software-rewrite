// Qt.
#include <QGuiApplication>

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "tst_constants.h"
#include "tst_databasecontrol.h"
#include "tst_messagemanager.h"
#include "tst_querymanager.h"
#include "tst_tagcategory.h"
#include "tst_userprofilepermissions.h"
#include "tst_userprofiles.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Application entry point.
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // A GUI application is required for the event system.
    QGuiApplication app( argc, argv );

    // DatabaseControl tests.
    DECLARE_UNIT_TEST( ConstantsTest )
    DECLARE_UNIT_TEST( DatabaseControlTest )
    DECLARE_UNIT_TEST( MessageManagerTest )
    DECLARE_UNIT_TEST( QueryManagerTest )
    DECLARE_UNIT_TEST( TagCategoryTest )
    DECLARE_UNIT_TEST( UserProfilePermissionsTest )
    DECLARE_UNIT_TEST( UserProfilesTest )

    return UnitTest::run();
}
