// External.
#include <string.h>

// Qt.
#include <QMetaObject>
#include <QQmlEngine>
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/messagemanager.h>

// Project.
#include "tst_messagemanager.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::MessageManagerTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
MessageManagerTest::MessageManagerTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::init()
{
    // Create class under test.
    m_messageManager = new MessageManager();
    QVERIFY( m_messageManager );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::testRegisterClass()
{
    // Register the class.
    MessageManager::registerClass();

    // Expected class and module names.
    QString expectedModuleName = "Message";
    QString expectedClassName = "Manager";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::testInvokables()
///
/// \brief  Tests invokable functions.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::testInvokables()
{
    QVERIFY( QMetaObject::invokeMethod( m_messageManager, "connectToHost", Q_ARG( QString, "localhost" ), Q_ARG( int, 1234 ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_messageManager, "disconnectFromHost" ) );
    QVERIFY( QMetaObject::invokeMethod( m_messageManager, "subscribe", Q_ARG( QString, "TEST_TOPIC" ) ) );
    QVERIFY( QMetaObject::invokeMethod( m_messageManager, "publish", Q_ARG( QString, "TEST_MESSAGE" ) ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::testConstants()
///
/// \brief  Tests message constants.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::testConstants()
{
    // Topics.
    QVERIFY( strcmp( MessageManager::_topicQuery, "query" ) == 0 );
    QVERIFY( strcmp( MessageManager::_topic,      "e-track/cabinet-1" ) == 0 );
    QVERIFY( strcmp( MessageManager::_debugTopic, "cpp/debugging" ) == 0 );

    // General.
    QVERIFY( strcmp( MessageManager::_command,   "command" ) == 0 );
    QVERIFY( strcmp( MessageManager::_reply,     "reply" ) == 0 );
    QVERIFY( strcmp( MessageManager::_event,     "event" ) == 0 );
    QVERIFY( strcmp( MessageManager::_data,      "data" ) == 0 );
    QVERIFY( strcmp( MessageManager::_packetId,  "packet_id" ) == 0 );
    QVERIFY( strcmp( MessageManager::_timestamp, "timestamp" ) == 0 );
    QVERIFY( strcmp( MessageManager::_result,    "result" ) == 0 );
    QVERIFY( strcmp( MessageManager::_success,   "success" ) == 0 );
    QVERIFY( strcmp( MessageManager::_fail,      "fail" ) == 0 );
    QVERIFY( strcmp( MessageManager::_info,  "info" ) == 0 );
    QVERIFY( strcmp( MessageManager::_tables,  "tables" ) == 0 );
    QVERIFY( strcmp( MessageManager::_trailingDataToRemove,  "trailing_data_to_remove" ) == 0 );


    // Commands.
    QVERIFY( strcmp( MessageManager::_helloWorld,               "hello_world" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newUser,                  "new_user" ) == 0 );
    QVERIFY( strcmp( MessageManager::_updateUser,               "update_user" ) == 0 );
    QVERIFY( strcmp( MessageManager::_removeUser,               "remove_user" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newUserGroup,             "new_user_group" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newUserToGroup,           "new_user_to_group" ) == 0 );
    QVERIFY( strcmp( MessageManager::_removeUserFromGroup,      "remove_user_from_group" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newKey,                   "new_key" ) == 0 );
    QVERIFY( strcmp( MessageManager::_removeKey,                "remove_key" ) == 0 );
    QVERIFY( strcmp( MessageManager::_updateKey,                "update_key" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newKeyUserPermission,     "new_key_user_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_removeKeyUserPermission,  "remove_key_user_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_updateKeyUserPermission,  "update_key_user_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_newKeyGroupPermission,    "new_key_usergroup_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_removeKeyGroupPermission, "remove_key_usergroup_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_updateKeyGroupPermission, "update_key_usergroup_access" ) == 0 );
    QVERIFY( strcmp( MessageManager::_resetUserPin,             "reset_user_pin" ) == 0 );

    // Events.
    QVERIFY( strcmp( MessageManager::_loginSuccess,   "login_success" ) == 0 );
    QVERIFY( strcmp( MessageManager::_loginFail,      "login_failed" ) == 0 );
    QVERIFY( strcmp( MessageManager::_logout,         "logout" ) == 0 );
    QVERIFY( strcmp( MessageManager::_accessDenied,   "access_denied" ) == 0 );
    QVERIFY( strcmp( MessageManager::_keyIn,          "key_in" ) == 0 );
    QVERIFY( strcmp( MessageManager::_keyOut,         "key_out" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MessageManagerTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void MessageManagerTest::cleanupTestCase()
{

}
