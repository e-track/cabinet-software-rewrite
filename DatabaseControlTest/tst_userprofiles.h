#ifndef TST_USERPROFILES_H
#define TST_USERPROFILES_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

// Statics.
class UserProfiles;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the UserProfiles class.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT UserProfilesTest : public UnitTest
{
    Q_OBJECT

public:
    explicit UserProfilesTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Data.
    void testConstants();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    UserProfiles* m_userProfiles;        ///< Class under test.
};

#endif // TST_USERPROFILES_H
