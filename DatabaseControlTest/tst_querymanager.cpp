// External.
#include <string.h>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <DatabaseControl/querymanager.h>
#include <DatabaseControl/usergroup.h>

// Project.
#include "tst_querymanager.h"

// Constants.
static const char* _pin = "1234";
static const int _testUserId = 11;
static const int _testUserGroupId = 22;

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::QueryManagerTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
QueryManagerTest::QueryManagerTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::init()
{
    // Test data.
    m_user.m_id = _testUserId;
    m_userGroup.m_id = _testUserGroupId;
    m_tag = new Tag( this );

    // Create class under test.
    m_queryManager = new QueryManager();
    QVERIFY( m_queryManager );

    // Init database.
    m_queryManager->initDatabase();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testConstants()
///
/// \brief  Tests the SQL queries and query parameters.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testConstants()
{
    // Database statics.
    QVERIFY( strcmp( QueryManager::_defaultDatabaseName, "DEFAULT_DB_CONNECTION" ) == 0 );

    // Statics for 'entry' table.
    QVERIFY( QueryManager::_entryUsePin == 1 );
    QVERIFY( QueryManager::_entryUseFingerprint == 2 );
    QVERIFY( QueryManager::_entryUseCard == 3 );

    // Statics for 'event_action' table.
    QVERIFY( QueryManager::_eventActionLogin == 1 );
    QVERIFY( QueryManager::_eventActionLoginFail == 2 );
    QVERIFY( QueryManager::_eventActionLogout == 3 );
    QVERIFY( QueryManager::_eventActionAccessDenied == 18 );
    QVERIFY( QueryManager::_eventActionKeyIn == 6 );
    QVERIFY( QueryManager::_eventActionKeyOut == 7 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testTempLogin()
///
/// \brief  Test login using the temporary PIN.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testLoginTempPin()
{
    // The query manager only constructs an SQL query so we only need to check
    // that this function exists.
    // The full temporary PIN login test is in DatabaseControlTest.
    m_queryManager->loginTempPin( _pin );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testLoginPin()
///
/// \brief  Test login using the PIN.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testLoginPin()
{
    // The query manager only constructs an SQL query so we only need to check
    // that this function exists.
    // The full temporary PIN login test is in DatabaseControlTest.

    m_queryManager->loginPin( m_user, _pin );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUpdatePin()
///
/// \brief  Test PIN can be updated.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUpdatePin()
{
    m_queryManager->updatePin( _pin, m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testLoginEvent()
///
/// \brief  Test login event is written to the database correctly.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testLoginEvent()
{
    m_queryManager->eventLogLoginSuccess( m_user );
    m_queryManager->eventLogLoginFail( m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testLoginEvent()
///
/// \brief  Test login event is written to the database correctly.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testLogoutEvent()
{
    m_queryManager->eventLogLogout( m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testLoginEvent()
///
/// \brief  Test login event is written to the database correctly.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testAccessDeniedEvent()
{
    m_queryManager->eventLogAccessDenied( m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserCommands()
///
/// \brief  Test user commands exist.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserCommands()
{
    m_queryManager->addUser( m_user );
    m_queryManager->updateUser( m_user );
    m_queryManager->removeUser( m_user, "TEST_TABLE" );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserProfilePermissions()
///
/// \brief  Test user profile permissions can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserProfilePermissions()
{
    m_queryManager->getUserProfilePermissions();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserProfiles()
///
/// \brief  Test user profiles can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserProfiles()
{
    m_queryManager->getUserProfiles();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserGroups()
///
/// \brief  Test users group can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserGroups()
{
    QSqlDatabase db = QSqlDatabase::database( QueryManager::_defaultDatabaseName );
    db.open();

    m_queryManager->getUsersGroup( m_user );
    m_queryManager->getUsersGroup( m_user );
    m_queryManager->addUserGroup( m_userGroup );
    m_queryManager->addUserToUserGroup( m_user.m_id, m_userGroup.m_id );
    m_queryManager->removeUserFromUserGroup( m_user.m_id, m_userGroup.m_id );
    m_queryManager->updateUserGroup( m_userGroup );
    m_queryManager->removeUserGroup( m_userGroup, "TEST_TABLE" );

    db.close();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserGroupSystemAccess()
///
/// \brief  Test user groups system access can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserGroupSystemAccess()
{
    m_queryManager->getUserGroupSystemAccess( m_userGroup );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserSystemAccess()
///
/// \brief  Test user system access can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserSystemAccess()
{
    m_queryManager->getUserSystemAccess( m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserGroupCabinetAccess()
///
/// \brief  Test user groups cabinet access can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserGroupCabinetAccess()
{
    m_queryManager->getUserGroupCabinetAccess( m_userGroup );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testUserCabinetAccess()
///
/// \brief  Test user cabinet access can be read from the database.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testUserCabinetAccess()
{
    m_queryManager->getUserCabinetAccess( m_user );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testTagCommands()
///
/// \brief  Test tag / key commands exist.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testTagCommands()
{
    m_queryManager->addTag( m_tag );
    m_queryManager->removeTag( m_tag, "TEST_TABLE" );
    m_queryManager->updateTag( m_tag );

    UserTagAccess userTagPermissions;
    m_queryManager->addTagUserPermissions( userTagPermissions );
    m_queryManager->removeTagUserPermissions( userTagPermissions );
    m_queryManager->updateTagUserPermissions( userTagPermissions );

    GroupTagAccess groupTagPermissions;
    m_queryManager->addTagGroupPermissions( groupTagPermissions );
    m_queryManager->removeTagGroupPermissions( groupTagPermissions );
    m_queryManager->updateTagGroupPermissions( groupTagPermissions );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testSqlQuery()
///
/// \brief  Test generic SQL query can be executed.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testSqlQuery()
{
    // Perform SQL query.
    m_queryManager->performSqlQuery( "" );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryFactoryDelete()
///
/// \brief  Test factory can produce delete queries.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryFactoryDelete()
{
    QString expectedDeleteQuery = "DELETE FROM test_table WHERE `test_column` = :test_column";
    QString deleteQuery = QueryManager::getDeleteQuery( "test_table", "test_column" );

    QVERIFY( deleteQuery == expectedDeleteQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryFactoryInsert()
///
/// \brief  Test factory can produce insert queries.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryFactoryInsert()
{
    QStringList columns = { "test_column_1", "test_column_2", "test_column_3" };
    QString expectedInsertQuery = "INSERT INTO test_table "
                                  "(`test_column_1`, `test_column_2`, `test_column_3`) "
                                  "VALUES (:test_column_1, :test_column_2, :test_column_3)";
    QString insertQuery = QueryManager::getInsertQuery( "test_table", columns );

    QVERIFY( insertQuery == expectedInsertQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryFactorySelect()
///
/// \brief  Test factory can produce select queries.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryFactorySelect()
{
    QStringList columns = { "test_column_1", "test_column_2", "test_column_3" };
    QString expectedSelectQuery = "SELECT `test_column_1`, `test_column_2`, `test_column_3` FROM test_table";
    QString selectQuery = QueryManager::getSelectQuery( "test_table", columns );

    QVERIFY( selectQuery == expectedSelectQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryFactorySelectAll()
///
/// \brief  Test factory can produce select all queries.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryFactorySelectAll()
{
    QString expectedSelectQuery = "SELECT * FROM test_table";
    QString selectQuery = QueryManager::getSelectAllQuery( "test_table" );

    QVERIFY( selectQuery == expectedSelectQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryFactoryUpdate()
///
/// \brief  Test factory can produce update queries.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryFactoryUpdate()
{
    QStringList columns = { "test_column_1", "test_column_2", "test_column_3" };
    QString expectedUpdateQuery = "UPDATE test_table "
                                  "SET `test_column_1` = :test_column_1, `test_column_2` = :test_column_2, `test_column_3` = :test_column_3";
    QString updateQuery = QueryManager::getUpdateQuery( "test_table", columns );

    QVERIFY( updateQuery == expectedUpdateQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryAppendWhere()
///
/// \brief  Test where clause can be added to a query.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryAppendWhere()
{
    QString selectQuery = QueryManager::getSelectAllQuery( "test_table" );
    QString expectedSelectWhereQuery = "SELECT * FROM test_table WHERE `test_column` = :test_column";
    QString selectWhereQuery = QueryManager::appendWhereClause( selectQuery, "test_column" );

    QVERIFY( selectWhereQuery == expectedSelectWhereQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testQueryAppendAnd()
///
/// \brief  Test and clause can be added to a query.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testQueryAppendAnd()
{
    QString selectQuery = QueryManager::getSelectAllQuery( "test_table" );
    QString expectedSelectWhereQuery = "SELECT * FROM test_table WHERE `test_column` = :test_column AND `test_column_2` = :test_column_2";
    QString selectWhereQuery = QueryManager::appendWhereClause( selectQuery, "test_column" );
    QString selectWhereAndQuery = QueryManager::appendAndClause( selectWhereQuery, "test_column_2" );

    QVERIFY( selectWhereAndQuery == expectedSelectWhereQuery );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::testBindingsFactory()
///
/// \brief  Test factory can produce bindings.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::testBindingsFactory()
{
    QString expectedBinding = ":test_binding";
    QString binding = QueryManager::getBinding( "test_binding" );

    QVERIFY( binding == expectedBinding );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::cleanup()
{
    delete m_queryManager;
    m_queryManager = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     QueryManagerTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void QueryManagerTest::cleanupTestCase()
{

}
