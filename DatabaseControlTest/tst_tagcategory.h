#ifndef TAGCATEGORYTEST_H
#define TAGCATEGORYTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

// Forward declarations.
class TagCategory;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the TagCategory class.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT TagCategoryTest : public UnitTest
{
    Q_OBJECT

public:
    explicit TagCategoryTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Data.
    void testData();
    void testConstants();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    TagCategory* m_tagCategory;     ///< Class under test.
};

#endif // TAGCATEGORYTEST_H
