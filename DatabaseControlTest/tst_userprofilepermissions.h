#ifndef TST_USERPROFILEPERMISSIONS_H
#define TST_USERPROFILEPERMISSIONS_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

// Statics.
class UserProfilePermissions;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the UserProfilePermissions class.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT UserProfilePermissionsTest : public UnitTest
{
    Q_OBJECT

public:
    explicit UserProfilePermissionsTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Data.
    void testConstants();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    UserProfilePermissions* m_userProfilePermissions;       ///< Class under test.
};

#endif // TST_USERPROFILEPERMISSIONS_H
