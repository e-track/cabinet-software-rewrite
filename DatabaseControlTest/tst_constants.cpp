// External.
#include <cstring>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/cabinet.h>
#include <DatabaseControl/constants.h>
#include <DatabaseControl/system.h>
#include <DatabaseControl/tag.h>
#include <DatabaseControl/user.h>
#include <DatabaseControl/usergroup.h>

// Project.
#include "tst_constants.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::ConstantsTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
ConstantsTest::ConstantsTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::init()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::testGeneralConstants()
///
/// \brief  Tests general db and message constants.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::testGeneralConstants()
{
    // General.
    QVERIFY( strcmp( DatabaseControlConstants::_id,   "id" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_all,  "*" ) == 0 );

    // Systems.
    QVERIFY( strcmp( DatabaseControlConstants::_systemsTable,            "tbl_systems" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_system,                  "system" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_systemName,              "system_name" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_welcomeText,             "welcome_text" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_systemTimezone,          "system_timezone" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_cardAuthentication,      "card_authentication" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_biometricAuthentication, "biometric_authentication" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_registeredDate,          "registered_date" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_lastCom,                 "last_com" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_compoundModule,          "compound_module" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_valetModule,             "valet_module" ) == 0 );

    // Cabinets.
    QVERIFY( strcmp( DatabaseControlConstants::_cabinetsTable, "tbl_cabinets" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_cabinetName,   "cabinet_name" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_slotCapacity,  "slot_capacity" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_startSlot,     "start_slot" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_endSlot,       "end_slot" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_slotWidth,     "slot_width" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_noOfLocks,     "no_of_locks" ) == 0 );

    // Users.
    QVERIFY( strcmp( DatabaseControlConstants::_usersTable,    "tbl_users" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_userId,        "user_id" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_username,      "username" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_password,      "password" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_firstName,     "first_name" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_lastName,      "last_name" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_isVisitor,     "is_visitor" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_profileImage,  "profile_image" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_phoneNumber,   "phone_number" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_isActive,      "is_active" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_activeFrom,    "active_from" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_keyLimit,      "key_limit" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_isTemp,        "is_temp" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_isFirstLogin,  "is_first_login" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_userProfile,   "user_profile" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_roleId,        "role_id" ) == 0 );

    // User profiles.
    QVERIFY( strcmp( DatabaseControlConstants::_userProfilesTable,           "tbl_user_profiles" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_userProfilePermissionsTable, "tbl_user_profile_permissions" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_profileName,                 "profile_name" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_profileDescription,          "profile_description" ) == 0 );

    // User profile permissions.
    QVERIFY( strcmp( DatabaseControlConstants::_permissionName, "permission_name" ) == 0 );

    // Access.
    QVERIFY( strcmp( DatabaseControlConstants::_isAccessGranted, "is_access_granted" ) == 0 );

    // Events.
    QVERIFY( strcmp( DatabaseControlConstants::_eventsLogTable, "tbl_events_log" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_eventAction,    "event_action" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_eventMessage,   "event_message" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_parentSystem,   "parent_system" ) == 0 );
    QVERIFY( strcmp( DatabaseControlConstants::_eventDate,      "event_date" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::testTagConstants()
///
/// \brief  Tests tag db and message constants.
///         TODO: Add TagTest and add this test to it.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::testTagConstants()
{
    // Tag.
    QVERIFY( strcmp( Tag::_keysTable,        "tbl_keys" ) == 0 );
    QVERIFY( strcmp( Tag::_keyId,            "key_id" ) == 0 );
    QVERIFY( strcmp( Tag::_id,               "id" ) == 0 );
    QVERIFY( strcmp( Tag::_cabinetId,        "cabinet" ) == 0 );
    QVERIFY( strcmp( Tag::_userId,           "user_id" ) == 0 );
    QVERIFY( strcmp( Tag::_category,         "category" ) == 0 );
    QVERIFY( strcmp( Tag::_keyName,          "key_name" ) == 0 );
    QVERIFY( strcmp( Tag::_keyDescription,   "key_description" ) == 0 );
    QVERIFY( strcmp( Tag::_keySerial,        "key_serial" ) == 0 );
    QVERIFY( strcmp( Tag::_slot,             "slot" ) == 0 );
    QVERIFY( strcmp( Tag::_currentSlot,      "current_slot" ) == 0 );
    QVERIFY( strcmp( Tag::_fob,              "fob" ) == 0 );
    QVERIFY( strcmp( Tag::_isStatic,         "is_static" ) == 0 );
    QVERIFY( strcmp( Tag::_dateAdded,        "date_added" ) == 0 );
    QVERIFY( strcmp( Tag::_startTime,        "start_time" ) == 0 );
    QVERIFY( strcmp( Tag::_endTime,          "end_time" ) == 0 );
    QVERIFY( strcmp( Tag::_isOut,            "is_out" ) == 0 );
    QVERIFY( strcmp( Tag::_isWrongSlot,      "is_wrong_slot" ) == 0 );
    QVERIFY( strcmp( Tag::_additionalFields, "additional_fields" ) == 0 );

    // User tag access.
    QVERIFY( strcmp( UserTagAccess::_userKeyAccessTable, "tbl_user_to_key_access" ) == 0 );
    QVERIFY( strcmp( UserTagAccess::_userId,             "user_id" ) == 0 );
    QVERIFY( strcmp( UserTagAccess::_keyId,              "key_id" ) == 0 );
    QVERIFY( strcmp( UserTagAccess::_isAccessGranted,    "is_access_granted" ) == 0 );

    // Group tag access.
    QVERIFY( strcmp( GroupTagAccess::_userGroupKeyAccessTable, "tbl_group_to_key_access" ) == 0 );
    QVERIFY( strcmp( GroupTagAccess::_userGroupId,             "user_group" ) == 0 );
    QVERIFY( strcmp( GroupTagAccess::_keyId,                   "key_id" ) == 0 );
    QVERIFY( strcmp( GroupTagAccess::_isAccessGranted,         "is_access_granted" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::testUserConstants()
///
/// \brief  Tests User and UserEntry db and message constants.
///         TODO: Add UserEntryTest and add this test to it.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::testUserConstants()
{
    QVERIFY( strcmp( UserEntry::_userEntryIdsTable, "tbl_user_entry_ids" ) == 0 );
    QVERIFY( strcmp( UserEntry::_userId,            "user_id" ) == 0 );
    QVERIFY( strcmp( UserEntry::_entryType,         "entry_type" ) == 0 );
    QVERIFY( strcmp( UserEntry::_entryValue,        "entry_value" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::testTagConstants()
///
/// \brief  Tests UserGroup db and message constants.
///         TODO: Add UserGroupTest and add this test to it.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::testUserGroupConstants()
{
    QVERIFY( strcmp( UserGroup::_userGroupTable,   "tbl_user_group" ) == 0 );
    QVERIFY( strcmp( UserGroup::_userToGroupTable, "tbl_user_to_groups" ) == 0 );
    QVERIFY( strcmp( UserGroup::_userId,           "user_id" ) == 0 );
    QVERIFY( strcmp( UserGroup::_userGroup,        "user_group" ) == 0 );
    QVERIFY( strcmp( UserGroup::_groupName,        "group_name" ) == 0 );
    QVERIFY( strcmp( UserGroup::_keyLimit,         "key_limit" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::testSystemConstants()
///
/// \brief  Tests system db and message constants.
///         TODO: Add SystemTest and add this test to it.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::testSystemConstants()
{
    // GroupSystemAccess.
    QVERIFY( strcmp( GroupSystemAccess::_groupSystemAccessTable, "tbl_group_system_access" ) == 0 );
    QVERIFY( strcmp( GroupSystemAccess::_userGroup,              "user_group" ) == 0 );
    QVERIFY( strcmp( GroupSystemAccess::_systemId,               "system_id" ) == 0 );
    QVERIFY( strcmp( GroupSystemAccess::_isAccessGranted,        "is_access_granted" ) == 0 );

    // UserSystemAccess.
    QVERIFY( strcmp( UserSystemAccess::_userSystemAccessTable, "tbl_user_system_access" ) == 0 );
    QVERIFY( strcmp( UserSystemAccess::_userId,                "user_id" ) == 0 );
    QVERIFY( strcmp( UserSystemAccess::_systemId,              "system_id" ) == 0 );
    QVERIFY( strcmp( UserSystemAccess::_isAccessGranted,       "is_access_granted" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::tetsCabinetConstants()
///
/// \brief  Tests cabinet db and message constants.
///         TODO: Add CabinetTest and add this test to it.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::tetsCabinetConstants()
{
    // GroupCabinetAccess constants.
    QVERIFY( strcmp( GroupCabinetAccess::_groupCabinetAccessTable, "tbl_group_cabinet_access" ) == 0 );
    QVERIFY( strcmp( GroupCabinetAccess::_userGroup,               "user_group" ) == 0 );
    QVERIFY( strcmp( GroupCabinetAccess::_cabinet,                 "cabinet" ) == 0 );
    QVERIFY( strcmp( GroupCabinetAccess::_isAccessGranted,          "is_access_granted" ) == 0 );

    // UserCabinetAccess constants.
    QVERIFY( strcmp( UserCabinetAccess::_userCabinetAccessTable, "tbl_user_cabinet_access" ) == 0 );
    QVERIFY( strcmp( UserCabinetAccess::_userId,                 "user_id" ) == 0 );
    QVERIFY( strcmp( UserCabinetAccess::_cabinet,                "cabinet" ) == 0 );
    QVERIFY( strcmp( UserCabinetAccess::_isAccessGranted,         "is_access_granted" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     ConstantsTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void ConstantsTest::cleanupTestCase()
{

}
