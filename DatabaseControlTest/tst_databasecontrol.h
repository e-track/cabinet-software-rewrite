#ifndef TST_DBCONTROL_H
#define TST_DBCONTROL_H

// Library.
#include <DatabaseControl/user.h>
#include <UnitTestSuite/unittest.h>

// Project.
#include "databasecontroltest_global.h"

class DatabaseControl;
class MockMessageManager;
class MockQueryManager;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the DatabaseControl class.
////////////////////////////////////////////////////////////////////////////////
class DATABASECONTROLTEST_EXPORT DatabaseControlTest : public UnitTest
{
    Q_OBJECT

public:
    explicit DatabaseControlTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();

    // User.
    void testLoggedInUser();
    void testLoggedInUserProperties();
    void testAuthenticateUser();

    // Login.
    void testLoginPin();
    void testLoginTempPin();
    void testLogout();

    // QML.
    void testInvokables();

    // PIN.
    void testSavePin();
    void testUpdatePinFromValidPayload();
    void testUpdatePinFromInvalidPayload();

    // Save.
    void testSaveAll();

    // General SQL query.
    void testSqlQuery();

    // Cabinets.
    void testCabinets();
    void testCabinetsProperties();
    void testCabinetPermissions();
    void testRegisteredCabinet();

    // Systems.
    void testSystemsFromValidPayload();
    void testSystemsFromInvalidPayload();
    void testSystem();
    void testSystemPermissions();

    // User profiles.
    void testUserProfiles();
    void testUserProfilePermissions();
    void testNewUserFromValidPayload();
    void testNewUserFromInvalidPayload();
    void testUpdateUserFromValidPayload();
    void testUpdateUserFromInvalidPayload();
    void testRemoveUserFromValidPayload();
    void testRemoveUserFromInvalidPayload();

    // User groups.
    void testAddUserGroupFromValidPayload();
    void testAddUserGroupFromInvalidPayload();
    void testAddUserToUserGroupFromValidPayload();
    void testAddUserToUserGroupFromInvalidPayload();
    void testRemoveUserFromUserGroupFromValidPayload();
    void testRemoveUserFromUserGroupFromInvalidPayload();
    void testUpdateUserGroupFromValidPayload();
    void testUpdateUserGroupFromInvalidPayload();
    void testRemoveUserGroupFromValidPayload();
    void testRemoveUserGroupFromInvalidPayload();

    // Tags / keys.
    void testNewKeyFromValidPayload();
    void testNewKeyFromInvalidPayload();
    void testRemoveKeyFromValidPayload();
    void testRemoveKeyFromInvalidPayload();
    void testUpdateKeyFromValidPayload();
    void testUpdateKeyFromInvalidPayload();

    void testNewKeyUserPermissionFromValidPayload();
    void testNewKeyUserPermissionFromInvalidPayload();
    void testRemoveKeyUserPermissionFromValidPayload();
    void testRemoveKeyUserPermissionFromInvalidPayload();
    void testUpdateKeyUserPermissionFromValidPayload();
    void testUpdateKeyUserPermissionFromInvalidPayload();
    void testNewKeyGroupPermissionFromValidPayload();
    void testNewKeyGroupPermissionFromInvalidPayload();
    void testRemoveKeyGroupPermissionFromValidPayload();
    void testRemoveKeyGroupPermissionFromInvalidPayload();
    void testUpdateKeyGroupPermissionFromValidPayload();
    void testUpdateKeyGroupPermissionFromInvalidPayload();

    void testKeyPermissions();
    void testKeyDisplayGroupSize();
    void testKeyDisplayGroupSizeProperties();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    DatabaseControl* m_databaseControl;         ///< Class under test.
    MockQueryManager* m_mockQueryManager;       ///< Mock query manager.
    MockMessageManager* m_mockMessageManager;   ///< Mock message manager.
};

#endif // TST_DBCONTROL_H
