// External.
#include <string.h>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/tagcategory.h>

// Project.
#include "tst_tagcategory.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::TagCategoryTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
TagCategoryTest::TagCategoryTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::init()
{
    // Create class under test.
    m_tagCategory = new TagCategory();
    QVERIFY( m_tagCategory );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::testConstants()
///
/// \brief  Tests tag category data.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::testData()
{
    QVERIFY( m_tagCategory->m_tagCategoryId == -1 );
    QVERIFY( m_tagCategory->m_tagCategoryType == "" );
    QVERIFY( m_tagCategory->m_tagCategoryIcon == "" );
    QVERIFY( m_tagCategory->m_tagCategoryName == "" );
    QVERIFY( m_tagCategory->m_tagCategoryShortName == "" );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::testConstants()
///
/// \brief  Tests tag category db and message constants.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::testConstants()
{
    // Tag category database tables.
    QVERIFY( strcmp( TagCategory::_tagCategoryTable,              "tbl_key_categories" ) == 0 );
    QVERIFY( strcmp( TagCategory::_groupToTagCategoryAccessTable, "tbl_group_to_key_category_access" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagToCategoriesTable,          "tbl_key_to_categories" ) == 0 );
    QVERIFY( strcmp( TagCategory::_userToTagCategoryAccessTable,  "tbl_user_to_key_category_access" ) == 0 );

    // Tag category database fields.
    QVERIFY( strcmp( TagCategory::_tagCategory,          "key_category" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagCategoryId,        "category_id" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagCategoryType,      "category_type" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagCategoryIcon,      "category_icon" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagCategoryName,      "category_name" ) == 0 );
    QVERIFY( strcmp( TagCategory::_tagCategoryShortName, "category_short_name" ) == 0 );

    // Tag category commands.
    QVERIFY( strcmp( TagCategory::_newKeyCategory,    "new_key_category" ) == 0 );
    QVERIFY( strcmp( TagCategory::_updateKeyCategory, "update_key_category" ) == 0 );
    QVERIFY( strcmp( TagCategory::_removeKeyCategory, "remove_key_category" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     TagCategoryTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void TagCategoryTest::cleanupTestCase()
{

}
