#include <iostream>
#include "uart_protocol.h"

using namespace std;
int main() {

    cout <<"Get Pegboard Status Request: " << endl;

    Pegboard_Status pegboard_status = getPegboardStatus(1);

    std::cout<< "Pegboard Address: " << (int)pegboard_status.addr << std::endl;
    std::cout<< "Bootloader:       " << (int)pegboard_status.bootloader << std::endl;
    std::cout<< "LED Error:        " << (int)pegboard_status.led_error << std::endl;
    std::cout<< "CRC Error:        " << (int)pegboard_status.crc_error << std::endl;
    std::cout<< "EEPROM Error:     " << (int)pegboard_status.e2_error << std::endl;

    for(uint8_t i = 0; i < 10; i++){
        std::cout<< "Peg[" << (int)i <<"] = " << (int)pegboard_status.pegsStatuses[i].home<<std::endl;
    }

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;


    cout <<"Read Peg Numbers Request: " << endl;

    Peg *pegs = readPegNumbers(1);
    Peg pegz[10];

    for(uint8_t i = 0; i < 10; i++){
        pegz[i] = *(pegs + i);
    }

    for(uint8_t i = 0; i < 10; i++){
        std::cout<< "Peg[" << (int)i <<"] = ";
        for(uint8_t j = 0; j < 5; j++){
            std::cout << "0x" << std::hex <<(int)pegz[i].id[j] << " ";
        }
        std::cout << "Signal Strength: " << "0x" << std::hex << (int)pegz[i].signal_strength << std::endl;
    }

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    uint8_t colors[10] = {LED_COLOR_OFF,
                          LED_COLOR_BLUE_FLASH,
                          LED_COLOR_BLUE_STATIC,
                          LED_COLOR_CYAN_FLASH,
                          LED_COLOR_CYAN_STATIC,
                          LED_COLOR_GREEN_FLASH,
                          LED_COLOR_GREEN_STATIC,
                          LED_COLOR_MAGENTA_FLASH,
                          LED_COLOR_MAGENTA_STATIC,
                          LED_COLOR_RED_FLASH};


    cout <<"LED Drive command 1 Request: " << endl;

    bool ack = led_drive_1(1, colors);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        cout << "-";
    }
    cout<< std::endl;

    for(uint8_t i = 1; i < 11; i++){

        cout <<"Single LED Drive command 1 Request for slot: " << dec << (int) i <<endl;

        bool ack = single_led_drive_1(1, i, colors[i-1]);

        if(ack)
            std::cout << "ACK" << std::endl;
        else
            std::cout << "NACK" << std::endl;

        for(uint8_t i = 0; i < 25; i++){
            cout << "-";
        }
        cout<< std::endl;

    }

    struct Rgb rgb_colors[10];
    for(uint8_t i = 0; i < 10; i++){
        rgb_colors[i].blue = i;
        rgb_colors[i].green = i;
        rgb_colors[i].red = i;
    }

    cout <<"LED Drive command 2 Request: " << endl;

    ack = led_drive_2(1, rgb_colors);

    if(ack)
        cout << "ACK" << endl;
    else
        cout << "NACK" << endl;

    for(uint8_t i = 0; i < 25; i++){
        cout << "-";
    }
    cout << endl;

    ack = led_setting(1, true, true, 4);

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    cout<< endl;

    bool solenoids[10] = {false,
                          false,
                          false,
                          false,
                          false,
                          false,
                          false,
                          false,
                          false,
                          false};


    cout <<"Multiple Solenoid Control Request: " << endl;

    ack = multiple_solenoid_control(1, solenoids);

    if(ack)
        cout << "ACK" << endl;
    else
        cout << "NACK" << endl;

    for(uint8_t i = 0; i < 25; i++){
        cout << "-";
    }
    cout << endl;

    for(uint8_t i = 1; i < 11; i++){

        cout <<"Single Solenoid Control Request for Slot: " << dec << (int)i << endl;

        ack = single_solenoid_control(1, i);

        if(ack)
            cout << "ACK" << endl;
        else
            cout << "NACK" << endl;

        for(uint8_t i = 0; i < 25; i++){
            std::cout << "-";
        }
        std::cout<< std::endl;
    }

    cout <<"Pegboard Read Setting Request: " << endl;

    struct Pegboard_Settings result = read_pegboard_settings(1);

    std::cout << "RFID Threshold = " << std::hex << (int)result.rfid_threshold << std::endl;
    std::cout << "Solenoid Timeout = " << std::hex << (int)result.solenoid_timeout << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    cout <<"Pegboard Write Setting Request: " << endl;

    ack = write_pegboard_setting(1, RFID_THRESHOLD, 0x31);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;


    cout <<"Pegboard Bootloader Request: " << endl;

    pegboard_enter_bootloader(1);

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    cout <<"Pegboard Hardware Reset Request: " << endl;

    pegboard_hardware_reset(1);

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout << std::endl;


    cout <<"Pegboard Application Version Request: " << endl;

    std::string version = get_pegboard_version(1, APPLICATION_VERSION_TYPE);

    std::cout << "Application Version: " << version << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    cout <<"Pegboard Bootloader Version Request: " << endl;

    version = get_pegboard_version(1, BOOTLOADER_VERSION_TYPE);

    std::cout << "Bootloader Version: " << version << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;


    cout <<"Interface Status Request: " << endl;

    Interface_Status status = getInterfaceStatus();

    cout << "Bootloader: " << (bool)status.bootloader << endl;
    cout << "Override: " << (bool)status.override << endl;
    cout << "Opto-Switch: " << (bool)status.opto_switch << endl;
    cout << "Lock Power: " << (bool)status.lock_power << endl;
    cout << "Door 2 Lock: " << (bool)status.lock2_door << endl;
    cout << "Door 2 Latch: " << (bool)status.lock2_latch << endl;
    cout << "Door 1 Lock: " << (bool)status.lock1_door << endl;
    cout << "Door 1 Latch: " << (bool)status.lock1_latch << endl;
    cout << "Alarm Timer: " << (bool)status.alarm_timer << endl;
    cout << "Sounder Timer: " << (bool)status.sounder_timer << endl;
    cout << "Change Occurred: " << (bool)status.change_occured << endl;
    cout << "E2 Error: " << (bool)status.e2_error << endl;
    cout << "Voltage = " << (float)status.voltage << endl;


    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    cout << endl;




    int n = 10;

    cout <<"Global Pegboard Status Request for Number of Pegboard: " << dec <<(int) n <<endl;

    Pegboard_Status * pb_status;

    pb_status = (Pegboard_Status *) calloc(n, sizeof(Pegboard_Status));

    readGlobalPegStatus(pb_status, n);


    for(uint8_t i = 0; i < n; i++){

        cout<< "Pegboard Address: " << dec << (int)pb_status[i].addr << endl;
        cout<< "Bootloader:       " << hex << (bool)pb_status[i].bootloader << endl;
        cout<< "LED Error:        " << hex << (bool)pb_status[i].led_error << endl;
        cout<< "CRC Error:        " << hex << (bool)pb_status[i].crc_error << endl;
        cout<< "EEPROM Error:     " << hex << (bool)pb_status[i].e2_error << endl;

        for(uint8_t j = 0; j < 10; j++){
            cout<< "Peg_home[" << (int)j <<"] = "  << dec << (bool)pb_status[i].pegsStatuses[j].home<<endl;
        }

        for(uint8_t j = 0; j < 10; j++){
            cout<< "Peg_found[" << (int)j <<"] = " << dec <<  (bool)pb_status[i].pegsStatuses[j].found<<std::endl;
        }

        for(uint8_t k = 0; k < 25; k++){
            cout << "-";
        }
        cout<< endl;

    }

    free(pb_status);

    bool slots[10] = {true,
                      true,
                      true,
                      true,
                      true,
                      true,
                      true,
                      true,
                      true,
                      true};


    cout <<"Multiple Authorisation Request: " << endl;

    ack = multipleAuthorisation(1, slots);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;



    for(uint8_t i = 1; i < 11; i++){

        cout <<"Single Authorisation Request: " << endl;
        cout <<"Authorisation Slot: " << dec << (int) i << endl;

        ack = authorisation(1, i);

        if(ack)
            std::cout << "ACK" << std::endl;
        else
            std::cout << "NACK" << std::endl;

        for(uint8_t k = 0; k < 25; k++){
            cout << "-";
        }
        cout<< endl;

    }

    cout <<"Interface Illumination Command: " << endl;
    ack = illumination(true, false, 255, 0);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Illumination Command: " << endl;
    ack = illumination(false, true, 0, 255);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Illumination Command: " << endl;
    ack = illumination(true, true, 255, 255);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;


    cout <<"Interface Lock Control Command: Lock 1" << endl;

    ack = lock_control(true, false);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Lock Control Command: Lock 2" << endl;

    ack = lock_control(false, true);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;


    cout <<"Interface Lock Control Command: Lock 1 and Lock 2" << endl;

    ack = lock_control(true, true);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Write Setting Command: No of Pegboards" << endl;

    ack = write_interface_setting(numberOfPegboards, 20);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Write Setting Command: Lock Timeout" << endl;

    ack = write_interface_setting(lock_timeout, 240);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Write Setting Command: Alarm Time" << endl;

    ack = write_interface_setting(alarm_time, 240);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Write Setting Command: Alarm Time" << endl;

    ack = write_interface_setting(sounder_time, 240);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Write Setting Command: Emergency and Alert Mode" << endl;

    ack = write_interface_setting(emergency_alert, PULL_FIRST_10_AND_NO_ALERT_ON_CHANGE);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Read Settings Command: " << endl;

    Interface_Settings interface_settings = read_interface_settings();

    cout << "Number of Pegboards: " << dec << (int) interface_settings.number_of_pegboards << endl;
    cout << "Lock Timeout: " << dec << (int) interface_settings.lock_timeout << endl;
    cout << "Alarm Time: " << dec << (int) interface_settings.alarm_time << endl;
    cout << "Sounder Time: " << dec << (int) interface_settings.sounder_time << endl;
    cout << "Emergency Alert: " << dec << (int) interface_settings.emergency_release << endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Sounds Command: " << endl;

    ack = sounds(true, true, true, true, 0xAA, 0x55);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Bootloader Command: " << endl;

    interface_enter_bootloader();

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Hardware Reset Command: " << endl;

    interface_reset();

    for(uint8_t k = 0; k < 25; k++){
        cout << "-";
    }
    cout<< endl;

    cout <<"Interface Application Version Request: " << endl;

    version = get_interface_version(APPLICATION_VERSION_TYPE);

    std::cout << "Application Version: " << version << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    cout <<"Interface Bootloader Version Request: " << endl;

    version = get_interface_version(BOOTLOADER_VERSION_TYPE);

    std::cout << "Bootloader Version: " << version << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    cout <<"Device Write Flash: " << endl;

    uint8_t encrypted_hex[130];

    for(uint8_t i = 0; i < 130; i++){
        encrypted_hex[i] = i;
    }

    ack = write_flash_application_code(INTERFACE_ADDR, encrypted_hex);

    if(ack)
        std::cout << "ACK" << std::endl;
    else
        std::cout << "NACK" << std::endl;

    for(uint8_t i = 0; i < 25; i++){
        std::cout << "-";
    }
    std::cout<< std::endl;

    Flash_Info_Block info = readFlashInfoBlock(1);

    cout << "Application Version: " << info.application_version << endl;
    cout << "Application Code Size: " << dec <<(int) info.application_size << endl;

    if(info.application_crc_high < 16)
        cout << "Application Code High CRC: 0x0" << hex <<  (int)info.application_crc_high << endl;
    else
        cout << "Application Code High CRC: 0x" << hex <<  (int)info.application_crc_high << endl;

    if(info.application_crc_high < 16)
        cout << "Application Code Low CRC: 0x0" << hex <<  (int)info.application_crc_low << endl;
    else
        cout << "Application Code Low CRC: 0x" << hex <<  (int)info.application_crc_low << endl;

    return 0;
}
