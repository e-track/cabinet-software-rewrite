//
// Created by Moe Samak on 09/08/2021.
//

#include "uart_protocol.h"
#include "emulated_uart.h"

uint8_t * tx_buf;
uint8_t * rx_buf;

/**
 * This function perform CRC16-FALSE calculation for any data length.
 *
 * @param pData
 * @param length
 * @return uint16_t
 */

uint16_t crc16(uint8_t* pData, int length)
{
    uint8_t i;
    uint16_t wCrc = 0xffff;
    while (length--) {
        wCrc ^= *(uint8_t *)pData++ << 8;
        for (i=0; i < 8; i++)
            wCrc = wCrc & 0x8000 ? (wCrc << 1) ^ 0x1021 : wCrc << 1;
    }
    return wCrc & 0xffff;
}

/**
 * This function performs a CRC check for received data bytes.
 *
 * @param data
 * @param data_len
 * @return bool
 */

bool crcCheck(uint8_t * data, uint8_t data_len){

    uint16_t calculated_crc = crc16(data, data_len);
    uint8_t high_crc = (calculated_crc >> 4) & 0x0F;
    uint8_t low_crc = calculated_crc & 0x0F;

    return !(high_crc != data[data_len - 2] || low_crc != data[data_len - 1]);
}

/**
 *
 * This function performs a Pegboard Status Request based on input Pegboard Address.
 * Pegboard Address from 1 to 60.
 *
 * @param address
 * @return struct Pegboard_Status
 */

struct Pegboard_Status getPegboardStatus(uint8_t address){

    // Allocate memory for tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_STATUS_TX_LEN, sizeof(uint8_t));

    // Start command.
    tx_buf[0] = START_CMD;

    // Pegboard status command and address.
    tx_buf[1] = PEGBOARD_STATUS_ADDR | address;

    // Transmit the tx buffer.
    uart_send(tx_buf, PEGBOARD_STATUS_TX_LEN);

    // Free the tx buffer.
    free(tx_buf);

    rx_buf = (uint8_t *) calloc(PEGBOARD_STATUS_RX_LEN_CRC, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_PegboardStatus(rx_buf);

    // Initialises a Pegboard_Status Struct.
    Pegboard_Status status;

    // Assigns the address.
    status.addr = (rx_buf[0] & MASK_ADDR);

    // Assigns the bootloader bit.
    status.bootloader = ((rx_buf[1] >> BOOTLOADER_BIT) & MASK_BIT);

    // Assigns the LED error bit.
    status.led_error = ((rx_buf[1] >> LED_ERROR_BIT) & MASK_BIT);

    // Assigns the crc error bit.
    status.crc_error = ((rx_buf[1] >> CRC_ERROR_BIT) & MASK_BIT);

    // Assigns the EEPROM error bit.
    status.e2_error = ((rx_buf[1] >> E2_ERROR_BIT) & MASK_BIT);

    // Extracts the Peg 10 bits into C Data type.
    status.pegsStatuses[9].home = ((rx_buf[1] >> 3) & MASK_BIT);
    status.pegsStatuses[9].found = ((rx_buf[1] >> 1) & MASK_BIT);

    // Extracts the Peg 9 bits into C Data type.
    status.pegsStatuses[8].home = ((rx_buf[1] >> 2) & MASK_BIT);
    status.pegsStatuses[8].found = (rx_buf[1] & MASK_BIT);

    // Extracts the Pegs 1 - 8 bits into C Data type.
    for(int i = 7; i >= 0; i--){
        status.pegsStatuses[i].found = (rx_buf[2] >> i & 0x01);
        status.pegsStatuses[i].home = (rx_buf[3] >> i & 0x01);
    }

    // Free the rx buffer.
    free(rx_buf);

    // Returns the Pegboard Status.
    return status;
}

/**
 *
 * This function reads the Peg Tag ID's and signal strengths based on Pegboard Address.
 * If a Peg is not sensed in the slot, 5 bytes of Zero's and Zero signal strength is received.
 * Pegboard Address from 1 to 60.
 *
 * @param address
 * @return Peg *
 */

Peg * readPegNumbers(uint8_t address){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_STATUS_TX_LEN, sizeof(uint8_t));

    // Assigns the Start Command.
    tx_buf[0] = START_CMD;

    // Assigns the address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the command.
    tx_buf[3] = PEGBOARD_NUMBERS_CMD;

    // Transmit the tx buffer.
    uart_send(tx_buf, PEGBOARD_NUMBERS_TX_LEN);

    // Free's the allocated tx memory.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(PEGBOARD_NUMBERS_RX_LEN_CRC, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_PegNumbers(rx_buf);

    // Initialises a Peg struct array.
    Peg pegs[10];

    // Initialises the pointer to be returned from the function.
    Peg *ret = pegs;

    // Local variable used to increment the pegs.
    uint8_t counter = 0;

    // Assigns the 5 RFID Tag ID's and Signal Strength into an array of Peg structs.
    for(uint8_t i = 1; i < 59; i += 6){
        pegs[counter].id[0] = rx_buf[i];
        pegs[counter].id[1] = rx_buf[i + 1];
        pegs[counter].id[2] = rx_buf[i + 2];
        pegs[counter].id[3] = rx_buf[i + 3];
        pegs[counter].id[4] = rx_buf[i + 4];
        pegs[counter].signal_strength = rx_buf[i + 5];
        counter++;
    }

    // Free the rx buffer.
    free(rx_buf);

    // Returns a pointer to the Peg struct.
    return ret;
}

/**
 *  This function provides LED control, static and flash based on Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 *
 * Check @LED_COLOR_* for further information about available static and flashing colors.
 *
 * @param address
 * @param pegColors
 * @return bool
 */
bool led_drive_1(uint8_t address, const uint8_t pegColors[10]){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_LED_DRIVE_1_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard address from the input parameter.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the LED drive 1 command.
    tx_buf[2] = PEGBOARD_LED_DRIVE_1_CMD;

    // Used to count through the colors received from the parameter.
    uint8_t counter = 0;

    // Assigns the colors into the tx buffer.
    for(uint8_t i = 3; i < 13; i++){
        tx_buf[i] = pegColors[counter];
        counter++;
    }

    // Calculate the CRC for the TX data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_LED_DRIVE_1_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_LED_DRIVE_1_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_LED_DRIVE_1_TX_LEN + 1] = crc & 0xFF;

    // Transmit the tx buffer.
    uart_send(tx_buf, PEGBOARD_LED_DRIVE_1_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);
}

/**
 *  This function provides LED control, static and flash based on Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 * Slot number from (1 - 10).
 *
 * Check @LED_COLOR_* for further information about available static and flashing colors.
 *
 * @param address
 * @param slotNumber
 * @param pegColor
 * @return bool
 */
bool single_led_drive_1(uint8_t address, uint8_t slotNumber, uint8_t pegColor){
    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_LED_DRIVE_1_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard address from the input parameter.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the LED drive 1 command.
    tx_buf[2] = PEGBOARD_LED_DRIVE_1_CMD;

    // Calculates the exact position of the slot in the tx buffer.
    int slotPosition = slotNumber + LED_DRIVE_1_FIRST_SLOT_POSITION;

    // Assigns the color to the calculated position.
    tx_buf[slotPosition] = pegColor;

    // Calculate the CRC for the TX data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_LED_DRIVE_1_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_LED_DRIVE_1_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_LED_DRIVE_1_TX_LEN + 1] = crc & 0xFF;

    // Transmit the tx buffer.
    uart_send(tx_buf, PEGBOARD_LED_DRIVE_1_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);
}

/**
 * This function provides static LED control based on RGB color codes and Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 *
 * @param address
 * @param pegColors
 * @return bool
 */

bool led_drive_2(uint8_t address, struct Rgb *pegColors){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_LED_DRIVE_2_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard address from the input parameter.
    tx_buf[1] = ADDR_MASK | address;
    tx_buf[2] = PEGBOARD_LED_DRIVE_2_CMD;

    uint8_t counter = 0;
    for(uint8_t i = 3; i < 33; i += 3){
        tx_buf[i] = pegColors[counter].blue;
        tx_buf[i+1] = pegColors[counter].green;
        tx_buf[i+2] = pegColors[counter].red;
        counter++;
    }

    // Calculate the CRC for the TX data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_LED_DRIVE_2_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_LED_DRIVE_2_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_LED_DRIVE_2_TX_LEN + 1] = crc & 0xFF;

    // Transmit the tx buffer.
    uart_send(tx_buf, PEGBOARD_LED_DRIVE_2_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);

}

/**
 * This function configures the LED and performs an Address show which displays the binary input
 * assigned using the switches which is used to assign the Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 *
 * @param address
 * @param flash_sync
 * @param show_address
 * @param brightness
 * @return bool
 */

bool led_setting(uint8_t address, bool flash_sync, bool show_address, uint8_t brightness){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_LED_SETTING_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard address from the input parameter.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard LED Settings command.
    tx_buf[2] = PEGBOARD_LED_SETTING_CMD;

    // Shifts and assigns the flash synchronization bit.
    tx_buf[3] = (flash_sync << FLASH_SYNC_BIT);

    // Shifts and assigns the show binary address bit.
    tx_buf[3] |= (show_address << SHOW_ADDR_BIT);

    // Assigns the brightness nibble.
    tx_buf[3] |= (brightness & 0x0F);

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, PEGBOARD_LED_SETTING_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_LED_SETTING_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_LED_SETTING_TX_LEN + 1] = crc & 0xFF;

    // Emulates the tx buffer transmission.
    uart_send(tx_buf, PEGBOARD_LED_SETTING_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);
}

/**
 * This function controls the lock/unlock of solenoids per Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 * The boolean array is the status of each solenoid in the track.
 * Where index 0 is slot 1 and index 9 is slot 10.
 * A true will pull the solenoid and false will release it.
 *
 * @param address
 * @param solenoids
 * @return bool
 */

bool multiple_solenoid_control(uint8_t address, const bool solenoids[10]){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_SOLENOID_CONTROL_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address input as parameter.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Solenoid Control command.
    tx_buf[2] = PEGBOARD_SOLENOID_CMD;

    // Shifts the bit to b1 which is the 10th solenoid bit location.
    tx_buf[3] = (solenoids[9] << 1) & 0x02;

    // Assigns bit 0 of byte 4 the solenoid 8 value.
    tx_buf[3] |= (solenoids[8]) & 0x01;

    // Shifts and assigns the solenoid values into their respective bit location.
    for(uint8_t i = 0; i < 8; i++){
        tx_buf[4] |= (solenoids[i] << i);
    }

    // Calculate the CRC for the TX data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_SOLENOID_CONTROL_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_SOLENOID_CONTROL_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_SOLENOID_CONTROL_TX_LEN + 1] = crc & 0xFF;

    // Transmits the tx buffer.
    uart_send(tx_buf, PEGBOARD_SOLENOID_CONTROL_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);
}


/**
 * This function controls the lock/unlock of solenoids per Pegboard Address.
 * Pegboard Address from 1 to 60 or 0 for Global.
 * The single solenoid is a number from 1 to 10.
 * The input number will be the solenoid that is triggered.
 *
 * @param address
 * @param solenoid
 * @return bool
 */

bool single_solenoid_control(uint8_t address, uint8_t solenoid){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_SOLENOID_CONTROL_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address input as parameter.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Solenoid Control command.
    tx_buf[2] = PEGBOARD_SOLENOID_CMD;

    // Shifts the bit to b1 which is the 10th solenoid bit location.
    tx_buf[3] = (solenoid == 10) ?  0x01 << 1 : tx_buf[3];

    // Assigns bit 0 of byte 4 the solenoid 9 value.
    tx_buf[3] = (solenoid == 9) ?  0x01 : tx_buf[3];

    tx_buf[4] = (solenoid > 0 & solenoid < 9) ? 0x01 << (solenoid - 1) : tx_buf[4];

    // Calculate the CRC for the TX data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_SOLENOID_CONTROL_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_SOLENOID_CONTROL_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_SOLENOID_CONTROL_TX_LEN + 1] = crc & 0xFF;

    // Transmits the tx buffer.
    uart_send(tx_buf, PEGBOARD_SOLENOID_CONTROL_TX_LEN_CRC);

    // Free the memory allocated to the Rx buffer.
    free(tx_buf);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // The received acknowledged pegboard address
    uint8_t ack_address = rx_buf[0];

    // Free the memory allocated to the Rx buffer.
    free(rx_buf);

    // Returns a boolean if address is a match.
    return (ack_address == address);
}

/**
 * This method performs a settings read request.
 * Pegboard Address from 1 to 60.
 * Solenoid timeout = (Value * 0.5 Seconds).
 *
 * @param address
 * @param setting
 * @return uint8_t
 */

struct Pegboard_Settings read_pegboard_settings(uint8_t address){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_READ_SETTING_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard Read Settings command.
    tx_buf[2] = PEGBOARD_READ_SETTINGS_CMD;

    uart_send(tx_buf, PEGBOARD_READ_SETTING_TX_LEN);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(PEGBOARD_READ_SETTING_RX_LEN, sizeof(uint8_t));

    // Receive into the newly allocated rx buffer.
    uart_receive_pb_settings(rx_buf);

    // Initialises a Pegboard_Settings struct.
    Pegboard_Settings pb_settings{};

    // Assigns the RFID Threshold value from the received bytes.
    pb_settings.rfid_threshold = rx_buf[1];

    // Assigns the Solenoid Timeout value from the received bytes.
    pb_settings.solenoid_timeout = rx_buf[2];

    // Returns the Pegboard_Settings struct.
    return pb_settings;
}

/**
 * This function writes a setting to the Pegboard, two types of settings are available:
 * RFID Threshold: Which is the sensitivity of the Antenna.
 * Solenoid Timeout: Is the time until the solenoid is released when solenoid command is performed.
 * The time is calculated in this format (Solenoid Timeout * 0.5 Seconds).
 *
 * Please see header file for RFID_THRESHOLD and SOLENOID_TIMEOUT.
 *
 * @param address
 * @param setting
 * @param setting_value
 * @return bool
 */

bool write_pegboard_setting(uint8_t address, uint8_t setting, uint8_t setting_value){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_WRITE_SETTING_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard Write Settings command.
    tx_buf[2] = PEGBOARD_WRITE_SETTINGS_CMD;

    // Assigns the setting byte location.
    tx_buf[3] = setting;

    // Assigns the setting value.
    tx_buf[4] = setting_value;

    // Calculate the CRC for the tx data.
    uint16_t crc = crc16(tx_buf, PEGBOARD_WRITE_SETTING_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[PEGBOARD_WRITE_SETTING_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[PEGBOARD_WRITE_SETTING_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, PEGBOARD_WRITE_SETTING_TX_LEN_CRC);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive the acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the received address matches the transmitted address.
    if(ack != address)
        return false;

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_WRITE_SETTING_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard Write Settings command.
    tx_buf[2] = PEGBOARD_WRITE_SETTINGS_CMD;

    // Complements the setting byte.
    tx_buf[3] = ~setting;

    // Complements the setting value byte.
    tx_buf[4] = ~setting_value;

    // Calculate the CRC for the TX data.
    crc = crc16(tx_buf, PEGBOARD_WRITE_SETTING_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[5] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[6] = crc & 0xFF;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, PEGBOARD_WRITE_SETTING_TX_LEN_CRC);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive an emulated acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, address);

    // Assign the received address to a local variable.
    ack = rx_buf[0];

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the received address matches the transmitted address.
    if(ack != address)
        return false;

    // Returns that command was successful.
    return true;
}

/**
 * This method sets the Pegboard into bootloader.
 * Pegboard Address from 1 to 60 or 0 for Global.
 *
 * @param address
 */

void pegboard_enter_bootloader(uint8_t address){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_BOOTLOADER_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Pegboard Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard enter into bootloader command.
    tx_buf[2] = PEGBOARD_BOOTLOADER_CMD;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, PEGBOARD_BOOTLOADER_TX_LEN);

    // Free's the allocated tx buffer memory.
    free(tx_buf);
}

/**
 * This method performs a Pegboard Hardware Reset.
 * Pegboard Address from 1 to 60 or 0 for Global.
 *
 * @param address
 */

void pegboard_hardware_reset(uint8_t address){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_RESET_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard hardware reset command.
    tx_buf[2] = PEGBOARD_RESET_CMD;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, PEGBOARD_RESET_TX_LEN);

    // Free's the allocated tx buffer memory.
    free(tx_buf);
}

/**
 * This function obtains the Pegboard version, either bootloader or application.
 *
 * Please see header file for more information on BOOTLOADER_VERSION_TYPE and APPLICATION_VERSION_TYPE.
 *
 * @param address : Pegboard Address.
 * @param version_type : BOOTLOADER_VERSION_TYPE or APPLICATION_VERSION_TYPE.
 * @return string
 */

std::string get_pegboard_version(uint8_t address, uint8_t version_type){

    std::string ret;
    ret = "";

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(PEGBOARD_VERSION_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | address;

    // Assigns the Pegboard version request command.
    tx_buf[2] = PEGBOARD_VERSION_CMD;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, PEGBOARD_VERSION_TX_LEN);

    // Free's the allocated tx buffer memory.
    free(tx_buf);

    // Allocates the rx buffer memory.
    rx_buf = (uint8_t *) calloc(PEGBOARD_VERSION_RX_LEN_CRC, sizeof(uint8_t));

    // Emulates the reception of Pegboard Version return data.
    uart_receive_version(rx_buf, address);

    // Assign the received address to a local variable.
    uint8_t ack = rx_buf[0];

    // Checks if the received address matches the transmitted address.
    if(ack != address){
        free(rx_buf);
        return ret;
    }

    // Checks which version type is input through the function parameter.
    if(version_type == BOOTLOADER_VERSION_TYPE){
        // Returns the Bootloader version.
        ret = std::to_string(rx_buf[1]) + "." + std::to_string(rx_buf[2]);
    }
    else if(version_type == APPLICATION_VERSION_TYPE){
        // Returns the Application version.
        ret = std::to_string(rx_buf[2]) + "." + std::to_string(rx_buf[3]);
    }

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Returns the version in string format.
    return ret;
}

/**
 * This method performs a Interface Status Request, please check @Interface_Status for more information.
 * Note: If power is removed from locks the lock status pins will be inactive.
 *
 * @return struct Interface_Status
 */
struct Interface_Status getInterfaceStatus(){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_STATUS_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = INTERFACE_STATUS_ADDR;

    // Emulates the transmission of tx buffer.
    uart_send(tx_buf, INTERFACE_STATUS_TX_LEN);

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t*) calloc(INTERFACE_STATUS_RX_LEN_CRC, sizeof(uint8_t));

    // Emulates the reception of the interface status request.
    uart_receive_interface_status(rx_buf);

    // Creates a struct to hold the received request.
    Interface_Status status{};

    // Assigns and masks the bootloader bit.
    status.bootloader = (bool)((rx_buf[1] >> 7) & MASK_BIT);

    // Assigns and masks the Manual Override bit.
    status.override = (bool)((rx_buf[1] >> 6) & MASK_BIT);

    // Assigns and masks the Optical Switch bit.
    status.opto_switch = (bool)((rx_buf[1] >> 5) & MASK_BIT);

    // Assigns and masks the Lock Power bit.
    status.lock_power = (bool)((rx_buf[1] >> 4) & MASK_BIT);

    // Assigns and masks the Lock 2 Door bit.
    status.lock2_door = (bool)((rx_buf[1] >> 3) & MASK_BIT);

    // Assigns and masks the Lock 2 Latch bit.
    status.lock2_latch = (bool)((rx_buf[1] >> 2) & MASK_BIT);

    // Assigns and masks the Lock 1 Door bit.
    status.lock1_door = (bool)((rx_buf[1] >> 1) & MASK_BIT);

    // Assigns and masks the Lock 1 Latch bit.
    status.lock1_latch = (bool)rx_buf[1];

    // Assigns and masks the Alarm timer bit.
    status.alarm_timer = (bool)((rx_buf[2] >> 7) & MASK_BIT);

    // Assigns and masks the Sounder Timer bit.
    status.sounder_timer = (bool)((rx_buf[2] >> 6) & MASK_BIT);

    // Assigns and masks the Change Occurred since last status request bit.
    status.change_occured = (bool)((rx_buf[2] >> 5) & MASK_BIT);

    // Assigns and masks the E2ROM error bit.
    status.e2_error = (bool)((rx_buf[2] >> 4) & MASK_BIT);

    // Assigns a 16 bit unsigned integer to shift the high nibble and low byte of the voltage value into.
    // Shifts the 4 Most significant bits into the unsigned integer.
    uint16_t temp_voltage = (uint16_t)(rx_buf[2] << 8);

    // Shifts the 8 Least significant into the unsigned integer.
    temp_voltage |= (rx_buf[3] & 0xff);

    // Voltage Calculation is as follows 12-bit integer * 0.004 mV.
    // Example: 3150 * 0.004 = 12.6V.
    status.voltage = ((float)temp_voltage * 0.004f);

    // Free's the allocated rx buffer memory.
    free(rx_buf);

    // Returns the struct.
    return status;
}
/**
 * This method performs a global Pegboard Status request, the number of pegboards connected to the interface
 * must be input into the parameters. As this function returns a dynamic number of bytes depending on number of
 * Pegboard that are assigned into the settings.
 *
 * @param status_array
 * @param numberOfPegboards
 */
void readGlobalPegStatus(Pegboard_Status * status_array, int numberOfPegboards){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_GLOBAL_PEG_STATUS_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface Read Global Peg Status command.
    tx_buf[2] = INTERFACE_READ_GLOBAL_PEG_STATUS_CMD;

    // Emulates the transmission of tx buffer.
    uart_send(tx_buf, INTERFACE_GLOBAL_PEG_STATUS_TX_LEN);

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Number of bytes to be received depends on the number of pegboard connected to the board.
    uint8_t rx_bytes = INTERFACE_GLOBAL_PEG_STATUS_RX_LEN_CRC + (numberOfPegboards * INTERFACE_GLOBAL_PEG_STATUS_TRACK_BYTES);

    // Allocate memory to the rx buffer.
    rx_buf = (uint8_t *) calloc(rx_bytes, sizeof(uint8_t));

    // Emulates the reception of Global Peg Status Response.
    uart_receive_global_peg_status(rx_buf, numberOfPegboards);

    uint8_t counter = 0;
    for(uint8_t i = 1; i < (numberOfPegboards * INTERFACE_GLOBAL_PEG_STATUS_TRACK_BYTES); i+=3){

        // Assigns the bootloader bit.
        status_array[counter].addr = counter + 1;

        // Assigns the bootloader bit.
        status_array[counter].bootloader = ((rx_buf[i] >> BOOTLOADER_BIT) & MASK_BIT);

        // Assigns the LED error bit.
        status_array[counter].led_error = ((rx_buf[i] >> LED_ERROR_BIT) & MASK_BIT);

        // Assigns the crc error bit.
        status_array[counter].crc_error = ((rx_buf[i] >> CRC_ERROR_BIT) & MASK_BIT);

        // Assigns the EEPROM error bit.
        status_array[counter].e2_error = ((rx_buf[i] >> E2_ERROR_BIT) & MASK_BIT);

        // Extracts the Peg 10 bits.
        status_array[counter].pegsStatuses[9].home = (bool)((rx_buf[i] >> 3) & MASK_BIT);

        status_array[counter].pegsStatuses[9].found = (bool)((rx_buf[i] >> 1) & MASK_BIT);

        // Extracts the Peg 9 bits.
        status_array[counter].pegsStatuses[8].home = (bool)((rx_buf[i] >> 2) & MASK_BIT);
        status_array[counter].pegsStatuses[8].found = (bool)(rx_buf[i] & MASK_BIT);

        // Extracts the Pegs 1 - 8 bits.
        for(int j = 7; j > -1; j--){
            status_array[counter].pegsStatuses[j].found = (bool)((rx_buf[i+1] >> j) & MASK_BIT);
            status_array[counter].pegsStatuses[j].home  = (bool)((rx_buf[i+2] >> j) & MASK_BIT);
        }

        counter++;
    }


    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

}

/**
 * If a peg has been authorised it can be removed from the pegboard without sounding the alarm.
 * Removal of unauthorised pegs will sound the alarm within 200mS.
 *
 * This method performs an authorisation for multiple pegs on the same pegboard.
 *
 * @param pegboardNumber
 * @param pegNumber
 * @return bool
 */

bool multipleAuthorisation(uint8_t pegboardNumber, bool pegs[10]){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_AUTHORISATION_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface Authorisation Command.
    tx_buf[2] = INTERFACE_AUTHORISATION_CMD;

    // Assigns the pegboard number which the authorisation is performed at.
    tx_buf[3] = pegboardNumber & 0x3f;

    // Shifts the peg 10 value into the peg 10 bit position.
    tx_buf[4] = (uint8_t)(pegs[9] << 1);

    // Assigns the peg 9 value into the byte.
    tx_buf[4] |= (uint8_t)(pegs[8]);

    // Assigns the pegs into their respective bit positions.
    for(uint8_t i = 0; i < 8; i++){
        tx_buf[5] |= (uint8_t)(pegs[i] << i);
    }

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, INTERFACE_AUTHORISATION_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[INTERFACE_AUTHORISATION_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[INTERFACE_AUTHORISATION_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of tx buffer.
    uart_send(tx_buf, INTERFACE_AUTHORISATION_TX_LEN_CRC);

    // Free's the allocated tx buffer memory.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Emulates the reception of an acknowledgment.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the acknowledgement address received matches the transmitted address.
    if(ack != INTERFACE_ADDR)
        return false;

    return true;

}

/**
 * If a peg has been authorised it can be removed from the pegboard without sounding the alarm.
 * Removal of unauthorised pegs will sound the alarm within 200mS.
 *
 * This method performs an authorisation for a single peg.
 *
 * @param pegboardNumber
 * @param pegNumber
 * @return bool
 */
bool authorisation(uint8_t pegboardNumber, uint8_t pegNumber){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_AUTHORISATION_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface authorisation command.
    tx_buf[2] = INTERFACE_AUTHORISATION_CMD;

    // Assigns the pegboard number to act on the authorisation.
    tx_buf[3] = pegboardNumber & 0x3f;

    // Checks which peg number is to be authorised and sets it's representing bit.
    tx_buf[4] = (pegNumber == 10) ? 0x02 : tx_buf[4];
    tx_buf[4] = (pegNumber == 9) ? 0x01 : tx_buf[4];
    tx_buf[5] = (pegNumber > 0 && pegNumber < 9) ? (0x01 << (pegNumber - 1)) : tx_buf[5];

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, INTERFACE_AUTHORISATION_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[INTERFACE_AUTHORISATION_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[INTERFACE_AUTHORISATION_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of tx buffer.
    uart_send(tx_buf, INTERFACE_AUTHORISATION_TX_LEN_CRC);

    // Free's the allocated tx buffer memory.
    free(tx_buf);

    // Allocates memory to the rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the acknowledgement address received matches the transmitted address.
    if(ack != INTERFACE_ADDR)
        return false;

    return true;

}

/**
 *
 * There are 2 illumination channels for connection to 12V LED tapes. The LEDs must NOT have
 * internal drivers. Each driver can control 1.2 A maximum load. Each channel has a brightness
 * register and a flag to enable theatre dimming.
 *
 * @param theatre_dimming_ch1
 * @param theatre_dimming_ch2
 * @param brightness_ch1
 * @param brightness_ch2
 * @return bool
 */
bool illumination(bool theatre_dimming_ch1, bool theatre_dimming_ch2, uint8_t brightness_ch1, uint8_t brightness_ch2){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_ILLUMINATION_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface illumination command.
    tx_buf[2] = INTERFACE_ILLUMINATION_CMD;

    // Sets the theatre dimming bit for channel 1.
    tx_buf[3] = theatre_dimming_ch1 & 0x01;

    // Sets the theatre dimming bit for channel 2.
    tx_buf[3] |= ((theatre_dimming_ch2 << 1) & 0x02);

    // Sets the brightness byte for channel 1.
    tx_buf[4] = brightness_ch1;

    // Sets the brightness byte for channel 2.
    tx_buf[5] = brightness_ch2;

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, INTERFACE_ILLUMINATION_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[INTERFACE_ILLUMINATION_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[INTERFACE_ILLUMINATION_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of tx buffer.
    uart_send(tx_buf, INTERFACE_ILLUMINATION_TX_LEN_CRC);

    // Free's the allocated tx buffer memory.
    free(tx_buf);

    // Allocates memory to the rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Performs an emulated acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the acknowledgement address received matches the transmitted address.
    if(ack != INTERFACE_ADDR)
        return false;

    return true;

}

/**
 *
 * Each interface card has an onboard sounder and also a driver for an off-board alarm. These can be
 * set to alarm immediately on events seen by the interface card, or controlled directly from the host.
 * If an output is set to follow a pattern then this byte will be recirculated left every 2 seconds, so the
 * sounds will go on/off every 250mS following the bit pattern.
 *
 * @param reset_alarm
 * @param alarm_pulse
 * @param reset_sounder
 * @param sounder_pulse
 * @param alarm_pattern
 * @param sounder_pattern
 * @return bool
 */
bool sounds(bool reset_alarm, bool alarm_pulse, bool reset_sounder, bool sounder_pulse, uint8_t alarm_pattern, uint8_t sounder_pattern){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_SOUNDS_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface sounds command.
    tx_buf[2] = INTERFACE_SOUNDS_CMD;

    // Shifts the reset alarm bit into position.
    tx_buf[3] = (uint8_t)(reset_alarm << 5);

    // Shifts the alarm pulse bit into position.
    tx_buf[3] |=  (uint8_t)(alarm_pulse << 4);

    // Shifts the reset alarm bit into position.
    tx_buf[3] |= (uint8_t)(reset_sounder << 1);

    // Shifts the reset sounder bit into position.
    tx_buf[3] |= (uint8_t)(sounder_pulse);

    // Assigns the sounder pulse pattern.
    tx_buf[4] = sounder_pattern;

    // Assigns the alarm pulse pattern.
    tx_buf[5] = alarm_pattern;

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, INTERFACE_SOUNDS_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[INTERFACE_SOUNDS_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[INTERFACE_SOUNDS_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, INTERFACE_SOUNDS_TX_LEN_CRC);

    // Allocates memory to the rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Performs an emulated acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the acknowledgement address received matches the transmitted address.
    if(ack != INTERFACE_ADDR)
        return false;

    return true;
}
/**
 * This method unlocks the connected locks, when the booleans are set to true.
 *
 * @param lock1
 * @param lock2
 * @return bool
 *
 */
bool lock_control(bool lock1, bool lock2){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_LOCK_CONTROL_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface lock control  command.
    tx_buf[2] = INTERFACE_LOCK_CONTROL_CMD;

    // Shifts and assigns the lock booleans into their bit positions.
    tx_buf[3] = ((lock1 << 1) & 0x02) | ((lock2) & 0x01);

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, INTERFACE_LOCK_CONTROL_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[INTERFACE_LOCK_CONTROL_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[INTERFACE_LOCK_CONTROL_TX_LEN + 1] = crc & 0xFF;

    // Transmits the tx buffer.
    uart_send(tx_buf, INTERFACE_LOCK_CONTROL_TX_LEN_CRC);

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Performs an emulated acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Checks if the acknowledgement address received matches the transmitted address.
    if(ack != INTERFACE_ADDR)
        return false;

    return true;

}

/**
 *
 * This method performs a read of Interface Board settings.
 *
 * @return struct Interface_Settings
 */
struct Interface_Settings read_interface_settings(){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(INTERFACE_READ_SETTINGS_TX_LEN, sizeof(uint8_t));

    // Assigns the Start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface Read Settings Command.
    tx_buf[2] = INTERFACE_READ_SETTINGS_CMD;

    // Transmits the tx buffer.
    uart_send(tx_buf, INTERFACE_READ_SETTINGS_TX_LEN);

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t*) calloc(INTERFACE_READ_SETTINGS_RX_LEN, sizeof(uint8_t));

    // Performs an emulated receive response.
    uart_receive_interface_settings(rx_buf);

    // Creates a local struct to populate with the received Interface Settings.
    Interface_Settings settings{};

    // Assigns the number of pegboards received.
    settings.number_of_pegboards = rx_buf[1];

    // Assigns the lock timeout received.
    settings.lock_timeout = rx_buf[2];

    // Assigns the alarm time received.
    settings.alarm_time = rx_buf[3];

    // Assigns the sounder time received.
    settings.sounder_time = rx_buf[4];

    // Assigns the emergency release mode received.
    settings.emergency_release = (bool)((rx_buf[5] >> 1) & MASK_BIT);

    // Assigns the alert on change mode received.
    settings.alert_on_change = (bool)(rx_buf[5] & MASK_BIT);

    // Free's the allocated tx buffer.
    free(tx_buf);

    // Returns the settings struct.
    return settings;
}

/**
 *
 * This method performs a setting write on the Interface Board, the various settings can be found in @IF_SETTINGS.
 * Also the various combinations of emergency pull mode and alert on change can be found in @IF_SETTINGS_CHOICE.
 *
 *
 * @param setting
 * @param setting_value
 * @return bool : Acknowledgement.
 *
 */
bool write_interface_setting( uint8_t setting, uint8_t setting_value){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_WRITE_SETTINGS_TX_LEN_CRC, sizeof(uint8_t));

    // Assign the Start Command.
    tx_buf[0] = START_CMD;

    // Assign the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assign the Command.
    tx_buf[2] = INTERFACE_WRITE_SETTINGS_CMD;

    // Assign the setting location.
    tx_buf[3] = setting;

    // Assign the setting value.
    tx_buf[4] = setting_value;

    // Calculate the CRC for the tx data.
    uint16_t crc = crc16(tx_buf, INTERFACE_WRITE_SETTINGS_TX_LEN);

    // Assign the high byte CRC.
    tx_buf[5] = (crc >> 8) & 0xFF;

    // Assign the low byte CRC.
    tx_buf[6] = crc & 0xFF;

    uart_send(tx_buf, INTERFACE_WRITE_SETTINGS_TX_LEN_CRC);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive the acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Receive the Acknowledgment address into a local variable.
    uint8_t ack = rx_buf[0];

    // Free the allocated tx buffer.
    free(tx_buf);

    // Free the allocated rx buffer.
    free(rx_buf);

    // Checks if the received address checks with the transmitted address.
    if(ack != INTERFACE_ADDR)
        // Returns that the Setting Write was unsuccessful.
        return false;

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_WRITE_SETTINGS_TX_LEN_CRC, sizeof(uint8_t));

    // Assigns the Start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface write setting command.
    tx_buf[2] = INTERFACE_WRITE_SETTINGS_CMD;

    // Complements the Setting location byte.
    tx_buf[3] = ~setting;

    // Complements the Setting value byte.
    tx_buf[4] = ~setting_value;

    // Calculate the CRC for the TX data.
    crc = crc16(tx_buf, INTERFACE_WRITE_SETTINGS_TX_LEN);

    // Assigns the high CRC byte.
    tx_buf[5] = (crc >> 8) & 0xFF;

    // Assigns the low CRC byte.
    tx_buf[6] = crc & 0xFF;

    // Transmit the complemented format.
    uart_send(tx_buf, INTERFACE_WRITE_SETTINGS_TX_LEN_CRC);

    // Allocate memory for rx buffer.
    rx_buf = (uint8_t *) calloc(ACK_LEN, sizeof(uint8_t));

    // Receive the acknowledgement.
    uart_receive_ack(rx_buf, ACK_LEN, INTERFACE_ADDR);

    // Assigns the received address to the local variable.
    ack = rx_buf[0];

    // Free's the allocated memory for the tx buffer.
    free(tx_buf);

    // Free's the allocated memory for the rx buffer.
    free(rx_buf);

    // Checks if the received address checks with the transmitted address.
    if(ack != INTERFACE_ADDR)
        // Returns that the Setting Write was unsuccessful.
        return false;

    // Returns that the Setting Write was successful.
    return true;
}

/**
 * Sets the Interface board into bootloader mode.
 */
void interface_enter_bootloader(){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_BOOTLOADER_TX_LEN, sizeof(uint8_t));

    // Assign the Start Command to the buffer.
    tx_buf[0] = START_CMD;

    // Assign the Address to the buffer.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assign the Command to the buffer.
    tx_buf[2] = INTERFACE_BOOTLOADER_CMD;

    // Transmit the tx buffer.
    uart_send(tx_buf, INTERFACE_BOOTLOADER_TX_LEN);

    // Free the allocated memory for the tx buffer.
    free(tx_buf);
}

/**
 * Performs an Interface Hardware Reset.
 */
void interface_reset(){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_RESET_TX_LEN, sizeof(uint8_t));

    // Assign the Start Command to the buffer.
    tx_buf[0] = START_CMD;

    // Assign the Address to the buffer.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assign the Command to the buffer.
    tx_buf[2] = INTERFACE_RESET_CMD;

    // Transmit the tx buffer.
    uart_send(tx_buf, INTERFACE_RESET_TX_LEN);

    // Free the allocated memory for the tx buffer.
    free(tx_buf);
}

/**
 * This function obtains the Interface version, either bootloader or application.
 *
 * Please see header file for more information on BOOTLOADER_VERSION_TYPE and APPLICATION_VERSION_TYPE.
 *
 * @param version_type : BOOTLOADER_VERSION_TYPE or APPLICATION_VERSION_TYPE.
 * @return string
 */

std::string get_interface_version(uint8_t version_type){

    std::string ret;
    ret = "";

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t *) calloc(INTERFACE_VERSION_TX_LEN, sizeof(uint8_t));

    // Assigns the start command.
    tx_buf[0] = START_CMD;

    // Assigns the Interface Address.
    tx_buf[1] = ADDR_MASK | INTERFACE_ADDR;

    // Assigns the Interface version request command.
    tx_buf[2] = INTERFACE_VERSION_CMD;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, INTERFACE_VERSION_TX_LEN);

    // Free's the allocated tx buffer memory.
    free(tx_buf);

    // Allocates the rx buffer memory.
    rx_buf = (uint8_t *) calloc(INTERFACE_VERSION_RX_LEN, sizeof(uint8_t));

    // Emulates the reception of Interface Version return data.
    uart_receive_version(rx_buf, INTERFACE_ADDR);

    // Assign the received address to a local variable.
    uint8_t ack = rx_buf[0];

    // Checks if the received address matches the transmitted address.
    if(ack != INTERFACE_ADDR){
        free(rx_buf);
        return ret;
    }

    // Checks which version type is input through the function parameter.
    if(version_type == BOOTLOADER_VERSION_TYPE){
        // Returns the Bootloader version.
        ret = std::to_string(rx_buf[1]) + "." + std::to_string(rx_buf[2]);
    }
    else if(version_type == APPLICATION_VERSION_TYPE){
        // Returns the Application version.
        ret = std::to_string(rx_buf[2]) + "." + std::to_string(rx_buf[3]);
    }

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Returns the version in string format.
    return ret;
}

struct Flash_Info_Block readFlashInfoBlock(uint8_t device_address){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(READ_FLASH_INFO_TX_LEN, sizeof(uint8_t));

    // Assign the start command.
    tx_buf[0] = START_CMD;

    // Assign the address.
    tx_buf[1] = ADDR_MASK | device_address;

    // Assign Write flash command.
    tx_buf[2] = READ_FLASH_INFO_CMD;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, READ_FLASH_INFO_TX_LEN);

    // Free's the memory allocated to the tx buffer.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t*) calloc(READ_FLASH_INFO_RX_LEN_CRC, sizeof(uint8_t));

    // Emulates the reception of flash info block.
    uart_receive_flash_info_block(rx_buf, device_address);

    // Initialises a Flash Info Struct to hold the received information.
    Flash_Info_Block info;

    // Assigns the application version into a string.
    info.application_version = (to_string(rx_buf[1]) + "." + to_string(rx_buf[2]));

    // Assigns the application size into a 16-bit variable.
    info.application_size = (rx_buf[3] << 8) | (rx_buf[4]);

    // Assigns the application code high CRC byte.
    info.application_crc_high = rx_buf[5];

    // Assigns the application code low CRC byte.
    info.application_crc_low = rx_buf[6];

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    return info;

}

/**
 * This sends a data block consisting of 2 bytes PIC address and 128 bytes of data. On reception of the
 * command with a good CRC the PIC erases the addressed block, decrypts the 128 bytes of data,
 * programs the PIC flash and then verifies it. If the data verifies OK then the reply is sent.
 * Note that it could take upto 10mS before this command replies as the processor halts whist erasing
 * and programming a flash block. After delaying a minimum of 10mS further blocks must be sent
 * until the PC/PI reaches the end of file. If pegboards are programmed individually then the reply will
 * be sent and the next block can be programmed as soon as the reply is seen. If the pegboards are
 * programmed globally they will not acknowledge, therefore the 10mS minimum MUST be adhered
 * to.
 *
 * ACK: 00 if write verifies OK, 01 for write verify error.
 *
 *
 * @param device_address : Pegboard Address (1 - 60) or Interface 61.
 * @param encrypted_hex_data : Encrypted Hex File received from Dave Woodfield.
 * @return bool
 */
bool write_flash_application_code(const uint8_t device_address, uint8_t encrypted_hex_data[130]){

    // Allocate the memory for the tx buffer.
    tx_buf = (uint8_t*) calloc(WRITE_FLASH_TX_LEN_CRC, sizeof(uint8_t));

    // Assign the start command.
    tx_buf[0] = START_CMD;

    // Assign the address.
    tx_buf[1] = ADDR_MASK | device_address;

    // Assign Write flash command.
    tx_buf[2] = WRITE_FLASH_CODE_CMD;

    // Counter used to iterate through the encrypted hex file.
    int counter = 0;
    for(int i = 3; i < 134; i++){
        // Assigns the encrypted data to the tx buffer.
        tx_buf[i] = encrypted_hex_data[counter];
        // Increments to the next byte in the encrypted hex data.
        counter++;
    }

    // Calculates the CRC for the tx buffer.
    uint16_t crc = crc16(tx_buf, WRITE_FLASH_TX_LEN);

    // Assigns the CRC high byte.
    tx_buf[WRITE_FLASH_TX_LEN] = (crc >> 8) & 0xFF;

    // Assigns the CRC low byte.
    tx_buf[WRITE_FLASH_TX_LEN + 1] = crc & 0xFF;

    // Emulates the transmission of the tx buffer.
    uart_send(tx_buf, WRITE_FLASH_TX_LEN_CRC);

    // Free's the tx buffer allocated memory.
    free(tx_buf);

    // Allocates memory for the rx buffer.
    rx_buf = (uint8_t*) calloc(WRITE_FLASH_RX_LEN, sizeof(uint8_t));

    // Emulates the reception of the serial data.
    uart_receive_flash_ack(rx_buf, device_address);

    // Masks the received address.
    uint8_t ack_addr = (rx_buf[0] & MASK_ADDR);

    // Masks the acknowledgement.
    uint8_t ack = (rx_buf[0] & 0xC0);

    // Free's the memory allocated to the rx buffer.
    free(rx_buf);

    // Returns successful if address and ack are received from the board.
    return ((ack == 0x00) & (ack_addr == device_address));

}