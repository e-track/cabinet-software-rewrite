//
// Created by Moe Samak on 18/08/2021.
//
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include "uart_protocol.h"

using namespace std;

#ifndef UNTITLED_EMULATED_UART_H
#define UNTITLED_EMULATED_UART_H

void uart_send(uint8_t *tx_data, uint8_t tx_len);

void uart_receive_PegboardStatus(uint8_t *rx_data);

void uart_receive_PegNumbers(uint8_t *rx_data);

void uart_receive_pb_settings(uint8_t *rx_data);

void uart_receive_version(uint8_t  *rx_data, uint8_t addr);

void uart_receive(uint8_t *rx_data, uint8_t rx_len);

void uart_receive_ack(uint8_t *rx_data, uint8_t rx_len, uint8_t addr);

void uart_receive_interface_settings(uint8_t* rx_data);

void uart_receive_interface_status(uint8_t *rx_data);

void uart_receive_global_peg_status(uint8_t * rx_data, int numberOfPegboards);

void uart_receive_flash_ack(uint8_t * rx_data, uint8_t address);

void uart_receive_flash_info_block(uint8_t * rx_data, uint8_t address);

#endif //UNTITLED_EMULATED_UART_H
