//
// Created by Moe Samak on 09/08/2021.
//

#ifndef UNTITLED_UART_PROTOCOL_H
#define UNTITLED_UART_PROTOCOL_H

#include <cstdint>
#include <cstdlib>
#include <iostream>

using namespace std;

/**
 *  LED Colors are defined according to Pegboard Protocol 16A.
 */
const uint8_t LED_COLOR_OFF = 0x00;
const uint8_t LED_COLOR_RED_STATIC = 0x11;
const uint8_t LED_COLOR_MAGENTA_STATIC = 0x22;
const uint8_t LED_COLOR_BLUE_STATIC = 0x33;
const uint8_t LED_COLOR_CYAN_STATIC = 0x44;
const uint8_t LED_COLOR_GREEN_STATIC = 0x55;
const uint8_t LED_COLOR_YELLOW_STATIC = 0x66;
const uint8_t LED_COLOR_WHITE_STATIC = 0x77;

const uint8_t LED_COLOR_RED_FLASH = 0x10;
const uint8_t LED_COLOR_MAGENTA_FLASH = 0x20;
const uint8_t LED_COLOR_BLUE_FLASH = 0x30;
const uint8_t LED_COLOR_CYAN_FLASH = 0x40;
const uint8_t LED_COLOR_GREEN_FLASH = 0x50;
const uint8_t LED_COLOR_YELLOW_FLASH = 0x60;
const uint8_t LED_COLOR_WHITE_FLASH = 0x70;

// Start Command Byte is sent on the start of each request.
#define START_CMD 0xBF

// Pegboard Status Address is the address used when requesting for a
// Pegboard status request.
#define PEGBOARD_STATUS_ADDR 0xC0

// Pegboard_ADDR is Ored with the pegboard number to target a specific pegboard.
#define ADDR_MASK 0x40

// Pegboard tag numbers request command.
#define PEGBOARD_NUMBERS_CMD 0xF0

// Pegboard led drive 1 request command.
#define PEGBOARD_LED_DRIVE_1_CMD 0xE1

// Pegboard led drive 2 request command.
#define PEGBOARD_LED_DRIVE_2_CMD 0xD2

// Pegboard led settings request command.
#define PEGBOARD_LED_SETTING_CMD 0xC3

// Pegboard solenoid request command.
#define PEGBOARD_SOLENOID_CMD 0xB4

// Pegboard read settings request command.
#define PEGBOARD_READ_SETTINGS_CMD 0x69

// Pegboard write settings request command.
#define PEGBOARD_WRITE_SETTINGS_CMD 0x5A

// Pegboard Version request command.
#define PEGBOARD_VERSION_CMD 0x4B

// Pegboard Bootloader command.
#define PEGBOARD_BOOTLOADER_CMD 0x3C

// Pegboard Hardware Reset command.
#define PEGBOARD_RESET_CMD 0X0F

// Defines the number of bytes expected to receive when waiting for an Acknowledgement
#define ACK_LEN 1

// Defines the number of bytes in the CRC.
#define CRC_LEN 2

// Defines the number of bytes sent in a Pegboard Status Request.
#define PEGBOARD_STATUS_TX_LEN 2

// Defines the number of bytes received in a Pegboard Status Request.
#define PEGBOARD_STATUS_RX_LEN 4

// Defines the number of bytes received in a Pegboard Status Request with CRC.
#define PEGBOARD_STATUS_RX_LEN_CRC (PEGBOARD_STATUS_RX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Numbers Request.
#define PEGBOARD_NUMBERS_TX_LEN 3

// Defines the number of bytes received in a Pegboard Numbers Request.
#define PEGBOARD_NUMBERS_RX_LEN 61

// Defines the number of bytes received in a Pegboard Numbers Request with CRC.
#define PEGBOARD_NUMBERS_RX_LEN_CRC (PEGBOARD_NUMBERS_RX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Led Drive 1 Request.
#define PEGBOARD_LED_DRIVE_1_TX_LEN 13

// Defines the number of bytes sent in a Pegboard Led Drive 1 Request with CRC.
#define PEGBOARD_LED_DRIVE_1_TX_LEN_CRC (PEGBOARD_LED_DRIVE_1_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Led Drive 2 Request.
#define PEGBOARD_LED_DRIVE_2_TX_LEN 33

// Defines the number of bytes sent in a Pegboard Led Drive 2 Request with CRC.
#define PEGBOARD_LED_DRIVE_2_TX_LEN_CRC (PEGBOARD_LED_DRIVE_2_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Led setting Request.
#define PEGBOARD_LED_SETTING_TX_LEN 4

// Defines the number of bytes sent in a Pegboard Led setting Request with CRC.
#define PEGBOARD_LED_SETTING_TX_LEN_CRC (PEGBOARD_LED_SETTING_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Solenoid Control Request.
#define PEGBOARD_SOLENOID_CONTROL_TX_LEN 5

// Defines the number of bytes sent in a Pegboard Solenoid Control Request with CRC.
#define PEGBOARD_SOLENOID_CONTROL_TX_LEN_CRC (PEGBOARD_SOLENOID_CONTROL_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Read Setting Request.
#define PEGBOARD_READ_SETTING_TX_LEN 3

// Defines the number of bytes received by a Pegboard Read Setting Request.
#define PEGBOARD_READ_SETTING_RX_LEN 19

// Defines the number of bytes sent in a Pegboard Write Settings Request.
#define PEGBOARD_WRITE_SETTING_TX_LEN 5

// Defines the number of bytes sent in a Pegboard Write Settings Request with CRC.
#define PEGBOARD_WRITE_SETTING_TX_LEN_CRC (PEGBOARD_WRITE_SETTING_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in a Pegboard Version Request.
#define PEGBOARD_VERSION_TX_LEN 3

// Defines the number of bytes received in a Pegboard Version Request.
#define PEGBOARD_VERSION_RX_LEN 5

// Defines the number of bytes received in a Pegboard Version Request.
#define PEGBOARD_VERSION_RX_LEN_CRC 7

// Defines the number of bytes sent in a Pegboard Bootloader request.
#define PEGBOARD_BOOTLOADER_TX_LEN 3

// Defines the number of bytes sent in a Pegboard Hardware Reset Request.
#define PEGBOARD_RESET_TX_LEN 3

// Used to mask the received bytes.
#define MASK_BIT 0x01

#define MASK_ADDR 0x3F

// Bootloader bit location.
#define BOOTLOADER_BIT 7

// Led Error bit location.
#define LED_ERROR_BIT 6

// CRC Error bit location.
#define CRC_ERROR_BIT 5

// EEPROM Error bit location.
#define E2_ERROR_BIT 4

// Flash Sync bit location in led_setting method.
#define FLASH_SYNC_BIT 7

// Show Address bit location in led_setting method.
#define SHOW_ADDR_BIT 6

// Used to identify which version type is requested in the get version request.
#define BOOTLOADER_VERSION_TYPE 0

// Used to identify which version type is requested in the get version request.
#define APPLICATION_VERSION_TYPE 1

// Interface Status Address used only when requesting the interface status.
#define INTERFACE_STATUS_ADDR 0xFD

// Interface Address.
#define INTERFACE_ADDR 0x3d

// Interface Authorisation Command code.
#define INTERFACE_AUTHORISATION_CMD 0xe1

// Interface Illumination Command code.
#define INTERFACE_ILLUMINATION_CMD 0xd2

// Interface Sounds Command code.
#define INTERFACE_SOUNDS_CMD 0xc3

// Interface Lock Control Command code.
#define INTERFACE_LOCK_CONTROL_CMD 0xb4

// Interface Read Settings Command code.
#define INTERFACE_READ_SETTINGS_CMD 0x69

// Interface Write Settings Command code.
#define INTERFACE_WRITE_SETTINGS_CMD 0x5a

// Interface Read Global Peg Status Command code.
#define INTERFACE_READ_GLOBAL_PEG_STATUS_CMD 0xf0

// Interface Read Version Command code.
#define INTERFACE_VERSION_CMD 0x4b

// Interface Enter Bootloader Command code.
#define INTERFACE_BOOTLOADER_CMD 0x3c

// Interface Hardware Reset Command code.
#define INTERFACE_RESET_CMD 0x0f

// Board Read Flash Info Block command.
#define READ_FLASH_INFO_CMD 0x2d

// Board Write Flash Application Code command.
#define WRITE_FLASH_CODE_CMD 0x1e

// Read Flash TX data Length.
#define READ_FLASH_INFO_TX_LEN 3

// Read Flash RX data Length.
#define READ_FLASH_INFO_RX_LEN 33

// Read Flash RX data Length with CRC.
#define READ_FLASH_INFO_RX_LEN_CRC 35

// Write Flash TX data Length.
#define WRITE_FLASH_TX_LEN 133

// Read Flash TX data Length with CRC.
#define WRITE_FLASH_TX_LEN_CRC 135

// Write Flash RX data Length.
#define WRITE_FLASH_RX_LEN 1

// Defines the number of bytes sent in an Interface Status Request.
#define INTERFACE_STATUS_TX_LEN 2

// Interface Status Request RX Data Length.
#define INTERFACE_STATUS_RX_LEN 4

// Interface Status Request RX Data Length with CRC.
#define INTERFACE_STATUS_RX_LEN_CRC (INTERFACE_STATUS_RX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Authorisation Request.
#define INTERFACE_AUTHORISATION_TX_LEN 6

// Defines the number of bytes sent in an Interface Authorisation Request with CRC.
#define INTERFACE_AUTHORISATION_TX_LEN_CRC (INTERFACE_AUTHORISATION_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Illumination Request.
#define INTERFACE_ILLUMINATION_TX_LEN 6

// Defines the number of bytes sent in an Interface Illumination Request with CRC.
#define INTERFACE_ILLUMINATION_TX_LEN_CRC (INTERFACE_ILLUMINATION_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Sounds Request.
#define INTERFACE_SOUNDS_TX_LEN 6

// Defines the number of bytes sent in an Interface Sounds Request with CRC.
#define INTERFACE_SOUNDS_TX_LEN_CRC (INTERFACE_SOUNDS_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Lock Control Request.
#define INTERFACE_LOCK_CONTROL_TX_LEN 4

// Defines the number of bytes sent in an Interface Lock Control Request with CRC.
#define INTERFACE_LOCK_CONTROL_TX_LEN_CRC (INTERFACE_LOCK_CONTROL_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Read Setting Request.
#define INTERFACE_READ_SETTINGS_TX_LEN 3

// Defines the number of bytes received in an Interface Read Setting Request.
#define INTERFACE_READ_SETTINGS_RX_LEN 19

// Defines the number of bytes sent in an Interface Write Settings Request.
#define INTERFACE_WRITE_SETTINGS_TX_LEN 5

// Defines the number of bytes sent in an Interface Write Settings Request with CRC.
#define INTERFACE_WRITE_SETTINGS_TX_LEN_CRC (INTERFACE_WRITE_SETTINGS_TX_LEN + CRC_LEN)

// Defines the number of bytes sent in an Interface Version Request.
#define INTERFACE_VERSION_TX_LEN 3

// Defines the number of bytes received in an Interface Version Request.
#define INTERFACE_VERSION_RX_LEN 7

// Defines the number of bytes sent in an Interface Enter Bootloader Request.
#define INTERFACE_BOOTLOADER_TX_LEN 3

// Defines the number of bytes sent in an Interface Hardware Reset Request.
#define INTERFACE_RESET_TX_LEN 3

// Defines the number of bytes sent in an Interface Global Peg Status Request.
#define INTERFACE_GLOBAL_PEG_STATUS_TX_LEN 3

// Defines the initial number of bytes received in an Interface Global Peg Status Request.
#define INTERFACE_GLOBAL_PEG_STATUS_RX_LEN 1

// Defines the initial number of bytes received in an Interface Global Peg Status Request with CRC.
#define INTERFACE_GLOBAL_PEG_STATUS_RX_LEN_CRC (INTERFACE_GLOBAL_PEG_STATUS_RX_LEN + CRC_LEN)

// Defines the number of bytes expected from each Pegboard during a Global Peg Status Request.
#define INTERFACE_GLOBAL_PEG_STATUS_TRACK_BYTES 3

// Defines the position of the first slot in the LED Drive format 1 command.
#define LED_DRIVE_1_FIRST_SLOT_POSITION 2



enum IF_SETTINGS_CHOICE{
    PULL_ANY_10_AND_NO_ALERT_ON_CHANGE = 0x00,
    PULL_ANY_10_AND_ALERT_ON_CHANGE = 0x01,
    PULL_FIRST_10_AND_NO_ALERT_ON_CHANGE = 0x02,
    PULL_FIRST_10_AND_ALERT_ON_CHANGE = 0x03
};

enum PB_SETTINGS{
    RFID_THRESHOLD = 0x00,
    SOLENOID_TIMEOUT = 0x01
};

enum IF_SETTINGS{
    numberOfPegboards = 0x00,
    lock_timeout = 0x01,
    alarm_time = 0x02,
    sounder_time = 0x03,
    emergency_alert = 0x04
};

struct Pegboard_Settings{
    uint8_t rfid_threshold;
    uint8_t solenoid_timeout;
};

struct Rgb{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct Pegs_Status{
    bool home;
    bool found;
};

struct Peg{
    uint8_t id[5];
    uint8_t  signal_strength;
    struct Pegs_Status pegsStatus;
};

struct Pegboard{
    uint8_t addr = 0x00;
    bool bootloader = false;
    bool led_error = false;
    bool crc_error = false;
    bool e2_error = false;
    struct Peg pegs[10]{};
};



struct Pegboard_Status{
    uint8_t addr;
    bool bootloader;
    bool led_error;
    bool crc_error;
    bool e2_error;
    struct Pegs_Status pegsStatuses[10];
};



struct Cabinet{
    struct Pegboard pegboards[20];
};


struct Interface_Status{
    bool bootloader;
    bool override;
    bool opto_switch;
    bool lock_power;
    bool lock2_door;
    bool lock2_latch;
    bool lock1_door;
    bool lock1_latch;
    bool alarm_timer;
    bool sounder_timer;
    bool change_occured;
    bool e2_error;
    float voltage;
};

struct Interface_Settings{
    uint8_t number_of_pegboards;
    uint8_t lock_timeout;
    uint8_t alarm_time;
    uint8_t sounder_time;
    bool emergency_release;
    bool alert_on_change;
};

struct Flash_Info_Block{
    string application_version;
    uint16_t application_size;
    uint8_t application_crc_high;
    uint8_t application_crc_low;
};

uint16_t crc16(uint8_t* pData, int length);

Pegboard_Status getPegboardStatus(uint8_t address);

Peg* readPegNumbers(uint8_t address);

bool led_drive_1(uint8_t address, const uint8_t pegColors[10]);

bool led_drive_2(uint8_t address, struct Rgb *pegColors);

bool led_setting(uint8_t address, bool flash_sync, bool show_address, uint8_t brightness);

bool multiple_solenoid_control(uint8_t address, const bool solenoids[10]);

bool single_solenoid_control(uint8_t address, uint8_t solenoid);

struct Pegboard_Settings read_pegboard_settings(uint8_t address);

bool write_pegboard_setting(uint8_t address, uint8_t setting, uint8_t setting_value);

void pegboard_enter_bootloader(uint8_t address);

void pegboard_hardware_reset(uint8_t address);

std::string get_pegboard_version(uint8_t address, uint8_t version_type);

struct Interface_Status getInterfaceStatus();

void readGlobalPegStatus(struct Pegboard_Status *status_array,int numberOfPegboards);

bool multipleAuthorisation(uint8_t pegboardNumber, bool pegs[10]);

bool authorisation(uint8_t pegboardNumber, uint8_t pegNumber);

bool illumination(bool theatre_dimming_ch1, bool theatre_dimming_ch2, uint8_t brightness_ch1, uint8_t brightness_ch2);

bool sounds(bool reset_alarm, bool alarm_pulse, bool reset_sounder, bool sounder_pulse, uint8_t alarm_pattern, uint8_t sounder_pattern);

bool lock_control(bool lock1, bool lock2);

struct Interface_Settings read_interface_settings();

bool write_interface_setting( uint8_t setting, uint8_t setting_value);

void interface_enter_bootloader();

void interface_reset();

std::string get_interface_version(uint8_t version_type);

bool write_flash_application_code(uint8_t device_address, uint8_t encrypted_hex_data[130]);

struct Flash_Info_Block readFlashInfoBlock(uint8_t device_address);

bool single_led_drive_1(uint8_t address, uint8_t slotNumber, uint8_t pegColor);

#endif //UNTITLED_UART_PROTOCOL_H
