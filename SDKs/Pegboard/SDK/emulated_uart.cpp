//
// Created by Moe Samak on 18/08/2021.
//

#include "emulated_uart.h"


void uart_send(uint8_t *tx_data, uint8_t tx_len){

    for(uint8_t i = 0; i < tx_len; i++){
        cout << dec << "Byte[" << (int)i << "] = ";
        if(tx_data[i] < 16)
            cout << hex << "0x0" << (int) tx_data[i] << endl;
        else
            cout << hex << "0x" << (int) tx_data[i] << endl;
    }

}

void uart_receive_PegboardStatus(uint8_t *rx_data){

    rx_data[0] = 0x01;
    rx_data[1] = 0x0f;
    rx_data[2] = 0xff;
    rx_data[3] = 0xff;

    uint16_t crc = (rx_data, 4);

    rx_data[4] = (crc >> 8) & 0xFF;
    rx_data[5] = crc & 0xFF;
}

void uart_receive_PegNumbers(uint8_t *rx_data){

    rx_data[0] = 0x01;

    for(uint8_t i = 0; i < 61; i++){
        rx_data[i] = i;
    }

    uint16_t crc = (rx_data, 60);

    rx_data[61] = (crc >> 8) & 0xFF;
    rx_data[62] = crc & 0xFF;

}

void uart_receive_pb_settings(uint8_t *rx_data){

    rx_data[0] = 0x01;
    for(uint8_t i = 1; i < 17; i++)
        rx_data[i] = i;

    uint16_t crc = (rx_data, PEGBOARD_READ_SETTING_RX_LEN);

    rx_data[PEGBOARD_READ_SETTING_RX_LEN] = (crc >> 8) & 0xFF;
    rx_data[PEGBOARD_READ_SETTING_RX_LEN + 1] = crc & 0xFF;

}

void uart_receive_version(uint8_t  *rx_data, uint8_t addr){
    rx_data[0] = addr;
    rx_data[1] = 0x02;
    rx_data[2] = 0x03;
    rx_data[3] = 0x10;
    rx_data[4] = 0x06;

    uint16_t crc = (rx_data, PEGBOARD_VERSION_RX_LEN);

    rx_data[PEGBOARD_VERSION_RX_LEN] = (crc >> 8) & 0xFF;
    rx_data[PEGBOARD_VERSION_RX_LEN + 1] = crc & 0xFF;
}


void uart_receive_ack(uint8_t *rx_data, uint8_t rx_len, uint8_t addr){

    rx_data[0] = addr;

}

void uart_receive_interface_settings(uint8_t* rx_data){

    rx_data[0] = INTERFACE_ADDR;
    rx_data[1] = 0x14;
    rx_data[2] = 0xf0;
    rx_data[3] = 0xf0;
    rx_data[4] = 0xf0;
    rx_data[5] = 0x3;

}

void uart_receive_interface_status(uint8_t *rx_data){

    rx_data[0] = 0xfd;
    rx_data[1] = 0x0f;
    rx_data[2] = 0x0c;
    rx_data[3] = 0x4e;

    uint16_t crc = (rx_data, INTERFACE_STATUS_RX_LEN);

    rx_data[INTERFACE_STATUS_RX_LEN] = (crc >> 8) & 0xFF;
    rx_data[INTERFACE_STATUS_RX_LEN + 1] = crc & 0xFF;

}

void uart_receive_global_peg_status(uint8_t * rx_data, int numberOfPegboards){

    rx_data[0] = INTERFACE_ADDR;

    uint8_t rx_bytes = INTERFACE_GLOBAL_PEG_STATUS_TRACK_BYTES * numberOfPegboards;

    uint8_t i = 0;
    for(i = 1; i < rx_bytes ; i+=3){

        rx_data[i] = i;
        rx_data[i+1] = i+1;
        rx_data[i+2] = i+2;

    }

    /*
    rx_data[1] = 0x00;
    rx_data[2] = 0x01;
    rx_data[3] = 0x01;
    rx_data[4] = 0x00;
    rx_data[5] = 0x02;
    rx_data[6] = 0x02;
    rx_data[7] = 0x00;
    rx_data[8] = 0x04;
    rx_data[9] = 0x04;
    rx_data[10] = 0x00;
    rx_data[11] = 0x08;
    rx_data[12] = 0x08;
    rx_data[13] = 0x00;
    rx_data[14] = 0x10;
    rx_data[15] = 0x10;
    rx_data[16] = 0x00;
    rx_data[17] = 0x20;
    rx_data[18] = 0x20;
    rx_data[19] = 0x00;
    rx_data[20] = 0x40;
    rx_data[21] = 0x40;
    rx_data[22] = 0x00;
    rx_data[23] = 0x80;
    rx_data[24] = 0x80;
    rx_data[25] = 0x05;
    rx_data[26] = 0x00;
    rx_data[27] = 0x00;
    rx_data[28] = 0x0a;
    rx_data[29] = 0x00;
    rx_data[30] = 0x00;
    */

    uint8_t rx_bytes_crc = rx_bytes + INTERFACE_GLOBAL_PEG_STATUS_RX_LEN;
    uint16_t crc = (rx_data, rx_bytes_crc);

    rx_data[rx_bytes_crc] = (crc >> 8) & 0xFF;
    rx_data[rx_bytes_crc + 1] = crc & 0xFF;

}

void uart_receive_flash_ack(uint8_t * rx_data, uint8_t address){
    rx_data[0] = address & 0x3f;
}

void uart_receive_flash_info_block(uint8_t * rx_data, uint8_t address){
    rx_data[0] = address;
    rx_data[1] = 0x02;
    rx_data[2] = 0x08;
    rx_data[4] = 0x80;
    rx_data[5] = 0x0d;

    uint16_t crc = (rx_data, READ_FLASH_INFO_RX_LEN);

    rx_data[READ_FLASH_INFO_RX_LEN] = (crc >> 8) & 0xFF;
    rx_data[READ_FLASH_INFO_RX_LEN + 1] = crc & 0xFF;

}