// Qt.
#include <QDebug>
#include <QMutexLocker>
#include <QObject>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <UF_API.h>

// Project.
#include "fingerprintoperationthread.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThread::FingerprintOperationThread()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
FingerprintOperationThread::FingerprintOperationThread( QObject* parent )
    : QThread( parent )
    , m_operation( Operation::None )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::setOperation( Operation operation )
///
/// \brief  Sets the current operation.
///
/// \param  operation   The operation to be run.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThread::setOperation( Operation operation )
{
    QMutexLocker locker( &m_mutex );

    if ( m_operation != operation )
    {
        m_operation = operation;

        if ( m_operation != Operation::None )
        {
            start();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::getOperation()
///
/// \brief  Gets the operation to be run.
///
/// \return The operation that will be run.
////////////////////////////////////////////////////////////////////////////////
Operation FingerprintOperationThread::getOperation()
{
    return m_operation;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::start()
///
/// \brief  Starts the thread.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThread::start()
{
    QThread::start();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::run()
///
/// \brief  Entry point for threaded operations.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThread::run()
{
    QMutexLocker locker( &m_mutex );

    switch ( m_operation )
    {
        case Operation::Enroll:
        {
            User user = DatabaseControl::instance()->getLoggedInUser();
            UINT32 userId = static_cast<UINT32> ( user.m_id );

            UF_ENROLL_OPTION option = UF_ENROLL_AUTO_ID;
            UINT32 fingerprintId = 0;
            UINT32 imageQuality = 0;

            if ( UF_Enroll( userId, option, &fingerprintId, &imageQuality ) == UF_RET_SUCCESS )
            {
                emit success();
                qDebug() << QString( "[FingerprintOperationThread::run] Enroll success. User %1, fingerprint %2" )
                                .arg( QString::number( userId ),
                                      QString::number( fingerprintId ) );

                DatabaseControl::instance()->addFingerprint( fingerprintId );
            }
            else
            {
                emit fail();
                qDebug() << QString( "[FingerprintOperationThread::run] Enroll fail" );
            }

            break;
        }

        case Operation::Identify:
        {
            UINT32 userId;
            BYTE subId;

            if ( UF_Identify( &userId, &subId ) == UF_RET_SUCCESS )
            {
                emit success();
                qDebug() << QString( "[FingerprintOperationThread::run] Identify success" );
            }
            else
            {
                emit fail();
                qDebug() << QString( "[FingerprintOperationThread::run] Identify fail" );
            }

            break;
        }

        case Operation::Verify:
        {
            UINT32 userId = 0;
            BYTE subId;

            if ( UF_Verify( userId, &subId ) == UF_RET_SUCCESS )
            {
                emit success();
                qDebug() << QString( "[FingerprintOperationThread::run] Verify success." );
            }
            else
            {
                emit fail();
                qDebug() << QString( "[FingerprintOperationThread::run] Verify fail." );
            }

            break;
        }

        case Operation::AutoIdentifyEnable:
        {
            UINT32 autoResponse = 0x31;
            UF_RET_CODE returnCodeAutoResponse = UF_SetSysParameter( UF_SYS_AUTO_RESPONSE, autoResponse );

            // Free scan.
            UINT32 freeScan = 0x32;
            UF_RET_CODE returnCodeFreeScan = UF_SetSysParameter( UF_SYS_FREE_SCAN, freeScan );

            if ( returnCodeAutoResponse == UF_RET_SUCCESS &&
                 returnCodeFreeScan == UF_RET_SUCCESS )
            {
                emit success();
                qDebug() << QString( "[FingerprintOperationThread::run] Auto identify enable success." );
            }
            else
            {
                emit fail();
                qDebug() << QString( "[FingerprintOperationThread::run] Auto identify enable fail." );
            }

            break;
        }

        case Operation::AutoIdentifyDisable:
        {
            UINT32 autoResponse = 0x30;
            UF_RET_CODE returnCodeAutoResponse = UF_SetSysParameter( UF_SYS_AUTO_RESPONSE, autoResponse );

            // Free scan.
            UINT32 freeScan = 0x30;
            UF_RET_CODE returnCodeFreeScan = UF_SetSysParameter( UF_SYS_FREE_SCAN, freeScan );

            if ( returnCodeAutoResponse == UF_RET_SUCCESS &&
                 returnCodeFreeScan == UF_RET_SUCCESS )
            {
                emit success();
                qDebug() << QString( "[FingerprintOperationThread::run] Auto identify disable success." );
            }
            else
            {
                emit fail();
                qDebug() << QString( "[FingerprintOperationThread::run] Auto identify disable fail." );
            }

            break;
        }

        case Operation::AutoIdentify:
        {
            BYTE packet[UF_PACKET_LEN];

            if ( UF_ReceivePacket( packet, 100 ) == UF_RET_SUCCESS )
            {
                if ( UF_GetPacketValue( UF_PACKET_FLAG, packet ) == UF_PROTO_RET_SCAN_SUCCESS )
                {
                    if ( UF_ReceivePacket( packet, 3000 ) == UF_RET_SUCCESS )
                    {
                        if( UF_GetPacketValue( UF_PACKET_FLAG, packet ) == UF_PROTO_RET_SUCCESS )
                        {
                            // TODO: This should return the user ID and sub (fingerprint) ID but it actually returns the sub ID twice.
                            // This isn't a problem as we can match the fingerprint ID with a user using the  user_entry_ids table
                            // (which we have to do with the card reader anyway) but it is somwthing that should probably be
                            // looked into at a later date.
                            UINT32 userId = UF_GetPacketValue( UF_PACKET_PARAM, packet );
                            UINT32 subId = UF_GetPacketValue( UF_PACKET_PARAM, packet );

                            int id = static_cast<int> ( subId );
                            emit autoIdentifySuccess( id );

                            qDebug() << QString( "[FingerprintOperationThread::run] Auto identify success %1 %2" ).arg( userId ).arg( subId );
                            break;
                        }
                    }
                }
            }

            emit autoIdentifyFail();

            break;
        }

        default:
        {
            break;
        }
    }
}
