#ifndef KEYPADCONTROL_H
#define KEYPADCONTROL_H

// Qt.
#include <QObject>

// Project.
#include "hardwarecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  Keypad controller.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT KeypadControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QString entry READ getEntry WRITE setEntry NOTIFY entryChanged )

public:
    explicit KeypadControl( QObject* parent = nullptr );
    virtual ~KeypadControl() = default;

    // Initialisation.
    HARDWARECONTROL_EXPORT static void registerClass();

    // Key entry.
    QString getEntry() const;
    void setEntry( const QString& entry );

    Q_INVOKABLE void invokeKeyEvent( int keyCode );

signals:
    void keyPressed( int keyCode );     ///< Signals a key press.
    void entryChanged();                ///< Signals an entry change.

protected:
    bool eventFilter( QObject* object, QEvent* event );

private:
    static KeypadControl* _keypad;      ///< KeypadControl singleton.

    QString m_entry;                    ///< Text entry.
};

#endif // KEYPADCONTROL_H
