#ifndef COMMSPROTOCOL_H
#define COMMSPROTOCOL_H

// External.
#include <stdint.h>

// Project.
#include "hardwarecontrol_global.h"
#include "subject.h"

////////////////////////////////////////////////////////////////////////////////
/// \enum   Enum representing the protocol commands.
////////////////////////////////////////////////////////////////////////////////
enum ProtocolCommand
{
    NULL_PROTOCOL_CMD = -1,     ///< No protocol command.
    CMD_GET_STATUS,             ///< Get status command.
    CMD_READ_PEG_NUMBERS,       ///< Read peg numbers (command 0).
    CMD_LED_DRIVE,              ///< LED drive (command 1).
    CMD_SOLENOIDS_LOCK,         ///< Solenoids and lock control (command 4).
    CMD_READ_ENVIRONMENT,       ///< Read environment (command 5).
    CMD_READ_SETTINGS,          ///< Read settings (command 9).
    CMD_WRITE_SETTINGS_1,       ///< Write settings (command A), first call.
    CMD_WRITE_SETTINGS_2,       ///< Write settings (command A), second call.
    CMD_INTERFACE_APP_VERSION,  ///< Get interface application version (command B).

    // Add new commands here.

    PROTOCOL_CMD_COUNT          ///< Total number of protocol commands.
};

////////////////////////////////////////////////////////////////////////////////
/// \struct The protocol status.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT ProtocolStatus
{
    uint8_t m_address;
    bool m_bootloader;
    bool m_ledError;
    bool m_e2Error;
    bool m_leftKeySensor;
    bool m_rightKeySensor;
    bool m_buttonPressed;
    bool m_lockOpen;
    bool m_lockUnlatched;
    bool m_reedSwitchOpen;
};

////////////////////////////////////////////////////////////////////////////////
/// \struct Peg.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT Peg
{
    uint8_t m_number[5];
    uint8_t m_signalLevel;
};

////////////////////////////////////////////////////////////////////////////////
/// \struct LED colour.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT Led
{
    uint8_t m_colour1;          ///< Colour 1.
    uint8_t m_colour2;          ///< Colour 2.
};


////////////////////////////////////////////////////////////////////////////////
/// \struct LED state.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT LedState
{
    uint8_t m_sounderState;     ///< Sounder state (on, off or pulse).
    uint8_t m_greenState;       ///< Green state (on, off or flash).
    uint8_t m_redState;         ///< Red state (on, off or flash).
};

////////////////////////////////////////////////////////////////////////////////
/// \struct The environment.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT Environment
{
    uint8_t m_temperature;
    uint8_t m_humidity;
};

////////////////////////////////////////////////////////////////////////////////
/// \struct Read settings.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT ReadSettings
{
    uint8_t m_rfidMinLevel;         ///< RFID minimum level for home detection.
    uint8_t m_solenoidTimeout;      ///< Solenoid timeout * 0.5s.
    bool m_sixthPeg;                ///< Enable sixth peg by setting bit 0.
};

////////////////////////////////////////////////////////////////////////////////
/// \struct Write settings.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT WriteSettings
{
    uint8_t m_location;     ///< Location to write to.
    uint8_t m_data;         ///< Data to write.
};

////////////////////////////////////////////////////////////////////////////////
/// \struct Interface application version.
///         TODO: Consider putting all versions in a single struct.
////////////////////////////////////////////////////////////////////////////////
struct HARDWARECONTROL_EXPORT InterfaceApplicationVersion
{
    uint8_t m_bootloaderMajor = 0;      ///< Bootloader major.
    uint8_t m_bootloaderMinor = 0;      ///< Bootloader minor.
    uint8_t m_applicationMajor = 0;     ///< Application major.
    uint8_t m_applicationMinor = 0;     ///< Application minor.
};

////////////////////////////////////////////////////////////////////////////////
/// \class  Base implementation of the comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT CommsProtocol : public Subject
{
public:
    explicit CommsProtocol();
    ~CommsProtocol();

    // Control.
    virtual bool init();
    ProtocolCommand getCommand() const;

    // Commands.
    virtual void commandGetStatus();
    virtual void commandReadPegNumbers();
    virtual void commandLedDrive();
    virtual void commandSolenoidsLock();
    virtual void commandReadEnvironment();
    virtual void commandReadSettings();
    virtual void commandWriteSettings();
    virtual void commandInterfaceAppVersion();

    // Data.
    ProtocolStatus getStatus() const;

    // Pegs.
    bool getPeg( unsigned int index, Peg& peg ) const;
    bool setPeg( unsigned int index, Peg peg );

    // LEDs.
    bool getLed( unsigned int index, Led& led ) const;
    bool setLed( unsigned int index, Led led );
    void setLedState( LedState ledState );
    LedState getLedState() const;

    // Solenoids and lock.
    uint8_t getSolenoidsLockState() const;
    void setSolenoidsLockState( uint8_t solenoidsLock );

    // Environment.
    Environment getEnvironment() const;

    // Settings.
    void setWriteSettings( WriteSettings writeSettings );
    ReadSettings getReadSettings() const;

    // Versions.
    void setInterfaceAppVersion( InterfaceApplicationVersion version );
    InterfaceApplicationVersion getInterfaceAppVersion() const;

    // Statics.
    HARDWARECONTROL_EXPORT static const unsigned int _numPegs;     ///< Total number of pegs.

    HARDWARECONTROL_EXPORT static uint8_t getColourByte( uint8_t colourLow, uint8_t colourHigh );

protected:
    virtual void write();
    virtual void read();

    // Commands.
    virtual void handleGetStatus();
    virtual void handleReadPegNumbers();
    virtual void handleLedDrive();
    virtual void handleSolenoidsLock();
    virtual void handleReadEnvironment();
    virtual void handleReadSettings();
    virtual void handleWriteSettings();
    virtual void handleInterfaceAppVersion();

    // Commands.
    ProtocolCommand m_command;      ///< Current protocol command.
    uint8_t* m_message;             ///< Command message that will be sent.
    size_t m_messageLength;         ///< Length of the command message.
    uint8_t* m_reply;               ///< Message reply.
    size_t m_replyLength;           ///< Length of the message reply.

    // Data.
    // TODO: This class should probably only retain the data that it receives
    // and everything else should be stored in the controller.
    // For example, the LEDs are set in the message and the reply is just an acknowledgement
    // whereas the get status reply contains the data that needs to be processed.
    ProtocolStatus m_status;                                ///< Current status.
    Peg* m_pegs;                                            ///< Array of pegs.
    Led* m_leds;                                            ///< Array of LEDs (TODO: move to controller?).
    LedState m_ledState;                                    ///< LED state (TODO: move to controller?).
    uint8_t m_solenoidsLock;                                ///< Solenoids and lock states (TODO: move to controller?).
    Environment m_environment;                              ///< Environment.
    ReadSettings m_readSettings;                            ///< Settings read.
    WriteSettings m_writeSettings;                          ///< Settings to write.
    InterfaceApplicationVersion m_interfaceAppVersion;      ///< Interface application versions.
};

#endif // COMMSPROTOCOL_H
