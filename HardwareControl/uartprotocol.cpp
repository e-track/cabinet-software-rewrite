// Project.
#include "uartprotocol.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     UartProtocol::UartProtocol()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
UartProtocol::UartProtocol()
{

}

#ifdef ENABLE_UART
#define EX_UART_NUM UART_NUM_0
#define PATTERN_CHR_NUM (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUFFER_SIZE (1024)
#define RD_BUFFER_SIZE (BUFFER_SIZE)
static QueueHandle_t uart0_queue;
#endif // ENABLE_UART

////////////////////////////////////////////////////////////////////////////////
/// \fn     UartProtocol::eventHandler()
///
/// \brief  Event handler where the data .
////////////////////////////////////////////////////////////////////////////////
void UartProtocol::eventHandler()
{
#ifdef ENABLE_UART
    uart_event_t event;
    size_t buffered_size;

    for( ;; )
    {
        //Waiting for UART event.
        if( xQueueReceive( uart0_queue, (void*) &event, ( portTickType ) portMAX_DELAY ) )
        {
            ESP_LOGI( TAG, "uart[%d] event:", EX_UART_NUM );

            switch( event.type )
            {
                // UART receving data.
                case UART_DATA:
                {
                    // Clear and initialise the reply array.
                    delete m_reply;
                    m_reply = new uint8_t[STATUS_REPLY_SIZE];

                    // Read the data into the array.
                    ESP_LOGI( TAG, "[UART DATA]: %d", event.size );
                    m_replyLength = uart_read_bytes( EX_UART_NUM, m_reply, event.size, portMAX_DELAY );

                    read();

                    // TODO: Do we need to do this write?
//                    ESP_LOGI( TAG, "[DATA EVT]:");
//                    uart_write_bytes( EX_UART_NUM, (const char*) data, event.size );
                    break;
                }

                // HW FIFO overflow detected.
                case UART_FIFO_OVF:
                {
                    ESP_LOGI( TAG, "hw fifo overflow" );

                    // If fifo overflow happened, you should consider adding flow control for your application.
                    // The ISR has already reset the rx FIFO.
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input( EX_UART_NUM );
                    xQueueReset( uart0_queue );
                    break;
                }

                // UART ring buffer full.
                case UART_BUFFER_FULL:
                {
                    ESP_LOGI( TAG, "[uart_event_task] Ring buffer full." );

                    // If buffer full happened, you should consider encreasing your buffer size
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input( EX_UART_NUM );
                    xQueueReset( uart0_queue );
                    break;
                }

                // UART RX break detected.
                case UART_BREAK:
                {
                    ESP_LOGI( TAG, "[uart_event_task] UART rx break." );
                    break;
                }

                // UART parity check error.
                case UART_PARITY_ERR:
                {
                    ESP_LOGI( TAG, "[uart_event_task] UART parity error." );
                    break;
                }

                // UART frame error.
                case UART_FRAME_ERR:
                {
                    ESP_LOGI( TAG, "[uart_event_task] UART frame error." );
                    break;
                }

                default:
                {
                    ESP_LOGI( TAG, "[uart_event_task] UART event type: %d.", event.type );
                    break;
                }
            }
        }
    }

    vTaskDelete( NULL );
#endif // ENABLE_UART
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UartProtocol::init()
///
/// \brief  Intialises the UART comms protocol.
///
/// \return True if initialisation was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool UartProtocol::init()
{
#ifdef ENABLE_UART
    // Set UART log level.
    esp_log_level_set( TAG_UART, ESP_LOG_INFO );

    // Initialize configuration structures using macro initializers.
    uart_config_t uartConfig =
    {
      .baud_rate = 125000,
      .data_bits = UART_DATA_8_BITS,
      .parity = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config( UART_PORT, &uartConfig );

    // Set UART pins(TX: GPIO5, RX: GPIO35, RTS: GPIO18, CTS: GPIO19).
    uart_set_pin( UART_PORT, GPIO_NUM_5, GPIO_NUM_35, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE );

    // Install UART driver using an event queue.
    uart_driver_install( UART_PORT, BUFFER_SIZE, 0, 0, NULL, 0 );

    //Create a task to handler UART event from ISR.
    xTaskCreate( eventHandler, "eventHandler", BUFFER_SIZE, NULL, 12, NULL );
#endif // ENABLE_UART
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     UartProtocol::write()
///
/// \brief  Writes the command using the UART comms.
////////////////////////////////////////////////////////////////////////////////
void UartProtocol::write()
{
#ifdef ENABLE_UART
    char* data = reinterpret_cast<char*> ( &m_command[0] );

    unsigned int bytesWritten = uart_write_bytes( UART_PORT, data, m_command.size() );

    return ( bytesWritten > 0 );
#endif // ENABLE_UART
}
