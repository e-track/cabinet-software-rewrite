#ifndef SIMULATEDPROTOCOL_H
#define SIMULATEDPROTOCOL_H

// Project.
#include "commsprotocol.h"
#include "hardwarecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  Simulated implementation of the comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT SimulatedProtocol : public CommsProtocol
{
public:
    explicit SimulatedProtocol();

    // Control.
    virtual bool init();

    // Commands.
    virtual void commandGetStatus();
    virtual void commandReadPegNumbers();
    virtual void commandLedDrive();
    virtual void commandSolenoidsLock();
    virtual void commandReadEnvironment();
    virtual void commandReadSettings();
    virtual void commandWriteSettings();
    virtual void commandInterfaceAppVersion();

protected:
    virtual void write();

    // Commands.
    virtual void handleGetStatus();
    virtual void handleReadPegNumbers();
    virtual void handleLedDrive();
    virtual void handleSolenoidsLock();
    virtual void handleReadEnvironment();
    virtual void handleReadSettings();
    virtual void handleWriteSettings();
    virtual void handleInterfaceAppVersion();
};

#endif // SIMULATEDPROTOCOL_H
