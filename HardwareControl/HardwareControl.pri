################################################################################
# Config.
################################################################################
QT += quick serialport

CONFIG += c++11

################################################################################
# Dependencies.
################################################################################
include( $$PWD/../DatabaseControl/DatabaseControl.pri )

# Fingerprint reader SDK.
unix {
    LIBS += -L$$(CABINETUI_INSTALLATION)/SFM_SDK/lib/linux -lSFM_SDK
}

win32 {
    LIBS += -L$$(CABINETUI_INSTALLATION)/SFM_SDK/lib/windows/x64 -lSFM_SDK
}

INCLUDEPATH += $$(CABINETUI_INSTALLATION)/SFM_SDK/include

################################################################################
# Library.
################################################################################
if(!build_hardwarecontrol) {
    LIBS += -L$$(CABINETUI_INSTALLATION)/HardwareControl -lHardwareControl

    INCLUDEPATH += $$PWD/..
    DEPENDPATH += $$PWD/..
} else {
################################################################################
# Sources.
################################################################################
SOURCES += \
    $$PWD/cabinetcontroller.cpp \
    $$PWD/cardreadercontroller.cpp \
    $$PWD/commsprotocol.cpp \
    $$PWD/fingerprintcontroller.cpp \
    $$PWD/fingerprintoperationthread.cpp \
    $$PWD/keypadcontrol.cpp \
    $$PWD/serialportprotocol.cpp \
    $$PWD/simulatedprotocol.cpp \
    $$PWD/subject.cpp \
    $$PWD/uartprotocol.cpp

################################################################################
# Headers.
################################################################################
HEADERS += \
    $$PWD/cabinetcontroller.h \
    $$PWD/cardreadercontroller.h \
    $$PWD/cardreaderenums.h \
    $$PWD/commsdefs.h \
    $$PWD/commsprotocol.h \
    $$PWD/fingerprintcontroller.h \
    $$PWD/fingerprintenums.h \
    $$PWD/fingerprintoperationthread.h \
    $$PWD/hardwarecontrol_global.h \
    $$PWD/keypadcontrol.h \
    $$PWD/observer.h \
    $$PWD/serialportprotocol.h \
    $$PWD/simulatedprotocol.h \
    $$PWD/subject.h \
    $$PWD/uartprotocol.h
}
