// Qt.
#include <QDebug>
#include <QSerialPortInfo>

// Project.
#include "commsdefs.h"
#include "serialportprotocol.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::SerialPortProtocol()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
SerialPortProtocol::SerialPortProtocol()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::init()
///
/// \brief  Intialises the serial port protocol.
///
/// \return True if initialisation was successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool SerialPortProtocol::init()
{
    bool success = false;

    // Iterate through available ports.
    for ( const QSerialPortInfo& info : QSerialPortInfo::availablePorts() )
    {
        qDebug() << QString( "--------------------------------------------------" );
        qDebug() << QString( "manufacturer: %1" ).arg( info.manufacturer() );
        qDebug() << QString( "portName: %1" ).arg( info.portName() );
        qDebug() << QString( "productIdentifier: %1" ).arg( info.productIdentifier() );
        qDebug() << QString( "description co: %1" ).arg( info.description() );
        qDebug() << QString( "serialNumber: %1" ).arg( info.serialNumber() );
        qDebug() << QString( "systemLocation: %1" ).arg( info.systemLocation() );
        qDebug() << QString( "vendorIdentifier: %1" ).arg( info.vendorIdentifier() );

        if ( info.manufacturer() == "FTDI" )
        {
            qDebug() << QString( "[SerialPortProtocol::initialise()] Found the comm port." );

            success = openPort( info );
            break;
        }
    }

    if ( success )
    {
        qDebug() << QString( "[SerialPortProtocol::initialise()] Success." );
    }
    else
    {
        qDebug() << QString( "[SerialPortProtocol::initialise()] Fail." );
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::openPort( QSerialPortInfo& info )
///
/// \brief  Opens the port and connects to the serial port signals.
///
/// \param  Details of the serial port to open.
///
/// \return True if the port was opened successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool SerialPortProtocol::openPort( const QSerialPortInfo& info )
{
    m_serialPort = new QSerialPort();
    m_serialPort->setPort( info );
    m_serialPort->setBaudRate( 125000 );

    if ( !m_serialPort )
    {
        qDebug() << QString( "[SerialPortProtocol::openPort] Serial port is null, cannot open port." );
        return false;
    }

    bool success = m_serialPort->isOpen();

    if ( !success )
    {
        success = m_serialPort->open( QIODevice::ReadWrite );

        if ( success )
        {
            QObject::connect( m_serialPort, &QSerialPort::readyRead,     this, &SerialPortProtocol::onReadyRead );
            QObject::connect( m_serialPort, &QSerialPort::errorOccurred, this, &SerialPortProtocol::onError );
            QObject::connect( m_serialPort, &QSerialPort::bytesWritten,  this, &SerialPortProtocol::onBytesWritten );

            qDebug() << QString( "[SerialPortProtocol::openPort] Success." );
        }
        else
        {
            qDebug() << QString( "[SerialPortProtocol::openPort] Error: %1." ).arg( m_serialPort->errorString() );
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::closePort()
///
/// \brief  Closes the port and disconnects from serial port signals.
///
/// \return True if the port was closed successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void SerialPortProtocol::closePort()
{
    if ( !m_serialPort )
    {
        qDebug() << QString( "[SerialPortProtocol::closePort] Serial port is null, cannot close port." );
        return;
    }

    m_serialPort->close();

    QObject::disconnect( m_serialPort, &QSerialPort::readyRead,     this, &SerialPortProtocol::onReadyRead );
    QObject::disconnect( m_serialPort, &QSerialPort::errorOccurred, this, &SerialPortProtocol::onError );
    QObject::disconnect( m_serialPort, &QSerialPort::bytesWritten,  this, &SerialPortProtocol::onBytesWritten );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::write()
///
/// \brief  Writes the command to the Qt serial port.
////////////////////////////////////////////////////////////////////////////////
void SerialPortProtocol::write()
{
    char* data = reinterpret_cast<char*> ( &m_message[0] );

    m_serialPort->write( data, m_messageLength );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::onReadyRead()
///
/// \brief  Handles incoming serial port data.
////////////////////////////////////////////////////////////////////////////////
void SerialPortProtocol::onReadyRead()
{
    qDebug() << QString( "[SerialPortProtocol::onReadyRead] " );

    if ( !m_serialPort )
    {
        qDebug() << QString( "[SerialPortProtocol::onReadyRead] Serial port is null, cannot process data." );
        return;
    }

    // Read all data.
    QByteArray data = m_serialPort->readAll();
    m_replyLength = data.length();

    delete m_reply;
    m_reply = new uint8_t[m_replyLength];

    for ( int i = 0; i < data.length(); ++i )
    {
        m_reply[i] = static_cast<uint8_t> ( data[i] );
    }

    read();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::onError( QSerialPort::SerialPortError error )
///
/// \brief  Handles serial port errors.
///
/// \param  The serial port error.
////////////////////////////////////////////////////////////////////////////////
void SerialPortProtocol::onError( QSerialPort::SerialPortError error )
{
    Q_UNUSED( error );

    if ( !m_serialPort )
    {
        qDebug() << QString( "[SerialPortProtocol::onError] Serial port is null, cannot process error." );
        return;
    }

    qDebug() << QString( "SerialPortProtocol::onError %1 (%2)" )
                    .arg( m_serialPort->errorString(),
                          m_serialPort->portName() );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SerialPortProtocol::onBytesWritten( qint64 bytes )
///
/// \brief  Handles bytes written to the serial port.
///
/// \param  The number of bytes that were written.
////////////////////////////////////////////////////////////////////////////////
void SerialPortProtocol::onBytesWritten( qint64 bytes )
{
    qDebug() << QString( "[SerialPortProtocol::onBytesWritten] Number of bytes written: %1 ").arg( bytes );
}
