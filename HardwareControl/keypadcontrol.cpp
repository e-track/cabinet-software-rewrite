// Qt.
#include <QGuiApplication>
#include <QKeyEvent>
#include <QQuickItem>
#include <QQuickView>
#include <QString>

// Project.
#include "keypadcontrol.h"

// Statics.
KeypadControl* KeypadControl::_keypad = nullptr;

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::KeypadControl( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  The QObject's parent.
////////////////////////////////////////////////////////////////////////////////
KeypadControl::KeypadControl( QObject* parent )
    : QObject( parent )
    , m_entry( "" )
{
    qApp->installEventFilter( this );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::invokeKeyEvent( int keyCode )
///
/// \brief  This will insert a key code into the event system so that the simulator
///         can create key events in the same way the external hardware would.
///         Presently, only a signal is used rather than the actual event system
///         but this will be changed when it's clear how the external devices
///         communicate to the UI.
///
/// \param  The keycode to be inserted into the event system.
////////////////////////////////////////////////////////////////////////////////
void KeypadControl::invokeKeyEvent( int keyCode )
{
    QQuickItem* focusObject = qobject_cast<QQuickItem*> ( QGuiApplication::focusObject() );

    if ( focusObject )
    {
        Qt::KeyboardModifiers modifiers;

        QKeyEvent press = QKeyEvent( QKeyEvent::KeyPress, keyCode, modifiers, QChar::fromLatin1( keyCode ), false );
        QCoreApplication::sendEvent( focusObject, &press);

        QKeyEvent release = QKeyEvent( QKeyEvent::KeyRelease, keyCode, modifiers, QChar::fromLatin1( keyCode ), false );
        QCoreApplication::sendEvent( focusObject, &release);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void KeypadControl::registerClass()
{
    qmlRegisterSingletonType<KeypadControl>( "Keypad",
                                             1,
                                             0,
                                             "Control",
                                             []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        if ( !_keypad )
        {
            _keypad = new KeypadControl();
        }

        return _keypad;
    });
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::getEntry() const
///
/// \brief  Gets the current keypad text entry.
///
/// \return The current keypad text entry.
////////////////////////////////////////////////////////////////////////////////
QString KeypadControl::getEntry() const
{
    return m_entry;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::setEntry( const QString& entry )
///
/// \brief  Sets the current keypad text entry.
///
/// \param  The new text entry (usually this will be an empty string).
////////////////////////////////////////////////////////////////////////////////
void KeypadControl::setEntry( const QString& entry )
{
    if ( m_entry != entry )
    {
        m_entry = entry;

        emit entryChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControl::eventFilter( QObject* object, QEvent* event )
///
/// \brief  Event filter to keyboard events.
///
/// \param  The watched object.
/// \param  The event.
///
/// \return True if the event was handled completely, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool KeypadControl::eventFilter( QObject* object, QEvent* event )
{
    bool handled = false;

    QKeyEvent* keyEvent = dynamic_cast<QKeyEvent*> ( event );

    if ( keyEvent && keyEvent->type() == QEvent::KeyRelease )
    {
        int key = keyEvent->key();

        switch ( key )
        {
            case Qt::Key_Enter:
            case Qt::Key_Return:
            case Qt::Key_F1:
            case Qt::Key_F2:
            case Qt::Key_F3:
            case Qt::Key_F4:
            case Qt::Key_F5:
            case Qt::Key_F6:
            case Qt::Key_F7:
            {
                break;
            }

            case Qt::Key_Backspace:
            {
                m_entry = m_entry.left( m_entry.length() - 1);

                emit entryChanged();
                break;
            }

            default:
            {
                if ( keyEvent->text().length() == 1 )
                {
                    setEntry( m_entry.append( QChar::fromLatin1( key ) ) );

                    emit entryChanged();
                }

                break;
            }
        }

        handled = true;

        emit keyPressed( key );
    }
    else
    {
        handled = QObject::eventFilter( object, event );
    }

    return handled;
}
