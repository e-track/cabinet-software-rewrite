#ifndef UARTPROTOCOL_H
#define UARTPROTOCOL_H

// External.
#ifdef ENABLE_UART
    #include <freertos/FreeRTOS.h>
    #include <freertos/task.h>
    #include <esp_system.h>
    #include <nvs_flash.h>
    #include <driver/gpio.h>
    #include <driver/uart.h>
    #include <freertos/queue.h>
    #include <esp_log.h>
    #include <sdkconfig.h>
#endif // ENABLE_UART

// Project.
#include "commsprotocol.h"
#include "hardwarecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  UART implementation of the comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT UartProtocol : public CommsProtocol
{
public:
    explicit UartProtocol();

    // Control.
    virtual bool init();

protected:
    virtual void write();

    void eventHandler();
};

#endif // UARTPROTOCOL_H
