// Qt.
#include <QDebug>
#include <QQuickItem>
#include <QQuickView>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>
#include <QTimer>

// Project.
#include "cabinetcontroller.h"
#include "serialportprotocol.h"

// Statics.
CabinetController* _cabinetController = nullptr;

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::CabinetController( CommsProtocol* commsProtocol, QObject* parent )
///
/// \brief  Constructor.
///
/// \param  The injected comms protocol to communicate with the pegboard.
/// \param  The QObject's parent.
////////////////////////////////////////////////////////////////////////////////
CabinetController::CabinetController( CommsProtocol* commsProtocol, QObject* parent )
    : QObject( parent )
    , m_pegboardComms( commsProtocol )
    , m_bootloaderVersion( "" )
    , m_applicationVersion( "" )
    , m_numKeySlots( 0 )
    , m_simulating( false )
    , m_internetAccess( false )
    , m_autoGenPin( false )
{
    m_pegboardComms->watch( this );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::~CabinetController()
///
/// \brief  Destructor.
////////////////////////////////////////////////////////////////////////////////
CabinetController::~CabinetController()
{
    if ( m_pegboardComms )
    {
        delete m_pegboardComms;
        m_pegboardComms = nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::registerClass()
{
    qmlRegisterSingletonType<CabinetController>( "HwControl",
                                                 1,
                                                 0,
                                                 "Cabinet",
                                                 []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        return CabinetController::instance();
    });
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::initialise()
///
/// \brief  Starts the initialise process by opening communications to the
///         interface card.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::initialise()
{
    if ( m_pegboardComms->init() )
    {
        m_pegboardComms->commandInterfaceAppVersion();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::enableSimulation( bool enabled )
///
/// \brief  Enables or disables simulation mode.
///
/// \param  True to enable simulation mode, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::enableSimulation( bool enabled )
{
    if ( m_simulating != enabled )
    {
        m_simulating = enabled;

        emit simulationChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::simulationEnabled() const
///
/// \brief  Checks whether simulation mode is enabled.
///
/// \param  True if simulation mode is enabled, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CabinetController::simulationEnabled() const
{
    return m_simulating;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::setNumberOfSlots( int numSlots )
///
/// \brief  Sets the number of key slots available to the cabinet.
///
/// \param  The number of key slots that are available to the cabinet.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::setNumberOfSlots( int numSlots )
{
    m_numKeySlots = numSlots;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::getNumberOfSlots() const
///
/// \brief  Gets the number of key slots in the cabinet.
///
/// \return The number of key slots that are available to the cabinet.
////////////////////////////////////////////////////////////////////////////////
int CabinetController::getNumberOfSlots() const
{
    return m_numKeySlots;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::getInternetAccess() const
///
/// \brief  Checks whether the cabinet has access to the internet.
///
/// \param  True if internet access exists, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CabinetController::getInternetAccess() const
{
    return m_internetAccess;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::getAutoGeneratePin() const
///
/// \brief  Checks whether the cabinet should auto generate PIN during registration.
///         If not then the user will need to enter their own PIN.
///
/// \param  True if the cabinet should auto generate a new PIN during registration.
////////////////////////////////////////////////////////////////////////////////
bool CabinetController::getAutoGeneratePin() const
{
    return m_autoGenPin;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::getBootloaderVersion() const
///
/// \brief  Gets the pegboard bootloader version.
///
/// \return The pegboard bootloader version.
////////////////////////////////////////////////////////////////////////////////
QString CabinetController::getBootloaderVersion() const
{
    return m_bootloaderVersion;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::getApplicationVersion() const
///
/// \brief  Gets the pegboard application version.
///
/// \return The pegboard application version.
////////////////////////////////////////////////////////////////////////////////
QString CabinetController::getApplicationVersion() const
{
    return m_applicationVersion;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::simulateInsertKey( int slot )
///
/// \brief  Simulates a key being inserted into the cabinet.
///
/// \param  The ID of the slot where the key was inserted into.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::simulateInsertKey( int slot )
{
    if ( m_simulating )
    {
        emit keyInserted( slot );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::simulateRemoveKey( int slot )
///
/// \brief  Simulates a key being removed from the cabinet.
///
/// \param  The ID of the slot where the key was removed from.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::simulateRemoveKey( int slot )
{
    if ( m_simulating )
    {
        emit keyRemoved( slot );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::instance()
///
/// \brief  Returns a static instance of CabinetController.
///         TODO: This function should be removed so that the application is
///         forced to choose between a real or simulated pegboard protocol.
///
/// \return A CabinetController instance.
////////////////////////////////////////////////////////////////////////////////
CabinetController* CabinetController::instance()
{
    if ( !_cabinetController )
    {
        SerialPortProtocol* serialPortComms = new SerialPortProtocol();

        _cabinetController = new CabinetController( serialPortComms );
    }

    return _cabinetController;

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::instance( CommsProtocol* commsProtocol )
///
/// \brief  Returns a static instance of CabinetController with an injected comms
///         protocol.
///
/// \param  An injected comms protocol.
///
/// \return A cabinet controller instance with the injected comms protocol.
////////////////////////////////////////////////////////////////////////////////
CabinetController* CabinetController::instance( CommsProtocol* commsProtocol )
{
    if ( !_cabinetController )
    {
        _cabinetController = new CabinetController( commsProtocol );
    }

    return _cabinetController;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::destroy()
///
/// \brief  Destroys the CabinetController instance.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::destroy()
{
    if ( _cabinetController )
    {
        delete _cabinetController;
        _cabinetController = nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::update()
///
/// \brief  Handles notification triggered from the observed subject.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::update()
{
    switch( m_pegboardComms->getCommand() )
    {
        case CMD_INTERFACE_APP_VERSION: { updateInterfaceAppVersion(); break; }

        default:
        {
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetController::updateInterfaceAppVersion()
///
/// \brief  Updates the interface app version.
////////////////////////////////////////////////////////////////////////////////
void CabinetController::updateInterfaceAppVersion()
{
    InterfaceApplicationVersion version = m_pegboardComms->getInterfaceAppVersion();

    m_bootloaderVersion = QString( "%1.%2" ).arg( version.m_bootloaderMajor )
                                            .arg( version.m_bootloaderMinor );
    m_applicationVersion = QString( "%1.%2" ).arg( version.m_applicationMajor )
                                             .arg( version.m_applicationMinor );
    emit versionsChanged();
}

