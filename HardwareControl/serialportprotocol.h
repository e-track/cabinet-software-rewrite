#ifndef SERIALPORTPROTOCOL_H
#define SERIALPORTPROTOCOL_H

// Qt.
#include <QObject>
#include <QSerialPort>

// Project.
#include "commsprotocol.h"
#include "hardwarecontrol_global.h"

// Forward declarations.
class QSerialPortInfo;

////////////////////////////////////////////////////////////////////////////////
/// \class  Qt serial port implementation of the comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT SerialPortProtocol : public QObject, public CommsProtocol
{
    Q_OBJECT

public:
    explicit SerialPortProtocol();

    // Control.
    virtual bool init();

    // Ports.
    bool openPort( const QSerialPortInfo& info );
    void closePort();

signals:
    void cmdReplyPegboardVersion( const QString& bootloaderVersion,
                                  const QString& applicationVersion );

private slots:
    void onReadyRead();
    void onError( QSerialPort::SerialPortError error );
    void onBytesWritten( qint64 bytes );

protected:
    virtual void write();

    // Serial port.
    QSerialPort* m_serialPort;
};

#endif // SERIALPORTPROTOCOL_H
