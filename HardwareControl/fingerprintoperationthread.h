#ifndef FINGERPRINTOPERATIONTHREAD_H
#define FINGERPRINTOPERATIONTHREAD_H

// Qt.
#include <QMutex>
#include <QThread>

// Project.
#include "hardwarecontrol_global.h"

enum Operation
{
    None = -1,
    Enroll,
    Identify,
    AutoIdentify,
    AutoIdentifyEnable,
    AutoIdentifyDisable,
    Verify
};

////////////////////////////////////////////////////////////////////////////////
/// \class  Allows fingerprint operations to be run concurrently.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT FingerprintOperationThread : public QThread
{
    Q_OBJECT

public:
    explicit FingerprintOperationThread( QObject* parent = nullptr );
    virtual ~FingerprintOperationThread() = default;

    // Operation.
    virtual void setOperation( Operation operation );
    virtual Operation getOperation();

    // Friendship.
    friend class MockFingerprintOperationThread;

    // Control.
    virtual void start();

signals:
    // Status.
    void success();
    void fail();

    // Auto identify.
    void autoIdentifySuccess( int fingerprintId );
    void autoIdentifyFail();

protected:
    // Threads.
    virtual void run() override;

private:
    Operation m_operation;          ///< Current operation that is being run.
    QMutex m_mutex;                 ///< Mutex to protect data.
};

#endif // FINGERPRINTOPERATIONTHREAD_H
