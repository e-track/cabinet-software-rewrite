// Qt.
#include <QDebug>
#include <QQuickView>
#include <QSerialPortInfo>
#include <QString>
#include <QTimer>

// Library.
#include <DatabaseControl/databasecontrol.h>

// Project.
#include "cardreadercontroller.h"
#include "cardreaderenums.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::CardreaderController( QObject* parent = nullptr )
///
/// \brief  Constructor.
///
/// \param  The QObject's parent.
////////////////////////////////////////////////////////////////////////////////
CardreaderController::CardreaderController( QObject* parent )
    : QObject( parent )
    , m_swiping( false )
    , m_timer( new QTimer() )
    , m_serialPort( nullptr )
    , m_status( Cardreader::STATUS_NULL )
{
    // Timer.
    m_timer->setInterval( 1000 );
    m_timer->setSingleShot( true );

    connect( m_timer, &QTimer::timeout, this, &CardreaderController::onTimeout );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::registerClass()
{
    qmlRegisterSingletonType<CardreaderController>( "HwControl",
                                                    1,
                                                    0,
                                                    "Cardreader",
                                                    []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        return new CardreaderController();
    });

    qmlRegisterUncreatableMetaObject( Cardreader::staticMetaObject,
                                      "HwControl.Enums",
                                      1,
                                      0,
                                      "Cardreader",
                                      "Cardreader enum error." );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::initialise()
///
/// \brief  Starts the initialise process by opening communications to the
///         card reader.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::initialise()
{
    bool success = false;

    // Iterate through available ports.
    for ( const QSerialPortInfo& info : QSerialPortInfo::availablePorts() )
    {
        qDebug() << QString( "--------------------------------------------------" );
        qDebug() << QString( "[CardreaderController::initialise] Manufacturer: %1" ).arg( info.manufacturer() );
        qDebug() << QString( "[CardreaderController::initialise] PortName: %1" ).arg( info.portName() );
        qDebug() << QString( "[CardreaderController::initialise] ProductIdentifier: %1" ).arg( info.productIdentifier() );
        qDebug() << QString( "[CardreaderController::initialise] Description co: %1" ).arg( info.description() );
        qDebug() << QString( "[CardreaderController::initialise] SerialNumber: %1" ).arg( info.serialNumber() );
        qDebug() << QString( "[CardreaderController::initialise] SystemLocation: %1" ).arg( info.systemLocation() );
        qDebug() << QString( "[CardreaderController::initialise] VendorIdentifier: %1" ).arg( info.vendorIdentifier() );

        if ( info.manufacturer() == "Elatec" )
        {
            m_serialPort = new QSerialPort( this );
            m_serialPort->setPort( info );
            m_serialPort->setBaudRate( QSerialPort::Baud9600 );

            setStatus( Cardreader::STATUS_READY );
            success = true;
            break;
        }
    }

    if ( success )
    {
        emit initialisationSuccess();
    }
    else
    {
        emit initialisationFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::simulateSwipe()
///
/// \brief  Simulates the card reader swipe.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::simulateSwipe()
{
    if ( !m_swiping )
    {
        m_timer->start();
        m_swiping = true;

        emit swipingChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::registerCard()
///
/// \brief  Starts the card registration process.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::registerCard()
{
    if ( openPort() )
    {
        setStatus( Cardreader::STATUS_REGISTER );
    }
    else
    {
        setStatus( Cardreader::STATUS_REGISTER_ERROR );
    }

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::identify( bool enabled )
///
/// \brief  Starts the automatic identification process.
///
/// \param  True to start auto identify.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::identify( bool enabled )
{
    if ( openPort() )
    {
        setStatus( Cardreader::STATUS_IDENTIFY );
    }
    else
    {
        setStatus( Cardreader::STATUS_IDENTIFY_ERROR );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::getStatus() const
///
/// \brief  Gets the status of the card reader.
////////////////////////////////////////////////////////////////////////////////
Cardreader::Status CardreaderController::getStatus()
{
    return m_status;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::isSwiping() const
///
/// \brief  Checks whether a card is being swiped.
///
/// \return True if a card is being swiped, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CardreaderController::isSwiping() const
{
    return m_swiping;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::onTimeout() const
///
/// \brief  Handles timeout.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::onTimeout()
{
    if ( m_swiping )
    {
        m_swiping = false;

        emit swipingChanged();
    }

    emit accepted();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::onReadyRead()
///
/// \brief  Handles incoming serial port data.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::onReadyRead()
{
    if ( !m_serialPort )
    {
        qDebug() << QString( "[CardreaderController::onReadyRead] Serial port is null, cannot process data." );
        return;
    }

    QByteArray data = m_serialPort->readAll();
    QString cardId = data.simplified();

    qDebug() << QString( "[CardreaderController::onReadyRead] Data: %1" )
                    .arg( cardId );

    // TODO: Check the data is correct, ignore anything that isn't as expected.
    if ( cardId.length() == 10 )
    {
        if ( m_status == Cardreader::STATUS_IDENTIFY )
        {
            if ( DatabaseControl::instance()->authenticateUser( -1, cardId ) )
            {
                setStatus( Cardreader::STATUS_IDENTIFY_SUCCESS );
                closePort();

                emit identifySuccess();
            }
        }
        else if ( m_status == Cardreader::STATUS_REGISTER )
        {
            setStatus( Cardreader::STATUS_REGISTER_SUCCESS );
            closePort();

            DatabaseControl::instance()->addCard( cardId );

            emit registerCardSuccess();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::onError( QSerialPort::SerialPortError error )
///
/// \brief  Handles serial port errors.
///
/// \param  The serial port error.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::onError( QSerialPort::SerialPortError error )
{
    if ( !m_serialPort )
    {
        qDebug() << QString( "[CardreaderController::onError] Serial port is null, cannot process error." );
        return;
    }

    if ( error == QSerialPort::ReadError )
    {
        qDebug() << QString( "CardreaderController::onError %1 (%2)" )
                        .arg( m_serialPort->errorString(),
                              m_serialPort->portName() );

        if ( m_status == Cardreader::STATUS_IDENTIFY )
        {
            setStatus( Cardreader::STATUS_IDENTIFY_ERROR );
            emit identifyFail();

            closePort();
        }
        else if ( m_status == Cardreader::STATUS_REGISTER )
        {
            setStatus( Cardreader::STATUS_REGISTER_ERROR );
            emit registerCardFail();

            closePort();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::openPort()
///
/// \brief  Opens the port and connects to the serial port signal.
///
/// \return True if the port was opened successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CardreaderController::openPort()
{
    if ( !m_serialPort )
    {
        qDebug() << QString( "[CardreaderController::openPort] Serial port is null, cannot open port." );
        return false;
    }

    bool success = m_serialPort->isOpen();

    if ( !success )
    {
        success = m_serialPort->open( QIODevice::ReadOnly );

        if ( success )
        {
            connect( m_serialPort, &QSerialPort::readyRead,     this, &CardreaderController::onReadyRead );
            connect( m_serialPort, &QSerialPort::errorOccurred, this, &CardreaderController::onError );
        }
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::closePort()
///
/// \brief  Closes the port and disconnects from serial port signals.
///
/// \return True if the port was closed successfully, otherwise false.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::closePort()
{
    if ( !m_serialPort )
    {
        qDebug() << QString( "[CardreaderController::closePort] Serial port is null, cannot close port." );
        return;
    }

    m_serialPort->close();

    disconnect( m_serialPort, &QSerialPort::readyRead,     this, &CardreaderController::onReadyRead );
    disconnect( m_serialPort, &QSerialPort::errorOccurred, this, &CardreaderController::onError );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderController::setStatus( Cardreader::Status status )
///
/// \brief  Sets the status of the register card operation.
///
/// \param  The new status of the register card operation.
////////////////////////////////////////////////////////////////////////////////
void CardreaderController::setStatus( Cardreader::Status status )
{
    if ( status != m_status )
    {
        m_status = status;

        emit statusChanged();
    }
}

