#include "simulatedprotocol.h"

SimulatedProtocol::SimulatedProtocol()
{

}

bool SimulatedProtocol::init()
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::getStatus()
///
/// \brief  Simulates sending a command to get the status.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandGetStatus()
{
    m_command = CMD_GET_STATUS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandReadPegNumbers()
///
/// \brief  Simulates sending a command to read the peg numbers.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandReadPegNumbers()
{
    m_command = CMD_READ_PEG_NUMBERS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandLedDrive()
///
/// \brief  Simulates sending a command to drive the LEDs.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandLedDrive()
{
    m_command = CMD_LED_DRIVE;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandSolenoidsLock()
///
/// \brief  Simulates sending a command to control solenoids and locks.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandSolenoidsLock()
{
    m_command = CMD_SOLENOIDS_LOCK;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandReadEnvironment()
///
/// \brief  Simulates sending a command to read the environment.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandReadEnvironment()
{
    m_command = CMD_READ_ENVIRONMENT;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandReadSettings()
///
/// \brief  Simulates sending a command to read the settings.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandReadSettings()
{
    m_command = CMD_READ_SETTINGS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandWriteSettings()
///
/// \brief  Simulates sending a command to write the settings.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandWriteSettings()
{
    m_command = ( m_command == CMD_WRITE_SETTINGS_1
                    ? CMD_WRITE_SETTINGS_2
                    : CMD_WRITE_SETTINGS_1 );

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::commandInterfaceAppVersion()
///
/// \brief  Simulates sending a command to get interface application version.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::commandInterfaceAppVersion()
{
    m_command = CMD_INTERFACE_APP_VERSION;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::write()
///
/// \brief  Simulates writing the command.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::write()
{
    // Nothing to write, so simulate a response that needs to be read.
    read();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleGetStatus()
///
/// \brief  Simulates processing the incoming data to handle the get status reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleGetStatus()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleReadPegNumbers()
///
/// \brief  Simulates processing the incoming data to handle the read peg numbers reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleReadPegNumbers()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleLedDrive()
///
/// \brief  Simulates processing the incoming data to handle the LED drive reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleLedDrive()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleSolenoidsLock()
///
/// \brief  Simulates processing the incoming data to handle the solenoids and lock reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleSolenoidsLock()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleReadEnvironment()
///
/// \brief  Simulates processing the incoming data to handle the read environment reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleReadEnvironment()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleReadSettings()
///
/// \brief  Simulates processing the incoming data to handle the read settings reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleReadSettings()
{
    notify();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleWriteSettings()
///
/// \brief  Simulates processing the incoming data to handle the write settings reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleWriteSettings()
{
    // The write settings command needs to run twice, firstly with normal
    // data, secondly with inverted data.
    if ( m_command == CMD_WRITE_SETTINGS_1 )
    {
        commandWriteSettings();
    }
    else
    {
        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SimulatedProtocol::handleInterfaceAppVersion()
///
/// \brief  Simulates processing the incoming data to handle the interface application
///         version reply.
////////////////////////////////////////////////////////////////////////////////
void SimulatedProtocol::handleInterfaceAppVersion()
{
    notify();
}
