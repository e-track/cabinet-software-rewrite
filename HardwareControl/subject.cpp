// Project.
#include "observer.h"
#include "subject.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     Subject::Subject()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
Subject::Subject()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Subject::connect( Observer* observer )
///
/// \brief  Connects the given observer to the subject so that it can be
///         notified of any changes.
///
/// \param  The observer.
////////////////////////////////////////////////////////////////////////////////
void Subject::watch( Observer* observer )
{
    m_observers.push_back( observer );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Subject::disconnect( Observer* observer )
///
/// \brief  Disconnects the given observer from the subject so that it will no
///         longer be notified of any changes.
///
/// \param  The observer.
////////////////////////////////////////////////////////////////////////////////
void Subject::unwatch( Observer* observer )
{
    m_observers.remove( observer );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     Subject::notify()
///
/// \brief  Notifies all observers that a change has occurred.
////////////////////////////////////////////////////////////////////////////////
void Subject::notify()
{
    for ( auto const& it : m_observers )
    {
        it->update();
    }
}
