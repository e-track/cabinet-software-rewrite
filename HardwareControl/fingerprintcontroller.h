#ifndef FINGERPRINTCONTROLLER_H
#define FINGERPRINTCONTROLLER_H

// Qt.
#include <QObject>
#include <QThread>
#include <QTimer>

// Library.
#include <UF_API.h>

// Project.
#include "fingerprintenums.h"
#include "fingerprintoperationthread.h"
#include "hardwarecontrol_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  Fingerprint reader controller.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT FingerprintController : public QObject
{
    Q_OBJECT
    Q_PROPERTY( Fingerprint::Status status READ getStatus NOTIFY statusChanged );

public:
    virtual ~FingerprintController();

    // Registration.
    HARDWARECONTROL_EXPORT static void registerClass();

    // Initialisation.
    HARDWARECONTROL_EXPORT Q_INVOKABLE void initialise( bool deleteTemplates = false );
    HARDWARECONTROL_EXPORT Q_INVOKABLE static void sdkVersion();

    // Simulation.
    Q_INVOKABLE void simulatePressed();
    Q_INVOKABLE void simulateReleased();

    // Operation.
    Q_INVOKABLE void enroll();
    Q_INVOKABLE void identify();
    Q_INVOKABLE void autoIdentify( bool enabled );
    Q_INVOKABLE void verify();

    // Enroll.
    Fingerprint::Status getStatus();

    // Delete.
    bool deleteAll();

    // Friendship.
    friend class FingerprintControllerTest;

    // Statics.
    HARDWARECONTROL_EXPORT static FingerprintController* instance();
    HARDWARECONTROL_EXPORT static FingerprintController* instance( FingerprintOperationThread* threadedOps );
    HARDWARECONTROL_EXPORT static void destroy();

signals: // TODO: Consider whether we need all of these. Just accessing the status may be sufficient so extend that enum instead.
    // Initialisation.
    void initialisationFail();
    void initialisationSuccess();

    // Status.
    void statusChanged();

    // Enroll.
    void enrollFail();
    void enrollSuccess();

    // Identify.
    void identifyFail();
    void identifySuccess();
    void autoIdentifyFail();
    void autoIdentifySuccess();
    void autoIdentifyEnableFail();
    void autoIdentifyEnableSuccess();
    void autoIdentifyDisableFail();
    void autoIdentifyDisableSuccess();

    // Verify.
    void verifyFail();
    void verifySuccess();

private slots:
    // Simulation.
    void onTimeout();

    // Status.
    void onSuccess();
    void onFail();

    void onAutoIdentifySuccess( int id );
    void onAutoIdentifyFail();

private:
    explicit FingerprintController( FingerprintOperationThread* threadedOps, QObject* parent = nullptr );

    // Enroll.
    void setStatus( Fingerprint::Status status );

    QTimer m_timer;                                     ///< Timer used for simulations.
    FingerprintOperationThread* m_threadedOps;          ///< Threaded operations.
    Fingerprint::Status m_status;                       ///< The current enroll status.

    // TODO: Use a member for non-threaded API calls to facilitate unit testing.

    // Statics.
    static void callback_UF_Enroll(unsigned char returnCode, UF_ENROLL_MODE enrollMode, int enrollSuccessCount);

    HARDWARECONTROL_EXPORT static int _majorVersion;    ///< SDK major version.
    HARDWARECONTROL_EXPORT static int _minorVersion;    ///< SDK minor version.
    HARDWARECONTROL_EXPORT static int _extraVersion;    ///< SDK extra version.
};

#endif // FINGERPRINTCONTROLLER_H
