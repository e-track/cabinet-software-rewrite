// External.
#include <stdexcept>

// Project.
#include "commsdefs.h"
#include "commsprotocol.h"

const unsigned int CommsProtocol::_numPegs = 5;

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::CommsProtocol()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
CommsProtocol::CommsProtocol()
    : Subject()
    , m_command( NULL_PROTOCOL_CMD )
    , m_message( nullptr )
    , m_messageLength( 0 )
    , m_reply( nullptr )
    , m_replyLength( 0 )
    , m_status( ProtocolStatus() )
    , m_pegs( new Peg[_numPegs] )
    , m_leds( new Led[_numPegs] )
    , m_ledState( { 0, 0, 0 } )
    , m_solenoidsLock( 0 )
    , m_environment( { 0, 0 } )
    , m_readSettings( { 0, 0, false } )
    , m_writeSettings( { 0, 0 } )
{
    for ( unsigned int i = 0; i < _numPegs; ++i )
    {
        m_pegs[i] = {};
        m_leds[i] = {};
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::~CommsProtocol()
///
/// \brief  Destructor.
////////////////////////////////////////////////////////////////////////////////
CommsProtocol::~CommsProtocol()
{
    delete[] m_pegs;
    m_pegs = nullptr;

    delete[] m_leds;
    m_leds = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::init()
///
/// \brief  Initialises the comms protocol. This should be overridden to provide
///         specific protocol initialisation.
///
/// \return Always false.
////////////////////////////////////////////////////////////////////////////////
bool CommsProtocol::init()
{
    throw std::runtime_error( "[CommsProtocol::init] Should not be called in the base class, please use derived implementation." );

    return false;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getCommand() const
///
/// \brief  Gets the current command.
///
/// \return The current command being run.
////////////////////////////////////////////////////////////////////////////////
ProtocolCommand CommsProtocol::getCommand() const
{
    return m_command;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getStatus()
///
/// \brief  Sends a command to get the status.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandGetStatus()
{
    delete m_message;

    m_messageLength = STATUS_SIZE;
    m_message = new uint8_t[m_messageLength];
    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( STATUS_ADDRESS_MASK | 1 );

    m_command = CMD_GET_STATUS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandReadPegNumbers()
///
/// \brief  Sends a command to read the peg numbers.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandReadPegNumbers()
{
    delete m_message;

    m_messageLength = READ_PEG_NUMBERS_SIZE;
    m_message = new uint8_t[m_messageLength];
    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = READ_PEG_NUMBERS_COMMAND;

    m_command = CMD_READ_PEG_NUMBERS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandLedDrive()
///
/// \brief  Sends a command to drive the LEDs.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandLedDrive()
{
    delete m_message;

    m_messageLength = LED_DRIVE_SIZE;
    m_message = new uint8_t[m_messageLength];
    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = LED_DRIVE_COMMAND;

    // TODO: Get these values from an LED struct.
    m_message[LED_DRIVE_BYTE_PEG_1_COLOUR] = CommsProtocol::getColourByte( m_leds[0].m_colour1, m_leds[0].m_colour2 );
    m_message[LED_DRIVE_BYTE_PEG_2_COLOUR] = CommsProtocol::getColourByte( m_leds[1].m_colour1, m_leds[1].m_colour2 );
    m_message[LED_DRIVE_BYTE_PEG_3_COLOUR] = CommsProtocol::getColourByte( m_leds[2].m_colour1, m_leds[2].m_colour2 );
    m_message[LED_DRIVE_BYTE_PEG_4_COLOUR] = CommsProtocol::getColourByte( m_leds[3].m_colour1, m_leds[3].m_colour2 );
    m_message[LED_DRIVE_BYTE_PEG_5_COLOUR] = CommsProtocol::getColourByte( m_leds[4].m_colour1, m_leds[4].m_colour2 );
    m_message[LED_DRIVE_BYTE_FLASH_SOUND] = ( m_ledState.m_sounderState | m_ledState.m_greenState | m_ledState.m_redState );
    m_message[LED_DRIVE_BYTE_CRC_HIGH] = 0;     // TODO: Check.
    m_message[LED_DRIVE_BYTE_CRC_LOW] = 0;      // TODO: Check.

    m_command = CMD_LED_DRIVE;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandSolenoidsLock()
///
/// \brief  Sends a command to control solenoids and locks.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandSolenoidsLock()
{
    delete m_message;

    m_messageLength = SOLENOIDS_LOCK_SIZE;
    m_message = new uint8_t[m_messageLength];
    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = SOLENOIDS_LOCK_COMMAND;

    m_message[SOLENOIDS_LOCK_BYTE_RELEASE] = m_solenoidsLock;
    m_message[SOLENOIDS_LOCK_BYTE_CRC_HIGH] = 0;     // TODO: Check.
    m_message[SOLENOIDS_LOCK_BYTE_CRC_LOW] = 0;      // TODO: Check.

    m_command = CMD_SOLENOIDS_LOCK;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandReadEnvironment()
///
/// \brief  Sends a command to read the environment.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandReadEnvironment()
{
    delete m_message;

    m_messageLength = READ_ENVIRONMENT_SIZE;
    m_message = new uint8_t[m_messageLength];
    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = READ_ENVIRONMENT_COMMAND;

    m_command = CMD_READ_ENVIRONMENT;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandReadSettings()
///
/// \brief  Sends a command to read the settings.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandReadSettings()
{
    delete m_message;

    m_messageLength = READ_SETTINGS_SIZE;
    m_message = new uint8_t[m_messageLength];

    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = READ_SETTINGS_COMMAND;

    m_command = CMD_READ_SETTINGS;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandWriteSettings()
///
/// \brief  Sends a command to write the settings.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandWriteSettings()
{
    delete m_message;

    m_messageLength = WRITE_SETTINGS_SIZE;
    m_message = new uint8_t[m_messageLength];

    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = WRITE_SETTINGS_COMMAND;

    // If the first command has been sent then invert the location and data.
    uint8_t location = m_command == CMD_WRITE_SETTINGS_1
                            ? ~m_writeSettings.m_location
                            : m_writeSettings.m_location;

    uint8_t data = m_command == CMD_WRITE_SETTINGS_1
                        ? ~m_writeSettings.m_data
                        : m_writeSettings.m_data;

    m_message[WRITE_SETTINGS_BYTE_LOCATION] = location;
    m_message[WRITE_SETTINGS_BYTE_DATA] = data;
    m_message[WRITE_SETTINGS_BYTE_CRC_HIGH] = 0;    // TODO: Check.
    m_message[WRITE_SETTINGS_BYTE_CRC_LOW] = 0;     // TODO: Check.

    m_command = ( m_command == CMD_WRITE_SETTINGS_1
                    ? CMD_WRITE_SETTINGS_2
                    : CMD_WRITE_SETTINGS_1 );

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::commandInterfaceAppVersion()
///
/// \brief  Sends a command to get interface application version.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::commandInterfaceAppVersion()
{
    delete m_message;

    m_messageLength = APP_VERSION_SIZE;
    m_message = new uint8_t[m_messageLength];

    m_message[COMMAND_BYTE_START] = START_CODE;
    m_message[COMMAND_BYTE_ADDRESS] = ( COMMAND_ADDRESS_MASK | 1 );
    m_message[COMMAND_BYTE_COMMAND] = APP_VERSION_COMMAND;

    m_command = CMD_INTERFACE_APP_VERSION;

    write();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getStatus() const
///
/// \brief  Gets the current comms status.
///
/// \return The current status of the comms.
////////////////////////////////////////////////////////////////////////////////
ProtocolStatus CommsProtocol::getStatus() const
{
    return m_status;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getPeg( unsigned int index ) const
///
/// \brief  Gets the peg at the given index.
///
/// \param  The index of the peg to get.
/// \param  The peg at the index (unchanged if the index is out of bounds).
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CommsProtocol::getPeg( unsigned int index, Peg& peg ) const
{
    bool success = false;

    if ( index < _numPegs )
    {
        peg = m_pegs[ index ];
        success = true;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setPeg( unsigned int index, Peg peg )
///
/// \brief  Sets the LED data at the given index.
///
/// \param  The index of the LED that will be changed.
/// \param  The LED state (colour, flashing mode etc).
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CommsProtocol::setPeg( unsigned int index, Peg peg )
{
    bool success = false;

    if ( index < _numPegs )
    {
        m_pegs[index] = peg;
        success = true;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getLed( unsigned int index ) const
///
/// \brief  Gets the LED at the given index.
///         TODO: It might be acceptable to just send the LED drive command and
///         not worry about the LED states. If so, we can remove this later.
///
/// \param  The index of the LED to get.
/// \param  The LED at the index (unchanged if the index is out of bounds).
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CommsProtocol::getLed( unsigned int index, Led& led ) const
{
    bool success = false;

    if ( index < _numPegs )
    {
        led = m_leds[ index ];
        success = true;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setLed( unsigned int index, Led led )
///
/// \brief  Sets the LED data at the given index.
///
/// \param  The index of the LED that will be changed.
/// \param  The LED state (colour, flashing mode etc).
///
/// \return True if successful, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool CommsProtocol::setLed( unsigned int index, Led led )
{
    bool success = false;

    if ( index < _numPegs )
    {
        m_leds[index] = led;
        success = true;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setLedState( LedState ledState )
///
/// \brief  Sets the LED state.
///
/// \param  The new LED state.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::setLedState( LedState ledState )
{
    m_ledState = ledState;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getLedState() const
///
/// \brief  Gets the LED state.
///
/// \return The LED state.
////////////////////////////////////////////////////////////////////////////////
LedState CommsProtocol::getLedState() const
{
    return m_ledState;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getSolenoidsLockState() const
///
/// \brief  Gets the solenoids and lock state.
///
/// \return The solenoids and lock state.
////////////////////////////////////////////////////////////////////////////////
uint8_t CommsProtocol::getSolenoidsLockState() const
{
    return m_solenoidsLock;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setSolenoidsLockState( uint8_t solenoidsLock )
///
/// \brief  Sets the solenoids and lock state.
///
/// \param  The new solenoids and lock state.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::setSolenoidsLockState( uint8_t solenoidsLock )
{
    m_solenoidsLock = solenoidsLock;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getEnvironment() const
///
/// \brief  Gets the environment state.
///
/// \return The environment state.
////////////////////////////////////////////////////////////////////////////////
Environment CommsProtocol::getEnvironment() const
{
    return m_environment;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setWriteSettings( WriteSettings settings )
///
/// \brief  Sets the settings to write.
///
/// \param  The new settings to be written.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::setWriteSettings( WriteSettings writeSettings )
{
    m_writeSettings = writeSettings;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getReadSettings() const
///
/// \brief  Gets the settings that were read.
///
/// \return The settings that were read.
////////////////////////////////////////////////////////////////////////////////
ReadSettings CommsProtocol::getReadSettings() const
{
    return m_readSettings;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::setInterfaceAppVersion( InterfaceApplicationVersion version )
///
/// \brief  Sets the interface application version.
///
/// \param  The new interface application version.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::setInterfaceAppVersion( InterfaceApplicationVersion version )
{
    m_interfaceAppVersion = version;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getInterfaceAppVersion() const
///
/// \brief  Gets the interface application version.
///
/// \return The interface application version.
////////////////////////////////////////////////////////////////////////////////
InterfaceApplicationVersion CommsProtocol::getInterfaceAppVersion() const
{
    return m_interfaceAppVersion;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::write()
///
/// \brief  Writes data to the comms device. This should be overridden to provide
///         specific protocol initialisation so should never be called.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::write()
{
    throw std::runtime_error( "[CommsProtocol::write] Should not be called in the base class, please use derived implementation." );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::read()
///
/// \brief  Processes the data that has been read and converts it to useful
///         data structures.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::read()
{
    switch( m_command )
    {
        case CMD_GET_STATUS:            { handleGetStatus();           break; }
        case CMD_READ_PEG_NUMBERS:      { handleReadPegNumbers();      break; }
        case CMD_LED_DRIVE:             { handleLedDrive();            break; }
        case CMD_SOLENOIDS_LOCK:        { handleSolenoidsLock();       break; }
        case CMD_READ_ENVIRONMENT:      { handleReadEnvironment();     break; }
        case CMD_READ_SETTINGS:         { handleReadSettings();        break; }
        case CMD_WRITE_SETTINGS_1:      { handleWriteSettings();       break; }
        case CMD_INTERFACE_APP_VERSION: { handleInterfaceAppVersion(); break; }

        default:
        {
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleGetStatus()
///
/// \brief  Processes the incoming data to handle the get status reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleGetStatus()
{
    if ( m_reply != nullptr && m_replyLength < STATUS_REPLY_SIZE )
    {
        // Byte 1.
        m_status.m_address = m_reply[REPLY_BYTE_ADDRESS];

        // Byte 2.
        m_status.m_bootloader = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_BOOTLOADER;
        m_status.m_ledError = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_LED_ERROR;
        m_status.m_e2Error = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_E2_ERROR;
        m_status.m_leftKeySensor = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_LEFT_KEY_HIGH;
        m_status.m_rightKeySensor = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_RIGHT_KEY_HIGH;
        m_status.m_buttonPressed = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_BUTTON_PRESSED;
        m_status.m_lockOpen = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_LOCK_OPEN;
        m_status.m_lockUnlatched = m_reply[STATUS_BYTE_STATUS] | STATUS_BIT_LOCK_UNLATCHED;

        // Byte 3.
        m_status.m_reedSwitchOpen = m_reply[STATUS_BYTE_REED_SWITCH] | STATUS_BIT_REED_SWITCH;

        // Byte 4 (unused).

        // Bytes 5 and 6 (TODO: CRC check).
        uint8_t crcHigh = m_reply[STATUS_BYTE_CRC_HIGH];
        uint8_t crcLow = m_reply[STATUS_BYTE_CRC_LOW];

        (void) crcHigh;
        (void) crcLow;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleReadPegNumbers()
///
/// \brief  Processes the incoming data to handle the read peg numbers reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleReadPegNumbers()
{
    if ( m_reply != nullptr && m_replyLength < READ_PEG_NUMBERS_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        // Bytes 2 to 31.
        for ( int i = 0; i < _numPegs; ++i )
        {
            int index = i * 6;

            m_pegs[i].m_number[0] = m_reply[index + 0];
            m_pegs[i].m_number[1] = m_reply[index + 1];
            m_pegs[i].m_number[2] = m_reply[index + 2];
            m_pegs[i].m_number[3] = m_reply[index + 3];
            m_pegs[i].m_number[4] = m_reply[index + 4];
            m_pegs[i].m_signalLevel = m_reply[index + 5];
        }

        // Bytes 32 and 33 (TODO: CRC check).
        uint8_t crcHigh = m_reply[READ_PEG_NUMBERS_BYTE_CRC_HIGH];
        uint8_t crcLow = m_reply[READ_PEG_NUMBERS_BYTE_CRC_LOW];

        (void) crcHigh;
        (void) crcLow;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleLedDrive()
///
/// \brief  Processes the incoming data to handle the LED drive reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleLedDrive()
{
    if ( m_reply != nullptr && m_replyLength < LED_DRIVE_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleSolenoidsLock()
///
/// \brief  Processes the incoming data to handle the solenoids and lock reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleSolenoidsLock()
{
    if ( m_reply != nullptr && m_replyLength < SOLENOIDS_LOCK_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleReadEnvironment()
///
/// \brief  Processes the incoming data to handle the read environment reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleReadEnvironment()
{
    if ( m_reply != nullptr && m_replyLength < READ_ENVIRONMENT_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        // Bytes 2 and 3.
        m_environment.m_temperature = m_reply[READ_ENVIRONMENT_BYTE_TEMPERATURE];
        m_environment.m_humidity = m_reply[READ_ENVIRONMENT_BYTE_HUMIDITY];

        // Bytes 32 and 33 (TODO: CRC check).
        uint8_t crcHigh = m_reply[READ_ENVIRONMENT_BYTE_CRC_HIGH];
        uint8_t crcLow = m_reply[READ_ENVIRONMENT_BYTE_CRC_LOW];

        (void) crcHigh;
        (void) crcLow;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleReadSettings()
///
/// \brief  Processes the incoming data to handle the read settings reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleReadSettings()
{
    if ( m_reply != nullptr && m_replyLength < READ_SETTINGS_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        // Bytes 2 to 4.
        m_readSettings.m_rfidMinLevel = m_reply[READ_SETTINGS_BYTE_RFID_MIN_LEVEL];
        m_readSettings.m_solenoidTimeout = m_reply[READ_SETTINGS_BYTE_SOLENOID_TIMEOUT];
        m_readSettings.m_sixthPeg = ( m_reply[READ_SETTINGS_BYTE_SIXTH_PEG] & READ_SETTINGS_BIT_SIXTH_PEG );

        // Bytes 18 and 19 (TODO: CRC check).
        uint8_t crcHigh = m_reply[READ_SETTINGS_BYTE_CRC_HIGH];
        uint8_t crcLow = m_reply[READ_SETTINGS_BYTE_CRC_LOW];

        (void) crcHigh;
        (void) crcLow;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleWriteSettings()
///
/// \brief  Processes the incoming data to handle the write settings reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleWriteSettings()
{
    if ( m_reply != nullptr && m_replyLength < WRITE_SETTINGS_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        // The write settings command needs to run twice, firstly with normal
        // data, secondly with inverted data.
        if ( m_command == CMD_WRITE_SETTINGS_1 )
        {
            commandWriteSettings();
        }
        else
        {
            notify();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::handleInterfaceAppVersion()
///
/// \brief  Processes the incoming data to handle the interface application
///         version reply.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocol::handleInterfaceAppVersion()
{
    if ( m_reply != nullptr && m_replyLength == APP_VERSION_REPLY_SIZE )
    {
        // Byte 1.
        uint8_t address = m_reply[REPLY_BYTE_ADDRESS];
        (void) address;

        m_interfaceAppVersion.m_bootloaderMajor = m_reply[APP_VERSION_BYTE_BOOTLOADER_HIGH];
        m_interfaceAppVersion.m_bootloaderMinor = m_reply[APP_VERSION_BYTE_BOOTLOADER_LOW];

        m_interfaceAppVersion.m_applicationMajor = m_reply[APP_VERSION_BYTE_APPLICATION_HIGH];
        m_interfaceAppVersion.m_applicationMinor = m_reply[APP_VERSION_BYTE_APPLICATION_LOW];

        // Bytes 18 and 19 (TODO: CRC check).
        uint8_t crcHigh = m_reply[APP_VERSION_BYTE_CRC_HIGH];
        uint8_t crcLow = m_reply[APP_VERSION_BYTE_CRC_LOW];

        (void) crcHigh;
        (void) crcLow;

        notify();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocol::getColourByte( uint8_t colourLow, uint8_t colourHigh )
///
/// \brief  Builds a single byte from two bytes to represent LED colours.
///         TODO: Consider moving this to a colour class.
///
/// \param  The first colour.
/// \param  The second colour.
///
/// \return The single byte containing both colours.
////////////////////////////////////////////////////////////////////////////////
uint8_t CommsProtocol::getColourByte( uint8_t colourLow, uint8_t colourHigh )
{
    return colourLow | ( colourHigh << 4 );
}
