// Qt.
#include <QDebug>
#include <QQuickView>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>

// Library.
#include <DatabaseControl/databasecontrol.h>

// Project.
#include "fingerprintcontroller.h"

// Statics.
FingerprintController* _fingerprintControl = nullptr;

int FingerprintController::_majorVersion = -1;
int FingerprintController::_minorVersion = -1;
int FingerprintController::_extraVersion = -1;

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::FingerprintController( QObject* parent = nullptr )
///
/// \brief  Constructor.
///
/// \param  An injected threaded operations.
/// \param  The QObject's parent.
////////////////////////////////////////////////////////////////////////////////
FingerprintController::FingerprintController( FingerprintOperationThread* threadedOps, QObject* parent )
    : QObject( parent )
    , m_threadedOps( threadedOps )
    , m_status( Fingerprint::STATUS_NULL )
{
    // Timer.
    m_timer.setInterval( 2000 );
    m_timer.setSingleShot( true );

    connect( &m_timer,       &QTimer::timeout,                     this, &FingerprintController::onTimeout );
    connect(  m_threadedOps, &FingerprintOperationThread::success, this, &FingerprintController::onSuccess );
    connect(  m_threadedOps, &FingerprintOperationThread::fail,    this, &FingerprintController::onFail );

    // Auto identify.
    connect(  m_threadedOps, &FingerprintOperationThread::autoIdentifySuccess, this, &FingerprintController::onAutoIdentifySuccess );
    connect(  m_threadedOps, &FingerprintOperationThread::autoIdentifyFail,    this, &FingerprintController::onAutoIdentifyFail );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
FingerprintController::~FingerprintController()
{
    delete m_threadedOps;
    m_threadedOps = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::registerClass()
///
/// \brief  Registers the class with the QML engine.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::registerClass()
{
    qmlRegisterSingletonType<FingerprintController>( "HwControl",
                                                     1,
                                                     0,
                                                     "Fingerprint",
                                                     []( QQmlEngine* engine, QJSEngine* jsEngine ) -> QObject*
    {
        Q_UNUSED( engine )
        Q_UNUSED( jsEngine )

        return FingerprintController::instance();
    });

    qmlRegisterUncreatableMetaObject( Fingerprint::staticMetaObject,
                                      "HwControl.Enums",
                                      1,
                                      0,
                                      "Fingerprint",
                                      "Fingerprint enum error." );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::simulatePressed()
///
/// \brief  Simulates the fingerprint reader being pressed.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::simulatePressed()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::simulateReleased()
///
/// \brief  Simulates the fingerprint reader being released.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::simulateReleased()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::initialise()
///
/// \brief  Starts the initialise process by opening communications to the
///         fingerprint reader.
///
/// \param  Indicates whether or not all saved templates should be cleared.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::initialise( bool deleteTemplates )
{
    bool commPortSuccess = false;
    bool deleteAllSuccess = true;   // Assume true for the optional operation.
    bool enrollModeSuccess = false;

    for ( const QSerialPortInfo& info : QSerialPortInfo::availablePorts() )
    {
        // The fingerprint reader uses a generic USB device so we can't use any of the QSerialPortInfo properties.
        // However, the card reader is Elatec and the pegboard is FTDI so we can skip those ports if we find them.
        if ( info.manufacturer() == "Elatec" ||
             info.manufacturer() == "FTDI")
        {
            continue;
        }

        // Comm port.
        int baudRate = QSerialPort::Baud115200;
        bool asciiMode = false;

        commPortSuccess = ( UF_InitCommPort( info.portName().toStdString().c_str(), baudRate, asciiMode ) == UF_RET_SUCCESS );

        if ( commPortSuccess )
        {
            qDebug() << QString( "[FingerprintController::initialise] Init comm port success." );

            // Delete all templates.
            if ( deleteTemplates )
            {
                deleteAllSuccess = deleteAll();
            }

            // Enroll mode.
            UINT32 enrollMode = UF_ENROLL_ONE_TIME;
            enrollModeSuccess = ( UF_SetSysParameter( UF_SYS_ENROLL_MODE, enrollMode ) == UF_RET_SUCCESS );

            if ( enrollModeSuccess )
            {
                qDebug() << QString( "[FingerprintController::initialise] Set enroll mode successful." );
            }
            else
            {
                qDebug() << QString( "[FingerprintController::initialise] Set enroll mode failed." );
            }
        }
        else
        {
            qDebug() << QString( "[FingerprintController::initialise] Init comm port fail.");
        }
    }

    if ( commPortSuccess &&
         deleteAllSuccess &&
         enrollModeSuccess )
    {
        emit initialisationSuccess();
    }
    else
    {
        emit initialisationFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::enroll()
///
/// \brief  Starts the enroll process to register a fingerprint.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::enroll()
{
    UF_SetEnrollCallback( callback_UF_Enroll );
    setStatus( Fingerprint::STATUS_ENROLL );

    m_threadedOps->setOperation( Operation::Enroll );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::identify()
///
/// \brief  Starts the identify process to find a match for the fingerprint.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::identify()
{
    setStatus( Fingerprint::STATUS_IDENTIFY );

    m_threadedOps->setOperation( Operation::Identify );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::autoIdentify()
///
/// \brief  If enabled then this starts the process to automatically listen and
///         find a match for the fingerprint otherwise the feature is disabled.
///
/// \param  True to enable auto identify mode.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::autoIdentify( bool enabled )
{
    if ( enabled )
    {
        m_threadedOps->setOperation( Operation::AutoIdentifyEnable );
    }
    else
    {
        m_threadedOps->setOperation( Operation::AutoIdentifyDisable );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::verify()
///
/// \brief  Starts the verify process to check the fingerprint matchs the given
///         details.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::verify()
{
    setStatus( Fingerprint::STATUS_VERIFY );

    m_threadedOps->setOperation( Operation::Verify );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::setStatus( Status status )
///
/// \brief  Sets the status of the current operation.
///
/// \param  The new status of the current operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::setStatus( Fingerprint::Status status )
{
    if ( m_status != status )
    {
        m_status = status;

        emit statusChanged();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::getStatus()
///
/// \brief  Gets the status of the current operation.
///
/// \return The status of the current operation.
////////////////////////////////////////////////////////////////////////////////
Fingerprint::Status FingerprintController::getStatus()
{
    return m_status;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::deleteAll()
///
/// \brief  Deletes all fingerprint templates on the reader.
///
/// \return True if the templates were deleted, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool FingerprintController::deleteAll()
{
    bool success = ( UF_DeleteAll() == UF_RET_SUCCESS );

    if ( success )
    {
        qDebug() << QString( "[FingerprintController::deleteAll] Success." );
    }
    else
    {
        qDebug() << QString( "[FingerprintController::deleteAll] Failed." );
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::sdkVersion()
///
/// \brief  Gets the SDK version.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::sdkVersion()
{
    UF_GetSDKVersion( &_majorVersion, &_minorVersion, &_extraVersion );

    qDebug() << QString( "SFM SDK version: %1.%2.%3" ).arg( _majorVersion )
                                                      .arg( _minorVersion )
                                                      .arg( _extraVersion );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::onTimeout() const
///
/// \brief  Handles timeout.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::onTimeout()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::onSuccess()
///
/// \brief  Handles successful threaded operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::onSuccess()
{
    Operation operation = m_threadedOps->getOperation();

    m_threadedOps->setOperation( Operation::None );

    switch ( operation )
    {
        case Operation::Enroll:
        {
            emit enrollSuccess();
            setStatus( Fingerprint::STATUS_ENROLL_SUCCESS );
            break;
        }

        case Operation::Identify:
        {
            emit identifySuccess();
            setStatus( Fingerprint::STATUS_IDENTIFY_SUCCESS );
            break;
        }

        case Operation::Verify:
        {
            emit verifySuccess();
            setStatus( Fingerprint::STATUS_VERIFY_SUCCESS );
            break;
        }

        case Operation::AutoIdentifyEnable:
        {
            emit autoIdentifyEnableSuccess();
            setStatus( Fingerprint::STATUS_AUTO_IDENTIFY );

            m_threadedOps->setOperation( Operation::AutoIdentify );
            break;
        }

        case Operation::AutoIdentifyDisable:
        {
            emit autoIdentifyDisableSuccess();
            setStatus( Fingerprint::STATUS_NULL );
            break;
        }

        default:
        {
            setStatus( Fingerprint::STATUS_NULL );
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::onFail()
///
/// \brief  Handles failed threaded operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::onFail()
{
    Operation operation = m_threadedOps->getOperation();

    m_threadedOps->setOperation( Operation::None );

    switch ( operation )
    {
        case Operation::Enroll:
        {
            qDebug() << QString( "FingerprintController::onFail Operation::Enroll" );

            emit enrollFail();
            setStatus( Fingerprint::STATUS_ENROLL_ERROR );

            m_threadedOps->setOperation( Operation::Enroll );
            break;
        }

        case Operation::Identify:
        {
            emit identifyFail();
            setStatus( Fingerprint::STATUS_IDENTIFY_ERROR );
            break;
        }

        case Operation::Verify:
        {
            emit verifyFail();
            setStatus( Fingerprint::STATUS_VERIFY_ERROR );
            break;
        }

        case Operation::AutoIdentifyEnable:
        {
            emit autoIdentifyEnableFail();
            setStatus( Fingerprint::STATUS_AUTO_IDENTIFY_ERROR );
            break;
        }

        case Operation::AutoIdentifyDisable:
        {
            emit autoIdentifyDisableFail();
            setStatus( Fingerprint::STATUS_AUTO_IDENTIFY_ERROR );
            break;
        }

        default:
        {
            setStatus( Fingerprint::STATUS_NULL );
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::onAutoIdentifySuccess( int id )
///
/// \brief  Handles successful auto identification.
///
/// \param  The identified fingeprint ID.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::onAutoIdentifySuccess( int id )
{
    // Reset the operation.
    m_threadedOps->setOperation( Operation::None );

    if ( DatabaseControl::instance()->authenticateUser( id, "" ) )
    {
        emit autoIdentifySuccess();
        setStatus( Fingerprint::STATUS_AUTO_IDENTIFY_SUCCESS );
    }
    else
    {
        onAutoIdentifyFail();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::onAutoIdentifyFail()
///
/// \brief  Handles failed auto identification.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::onAutoIdentifyFail()
{
    // Reset the operation.
    m_threadedOps->setOperation( Operation::None );

    emit autoIdentifyFail();
    setStatus( Fingerprint::STATUS_AUTO_IDENTIFY_ERROR );

    // Restart auto identify.
    m_threadedOps->setOperation( Operation::AutoIdentify );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::instance()
///
/// \brief  Returns a static instance of FingerprintController.
///
/// \return A FingerprintController instance.
////////////////////////////////////////////////////////////////////////////////
FingerprintController* FingerprintController::instance()
{
    if ( !_fingerprintControl )
    {
        FingerprintOperationThread* threadedOps = new FingerprintOperationThread();

        _fingerprintControl = new FingerprintController( threadedOps );
    }

    return _fingerprintControl;

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::instance( FingerprintOperationThread* threadedOps )
///
/// \brief  Returns a static instance of FingerprintController with an injected threaded
///         operations.
///
/// \param  An injected threaded operations.
///
/// \return A FingerprintController instance with the injected threaded operations.
////////////////////////////////////////////////////////////////////////////////
FingerprintController* FingerprintController::instance( FingerprintOperationThread* threadedOps )
{
    if ( !_fingerprintControl )
    {
        _fingerprintControl = new FingerprintController( threadedOps );
    }

    return _fingerprintControl;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::destroy()
///
/// \brief  Destroys the FingerprintController instance.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::destroy()
{
    if ( _fingerprintControl )
    {
        delete _fingerprintControl;
        _fingerprintControl = nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintController::callback_UF_Enroll( unsigned char returnCode, UF_ENROLL_MODE enrollMode, int numSuccesses )
///
/// \brief  Callback for UF_Identify.
///
/// \param  Return code.
/// \param  Enroll mode.
/// \param  Number of successful fingerprint reads.
////////////////////////////////////////////////////////////////////////////////
void FingerprintController::callback_UF_Enroll( unsigned char returnCode, UF_ENROLL_MODE enrollMode, int enrollSuccessCount )
{
    qDebug() << QString( "[FingerprintController::callback_UF_Enroll] Return code: %1, enroll mode: %2, enroll success count: %3." )
                    .arg( returnCode )
                    .arg( enrollMode )
                    .arg( enrollSuccessCount );

    if ( enrollSuccessCount == 1 )
    {
        FingerprintController::instance()->setStatus( Fingerprint::STATUS_ENROLL_IN_PROGRESS );
    }
}
