#ifndef CARDREADERENUMS_H
#define CARDREADERENUMS_H

// Qt.
#include <QObject>

// Project.
#include "hardwarecontrol_global.h"

namespace Cardreader
{
    HARDWARECONTROL_EXPORT Q_NAMESPACE

    enum Status
    {
        STATUS_NULL = -1,
        STATUS_READY,
        STATUS_REGISTER,
        STATUS_REGISTER_SUCCESS,
        STATUS_REGISTER_ERROR,
        STATUS_IDENTIFY,
        STATUS_IDENTIFY_SUCCESS,
        STATUS_IDENTIFY_ERROR
    };
    Q_ENUM_NS(Status)
}

#endif // CARDREADERENUMS_H
