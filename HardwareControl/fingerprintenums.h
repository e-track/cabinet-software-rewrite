#ifndef FINGERPRINTENUMS_H
#define FINGERPRINTENUMS_H

// Qt.
#include <QObject>

// Project.
#include "hardwarecontrol_global.h"

namespace Fingerprint
{
    HARDWARECONTROL_EXPORT Q_NAMESPACE

    enum Status
    {
        STATUS_NULL = -1,               ///< Invalid status.

        // Enroll.
        STATUS_ENROLL,                  ///< Enroll has started.
        STATUS_ENROLL_IN_PROGRESS,      ///< Enroll is in progress (i.e. 1 of 2 fingerprints read).
        STATUS_ENROLL_SUCCESS,          ///< Enroll was successful.
        STATUS_ENROLL_ERROR,            ///< Enroll failed and is retrying.

        // Identify.
        STATUS_IDENTIFY,                ///< Identify has started.
        STATUS_IDENTIFY_SUCCESS,        ///< Identify was successful.
        STATUS_IDENTIFY_ERROR,          ///< Identify failed and is retrying.

        // Auto identify.
        STATUS_AUTO_IDENTIFY,           ///< Auto identify has started.
        STATUS_AUTO_IDENTIFY_SUCCESS,   ///< Auto identify was successful.
        STATUS_AUTO_IDENTIFY_ERROR,     ///< Auto identify failed and is retrying.

        // Verify.
        STATUS_VERIFY,                  ///< Verify has started.
        STATUS_VERIFY_SUCCESS,          ///< Verify was successful.
        STATUS_VERIFY_ERROR             ///< Verify failed and is retrying.
    };
    Q_ENUM_NS(Status)
}

#endif // FINGERPRINTENUMS_H
