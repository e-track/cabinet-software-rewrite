#ifndef CARDREADERCONTROLLER_H
#define CARDREADERCONTROLLER_H

// Qt.
#include <QObject>
#include <QSerialPort>

// Project.
#include "cardreaderenums.h"
#include "hardwarecontrol_global.h"

class QTimer;

////////////////////////////////////////////////////////////////////////////////
/// \class  Card reader controller.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT CardreaderController : public QObject
{
    Q_OBJECT
    Q_PROPERTY( bool swiping              READ isSwiping NOTIFY swipingChanged )
    Q_PROPERTY( Cardreader::Status status READ getStatus NOTIFY statusChanged );

public:
    explicit CardreaderController( QObject* parent = nullptr );
    virtual ~CardreaderController() = default;

    // Initialisation.
    HARDWARECONTROL_EXPORT static void registerClass();
    HARDWARECONTROL_EXPORT Q_INVOKABLE void initialise();

    // Operation.
    Q_INVOKABLE void simulateSwipe();
    Q_INVOKABLE void registerCard();
    Q_INVOKABLE void identify( bool enabled );

    // Status.
    Cardreader::Status getStatus();

    // Swiping.
    bool isSwiping() const;

signals:
    void swipingChanged();
    void accepted();

    // Initialisation.
    void initialisationFail();
    void initialisationSuccess();

    // Status.
    void statusChanged();
    void registerCardSuccess();
    void registerCardFail();
    void identifySuccess();
    void identifyFail();

private slots:
    void onTimeout();
    void onReadyRead();
    void onError( QSerialPort::SerialPortError error );

private:
    bool openPort();
    void closePort();
    void setStatus( Cardreader::Status status );

    bool m_swiping;
    QTimer* m_timer;
    QSerialPort* m_serialPort;

    Cardreader::Status m_status;        ///< The current status of the card reader.
};

#endif // CARDREADERCONTROLLER_H
