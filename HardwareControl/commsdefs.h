#ifndef COMMSDEFS_H
#define COMMSDEFS_H

// External.
#include <cstdint>

// Commands.
static const uint8_t START_CODE = 0xbf;                                 ///< Start code byte.
static const uint8_t NULL_COMMAND = 0x0;                                ///< Null command.
static const uint8_t COMMAND_ADDRESS_MASK = 0x40;                       ///< Address mask.
static const uint8_t COMMAND_BYTE_START = 0;                            ///< Start code byte.
static const uint8_t COMMAND_BYTE_ADDRESS = 1;                          ///< Address mask byte.
static const uint8_t COMMAND_BYTE_COMMAND = 2;                          ///< Command code byte.

// Replies.
static const uint8_t REPLY_BYTE_ADDRESS = 0;                            ///< Reply address.

// Get status.
static const uint8_t STATUS_SIZE = 2;                                   ///< Size of command.
static const uint8_t STATUS_ADDRESS_MASK = 0xc0;                        ///< Address mask (should this be 0x40 like all the others?).
static const uint8_t STATUS_BIT_BOOTLOADER = 7;                         ///< In bootloader bit.
static const uint8_t STATUS_BIT_LED_ERROR = 6;                          ///< Led error bit.
static const uint8_t STATUS_BIT_E2_ERROR = 5;                           ///< E2 stored data error bit.
static const uint8_t STATUS_BIT_LEFT_KEY_HIGH = 4;                      ///< Left key sensor high bit.
static const uint8_t STATUS_BIT_RIGHT_KEY_HIGH = 3;                     ///< Right key sensor high bit.
static const uint8_t STATUS_BIT_BUTTON_PRESSED = 2;                     ///< Button pressed bit.
static const uint8_t STATUS_BIT_LOCK_OPEN = 1;                          ///< Lock open bit.
static const uint8_t STATUS_BIT_LOCK_UNLATCHED = 0;                     ///< Lock unlatched bit.
static const uint8_t STATUS_BIT_REED_SWITCH = 7;                        ///< Reed switch open bit.

// Get status reply.
static const uint8_t STATUS_REPLY_SIZE = 6;                             ///< Size of reply.
static const uint8_t STATUS_BYTE_STATUS = 1;                            ///< Status byte.
static const uint8_t STATUS_BYTE_REED_SWITCH = 2;                       ///< Reed switch byte.
static const uint8_t STATUS_BYTE_4_UNUSED = 3;                          ///< Unused byte.
static const uint8_t STATUS_BYTE_CRC_HIGH = 4;                          ///< CRC high byte.
static const uint8_t STATUS_BYTE_CRC_LOW = 5;                           ///< CRC low byte.

// Read peg numbers command.
static const uint8_t READ_PEG_NUMBERS_SIZE = 3;                         ///< Size of command.
static const uint8_t READ_PEG_NUMBERS_COMMAND = 0xf0;                   ///< Command 0.

// Read peg numbers reply.
static const uint8_t READ_PEG_NUMBERS_REPLY_SIZE = 33;                  ///< Size of reply.
static const uint8_t READ_PEG_NUMBERS_BYTE_CRC_HIGH = 31;               ///< CRC high byte.
static const uint8_t READ_PEG_NUMBERS_BYTE_CRC_LOW = 32;                ///< CRC low byte.

// LED colours.
static const uint8_t LED_BLACK = 0x0;                                   ///< Black (off).
static const uint8_t LED_RED = 0x1;                                     ///< Red.
static const uint8_t LED_MAGENTA = 0x2;                                 ///< Magenta.
static const uint8_t LED_BLUE = 0x3;                                    ///< Blue.
static const uint8_t LED_CYAN = 0x4;                                    ///< Cyan.
static const uint8_t LED_GREEN = 0x5;                                   ///< Green,
static const uint8_t LED_YELLOW = 0x6;                                  ///< Yellow.
static const uint8_t LED_WHITE = 0x7;                                   ///< White.

// LED states.
static const uint8_t LED_SOUNDER_PULSE = 0x20;                          ///< Sounder will pulse (overides LED_SOUNDER_ON).
static const uint8_t LED_SOUNDER_ON = 0x10;                             ///< Sounder is on.
static const uint8_t LED_GREEN_FLASH = 0x08;                            ///< Green LED will flash (overrides LED_GREEN_ON).
static const uint8_t LED_RED_FLASH = 0x04;                              ///< Red LED will flash (overrides LED_RED_ON).
static const uint8_t LED_GREEN_ON = 0x02;                               ///< Green LED is on.
static const uint8_t LED_RED_ON = 0x01;                                 ///< Red LED is on.
static const uint8_t LED_OFF = 0x00;                                    ///< LED is off.

// LED drive command.
static const uint8_t LED_DRIVE_SIZE = 11;                               ///< Size of command.
static const uint8_t LED_DRIVE_COMMAND = 0xe1;                          ///< Command 1.
static const uint8_t LED_DRIVE_BYTE_PEG_1_COLOUR = 3;                   ///< Peg 1 colour byte.
static const uint8_t LED_DRIVE_BYTE_PEG_2_COLOUR = 4;                   ///< Peg 2 colour byte.
static const uint8_t LED_DRIVE_BYTE_PEG_3_COLOUR = 5;                   ///< Peg 3 colour byte.
static const uint8_t LED_DRIVE_BYTE_PEG_4_COLOUR = 6;                   ///< Peg 4 colour byte.
static const uint8_t LED_DRIVE_BYTE_PEG_5_COLOUR = 7;                   ///< Peg 5 colour byte.
static const uint8_t LED_DRIVE_BYTE_FLASH_SOUND = 8;                    ///< Sounder, green / red LED flashing byte.
static const uint8_t LED_DRIVE_BYTE_CRC_HIGH = 9;                       ///< CRC high byte.
static const uint8_t LED_DRIVE_BYTE_CRC_LOW = 10;                       ///< CRC low byte.

// LED drive command bytes 4 to 8.
static const uint8_t LED_DRIVE_BIT_COLOUR_1_RED = 0;                    ///< Peg LED colour 1 red bit.
static const uint8_t LED_DRIVE_BIT_COLOUR_1_GREEN = 1;                  ///< Peg LED colour 1 green bit.
static const uint8_t LED_DRIVE_BIT_COLOUR_1_BLUE = 2;                   ///< Peg LED colour 1 blue bit.
static const uint8_t LED_DRIVE_BIT_COLOUR_2_RED = 4;                    ///< Peg LED colour 2 red bit.
static const uint8_t LED_DRIVE_BIT_COLOUR_2_GREEN = 5;                  ///< Peg LED colour 2 green bit.
static const uint8_t LED_DRIVE_BIT_COLOUR_2_BLUE = 6;                   ///< Peg LED colour 2 blue bit.

// LED drive command byte 9.
static const uint8_t LED_DRIVE_BIT_SOUNDER_PULSE = 5;                   ///< Sounder pulse (overides LED_DRIVE_BIT_SOUNDER_ON).
static const uint8_t LED_DRIVE_BIT_SOUNDER_ON = 4;                      ///< Sounder on.
static const uint8_t LED_DRIVE_BIT_GREEN_FLASH = 3;                     ///< Green LED flash (overides LED_DRIVE_BIT_GREEN_ON).
static const uint8_t LED_DRIVE_BIT_RED_FLASH = 2;                       ///< Red LED flash (overides LED_DRIVE_BIT_RED_ON).
static const uint8_t LED_DRIVE_BIT_GREEN_ON = 1;                        ///< Green LED on.
static const uint8_t LED_DRIVE_BIT_RED_ON = 0;                          ///< Red LED on.

// LED drive reply.
static const uint8_t LED_DRIVE_REPLY_SIZE = 1;                          ///< Size of reply.

// Solenoids and lock control command.
static const uint8_t SOLENOIDS_LOCK_SIZE = 6;                           ///< Size of command.
static const uint8_t SOLENOIDS_LOCK_COMMAND = 0xb4;                     ///< Command 4.
static const uint8_t SOLENOIDS_LOCK_BYTE_RELEASE = 3;                   ///< Release lock or solenoid byte.
static const uint8_t SOLENOIDS_LOCK_BYTE_CRC_HIGH = 4;                  ///< CRC high byte.
static const uint8_t SOLENOIDS_LOCK_BYTE_CRC_LOW = 5;                   ///< CRC low byte.

// Solenoids and lock control byte 4.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_LOCK = 7;               ///< Release lock bit.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_PEG_5 = 4;              ///< Release peg 5 bit.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_PEG_4 = 3;              ///< Release peg 4 bit.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_PEG_3 = 2;              ///< Release peg 3 bit.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_PEG_2 = 1;              ///< Release peg 2 bit.
static const uint8_t SOLENOIDS_LOCK_BIT_RELEASE_PEG_1 = 0;              ///< Release peg 1 bit.

// Solenoids and lock control reply.
static const uint8_t SOLENOIDS_LOCK_REPLY_SIZE = 1;                     ///< Size of reply.

// Read environment command.
static const uint8_t READ_ENVIRONMENT_SIZE = 3;                         ///< Size of command.
static const uint8_t READ_ENVIRONMENT_COMMAND = 0xa5;                   ///< Command 5.

// Read environment reply.
static const uint8_t READ_ENVIRONMENT_REPLY_SIZE = 5;                   ///< Size of reply.
static const uint8_t READ_ENVIRONMENT_BYTE_TEMPERATURE = 1;             ///< Temperature byte.
static const uint8_t READ_ENVIRONMENT_BYTE_HUMIDITY = 2;                ///< Humidity byte.
static const uint8_t READ_ENVIRONMENT_BYTE_CRC_HIGH = 3;                ///< CRC high byte.
static const uint8_t READ_ENVIRONMENT_BYTE_CRC_LOW = 4;                 ///< CRC low byte.

// Read settings command.
static const uint8_t READ_SETTINGS_SIZE = 3;                            ///< Size of command.
static const uint8_t READ_SETTINGS_COMMAND = 0x69;                      ///< Command 9.

// Read settings reply.
static const uint8_t READ_SETTINGS_REPLY_SIZE = 19;                     ///< Size of reply.
static const uint8_t READ_SETTINGS_BYTE_RFID_MIN_LEVEL = 0;             ///< RFID minimum level byte.
static const uint8_t READ_SETTINGS_BYTE_SOLENOID_TIMEOUT = 1;           ///< Solenoid timeout byte.
static const uint8_t READ_SETTINGS_BYTE_SIXTH_PEG = 2;                  ///< Sixth peg enabled byte.
static const uint8_t READ_SETTINGS_BYTE_CRC_HIGH = 17;                  ///< CRC high byte.
static const uint8_t READ_SETTINGS_BYTE_CRC_LOW = 18;                   ///< CRC low byte.
static const uint8_t READ_SETTINGS_BIT_SIXTH_PEG = 0;                   ///< Sixth peg enabled bit.

// Write settings command.
static const uint8_t WRITE_SETTINGS_SIZE = 7;                           ///< Size of command.
static const uint8_t WRITE_SETTINGS_COMMAND = 0x5a;                     ///< Command A.
static const uint8_t WRITE_SETTINGS_BYTE_LOCATION = 0x03;               ///< E2 location byte.
static const uint8_t WRITE_SETTINGS_BYTE_DATA = 0x04;                   ///< Data byte.
static const uint8_t WRITE_SETTINGS_BYTE_RFID_MIN_LEVEL = 0x00;         ///< RFID minimum level byte.
static const uint8_t WRITE_SETTINGS_BYTE_SOLENOID_TIMEOUT = 0x01;       ///< Solenoid timeout byte.
static const uint8_t WRITE_SETTINGS_BYTE_CRC_HIGH = 5;                  ///< CRC high byte.
static const uint8_t WRITE_SETTINGS_BYTE_CRC_LOW = 6;                   ///< CRC low byte.

// Write settings reply.
static const uint8_t WRITE_SETTINGS_REPLY_SIZE = 1;                     ///< Size of reply.

// Application version command.
static const uint8_t APP_VERSION_COMMAND = 0x4b;                        ///< Application version command (command B).
static const uint8_t APP_VERSION_SIZE = 3;                              ///< Size of application version command.

// Application version reply.
static const uint8_t APP_VERSION_REPLY_SIZE = 7;                        ///< Size of application version reply (command B).
static const uint8_t APP_VERSION_BYTE_BOOTLOADER_HIGH = 0x1;            ///< Bootloader version high byte.
static const uint8_t APP_VERSION_BYTE_BOOTLOADER_LOW = 0x2;             ///< Bootloader version low byte.
static const uint8_t APP_VERSION_BYTE_APPLICATION_HIGH = 0x3;           ///< Application version high byte.
static const uint8_t APP_VERSION_BYTE_APPLICATION_LOW = 0x4;            ///< Application version low byte.
static const uint8_t APP_VERSION_BYTE_CRC_HIGH = 0x5;                   ///< CRC high byte.
static const uint8_t APP_VERSION_BYTE_CRC_LOW = 0x6;                    ///< CRC low byte.

#endif // COMMSDEFS_H
