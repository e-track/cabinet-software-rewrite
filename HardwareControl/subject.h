#ifndef SUBJECT_H
#define SUBJECT_H

// External.
#include <list>

// Project.
#include "hardwarecontrol_global.h"

// Forward declarations.
class Observer;

////////////////////////////////////////////////////////////////////////////////
/// \class  Provides a mechanism to allow observers to monity it for any changes.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT Subject
{
public:
    explicit Subject();
    virtual ~Subject() = default;

    virtual void watch( Observer* observer );
    virtual void unwatch( Observer* observer );
    virtual void notify();

protected:
    std::list<Observer*> m_observers;
};

#endif // SUBJECT_H
