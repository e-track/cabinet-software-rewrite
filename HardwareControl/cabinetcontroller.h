#ifndef CABINETCONTROLLER_H
#define CABINETCONTROLLER_H

// Qt.
#include <QObject>
#include <QSerialPort>

// Project.
#include "hardwarecontrol_global.h"
#include "observer.h"

class CommsProtocol;
class QTimer;

////////////////////////////////////////////////////////////////////////////////
/// \class  Cabinet controller.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROL_EXPORT CabinetController : public QObject, public Observer
{
    Q_OBJECT
    Q_PROPERTY( int simulationEnabled   READ simulationEnabled  WRITE enableSimulation NOTIFY simulationChanged )
    Q_PROPERTY( bool internetAccess     READ getInternetAccess                         NOTIFY internetAccessChanged )
    Q_PROPERTY( bool autoGeneratePin    READ getAutoGeneratePin                        NOTIFY autoGenPinChanged )

    Q_PROPERTY( QString bootloaderVersion  READ getBootloaderVersion  NOTIFY versionsChanged )
    Q_PROPERTY( QString applicationVersion READ getApplicationVersion NOTIFY versionsChanged )

public:
    virtual ~CabinetController();

    // Initialisation.
    HARDWARECONTROL_EXPORT static void registerClass();
    HARDWARECONTROL_EXPORT Q_INVOKABLE void initialise();

    // Simulation.
    void enableSimulation( bool enabled );
    bool simulationEnabled() const;

    // Key slots.
    void setNumberOfSlots( int numSlots );
    int getNumberOfSlots() const;

    // Config.
    bool getInternetAccess() const;
    bool getAutoGeneratePin() const;

    // Versions.
    QString getBootloaderVersion() const;
    QString getApplicationVersion() const;

    // Key actions.
    Q_INVOKABLE void simulateInsertKey( int slot );
    Q_INVOKABLE void simulateRemoveKey( int slot );

    // Statics.
    HARDWARECONTROL_EXPORT static CabinetController* instance();
    HARDWARECONTROL_EXPORT static CabinetController* instance( CommsProtocol* commsProtocol );
    HARDWARECONTROL_EXPORT static void destroy();

    // Notification.
    virtual void update();

    // Friendship.
    friend class CabinetControllerTest;

signals:
    // Key actions.
    void keyInserted( int slot );
    void keyRemoved( int slot );

    // Simulation.
    void simulationChanged();

    // Config.
    void internetAccessChanged();
    void autoGenPinChanged();

    // Versions.
    void versionsChanged();

private:
    explicit CabinetController( CommsProtocol* commsProtocol, QObject* parent = nullptr );

    // NotificationHandlers.
    void updateInterfaceAppVersion();

    // Data.
    CommsProtocol* m_pegboardComms;             ///< Provides cabinet data (TODO: use smart pointers).

    // Interface application version.
    QString m_bootloaderVersion;                ///< Bootloader version.
    QString m_applicationVersion;               ///< Application version.

    // Key slots.
    int m_numKeySlots;                          ///< The number of key slots (TODO: replace with an array).

    // Simulation.
    bool m_simulating;                          ///< Simulation mode is enabled.

    // Config.
    bool m_internetAccess;                      ///< True if internet access is available.
    bool m_autoGenPin;                          ///< True if PIN needs to be automatically generated.
};

#endif // CABINETCONTROLLER_H
