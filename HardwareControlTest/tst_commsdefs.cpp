// External.
#include <stdint.h>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/commsdefs.h>

// Project.
#include "tst_commsdefs.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::CommsDefsTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
CommsDefsTest::CommsDefsTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::init()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testGeneral()
///
/// \brief  Tests that the comms constants and definitions are correct.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testGeneral()
{
    // Commands.
    QVERIFY( START_CODE == 0xbf );
    QVERIFY( NULL_COMMAND == 0x0 );
    QVERIFY( COMMAND_ADDRESS_MASK == 0x40 );
    QVERIFY( COMMAND_BYTE_START == 0 );
    QVERIFY( COMMAND_BYTE_ADDRESS == 1 );
    QVERIFY( COMMAND_BYTE_COMMAND == 2 );

    // Replies.
    QVERIFY( REPLY_BYTE_ADDRESS == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testLedColours()
///
/// \brief  Tests that the LED colour constants are correct.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testLedColours()
{
    QVERIFY( LED_BLACK == 0x0 );
    QVERIFY( LED_RED == 0x1 );
    QVERIFY( LED_MAGENTA == 0x2 );
    QVERIFY( LED_BLUE == 0x3 );
    QVERIFY( LED_CYAN == 0x4 );
    QVERIFY( LED_GREEN == 0x5 );
    QVERIFY( LED_YELLOW == 0x6 );
    QVERIFY( LED_WHITE == 0x7 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testLedStates()
///
/// \brief  Tests that the LED state constants are correct.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testLedStates()
{
    QVERIFY( LED_SOUNDER_PULSE == 0x20 );
    QVERIFY( LED_SOUNDER_ON == 0x10 );
    QVERIFY( LED_GREEN_FLASH == 0x08 );
    QVERIFY( LED_RED_FLASH == 0x04 );
    QVERIFY( LED_GREEN_ON == 0x02 );
    QVERIFY( LED_RED_ON == 0x01 );
    QVERIFY( LED_OFF == 0x00 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testGetStatus()
///
/// \brief  Tests get status constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testGetStatus()
{
    // Command.
    QVERIFY( STATUS_SIZE == 2 );
    QVERIFY( STATUS_ADDRESS_MASK == 0xc0 );

    // Reply.
    QVERIFY( STATUS_REPLY_SIZE == 6 );
    QVERIFY( STATUS_BYTE_STATUS == 1 );
    QVERIFY( STATUS_BYTE_REED_SWITCH == 2 );
    QVERIFY( STATUS_BYTE_4_UNUSED == 3 );
    QVERIFY( STATUS_BYTE_CRC_HIGH == 4 );
    QVERIFY( STATUS_BYTE_CRC_LOW == 5 );

    // Reply byte 2.
    QVERIFY( STATUS_BIT_BOOTLOADER == 7 );
    QVERIFY( STATUS_BIT_LED_ERROR == 6 );
    QVERIFY( STATUS_BIT_E2_ERROR == 5 );
    QVERIFY( STATUS_BIT_LEFT_KEY_HIGH == 4 );
    QVERIFY( STATUS_BIT_RIGHT_KEY_HIGH == 3 );
    QVERIFY( STATUS_BIT_BUTTON_PRESSED == 2 );
    QVERIFY( STATUS_BIT_LOCK_OPEN == 1 );
    QVERIFY( STATUS_BIT_LOCK_UNLATCHED == 0 );

    // Reply byte 3.
    QVERIFY( STATUS_BIT_REED_SWITCH == 7 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testReadPegNumbers()
///
/// \brief  Tests read peg numbers constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testReadPegNumbers()
{
    // Command.
    QVERIFY( READ_PEG_NUMBERS_SIZE == 3 );
    QVERIFY( READ_PEG_NUMBERS_COMMAND == 0xf0 );

    // Reply.
    QVERIFY( READ_PEG_NUMBERS_REPLY_SIZE == 33 );
    QVERIFY( READ_PEG_NUMBERS_BYTE_CRC_HIGH == 31 );
    QVERIFY( READ_PEG_NUMBERS_BYTE_CRC_LOW == 32 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testLedDrive()
///
/// \brief  Tests LED drive constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testLedDrive()
{
    // Command.
    QVERIFY( LED_DRIVE_SIZE == 11 );
    QVERIFY( LED_DRIVE_COMMAND == 0xe1 );
    QVERIFY( LED_DRIVE_BYTE_PEG_1_COLOUR == 3 );
    QVERIFY( LED_DRIVE_BYTE_PEG_2_COLOUR == 4 );
    QVERIFY( LED_DRIVE_BYTE_PEG_3_COLOUR == 5 );
    QVERIFY( LED_DRIVE_BYTE_PEG_4_COLOUR == 6 );
    QVERIFY( LED_DRIVE_BYTE_PEG_5_COLOUR == 7 );
    QVERIFY( LED_DRIVE_BYTE_FLASH_SOUND == 8 );
    QVERIFY( LED_DRIVE_BYTE_CRC_HIGH == 9 );
    QVERIFY( LED_DRIVE_BYTE_CRC_LOW == 10 );

    // Command bytes 4 to 8.
    QVERIFY( LED_DRIVE_BIT_COLOUR_1_RED == 0 );
    QVERIFY( LED_DRIVE_BIT_COLOUR_1_GREEN == 1 );
    QVERIFY( LED_DRIVE_BIT_COLOUR_1_BLUE == 2 );
    QVERIFY( LED_DRIVE_BIT_COLOUR_2_RED == 4 );
    QVERIFY( LED_DRIVE_BIT_COLOUR_2_GREEN == 5 );
    QVERIFY( LED_DRIVE_BIT_COLOUR_2_BLUE == 6 );

    // Command byte 9.
    QVERIFY( LED_DRIVE_BIT_SOUNDER_PULSE == 5 );
    QVERIFY( LED_DRIVE_BIT_SOUNDER_ON == 4 );
    QVERIFY( LED_DRIVE_BIT_GREEN_FLASH == 3 );
    QVERIFY( LED_DRIVE_BIT_RED_FLASH == 2 );
    QVERIFY( LED_DRIVE_BIT_GREEN_ON == 1 );
    QVERIFY( LED_DRIVE_BIT_RED_ON == 0 );

    // Reply.
    QVERIFY( LED_DRIVE_REPLY_SIZE == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testSolenoidsLockControl()
///
/// \brief  Tests solenoids and lock control constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testSolenoidsLockControl()
{
    // Command.
    QVERIFY( SOLENOIDS_LOCK_SIZE == 6 );
    QVERIFY( SOLENOIDS_LOCK_COMMAND == 0xb4 );
    QVERIFY( SOLENOIDS_LOCK_BYTE_RELEASE == 3 );
    QVERIFY( SOLENOIDS_LOCK_BYTE_CRC_HIGH == 4 );
    QVERIFY( SOLENOIDS_LOCK_BYTE_CRC_LOW == 5 );

    // Command byte 4.
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_LOCK == 7 );
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_PEG_5 == 4 );
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_PEG_4 == 3 );
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_PEG_3 == 2 );
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_PEG_2 == 1 );
    QVERIFY( SOLENOIDS_LOCK_BIT_RELEASE_PEG_1 == 0 );

    // Reply.
    QVERIFY( SOLENOIDS_LOCK_REPLY_SIZE == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testReadEnvironment()
///
/// \brief  Tests read environment constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testReadEnvironment()
{
    // Command.
    QVERIFY( READ_ENVIRONMENT_SIZE == 3 );
    QVERIFY( READ_ENVIRONMENT_COMMAND == 0xa5 );

    // Reply.
    QVERIFY( READ_ENVIRONMENT_REPLY_SIZE == 5 );
    QVERIFY( READ_ENVIRONMENT_BYTE_TEMPERATURE == 1 );
    QVERIFY( READ_ENVIRONMENT_BYTE_HUMIDITY == 2 );
    QVERIFY( READ_ENVIRONMENT_BYTE_CRC_HIGH == 3 );
    QVERIFY( READ_ENVIRONMENT_BYTE_CRC_LOW == 4 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testReadSettings()
///
/// \brief  Tests read settings constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testReadSettings()
{
    // Command.
    QVERIFY( READ_SETTINGS_SIZE == 3 );
    QVERIFY( READ_SETTINGS_COMMAND == 0x69 );

    // Reply.
    QVERIFY( READ_SETTINGS_REPLY_SIZE == 19 );
    QVERIFY( READ_SETTINGS_BYTE_RFID_MIN_LEVEL == 0 );
    QVERIFY( READ_SETTINGS_BYTE_SOLENOID_TIMEOUT == 1 );
    QVERIFY( READ_SETTINGS_BYTE_SIXTH_PEG == 2 );
    QVERIFY( READ_SETTINGS_BYTE_CRC_HIGH == 17 );
    QVERIFY( READ_SETTINGS_BYTE_CRC_LOW == 18 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testWriteSettings()
///
/// \brief  Tests write settings constants.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testWriteSettings()
{
    // Command.
    QVERIFY( WRITE_SETTINGS_SIZE == 7 );
    QVERIFY( WRITE_SETTINGS_COMMAND == 0x5a );
    QVERIFY( WRITE_SETTINGS_BYTE_LOCATION == 0x3 );
    QVERIFY( WRITE_SETTINGS_BYTE_DATA == 0x4 );
    QVERIFY( WRITE_SETTINGS_BYTE_RFID_MIN_LEVEL == 0x0 );
    QVERIFY( WRITE_SETTINGS_BYTE_SOLENOID_TIMEOUT == 0x1 );
    QVERIFY( WRITE_SETTINGS_BYTE_CRC_HIGH == 0x5 );
    QVERIFY( WRITE_SETTINGS_BYTE_CRC_LOW == 0x6 );

    // Reply.
    QVERIFY( WRITE_SETTINGS_REPLY_SIZE == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::testAppVersion()
///
/// \brief  Tests application version command.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::testAppVersion()
{
    // Command.
    QVERIFY( APP_VERSION_SIZE == 3 );
    QVERIFY( APP_VERSION_COMMAND == 0x4b );

    // Reply.
    QVERIFY( APP_VERSION_REPLY_SIZE == 7 );
    QVERIFY( APP_VERSION_BYTE_BOOTLOADER_HIGH == 0x1 );
    QVERIFY( APP_VERSION_BYTE_BOOTLOADER_LOW == 0x2 );
    QVERIFY( APP_VERSION_BYTE_APPLICATION_HIGH == 0x3 );
    QVERIFY( APP_VERSION_BYTE_APPLICATION_LOW == 0x4 );
    QVERIFY( APP_VERSION_BYTE_CRC_HIGH == 0x5 );
    QVERIFY( APP_VERSION_BYTE_CRC_LOW == 0x6 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsDefsTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void CommsDefsTest::cleanupTestCase()
{

}
