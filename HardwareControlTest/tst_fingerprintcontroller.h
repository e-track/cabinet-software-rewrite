#ifndef TST_FINGERPRINTCONTROLLER_H
#define TST_FINGERPRINTCONTROLLER_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

class FingerprintController;
class MockFingerprintOperationThread;
class MockQueryManager;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the FingerprintController class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT FingerprintControllerTest : public UnitTest
{
    Q_OBJECT

public:
    explicit FingerprintControllerTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();

    // Initialisation.
    void testInitialisation();
    void testInitialisationInvokable();
    void testSdkVersion();
    void testSdkVersionInvokable();

    // Simulation.
    void testSimulationInvokables();

    // Operation.
    void testOperationSignals();
    void testOperationInvokables();

    // Enroll.
    void testEnroll();
    void testEnrollCallback();
    void testStatusProperties();

    // Identify.
    void testIdentify();
    void testAutoIdentify();
    void testAutoIdentifyEnable();
    void testAutoIdentifyDisable();

    // Verify.
    void testVerify();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    FingerprintController* m_fingerprintController;     ///< Class under test.
    MockFingerprintOperationThread* m_mockOpThread;     ///< Mock FingerprintOperationThread.
    MockQueryManager* m_mockQueryManager;               ///< Mock query manager.
};

#endif // TST_FINGERPRINTCONTROLLER_H
