// Qt.
#include <QByteArray>
#include <QMetaObject>
#include <QQmlEngine>
#include <QSignalSpy>
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/cardreadercontroller.h>

// Project.
#include "tst_cardreadercontroller.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::CardreaderControllerTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
CardreaderControllerTest::CardreaderControllerTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::init()
{
    // Create class under test.
    m_cardreaderController = new CardreaderController();
    QVERIFY( m_cardreaderController );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::testRegisterClass()
{
    // Register the class.
    CardreaderController::registerClass();

    // Expected class and module names.
    QString expectedModuleName = "HwControl";
    QString expectedClassName = "Cardreader";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::testOperationSignals()
///
/// \brief  Tests fingerprint operation signals.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::testOperationSignals()
{
    QVERIFY( QObject::connect( m_cardreaderController, &CardreaderController::swipingChanged, [] () {} ) );
    QVERIFY( QObject::connect( m_cardreaderController, &CardreaderController::accepted, [] () {} ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::testOperationInvokables()
///
/// \brief  Tests that the fingerprint operations can be invoked.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::testOperationInvokables()
{
    QSignalSpy pressedSpy( m_cardreaderController, &CardreaderController::swipingChanged );

    QVERIFY( QMetaObject::invokeMethod( m_cardreaderController, "simulateSwipe" ) );
    QVERIFY( pressedSpy.count() == 1 );
}

void CardreaderControllerTest::testCard()
{

}

void CardreaderControllerTest::testCardStatus()
{

}

void CardreaderControllerTest::testCardStatusProperties()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::testInitialisation()
///
/// \brief  Tests initialisation.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::testInitialisation()
{
    // Signal spies.
    QSignalSpy initialisationSuccessSpy( m_cardreaderController, &CardreaderController::initialisationSuccess );
    QSignalSpy initialisationFailSpy( m_cardreaderController, &CardreaderController::initialisationFail );

    // TODO: Use mocks for the non-threaded operations.

    m_cardreaderController->initialise();

    // Verify initCommPort signals.
    QVERIFY( initialisationSuccessSpy.count() + initialisationFailSpy.count() == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::testInitialisationInvokable()
///
/// \brief  Tests that the initialisaton function can be invoked.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::testInitialisationInvokable()
{
    QVERIFY( QMetaObject::invokeMethod( m_cardreaderController, "initialise" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CardreaderControllerTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void CardreaderControllerTest::cleanupTestCase()
{

}
