#ifndef TST_SIMULATEDPROTOCOL_H
#define TST_SIMULATEDPROTOCOL_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

// Forward declarations.
class SimulatedProtocol;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for SimualtedProtocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT SimulatedProtocolTest : public UnitTest
{
    Q_OBJECT

public:
    explicit SimulatedProtocolTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    SimulatedProtocol* m_simulatedProtocol;     ///< Class under test.
};

#endif // TST_SIMULATEDPROTOCOL_H
