################################################################################
# Config.
################################################################################
CONFIG += build_hardwarecontroltest console
CONFIG -= app_bundle

# Comment out the line below to build a standalone unit test application.
TEMPLATE = lib
DEFINES += HARDWARECONTROLTEST_LIBRARY

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

################################################################################
# Dependencies.
################################################################################
include( $$PWD/HardwareControlTest.pri )

################################################################################
# Installation
################################################################################
TARGET = HardwareControlTest
unix: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET
win32: target.path = $$(CABINETUI_INSTALLATION)/$$TARGET

!isEmpty(target.path): INSTALLS += target
