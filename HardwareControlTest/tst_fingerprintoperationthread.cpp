// Qt.
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/fingerprintoperationthread.h>

// Project.
#include "tst_fingerprintoperationthread.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::FingerprintOperationThreadTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
FingerprintOperationThreadTest::FingerprintOperationThreadTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::init()
{
    // Initialise data.
    m_operations.append( Operation::Enroll);
    m_operations.append( Operation::Identify);
    m_operations.append( Operation::AutoIdentify);
    m_operations.append( Operation::AutoIdentifyEnable);
    m_operations.append( Operation::AutoIdentifyDisable);
    m_operations.append( Operation::Verify);
    m_operations.append( Operation::None);

    // Create class under test.
    m_operationThread = new FingerprintOperationThread();
    QVERIFY( m_operationThread );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::testOperation()
///
/// \brief  Tests the fingerprint operations.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::testOperation()
{
    // Default.
    Operation expectedOperation = Operation::None;
    Operation operation = m_operationThread->getOperation();

    QVERIFY( operation == expectedOperation );

    // Check operation matches the one set.
    for ( Operation operation : m_operations )
    {
        expectedOperation = operation;
        m_operationThread->setOperation( expectedOperation );
        operation = m_operationThread->getOperation();

        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::testOperationRun()
///
/// \brief  Tests the threaded fingerprint operations.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::testOperationRun()
{
    // Signal spies.
    QSignalSpy successSpy( m_operationThread, &FingerprintOperationThread::success );
    QSignalSpy failSpy( m_operationThread, &FingerprintOperationThread::fail );

    // Enroll.
    m_operationThread->setOperation( Operation::Enroll );
    m_operationThread->start();

    QVERIFY( m_operationThread->isRunning() );

    // TODO: Use mocks for the threads.
    while ( m_operationThread->isRunning() ) {}

    QVERIFY( successSpy.count() + failSpy.count() == 1 );

    // Identify.
    m_operationThread->setOperation( Operation::Identify );
    m_operationThread->start();

    QVERIFY( m_operationThread->isRunning() );

    // TODO: Use mocks for the threads.
    while ( m_operationThread->isRunning() ) {}

    QVERIFY( successSpy.count() + failSpy.count() == 2 );

    // Verify.
    m_operationThread->setOperation( Operation::Verify );
    m_operationThread->start();

    QVERIFY( m_operationThread->isRunning() );

    // TODO: Use mocks for the threads.
    while ( m_operationThread->isRunning() ) {}

    QVERIFY( successSpy.count() + failSpy.count() == 3 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::cleanup()
{
    m_operations.clear();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintOperationThreadTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void FingerprintOperationThreadTest::cleanupTestCase()
{

}
