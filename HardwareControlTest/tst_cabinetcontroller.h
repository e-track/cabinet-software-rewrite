#ifndef TST_CABINETCONTROLLER_H
#define TST_CABINETCONTROLLER_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

class CabinetController;
class MockCommsProtocol;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the CabinetController class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT CabinetControllerTest : public UnitTest
{
    Q_OBJECT

public:
    explicit CabinetControllerTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();
    void testClassType();

    // Simulation.
    void testSimulation();
    void testSimulationProperties();

    // Key insert and release.
    void testSlots();
    void testKeySignals();
    void testKeyInvokables();

    // Config.
    void testConfig();
    void testConfigProperties();

    // Commands.
    void testGetStatus();
    void testReadPegNumbers();
    void testLedDrive();
    void testSolenoidsLock();
    void testReadEnvironment();
    void testReadSettings();
    void testWriteSettings();
    void testInterfaceAppVersion();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    CabinetController* m_cabinetController;         ///< Class under test.
    MockCommsProtocol* m_mockCommsProtocol;         ///< Mock comms protocol.
};

#endif // TST_CABINETCONTROLLER_H
