#ifndef TST_COMMSPROTOCOL_H
#define TST_COMMSPROTOCOL_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

// Forward declarations.
class CommsProtocol;

////////////////////////////////////////////////////////////////////////////////
/// \brief  Unit tests for the CommsProtocol class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT CommsProtocolTest : public UnitTest
{
    Q_OBJECT

public:
    explicit CommsProtocolTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Classes.
    void testInitialisation();
    void testEnums();
    void testDataStructures();

    // Commands.
    void testCommand();
    void testCommandGetStatus();
    void testCommandReadPegNumbers();
    void testCommandLedDrive();
    void testCommandSolenoidsLock();
    void testCommandReadEnvironment();
    void testCommandReadSettings();
    void testCommandInterfaceAppVersion();

    // Status.
    void testStatus();

    // Pegs.
    void testPegs();

    // LEDs.
    void testLeds();
    void testLedColours();
    void testLedStates();

    // Solenoids and lock.
    void testSolenoidsLock();

    // Environment.
    void testEnvironment();

    // Settings.
    void testSettings();

    // Versions.
    void testInterfaceApplicationVersion();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    CommsProtocol* m_commsProtocol;
};

#endif // TST_COMMSPROTOCOL_H
