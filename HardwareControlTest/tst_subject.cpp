// Qt.
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/subject.h>

// Project.
#include "mocks/mockobserver.h"
#include "tst_subject.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::SubjectTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
SubjectTest::SubjectTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::init()
{
    // Create mock observer.
    m_mockObserver = new MockObserver();
    QVERIFY( m_mockObserver );

    // Create class under test.
    m_subject = new Subject();
    QVERIFY( m_subject );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::testConnect()
///
/// \brief  Tests connected observers are notified of changes in the subject.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::testConnect()
{
    m_mockObserver->signalCount = 0;

    // Connect the obsever to the subject.
    m_subject->watch( m_mockObserver );

    QVERIFY( m_mockObserver->signalCount == 0 );

    // Allow the subject to signal to its observers.
    const unsigned int expectedCount = 5;

    for( unsigned int i = 0; i < expectedCount; ++i )
    {
        m_subject->notify();
    }

    // Check the signals have been picked up.
    QVERIFY( m_mockObserver->signalCount == expectedCount );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::testDisonnect()
///
/// \brief  Tests disconnected observers are not notified of changes in the subject.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::testDisonnect()
{
    // Reset the signal count in the observer.
    m_mockObserver->signalCount = 0;

    // Connect the observer and signal at least once.
    m_subject->watch( m_mockObserver );
    m_subject->notify();

    // Disconnect the obsever to the subject.
    m_subject->unwatch( m_mockObserver );

    // The signal count should be at least 1
    QVERIFY( m_mockObserver->signalCount == 1 );

    // Allow the subject to signal to its observers.
    const unsigned int expectedCount = 5;

    for( unsigned int i = 0; i < expectedCount; ++i )
    {
        m_subject->notify();
    }

    // Check the signals have been picked up.
    QVERIFY( m_mockObserver->signalCount == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     SubjectTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void SubjectTest::cleanupTestCase()
{

}
