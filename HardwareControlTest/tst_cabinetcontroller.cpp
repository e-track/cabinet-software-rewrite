// Qt.
#include <QMetaObject>
#include <QQmlEngine>
#include <QSignalSpy>
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/cabinetcontroller.h>
#include <HardwareControl/commsdefs.h>

// Project.
#include "mocks/mockcommsprotocol.h"
#include "tst_cabinetcontroller.h"

// Statics.
static bool _onOff[] = { true, false };        // Convenient bool pair.

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::CabinetControllerTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
CabinetControllerTest::CabinetControllerTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::init()
{
    // Create mock comms protocol.
    m_mockCommsProtocol = new MockCommsProtocol();
    QVERIFY( m_mockCommsProtocol );

    // Create class under test.
    m_cabinetController = CabinetController::instance( m_mockCommsProtocol );
    QVERIFY( m_cabinetController );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testRegisterClass()
{
    // Register the class.
    CabinetController::registerClass();

    // Expected class and module names.
    QString expectedModuleName = "HwControl";
    QString expectedClassName = "Cabinet";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testClassType()
///
/// \brief  Tests that the comms controller is an observer.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testClassType()
{
    Observer* observer = dynamic_cast<Observer*> ( m_cabinetController );

    QVERIFY( observer );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testSimulation()
///
/// \brief  Tests simulation mode.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testSimulation()
{
    // Simulation mode changed signal spy.
    QSignalSpy spy( m_cabinetController, &CabinetController::simulationChanged );

    // Default simulation.
    bool defaultSimulation = false;
    bool simulation = m_cabinetController->simulationEnabled();

    QVERIFY( defaultSimulation == simulation );

    // Expected simulation matches the one set.
    for ( bool expectedSimulation : _onOff )
    {
        m_cabinetController->enableSimulation( expectedSimulation );
        simulation = m_cabinetController->simulationEnabled();

        QVERIFY( simulation == expectedSimulation );
    }

    QVERIFY( spy.count() == 2 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testSimulationProperties()
///
/// \brief  Tests simulation mode properties.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testSimulationProperties()
{
    // Simulation mode changed signal spy.
    QSignalSpy spy( m_cabinetController, &CabinetController::simulationChanged );

    // Simulation mode is readable.
    QVariant simulation = m_cabinetController->property( "simulationEnabled" );
    QVERIFY( simulation.isValid() );

    // Default simulation mode.
    bool defaultSimulation = false;

    QVERIFY( defaultSimulation == simulation.toBool() );

    // Expected simulation matches the one set.
    for ( bool expectedSimulation : _onOff )
    {
        // Simulation mode is writable.
        QVERIFY( m_cabinetController->setProperty( "simulationEnabled", expectedSimulation ) );

        simulation = m_cabinetController->property( "simulationEnabled" );

        QVERIFY( simulation.toBool() == expectedSimulation );
    }

    QVERIFY( spy.count() == 2 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testSlots()
///
/// \brief  Tests that the cabinet controller can be set up with the appropriate
///         number of slots.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testSlots()
{
    // Default number of slots.
    int defaultNumSlots = 0;
    int numSlots = m_cabinetController->getNumberOfSlots();

    QVERIFY( numSlots == defaultNumSlots );

    // Expected number of slots matches the one set.
    int expectedNumSlots = 20;
    m_cabinetController->setNumberOfSlots( expectedNumSlots );
    numSlots = m_cabinetController->getNumberOfSlots();

    QVERIFY( numSlots == expectedNumSlots );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testKeySignals()
///
/// \brief  Tests that the key insert and release event signals exist.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testKeySignals()
{
    QVERIFY( QObject::connect( m_cabinetController, &CabinetController::keyInserted, [] () {} ) );
    QVERIFY( QObject::connect( m_cabinetController, &CabinetController::keyRemoved, [] () {} ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testKeyInvokables()
///
/// \brief  Tests that the key insert and release events can be invoked.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testKeyInvokables()
{
    // Invokables should only be available if the simulation mode is on.
    for ( bool simulation : _onOff )
    {
        m_cabinetController->enableSimulation( simulation );

        QSignalSpy insertSpy( m_cabinetController, &CabinetController::keyInserted );
        QSignalSpy removeSpy( m_cabinetController, &CabinetController::keyRemoved );

        QVERIFY( QMetaObject::invokeMethod( m_cabinetController, "simulateInsertKey", Q_ARG( int, 0 ) ) );
        QVERIFY( QMetaObject::invokeMethod( m_cabinetController, "simulateRemoveKey", Q_ARG( int, 0 ) ) );

        bool expectedSpyCount = simulation ? 1 : 0;

        QVERIFY( insertSpy.count() == expectedSpyCount );
        QVERIFY( removeSpy.count() == expectedSpyCount );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testConfig()
///
/// \brief  Tests configuration.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testConfig()
{
    // Signal spies.
    QSignalSpy internetSpy( m_cabinetController, &CabinetController::internetAccessChanged );
    QSignalSpy autoPinSpy( m_cabinetController, &CabinetController::autoGenPinChanged );

    // Default config.
    bool defaultInternet = false;
    bool defaultAutoPin = false;

    bool internet = m_cabinetController->getInternetAccess();
    bool autoPin = m_cabinetController->getAutoGeneratePin();

    QVERIFY( internet == defaultInternet );
    QVERIFY( autoPin == defaultAutoPin );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testConfigProperties()
///
/// \brief  Tests config properties.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testConfigProperties()
{
    // Signal spies.
    QSignalSpy internetSpy( m_cabinetController, &CabinetController::internetAccessChanged );
    QSignalSpy autoPinSpy( m_cabinetController, &CabinetController::autoGenPinChanged );

    // Config is readable.
    QVariant internet = m_cabinetController->property( "internetAccess" );
    QVERIFY( internet.isValid() );

    QVariant autoPin = m_cabinetController->property( "autoGeneratePin" );
    QVERIFY( autoPin.isValid() );

    // Default config.
    bool defaultInternet = false;
    bool defaultAutoPin = false;

    QVERIFY( defaultInternet == internet.toBool() );
    QVERIFY( defaultAutoPin == autoPin.toBool() );

    // Config is read-only.
    QVERIFY( !m_cabinetController->setProperty( "internetAccess", true ) );
    QVERIFY( !m_cabinetController->setProperty( "autoGeneratePin", true ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testGetStatus()
///
/// \brief  Tests the get status command.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testGetStatus()
{
    // Protocol runs the get status command.
    m_mockCommsProtocol->doGetStatus();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == STATUS_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( STATUS_ADDRESS_MASK | address ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testReadPegNumbers()
///
/// \brief  Tests the read peg numbers command 0.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testReadPegNumbers()
{
    // Protocol runs the read peg numbers command.
    m_mockCommsProtocol->doReadPegNumbers();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == READ_PEG_NUMBERS_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == READ_PEG_NUMBERS_COMMAND );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testLedDrive()
///
/// \brief  Tests the LED drive command 1.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testLedDrive()
{
    // Protocol runs the LED drive command.
    m_mockCommsProtocol->doLedDrive();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == LED_DRIVE_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == LED_DRIVE_COMMAND );

    // TODO: Mock the rest of the message.
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testSolenoidsLock()
///
/// \brief  Tests the solenoids lock command 4.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testSolenoidsLock()
{
    // Protocol runs the solenoids lock command.
    m_mockCommsProtocol->doSolenoidsLock();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == SOLENOIDS_LOCK_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == SOLENOIDS_LOCK_COMMAND );

    // TODO: Mock the rest of the message.
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testReadEnvironment()
///
/// \brief  Tests the read environment command 5.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testReadEnvironment()
{
    // Protocol runs the read environment command.
    m_mockCommsProtocol->doReadEnvironment();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == READ_ENVIRONMENT_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == READ_ENVIRONMENT_COMMAND );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testReadSettings()
///
/// \brief  Tests the read settings command 9.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testReadSettings()
{
    // Protocol runs the read settings command.
    m_mockCommsProtocol->doReadSettings();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == READ_SETTINGS_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == READ_SETTINGS_COMMAND );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testWriteSettings()
///
/// \brief  Tests the write settings command A.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testWriteSettings()
{
    QList<WriteSettings> writeSettingsList;
    writeSettingsList.append( { WRITE_SETTINGS_BYTE_RFID_MIN_LEVEL, 0x11 } );
    writeSettingsList.append( { WRITE_SETTINGS_BYTE_SOLENOID_TIMEOUT, 0x22 } );

    for ( int i = 0; i < writeSettingsList.count(); ++i )
    {
        WriteSettings writeSettings = writeSettingsList.at( i );

        m_mockCommsProtocol->setWriteSettings( writeSettings );

        uint8_t address = 1;

        // Protocol runs the write settings command.
        m_mockCommsProtocol->doWriteSettings();

        // The mock write function sets up the mock command so it should match the real command.
        QVERIFY( m_mockCommsProtocol->m_mockMessageLength == WRITE_SETTINGS_SIZE );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_START] == START_CODE );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_ADDRESS] == ( COMMAND_ADDRESS_MASK | address ) );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_COMMAND] == WRITE_SETTINGS_COMMAND );

        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_LOCATION] == writeSettings.m_location );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_DATA] == writeSettings.m_data );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_CRC_HIGH] == 0x0 );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_CRC_LOW] == 0x0 );

        // Protocol runs the write settings command again with inverted data.
        m_mockCommsProtocol->doWriteSettings();

        // The mock write function sets up the mock command so it should match the real command.
        QVERIFY( m_mockCommsProtocol->m_mockMessageLength == WRITE_SETTINGS_SIZE );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_START] == START_CODE );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_ADDRESS] == ( COMMAND_ADDRESS_MASK | address ) );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[COMMAND_BYTE_COMMAND] == WRITE_SETTINGS_COMMAND );

        // Calculate inverse.
        uint8_t expectedLocation = ~writeSettings.m_location;
        uint8_t expectedData = ~writeSettings.m_data;

        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_LOCATION] == expectedLocation);
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_DATA] == expectedData );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_CRC_HIGH] == 0x0 );
        QVERIFY( m_mockCommsProtocol->m_mockMessage[WRITE_SETTINGS_BYTE_CRC_LOW] == 0x0 );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::testInterfaceAppVersion()
///
/// \brief  Tests the interface application version command.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::testInterfaceAppVersion()
{
    // Mocks the interface application version command.
    m_mockCommsProtocol->doInterfaceAppVersion();

    uint8_t address = 1;

    // The mock write function sets up the mock command so it should match the real command.
    QVERIFY( m_mockCommsProtocol->m_mockMessageLength == APP_VERSION_SIZE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[0] == START_CODE );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[1] == ( COMMAND_ADDRESS_MASK | address ) );
    QVERIFY( m_mockCommsProtocol->m_mockMessage[2] == APP_VERSION_COMMAND );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::cleanup()
{
    CabinetController::destroy();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CabinetControllerTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void CabinetControllerTest::cleanupTestCase()
{

}
