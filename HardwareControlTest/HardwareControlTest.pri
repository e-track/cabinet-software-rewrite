################################################################################
# Dependencies.
################################################################################
include( $$PWD/../HardwareControl/HardwareControl.pri )
include( $$PWD/../UnitTestSuite/UnitTestSuite.pri )

################################################################################
# Library.
################################################################################
if(!build_hardwarecontroltest) {
    LIBS += -L$$(CABINETUI_INSTALLATION)/HardwareControlTest -lHardwareControlTest

    INCLUDEPATH += $$PWD/..
    DEPENDPATH += $$PWD/..
} else {
################################################################################
# Sources.
################################################################################
SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mocks/mockcommsprotocol.cpp \
    $$PWD/mocks/mockfingerprintoperationthread.cpp \
    $$PWD/mocks/mockobserver.cpp \
    $$PWD/mocks/mockquerymanager.cpp \
    $$PWD/tst_cabinetcontroller.cpp \
    $$PWD/tst_cardreadercontroller.cpp \
    $$PWD/tst_commsdefs.cpp \
    $$PWD/tst_commsprotocol.cpp \
    $$PWD/tst_fingerprintcontroller.cpp \
    $$PWD/tst_fingerprintoperationthread.cpp \
    $$PWD/tst_keypadcontrol.cpp \
    $$PWD/tst_serialportprotocol.cpp \
    $$PWD/tst_simulatedprotocol.cpp \
    $$PWD/tst_subject.cpp \
    $$PWD/tst_uartprotocol.cpp

################################################################################
# Headers.
################################################################################
HEADERS += \
    $$PWD/hardwarecontroltest_global.h \
    $$PWD/mocks/mockcommsprotocol.h \
    $$PWD/mocks/mockfingerprintoperationthread.h \
    $$PWD/mocks/mockobserver.h \
    $$PWD/mocks/mockquerymanager.h \
    $$PWD/tst_cabinetcontroller.h \
    $$PWD/tst_cardreadercontroller.h \
    $$PWD/tst_commsdefs.h \
    $$PWD/tst_commsprotocol.h \
    $$PWD/tst_fingerprintcontroller.h \
    $$PWD/tst_fingerprintoperationthread.h \
    $$PWD/tst_keypadcontrol.h \
    $$PWD/tst_serialportprotocol.h \
    $$PWD/tst_simulatedprotocol.h \
    $$PWD/tst_subject.h \
    $$PWD/tst_uartprotocol.h
}
