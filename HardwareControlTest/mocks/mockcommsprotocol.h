#ifndef MOCKCOMMSPROTOCOL_H
#define MOCKCOMMSPROTOCOL_H

// Library.
#include <HardwareControl/commsprotocol.h>

////////////////////////////////////////////////////////////////////////////////
/// \class  Mock implementation of CommsProtocol.
////////////////////////////////////////////////////////////////////////////////
class MockCommsProtocol : public CommsProtocol
{
public:
    explicit MockCommsProtocol();

    // Control.
    virtual bool init();

    // Commands.
    void doGetStatus();
    void doReadPegNumbers();
    void doLedDrive();
    void doSolenoidsLock();
    void doReadEnvironment();
    void doReadSettings();
    void doWriteSettings();
    void doInterfaceAppVersion();

    // Mock properties.
    uint8_t* m_mockMessage;
    size_t m_mockMessageLength;

    uint8_t* m_mockReply;
    size_t m_mockReplyLength;

protected:
    virtual void write();
};

#endif // MOCKCOMMSPROTOCOL_H
