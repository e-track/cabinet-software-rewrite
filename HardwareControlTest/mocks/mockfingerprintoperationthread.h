#ifndef MOCKFINGERPRINTOPERATIONTHREAD_H
#define MOCKFINGERPRINTOPERATIONTHREAD_H

// Library.
#include <HardwareControl/fingerprintoperationthread.h>

// Project.
#include "hardwarecontroltest_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  Mock implementation of FingerprintOperationThread.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT MockFingerprintOperationThread : public FingerprintOperationThread
{
    Q_OBJECT

public:
    explicit MockFingerprintOperationThread( QObject* parent = nullptr );
    virtual ~MockFingerprintOperationThread() = default;

    // Mock functions.
    void doSuccess();
    void doFail();
    void doAutoIdentifySuccess();
    void doAutoIdentifyFail();

    // Operation.
    virtual void setOperation( Operation operation );

    // Control.
    virtual void start();

protected:
    // Threads.
    virtual void run() override;
};

#endif // MOCKFINGERPRINTOPERATIONTHREAD_H
