// Library.
#include <DatabaseControl/constants.h>

// Project.
#include "mockquerymanager.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::MockQueryManager( QObject* parent )
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
MockQueryManager::MockQueryManager( QObject* parent )
    : QueryManager( parent )
    , m_next( 1 )
    , m_nextCount( 0 )
{

}


////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::loginSuccessEvent( const User& user )
///
/// \brief  Adds a successful login event to the database.
///
/// \param  Authenticated user that logged in.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::loginSuccessEvent( const User& user )
{
    Q_UNUSED( user );

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::loginFailEvent( const User& user )
///
/// \brief  Adds a failed login event to the database.
///
/// \param  Authenticated user that failed to log in.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::loginFailEvent( const User& user )
{
    Q_UNUSED( user );

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::logoutEvent( const User& user )
///
/// \brief  Adds a successful logout event to the database.
///
/// \param  Authenticated user that logged out.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::logoutEvent( const User& user )
{
    Q_UNUSED( user );

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::accessDeniedEvent( const User& user )
///
/// \brief  Adds a successful access denied event to the database.
///
/// \param  User that was denied access.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::accessDeniedEvent( const User& user )
{
    Q_UNUSED( user );

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUser( const int userId )
///
/// \brief  Mocks getting the user that corresponds to the given user ID.
///
/// \param  The user ID to match.
///
/// \return True to mock succes, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUser( const int userId )
{
    Q_UNUSED( userId );

    // Reset mock next count.
    m_nextCount = 0;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserEntry( const int fingerprintId, const int cardId )
///
/// \brief  Mocks getting the user entry that corresponds to the given fingerprint ID.
///
/// \param  The fingerprint ID to match.
/// \param  The fingerprint ID to match.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserEntry( const int fingerprintId, const QString& cardId )
{
    Q_UNUSED( fingerprintId );
    Q_UNUSED( cardId );

    // Reset mock next count.
    m_nextCount = 0;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getValue( const QString& key )
///
/// \brief  Mocks the value to be returned from the SQL query result for the given key.
///
/// \param  The requested key.
///
/// \return The mocked value for the given key.
////////////////////////////////////////////////////////////////////////////////
QVariant MockQueryManager::getValue( const QString& key )
{
    return m_mockValues[key];
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getNext()
///
/// \brief  Mocks positioning the query to the next result.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getNext()
{
    bool success = ( m_nextCount < m_next );

    if ( m_nextCount < m_next )
    {
        m_nextCount++;
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserCabinetAccess( const User& user )
///
/// \brief  Gets the cabinet access for the given user.
///
/// \param  The user to search for.
///
/// \return True to mock a successful database read, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserCabinetAccess( const User& user )
{
    Q_UNUSED( user );

    // Reset mock next count.
    m_nextCount = 0;

    m_mockValues[DatabaseControlConstants::_isAccessGranted] = true;

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockQueryManager::getUserSystemAccess( const User& user )
///
/// \brief  Mocks getting the system access for the given user.
///
/// \param  The user to search for.
///
/// \return True to mock success, otherwise false.
////////////////////////////////////////////////////////////////////////////////
bool MockQueryManager::getUserSystemAccess( const User& user )
{
    Q_UNUSED( user );

    // Reset mock next count.
    m_nextCount = 0;

    m_mockValues[DatabaseControlConstants::_isAccessGranted] = true;

    return true;
}
