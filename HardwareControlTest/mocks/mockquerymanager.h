#ifndef MOCKQUERYMANAGER_H
#define MOCKQUERYMANAGER_H

// Library.
#include <DatabaseControl/querymanager.h>
#include <DatabaseControl/user.h>

////////////////////////////////////////////////////////////////////////////////
/// \class  Mock implementation of QueryManager.
////////////////////////////////////////////////////////////////////////////////
class MockQueryManager : public QueryManager
{
    Q_OBJECT

public:
    explicit MockQueryManager( QObject* parent = nullptr );
    ~MockQueryManager() = default;

    // Events.
    virtual bool loginSuccessEvent( const User& user );
    virtual bool loginFailEvent( const User& user );
    virtual bool logoutEvent( const User& user );
    virtual bool accessDeniedEvent( const User& user );

    // User.
    virtual bool getUser( const int userId );
    virtual bool getUserEntry( const int fingerprintId, const QString& cardId );

    // Value.
    virtual QVariant getValue( const QString& key );
    virtual bool getNext();

    // Cabinets.
    virtual bool getUserCabinetAccess( const User& user );

    // Systems.
    virtual bool getUserSystemAccess( const User& user );

    // Values.
    QMap<QString, QVariant> m_mockValues;   ///< Mock values to be returned.
    int m_next;                             ///< Mocks the number of next query results.
    int m_nextCount;                        ///< Counts the number of next query results.
};

#endif // MOCKQUERYMANAGER_H
