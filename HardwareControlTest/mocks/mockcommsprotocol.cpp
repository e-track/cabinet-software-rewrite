// Library.
#include <HardwareControl/commsdefs.h>

// Project.
#include "mockcommsprotocol.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::MockCommsProtocol()
///
/// \brief  Constructor.
////////////////////////////////////////////////////////////////////////////////
MockCommsProtocol::MockCommsProtocol()
    : m_mockMessage( nullptr )
    , m_mockMessageLength( 0 )
    , m_mockReply( nullptr )
    , m_mockReplyLength( 0 )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::init()
///
/// \brief  Mocks the comms protocol initialisation.
///
/// \return True to mock successful initialisation, otherwise, false.
////////////////////////////////////////////////////////////////////////////////
bool MockCommsProtocol::init()
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doGetStatus()
///
/// \brief  Runs the command to get the status.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doGetStatus()
{
    commandGetStatus();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doGetStatus()
///
/// \brief  Runs the command to read the peg numbers.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doReadPegNumbers()
{
    commandReadPegNumbers();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doLedDrive()
///
/// \brief  Runs the command to drive LEDs.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doLedDrive()
{
    commandLedDrive();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doSolenoidsLock()
///
/// \brief  Runs the command to control solenoids and lock.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doSolenoidsLock()
{
    commandSolenoidsLock();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doReadEnvironment()
///
/// \brief  Runs the command to read environment.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doReadEnvironment()
{
    commandReadEnvironment();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doReadSettings()
///
/// \brief  Runs the command to read settings.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doReadSettings()
{
    commandReadSettings();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doWriteSettings()
///
/// \brief  Runs the command to write settings.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doWriteSettings()
{
    commandWriteSettings();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::doInterfaceAppVersion()
///
/// \brief  Runs the command to get the interface application version.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::doInterfaceAppVersion()
{
    commandInterfaceAppVersion();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     MockCommsProtocol::write()
///
/// \brief  Mocks writing a command.
////////////////////////////////////////////////////////////////////////////////
void MockCommsProtocol::write()
{
    m_mockMessage = m_message;
    m_mockMessageLength = m_messageLength;
}
