#ifndef MOCKOBSERVER_H
#define MOCKOBSERVER_H

// Library.
#include <HardwareControl/observer.h>

////////////////////////////////////////////////////////////////////////////////
/// \class  Mock implementation of Observer.
////////////////////////////////////////////////////////////////////////////////
class MockObserver : public Observer
{
public:
    explicit MockObserver();

    virtual void update();

    int signalCount;
};

#endif // MOCKOBSERVER_H
