#ifndef TST_KEYPADCONTROL_H
#define TST_KEYPADCONTROL_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

class KeypadControl;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the KeypadControl class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT KeypadControlTest : public UnitTest
{
    Q_OBJECT

public:
    explicit KeypadControlTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    KeypadControl* m_keypadControl;     ///< Class under test.
};

#endif // TST_KEYPADCONTROL_H
