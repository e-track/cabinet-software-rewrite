﻿#ifndef TST_COMMSDEFS_H
#define TST_COMMSDEFS_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the comms definitions.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT CommsDefsTest : public UnitTest
{
    Q_OBJECT

public:
    explicit CommsDefsTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // General.
    void testGeneral();
    void testGetStatus();

    // Pegs.
    void testReadPegNumbers();

    // LED.
    void testLedDrive();
    void testLedColours();
    void testLedStates();

    // Solenoids.
    void testSolenoidsLockControl();

    // Environment.
    void testReadEnvironment();

    // Settings.
    void testReadSettings();
    void testWriteSettings();

    // Version.
    void testAppVersion();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();
};

#endif // TST_COMMSDEFS_H
