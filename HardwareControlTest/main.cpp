// Qt.
#include <QGuiApplication>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <UnitTestSuite/unittest.h>

// Project.
#include "tst_cabinetcontroller.h"
#include "tst_cardreadercontroller.h"
#include "tst_commsdefs.h"
#include "tst_commsprotocol.h"
#include "tst_fingerprintcontroller.h"
#include "tst_fingerprintoperationthread.h"
#include "tst_keypadcontrol.h"
#include "tst_serialportprotocol.h"
#include "tst_simulatedprotocol.h"
#include "tst_subject.h"
#include "tst_uartprotocol.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief  Application entry point.
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // A GUI application is required for the event system.
    QGuiApplication app( argc, argv );

    // HardwareControl tests.
    DECLARE_UNIT_TEST( CabinetControllerTest )
    DECLARE_UNIT_TEST( CardreaderControllerTest )
    DECLARE_UNIT_TEST( CommsDefsTest )
    DECLARE_UNIT_TEST( CommsProtocolTest )
    DECLARE_UNIT_TEST( FingerprintControllerTest )
    DECLARE_UNIT_TEST( FingerprintOperationThreadTest )
    DECLARE_UNIT_TEST( KeypadControlTest )
    DECLARE_UNIT_TEST( SerialPortProtocolTest )
    DECLARE_UNIT_TEST( SimulatedProtocolTest )
    DECLARE_UNIT_TEST( SubjectTest )
    DECLARE_UNIT_TEST( UartProtocolTest )

    return UnitTest::run();
}
