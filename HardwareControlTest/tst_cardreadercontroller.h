#ifndef CARDREADERCONTROLLERTEST_H
#define CARDREADERCONTROLLERTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

class CardreaderController;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the CardreaderController class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT CardreaderControllerTest : public UnitTest
{
    Q_OBJECT

public:
    explicit CardreaderControllerTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Registration.
    void testRegisterClass();

    // Operation.
    void testOperationSignals();
    void testOperationInvokables();

    // Enroll.
    void testCard();
    void testCardStatus();
    void testCardStatusProperties();

    // Initialisation.
    void testInitialisation();
    void testInitialisationInvokable();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    CardreaderController* m_cardreaderController;       ///< Class under test.
};

#endif // CARDREADERCONTROLLERTEST_H
