#ifndef TST_SERIALPORTPROTOCOL_H
#define TST_SERIALPORTPROTOCOL_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

// Forward declarations.
class SerialPortProtocol;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the Qt serial port comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT SerialPortProtocolTest : public UnitTest
{
    Q_OBJECT

public:
    explicit SerialPortProtocolTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    SerialPortProtocol* m_serialPortProtocol;       ///< Class under test.
};

#endif // TST_SERIALPORTPROTOCOL_H
