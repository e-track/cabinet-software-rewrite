#ifndef TST_FINGERPRINTOPERATIONTHREAD_H
#define TST_FINGERPRINTOPERATIONTHREAD_H

// Qt.
#include <QList>

// Library.
#include <HardwareControl/fingerprintoperationthread.h>
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

class FingerprintOperationThread;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the FingerprintOperationThread class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT FingerprintOperationThreadTest : public UnitTest
{
    Q_OBJECT

public:
    explicit FingerprintOperationThreadTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Operation.
    void testOperation();
    void testOperationRun();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    FingerprintOperationThread* m_operationThread;      ///< Class under test.
    QList<Operation> m_operations;
};

#endif // FINGERPRINTOPERATIONTHREAD_H
