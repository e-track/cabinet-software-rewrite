// Qt.
#include <QByteArray>
#include <QMetaObject>
#include <QQmlEngine>
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/keypadcontrol.h>

// Project.
#include "tst_keypadcontrol.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::KeypadControlTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
KeypadControlTest::KeypadControlTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void KeypadControlTest::init()
{
    // Create class under test.
    m_keypadControl = new KeypadControl();
    QVERIFY( m_keypadControl );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void KeypadControlTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void KeypadControlTest::testRegisterClass()
{
    // Register the class.
    KeypadControl::registerClass();

    // Expected class and module names.
    QString expectedModuleName = "Keypad";
    QString expectedClassName = "Control";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void KeypadControlTest::cleanup()
{
    delete m_keypadControl;
    m_keypadControl = nullptr;
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     KeypadControlTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void KeypadControlTest::cleanupTestCase()
{

}
