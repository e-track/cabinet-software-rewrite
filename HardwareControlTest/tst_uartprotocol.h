#ifndef UARTPROTOCOLTEST_H
#define UARTPROTOCOLTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

// Forward declarations.
class UartProtocol;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the UART comms protocol.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT UartProtocolTest : public UnitTest
{
    Q_OBJECT

public:
    explicit UartProtocolTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    UartProtocol* m_uartProtocol;           ///< Class under test.
};

#endif // UARTPROTOCOLTEST_H
