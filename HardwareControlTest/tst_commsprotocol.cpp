// External.
#include <stdexcept>

// Qt.
#include <QtTest/QtTest>

// Library.
#include <HardwareControl/commsprotocol.h>
#include <HardwareControl/commsdefs.h>

// Project.
#include "tst_commsprotocol.h"

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::CommsProtocolTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
CommsProtocolTest::CommsProtocolTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::init()
{
    // Create class under test.
    m_commsProtocol = new CommsProtocol();
    QVERIFY( m_commsProtocol );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::initTestCase()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testInitialisation()
///
/// \brief  Tests the init function asserts if called in the base class.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testInitialisation()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->init();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testEnums()
///
/// \brief  Tests the enums are defined correctly.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testEnums()
{
    // Test the enum.
    QVERIFY( ProtocolCommand::NULL_PROTOCOL_CMD == -1 );
    QVERIFY( ProtocolCommand::CMD_GET_STATUS == 0 );
    QVERIFY( ProtocolCommand::CMD_READ_PEG_NUMBERS == 1 );
    QVERIFY( ProtocolCommand::CMD_LED_DRIVE == 2 );
    QVERIFY( ProtocolCommand::CMD_SOLENOIDS_LOCK == 3 );
    QVERIFY( ProtocolCommand::CMD_READ_ENVIRONMENT == 4 );
    QVERIFY( ProtocolCommand::CMD_READ_SETTINGS == 5 );
    QVERIFY( ProtocolCommand::CMD_WRITE_SETTINGS_1 == 6 );
    QVERIFY( ProtocolCommand::CMD_WRITE_SETTINGS_2 == 7 );
    QVERIFY( ProtocolCommand::CMD_INTERFACE_APP_VERSION == 8 );

    // Test other commands haven't been added.
    QVERIFY( ProtocolCommand::PROTOCOL_CMD_COUNT == 9 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testDataStructures()
///
/// \brief  Tests the data structures are defined correctly.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testDataStructures()
{
    // Protocol status.
    ProtocolStatus protocolStatus = {};

    QVERIFY( protocolStatus.m_address == 0 );
    QVERIFY( protocolStatus.m_bootloader == false );
    QVERIFY( protocolStatus.m_ledError == false );
    QVERIFY( protocolStatus.m_e2Error == false );
    QVERIFY( protocolStatus.m_leftKeySensor == false );
    QVERIFY( protocolStatus.m_rightKeySensor == false );
    QVERIFY( protocolStatus.m_buttonPressed == false );
    QVERIFY( protocolStatus.m_lockOpen == false );
    QVERIFY( protocolStatus.m_lockUnlatched == false );
    QVERIFY( protocolStatus.m_reedSwitchOpen == false );

    int expectedStatusSize = ( sizeof( bool ) * 9 ) + ( sizeof( uint8_t ) );
    int statusSize = sizeof( ProtocolStatus );
    QVERIFY( statusSize == expectedStatusSize );

    // Peg.
    Peg peg = {};
    QVERIFY( peg.m_number[0] == 0 );
    QVERIFY( peg.m_number[1] == 0 );
    QVERIFY( peg.m_number[2] == 0 );
    QVERIFY( peg.m_number[3] == 0 );
    QVERIFY( peg.m_number[4] == 0 );
    QVERIFY( peg.m_signalLevel == 0 );

    int expectedPegSize = sizeof( uint8_t ) * 6;
    int pegSize = sizeof( Peg );
    QVERIFY( pegSize == expectedPegSize );

    // LED.
    Led led = {};
    QVERIFY( led.m_colour1 == 0 );
    QVERIFY( led.m_colour2 == 0 );

    int expectedLedSize = sizeof( uint8_t ) * 2;
    int ledSize = sizeof( Led );
    QVERIFY( ledSize == expectedLedSize );

    // LED state.
    LedState ledState = {};
    QVERIFY( ledState.m_sounderState == 0 );
    QVERIFY( ledState.m_greenState == 0 );
    QVERIFY( ledState.m_redState == 0 );

    int expectedLedStateSize = sizeof( uint8_t ) * 3;
    int ledStateSize = sizeof( LedState );
    QVERIFY( ledStateSize == expectedLedStateSize );

    // Environment.
    Environment environment = {};
    QVERIFY( environment.m_temperature == 0 );
    QVERIFY( environment.m_humidity == 0 );

    int expectedEnvironmentSize = sizeof( uint8_t ) * 2;
    int environmentSize = sizeof( Environment );
    QVERIFY( environmentSize == expectedEnvironmentSize );

    // Read settings.
    ReadSettings readSettings = {};
    QVERIFY( readSettings.m_rfidMinLevel == 0 );
    QVERIFY( readSettings.m_solenoidTimeout == 0 );
    QVERIFY( readSettings.m_sixthPeg == false );

    int expectedReadSettingsSize = ( sizeof( uint8_t ) * 2 ) + sizeof ( bool );
    int readSettingsSize = sizeof( ReadSettings );
    QVERIFY( readSettingsSize == expectedReadSettingsSize );

    // Write settings.
    WriteSettings writeSettings = {};
    QVERIFY( writeSettings.m_location == 0 );
    QVERIFY( writeSettings.m_data == 0 );

    int expectedWriteSettingsSize = sizeof( uint8_t ) * 2;
    int writeSettingsSize = sizeof( WriteSettings );
    QVERIFY( writeSettingsSize == expectedWriteSettingsSize );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommand()
///
/// \brief  Tests the current command can be obtained.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommand()
{
    ProtocolCommand defaultCommand = ProtocolCommand::NULL_PROTOCOL_CMD;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == defaultCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandGetStatus()
///
/// \brief  Tests the get status command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandGetStatus()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandGetStatus();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_GET_STATUS;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandReadPegNumbers()
///
/// \brief  Tests the read peg numbers command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandReadPegNumbers()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandReadPegNumbers();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_READ_PEG_NUMBERS;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandLedDrive()
///
/// \brief  Tests the LED drive command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandLedDrive()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        // TODO: Set LEDs before running the command. The class should
        // probably save the state of the LEDs before the command is sent
        // so its state can be maintained.
        m_commsProtocol->commandLedDrive();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_LED_DRIVE;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandSolenoidsLock()
///
/// \brief  Tests the solenoids and lock control command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandSolenoidsLock()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandSolenoidsLock();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_SOLENOIDS_LOCK;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandReadEnvironment()
///
/// \brief  Tests the read environment command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandReadEnvironment()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandReadEnvironment();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_READ_ENVIRONMENT;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandReadSettings()
///
/// \brief  Tests the read settings command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandReadSettings()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandReadSettings();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_READ_SETTINGS;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testCommandInterfaceAppVersion()
///
/// \brief  Tests the interface application version command.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testCommandInterfaceAppVersion()
{
    // Cannot initialise a comms protocol so it should throw an exception.
    bool caught = false;

    try
    {
        m_commsProtocol->commandInterfaceAppVersion();
    }
    catch ( std::runtime_error )
    {
        caught = true;
    }

    QVERIFY( caught );

    // Test the command has actually been set even if no data was read or written.
    ProtocolCommand expectedCommand = ProtocolCommand::CMD_INTERFACE_APP_VERSION;
    ProtocolCommand command = m_commsProtocol->getCommand();

    QVERIFY( command == expectedCommand );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testStatus()
///
/// \brief  Tests access to the status.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testStatus()
{
    // Check default status.
    ProtocolStatus status = m_commsProtocol->getStatus();

    QVERIFY( status.m_address == 0 );
    QVERIFY( status.m_bootloader == false );
    QVERIFY( status.m_ledError == false );
    QVERIFY( status.m_e2Error == false );
    QVERIFY( status.m_leftKeySensor == false );
    QVERIFY( status.m_rightKeySensor == false );
    QVERIFY( status.m_buttonPressed == false );
    QVERIFY( status.m_lockOpen == false );
    QVERIFY( status.m_lockUnlatched == false );
    QVERIFY( status.m_reedSwitchOpen == false );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testPegs()
///
/// \brief  Tests access to the pegs.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testPegs()
{
    Peg peg;

    QList<Peg> expectedPegs;
    expectedPegs.append( { { 0x01, 0x02, 0x03, 0x04, 0x05 }, 0x06 } );
    expectedPegs.append( { { 0x07, 0x08, 0x09, 0x0a, 0x0b }, 0x0c } );
    expectedPegs.append( { { 0x0d, 0x0e, 0x0f, 0x10, 0x11 }, 0x12 } );
    expectedPegs.append( { { 0x13, 0x14, 0x15, 0x16, 0x17 }, 0x18 } );
    expectedPegs.append( { { 0x19, 0x1a, 0x1b, 0x1c, 0x1d }, 0x1e } );

    // Test getting and setting the peg with out of bounds index.
    QVERIFY( !m_commsProtocol->getPeg( -1, peg ) );
    QVERIFY( !m_commsProtocol->getPeg( 6, peg ) );
    QVERIFY( !m_commsProtocol->setPeg( -1, expectedPegs.at( 0 ) ) );
    QVERIFY( !m_commsProtocol->setPeg( 6, expectedPegs.at( 0 ) ) );

    // Check default pegs.
    for ( unsigned int i = 0; i < CommsProtocol::_numPegs; ++i )
    {
        QVERIFY( m_commsProtocol->getPeg( i, peg ) );
        QVERIFY( peg.m_number[0] == 0 );
        QVERIFY( peg.m_number[1] == 0 );
        QVERIFY( peg.m_number[2] == 0 );
        QVERIFY( peg.m_number[3] == 0 );
        QVERIFY( peg.m_number[4] == 0 );
        QVERIFY( peg.m_signalLevel == 0 );
    }

    // Set the new pegs.
    for ( int i = 0; i < expectedPegs.count(); ++i )
    {
        QVERIFY( m_commsProtocol->setPeg( i, expectedPegs.at( i ) ) );
    }

    // Test peg matches the ones set.
    for ( int i = 0; i < expectedPegs.count(); ++i )
    {
        Peg peg;
        Peg expectedPeg = expectedPegs.at( i );

        QVERIFY( m_commsProtocol->getPeg( i, peg ) );
        QVERIFY( peg.m_number[0] == expectedPeg.m_number[0] );
        QVERIFY( peg.m_number[1] == expectedPeg.m_number[1] );
        QVERIFY( peg.m_number[2] == expectedPeg.m_number[2] );
        QVERIFY( peg.m_number[3] == expectedPeg.m_number[3] );
        QVERIFY( peg.m_number[4] == expectedPeg.m_number[4] );
        QVERIFY( peg.m_signalLevel == expectedPeg.m_signalLevel );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testLeds()
///
/// \brief  Tests access to the LEDs.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testLeds()
{
    Led led;

    QList<Led> expectedLeds;
    expectedLeds.append( { 0x01, 0x02 } );
    expectedLeds.append( { 0x03, 0x04 } );
    expectedLeds.append( { 0x05, 0x06 } );
    expectedLeds.append( { 0x07, 0x08 } );
    expectedLeds.append( { 0x09, 0x0a } );

    // Test getting and setting the led with out of bounds index.
    QVERIFY( !m_commsProtocol->getLed( -1, led ) );
    QVERIFY( !m_commsProtocol->getLed( 6, led ) );
    QVERIFY( !m_commsProtocol->setLed( -1, expectedLeds.at( 0 ) ) );
    QVERIFY( !m_commsProtocol->setLed( 6, expectedLeds.at( 0 ) ) );

    // Check default LEDs.
    for ( unsigned int i = 0; i < CommsProtocol::_numPegs; ++i )
    {
        QVERIFY( m_commsProtocol->getLed( i, led ) );
        QVERIFY( led.m_colour1 == 0 );
        QVERIFY( led.m_colour2 == 0 );
    }

    // Set the new LEDs.
    for ( int i = 0; i < expectedLeds.count(); ++i )
    {
        QVERIFY( m_commsProtocol->setLed( i, expectedLeds.at( i ) ) );
    }

    // Test LED matches the ones set.
    for ( int i = 0; i < expectedLeds.count(); ++i )
    {
        Led led;
        Led expectedLed = expectedLeds.at( i );

        QVERIFY( m_commsProtocol->getLed( i, led ) );
        QVERIFY( led.m_colour1 == expectedLed.m_colour1 );
        QVERIFY( led.m_colour2 == expectedLed.m_colour2 );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testLedColours()
///
/// \brief  Tests LED colours.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testLedColours()
{
    QList<uint8_t> colours;
    colours.append( LED_BLACK );
    colours.append( LED_RED );
    colours.append( LED_MAGENTA );
    colours.append( LED_BLUE );
    colours.append( LED_CYAN );
    colours.append( LED_GREEN );
    colours.append( LED_YELLOW );
    colours.append( LED_WHITE );

    QList<uint8_t> expectedColours = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                                       0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                                       0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
                                       0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
                                       0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
                                       0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57,
                                       0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
                                       0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77 };

    int count = 0;

    for ( int i = 0; i < colours.count(); ++i )
    {
        uint8_t colourHigh = colours.at( i );

        for ( int j = 0; j < colours.count(); ++j )
        {
            uint8_t colourLow = colours.at( j );
            uint8_t colour = CommsProtocol::getColourByte( colourLow, colourHigh );
            uint8_t expectedColour = expectedColours.at( count++ );

            QVERIFY( colour == expectedColour );
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testLedColours()
///
/// \brief  Tests LED states.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testLedStates()
{
    // Check default LED state.
    LedState ledState = m_commsProtocol->getLedState();
    QVERIFY( ledState.m_sounderState == 0 );
    QVERIFY( ledState.m_greenState == 0 );
    QVERIFY( ledState.m_redState == 0 );

    QList<LedState> expectedLedStates;
    expectedLedStates.append( { LED_OFF,           LED_OFF,         LED_OFF } );
    expectedLedStates.append( { LED_OFF,           LED_OFF,         LED_RED_FLASH } );
    expectedLedStates.append( { LED_OFF,           LED_OFF,         LED_RED_ON } );
    expectedLedStates.append( { LED_OFF,           LED_GREEN_FLASH, LED_OFF } );
    expectedLedStates.append( { LED_OFF,           LED_GREEN_ON,    LED_OFF } );
    expectedLedStates.append( { LED_SOUNDER_PULSE, LED_OFF,         LED_OFF } );
    expectedLedStates.append( { LED_SOUNDER_ON,    LED_OFF,         LED_OFF } );

    for ( int i = 0; i < expectedLedStates.count(); ++i )
    {
        // Set the new LED state.
        LedState expectedLedState = expectedLedStates.at( i );
        m_commsProtocol->setLedState( expectedLedState );

        // Test LED state matches the one set.
        ledState = m_commsProtocol->getLedState();

        QVERIFY( ledState.m_sounderState == expectedLedState.m_sounderState );
        QVERIFY( ledState.m_greenState == expectedLedState.m_greenState );
        QVERIFY( ledState.m_redState == expectedLedState.m_redState );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testSolenoidsLock()
///
/// \brief  Tests access to the solenoids and lock.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testSolenoidsLock()
{
    // Check default solenoids and lock state.
    uint8_t solenoidsLockStates = m_commsProtocol->getSolenoidsLockState();
    QVERIFY( solenoidsLockStates == 0 );

    QList<uint8_t> expectedSolenoidsLockStates;
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_LOCK );
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_PEG_5 );
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_PEG_4 );
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_PEG_3 );
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_PEG_2 );
    expectedSolenoidsLockStates.append( SOLENOIDS_LOCK_BIT_RELEASE_PEG_1 );

    for ( int i = 0; i < expectedSolenoidsLockStates.count(); ++i )
    {
        // Set the new solenoids and lock state.
        uint8_t expectedSolenoidsLockState = expectedSolenoidsLockStates.at( i );
        m_commsProtocol->setSolenoidsLockState( expectedSolenoidsLockState );

        // Test the solenoids and lock state matches the one set.
        solenoidsLockStates = m_commsProtocol->getSolenoidsLockState();
        QVERIFY( solenoidsLockStates == expectedSolenoidsLockState );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testEnvironment()
///
/// \brief  Tests access to the environment status.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testEnvironment()
{
    // Check default environment.
    Environment environment = m_commsProtocol->getEnvironment();

    QVERIFY( environment.m_temperature == 0 );
    QVERIFY( environment.m_humidity == 0 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testSettings()
///
/// \brief  Tests access to the settings.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testSettings()
{
    // Check default read settings.
    ReadSettings settings = m_commsProtocol->getReadSettings();
    QVERIFY( settings.m_rfidMinLevel == 0 );
    QVERIFY( settings.m_solenoidTimeout == 0 );
    QVERIFY( settings.m_sixthPeg == false );

    QList<WriteSettings> expectedWriteSettingsList;
    expectedWriteSettingsList.append( { 0x01, 0x02 } );
    expectedWriteSettingsList.append( { 0x03, 0x04 } );

    for ( int i = 0; i < expectedWriteSettingsList.count(); ++i )
    {
        // Set the write settings.
        WriteSettings expectedWriteSettings = expectedWriteSettingsList.at( i );
        m_commsProtocol->setWriteSettings( expectedWriteSettings );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::testInterfaceApplicationVersion()
///
/// \brief  Tests access to the interface application version.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::testInterfaceApplicationVersion()
{
    // Check default bootloader version.
    InterfaceApplicationVersion version = m_commsProtocol->getInterfaceAppVersion();
    QVERIFY( version.m_bootloaderMajor == 0 );
    QVERIFY( version.m_bootloaderMinor == 0 );
    QVERIFY( version.m_applicationMajor == 0 );
    QVERIFY( version.m_applicationMinor == 0 );

    QList<InterfaceApplicationVersion> expectedVersions;
    expectedVersions.append( { 0x11, 0x22, 0x33, 0x44 } );
    expectedVersions.append( { 0x55, 0x66, 0x77, 0x88 } );

    for ( int i = 0; i < expectedVersions.count(); ++i )
    {
        // Set the write settings.
        InterfaceApplicationVersion expectedVersion = expectedVersions.at( i );
        m_commsProtocol->setInterfaceAppVersion( expectedVersion );

        version = m_commsProtocol->getInterfaceAppVersion();

        QVERIFY( version.m_bootloaderMajor == expectedVersion.m_bootloaderMajor );
        QVERIFY( version.m_bootloaderMinor == expectedVersion.m_bootloaderMinor );
        QVERIFY( version.m_applicationMajor == expectedVersion.m_applicationMajor );
        QVERIFY( version.m_applicationMinor == expectedVersion.m_applicationMinor );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::cleanup()
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     CommsProtocolTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void CommsProtocolTest::cleanupTestCase()
{

}
