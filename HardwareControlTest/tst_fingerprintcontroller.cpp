// Qt.
#include <QByteArray>
#include <QMetaObject>
#include <QQmlEngine>
#include <QSignalSpy>
#include <QtTest/QtTest>

// Library.
#include <DatabaseControl/databasecontrol.h>
#include <DatabaseControl/messagemanager.h>
#include <HardwareControl/fingerprintcontroller.h>
#include <HardwareControl/fingerprintenums.h>

// Project.
#include "mocks/mockfingerprintoperationthread.h"
#include "mocks/mockquerymanager.h"
#include "tst_fingerprintcontroller.h"

// Statics.
static bool _onOff[] = { true, false };        // Convenient bool pair.

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::FingerprintControllerTest()
///
/// \brief  Constructor.
///
/// \param  Parent.
////////////////////////////////////////////////////////////////////////////////
FingerprintControllerTest::FingerprintControllerTest( QObject* parent )
    : UnitTest( parent )
{

}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::init()
///
/// \brief  Initialises the test case, called before every test.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::init()
{
    // Create mock operation thread.
    m_mockOpThread = new MockFingerprintOperationThread();
    QVERIFY( m_mockOpThread );

    // Create class under test.
    m_fingerprintController = FingerprintController::instance( m_mockOpThread );
    QVERIFY( m_fingerprintController );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::initTestCase()
///
/// \brief  Initialises the test case, called once before the test run.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::initTestCase()
{
    // Create mock query manager that will be the same for all tests.
    m_mockQueryManager = new MockQueryManager();
    QVERIFY( m_mockQueryManager );

    // TODO: Possibly inject a mock message manager.

    // Inject into the database controller.
    DatabaseControl::instance( m_mockQueryManager, new MessageManager() );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testRegisterClass()
///
/// \brief  Tests class can be registered correctly.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testRegisterClass()
{
    // Register the class.
    FingerprintController::registerClass();

    // Check expected class registration.
    QString expectedModuleName = "HwControl";
    QString expectedClassName = "Fingerprint";

    int typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );

    // Check enum registration.
    expectedModuleName = "HwControl.Enums";

    typeId = qmlTypeId( expectedModuleName.toStdString().c_str(), 1, 0, expectedClassName.toStdString().c_str() );
    QVERIFY( typeId > -1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testInitialisation()
///
/// \brief  Tests initialisation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testInitialisation()
{
    // Signal spies.
    QSignalSpy initialisationSuccessSpy( m_fingerprintController, &FingerprintController::initialisationSuccess );
    QSignalSpy initialisationFailSpy( m_fingerprintController, &FingerprintController::initialisationFail );

    // TODO: Use mocks for the non-threaded operations.

    m_fingerprintController->initialise();

    // Verify initCommPort signals.
    QVERIFY( initialisationSuccessSpy.count() + initialisationFailSpy.count() == 1 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testInitialisationInvokable()
///
/// \brief  Tests that the initialisaton function can be invoked.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testInitialisationInvokable()
{
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "initialise" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testSdkVersion()
///
/// \brief  Tests SDK version.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testSdkVersion()
{
    // Default.
    int expectedMajorVersion = -1;
    int expectedMinorVersion = -1;
    int expectedExtraVersion = -1;

    QVERIFY( FingerprintController::_majorVersion == expectedMajorVersion );
    QVERIFY( FingerprintController::_minorVersion == expectedMinorVersion );
    QVERIFY( FingerprintController::_extraVersion == expectedExtraVersion );

    // Get SDK version.
    m_fingerprintController->sdkVersion();

    expectedMajorVersion = 3;
    expectedMinorVersion = 3;
    expectedExtraVersion = 0;

    QVERIFY( FingerprintController::_majorVersion == expectedMajorVersion );
    QVERIFY( FingerprintController::_minorVersion == expectedMinorVersion );
    QVERIFY( FingerprintController::_extraVersion == expectedExtraVersion );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testSdkVersionInvokable()
///
/// \brief  Tests that the SDK version function can be invoked.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testSdkVersionInvokable()
{
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "sdkVersion" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testSimulationInvokables()
///
/// \brief  Tests that the fingerprint simulation functions can be invoked.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testSimulationInvokables()
{
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "simulatePressed" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testOperationSignals()
///
/// \brief  Tests fingerprint operation signals.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testOperationSignals()
{
    // Redundant now?
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testOperationInvokables()
///
/// \brief  Tests that the fingerprint operations can be invoked.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testOperationInvokables()
{
    // Enroll.
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "enroll" ) );

    // Identify.
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "identify" ) );

    // Verify.
    QVERIFY( QMetaObject::invokeMethod( m_fingerprintController, "verify" ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testEnroll()
///
/// \brief  Tests the enroll operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testEnroll()
{
    Fingerprint::Status expectedEnrollStatus = Fingerprint::STATUS_NULL;
    Fingerprint::Status enrollStatus = m_fingerprintController->getStatus();

    QVERIFY( enrollStatus == expectedEnrollStatus );

    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy successSpy( m_fingerprintController, &FingerprintController::enrollSuccess );
        QSignalSpy failSpy( m_fingerprintController, &FingerprintController::enrollFail );

        m_fingerprintController->enroll();
        QVERIFY( successSpy.count() == 0 && failSpy.count() == 0 );

        expectedEnrollStatus = Fingerprint::STATUS_ENROLL;
        enrollStatus = m_fingerprintController->getStatus();
        QVERIFY( enrollStatus == expectedEnrollStatus );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doSuccess();
            QVERIFY( successSpy.count() == 1 && failSpy.count() == 0 );

            expectedEnrollStatus = Fingerprint::STATUS_ENROLL_SUCCESS;
            enrollStatus = m_fingerprintController->getStatus();
            QVERIFY( enrollStatus == expectedEnrollStatus );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doFail();
            QVERIFY( successSpy.count() == 0 && failSpy.count() == 1 );

            expectedEnrollStatus = Fingerprint::STATUS_ENROLL_ERROR;
            enrollStatus = m_fingerprintController->getStatus();
            QVERIFY( enrollStatus == expectedEnrollStatus );
        }

        // The operation should only stop if the response is success.
        Operation expectedOperation = success ? Operation::None : Operation::Enroll;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // The operation shouldn't change once the thread finishes.
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testEnrollCallback()
///
/// \brief  Tests the callback function for the enroll operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testEnrollCallback()
{
    QSignalSpy spy( m_fingerprintController, &FingerprintController::statusChanged );

    for ( int i = 0; i < 2; ++i )
    {
        // Start enroll.
        m_fingerprintController->enroll();

        // Mimick callback.
        FingerprintController::callback_UF_Enroll( 0, UF_ENROLL_MODE::UF_ENROLL_ONE_TIME, i );
        Fingerprint::Status enrollStatus = m_fingerprintController->getStatus();

        // Num enrolls should only be updated when the enroll success count is 1
        // (i.e. when 2 fingerprint templates are required).
        Fingerprint::Status expectedEnrollStatus =  ( i == 1
                                                        ? Fingerprint::STATUS_ENROLL_IN_PROGRESS
                                                        : Fingerprint::STATUS_ENROLL );

        QVERIFY( enrollStatus == expectedEnrollStatus );

        // Start enroll.
        m_fingerprintController->enroll();

        // Num enrolls should always be zero when enroll starts.
        expectedEnrollStatus = Fingerprint::STATUS_ENROLL;
        enrollStatus = m_fingerprintController->getStatus();

        QVERIFY( enrollStatus == expectedEnrollStatus );
    }

    // One signal is expected after the calls to enroll() and one after the callback when
    // the enroll count is 1.
    QVERIFY( spy.count() == 3 );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testStatusProperties()
///
/// \brief  Tests the enroll status properties.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testStatusProperties()
{
    // Enroll status readable.
    QVariant enrollStatus = m_fingerprintController->property( "status" );
    QVERIFY( enrollStatus.isValid() );

    // Enroll status is not writable.
    QVERIFY( !m_fingerprintController->setProperty( "enrollStatus", Fingerprint::STATUS_ENROLL ) );
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testIdentify()
///
/// \brief  Tests the identify operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testIdentify()
{
    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy successSpy( m_fingerprintController, &FingerprintController::identifySuccess );
        QSignalSpy failSpy( m_fingerprintController, &FingerprintController::identifyFail );

        m_fingerprintController->identify();
        QVERIFY( successSpy.count() == 0 && failSpy.count() == 0 );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doSuccess();
            QVERIFY( successSpy.count() == 1 && failSpy.count() == 0 );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doFail();
            QVERIFY( successSpy.count() == 0 && failSpy.count() == 1 );
        }

        // The operation should always stop following a response.
        Operation expectedOperation = Operation::None;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // The operation shouldn't change once the thread finishes.
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testVerify()
///
/// \brief  Tests the verify operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testVerify()
{
    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy successSpy( m_fingerprintController, &FingerprintController::verifySuccess );
        QSignalSpy failSpy( m_fingerprintController, &FingerprintController::verifyFail );

        m_fingerprintController->verify();
        QVERIFY( successSpy.count() == 0 && failSpy.count() == 0 );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doSuccess();
            QVERIFY( successSpy.count() == 1 && failSpy.count() == 0 );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doFail();
            QVERIFY( successSpy.count() == 0 && failSpy.count() == 1 );
        }

        // The operation should always stop following a response.
        Operation expectedOperation = Operation::None;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // The operation shouldn't change once the thread finishes.
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testAutoIdentify()
///
/// \brief  Tests the auto identify operation.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testAutoIdentify()
{
    // Enable auto identify operation.
    m_fingerprintController->autoIdentify( true );

    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy successSpy( m_fingerprintController, &FingerprintController::autoIdentifySuccess );
        QSignalSpy failSpy( m_fingerprintController, &FingerprintController::autoIdentifyFail );

        QVERIFY( successSpy.count() == 0 && failSpy.count() == 0 );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doAutoIdentifySuccess();
            QVERIFY( successSpy.count() == 1 && failSpy.count() == 0 );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doAutoIdentifyFail();
            QVERIFY( successSpy.count() == 0 && failSpy.count() == 1 );
        }

        // The operation should stay running if the operation fails .
        Operation expectedOperation = success ? Operation::None : Operation::AutoIdentify;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // The operation shouldn't change once the thread finishes.
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testAutoIdentifyEnable()
///
/// \brief  Tests the operation to enable auto identify.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testAutoIdentifyEnable()
{
    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy enableSuccessSpy( m_fingerprintController, &FingerprintController::autoIdentifyEnableSuccess );
        QSignalSpy enableFailSpy( m_fingerprintController, &FingerprintController::autoIdentifyEnableFail );

        m_fingerprintController->autoIdentify( true );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doSuccess();
            QVERIFY( enableSuccessSpy.count() == 1 && enableFailSpy.count() == 0 );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doFail();
            QVERIFY( enableSuccessSpy.count() == 0 && enableFailSpy.count() == 1 );
        }

        // If the enable operation was successful then the controller should automatically
        // move to the auto identify operation.
        Operation expectedOperation = success ? Operation::AutoIdentify : Operation::None;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // The operation shouldn't change once the thread finishes.
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::testAutoIdentifyDisable()
///
/// \brief  Tests the operation to disable auto identify.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::testAutoIdentifyDisable()
{
    for ( bool success : _onOff )
    {
        // Signal spies.
        QSignalSpy disableSuccessSpy( m_fingerprintController, &FingerprintController::autoIdentifyDisableSuccess );
        QSignalSpy disableFailSpy( m_fingerprintController, &FingerprintController::autoIdentifyDisableFail );

        m_fingerprintController->autoIdentify( false );

        if ( success )
        {
            // If the operation is successful then the success signal should be emitted.
            m_mockOpThread->doSuccess();
            QVERIFY( disableSuccessSpy.count() == 1 && disableFailSpy.count() == 0 );
        }
        else
        {
            // If the operation fails then the fail signal should be emitted.
            m_mockOpThread->doFail();
            QVERIFY( disableSuccessSpy.count() == 0 && disableFailSpy.count() == 1 );
        }

        // The operation should always stop following a response.
        Operation expectedOperation = Operation::None;
        Operation operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );

        // There should be no operation once the current one has finished.
        expectedOperation = Operation::None;
        operation = m_mockOpThread->getOperation();
        QVERIFY( operation == expectedOperation );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::cleanup()
///
/// \brief  Cleans up the test case, called after every test function.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::cleanup()
{
    FingerprintController::destroy();
}

////////////////////////////////////////////////////////////////////////////////
/// \fn     FingerprintControllerTest::cleanupTestCase()
///
/// \brief  Cleans up the test case, called at the end of the test run.
////////////////////////////////////////////////////////////////////////////////
void FingerprintControllerTest::cleanupTestCase()
{

}
