#ifndef SUBJECTTEST_H
#define SUBJECTTEST_H

// Library.
#include <UnitTestSuite/unittest.h>

// Project.
#include "hardwarecontroltest_global.h"

// Forward declarations.
class MockObserver;
class Subject;

////////////////////////////////////////////////////////////////////////////////
/// \class  Unit tests for the Subject class.
////////////////////////////////////////////////////////////////////////////////
class HARDWARECONTROLTEST_EXPORT SubjectTest : public UnitTest
{
    Q_OBJECT

public:
    explicit SubjectTest( QObject* parent = nullptr );

private slots:

    // Test initialisation.
    void init();
    void initTestCase();

    // Connection and disconnection.
    void testConnect();
    void testDisonnect();

    // Test cleanup.
    void cleanup();
    void cleanupTestCase();

private:
    Subject* m_subject;                 ///< Class under test.
    MockObserver* m_mockObserver;       ///< Mock observer.
};

#endif // SUBJECTTEST_H
