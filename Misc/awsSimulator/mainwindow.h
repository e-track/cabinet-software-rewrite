#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class QStringListModel;

enum LoadType
{
    NULL_LOAD = -1,
    LIVE,
    DROP,
    COMPLETE
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void resetUIAndCounters();
    
    void displayMessageAndRaiseDockDoor(QString messageHeader, QString messageInfo);
    void displayWarningMessage(QString messageHeader, QString messageInfo);


private slots:
    void onPushButton1Clicked();
    void onPushButton2Clicked();
    void onPushButton3Clicked();
    void onPushButton4Clicked();
    void onPushButton5Clicked();
    void onPushButton6Clicked();
    void onBtnOpenDoorClicked();
    void onBtnResetClicked();
    void onBtnCloseClicked();
    void onBtnODMAccessClicked();


    void onTimeout();

private:
    bool indicateIfKeyPlacedCorrectly( QString keyType, int sitePos, int driverPos, int dropPos, int userPos );


    Ui::MainWindow* ui;

    int m_siteKeyCount;
    int m_driverKeyCount;
    int m_dropKeyCount;
    int m_userKeyCount;
    bool m_doorClosed;

    QStringListModel* m_availableKeysModel;
    QTimer* m_raiseDockDoor;
    LoadType m_loadType;
};

#endif // MAINWINDOW_H
