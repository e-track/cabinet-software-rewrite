#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStringListModel>
#include <QMessageBox>


MainWindow::MainWindow( QWidget* parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
    , m_siteKeyCount( 0 )
    , m_driverKeyCount( 0 )
    , m_dropKeyCount( 0 )
    , m_userKeyCount( 0 )
    , m_doorClosed( 1 )
    , m_availableKeysModel( new QStringListModel( this ) )
    , m_raiseDockDoor( new QTimer( this ) )
    , m_loadType( NULL_LOAD )
{
    ui->setupUi(this);

    QStringList availableKeys;

    QString name = QString("Site key 1");
    availableKeys.append(name);
    name = QString("Site key 2");
    availableKeys.append(name);

    name = QString("Drop Key");
    availableKeys.append(name);

    for(int i=0; i < 5; ++i)
    {
        QString name = QString("Driver key %1").arg(i + 1);
        availableKeys.append(name);
    }

    name = QString("User Key");
    availableKeys.append(name);

    m_availableKeysModel->setStringList(availableKeys);
    ui->availableKeys->setModel(m_availableKeysModel);

    ui->gbCabinet->setEnabled(false);
    ui->gbUserActions->setEnabled(false);
    ui->pushButton6->setEnabled(false);
    ui->plainTextEdit6->setEnabled(false);
    ui->lbI1Input6->setEnabled(false);
    ui->btnODMAccess->setEnabled(false);
    ui->btnClose->setEnabled(false);

    // timer event to represent time for dock door to open a signal will have to be sent and received to detect open and closed
    m_raiseDockDoor->setInterval(2000 );
    m_raiseDockDoor->setSingleShot( true );

    connect( m_raiseDockDoor, &QTimer::timeout, this, &MainWindow::onTimeout );

    connect( ui->pushButton1, &QPushButton::clicked, this, &MainWindow::onPushButton1Clicked );
    connect( ui->pushButton2, &QPushButton::clicked, this, &MainWindow::onPushButton2Clicked );
    connect( ui->pushButton3, &QPushButton::clicked, this, &MainWindow::onPushButton3Clicked );
    connect( ui->pushButton4, &QPushButton::clicked, this, &MainWindow::onPushButton4Clicked );
    connect( ui->pushButton5, &QPushButton::clicked, this, &MainWindow::onPushButton5Clicked );
    connect( ui->pushButton6, &QPushButton::clicked, this, &MainWindow::onPushButton6Clicked );
    connect( ui->btnReset, &QPushButton::clicked, this, &MainWindow::onBtnResetClicked );
    connect( ui->btnClose, &QPushButton::clicked, this, &MainWindow::onBtnCloseClicked );
    connect( ui->btnODMAccess, &QPushButton::clicked, this, &MainWindow::onBtnODMAccessClicked );
    connect( ui->btnOpenDoor, &QPushButton::clicked, this, &MainWindow::onBtnOpenDoorClicked );
}

MainWindow::~MainWindow()
{
    delete ui;

    delete m_availableKeysModel;
}

//Push Button 1 reperesents a site key position I1. This action will have to be detected by a getStatus
void MainWindow::onPushButton1Clicked()
{
     if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {
        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "SiteKey";

        ui->plainTextEdit1->setPlainText(name);

        bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

        if ( validKey){
             ui->checkBox_4->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             ui->checkBox_9->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
            m_siteKeyCount++;
        } else {
            ui->checkBox_4->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
            ui->checkBox_9->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
        }
    }
}

//Push Button 2 reperesents a site key position I2. This action will have to be detected by a getStatus
void MainWindow::onPushButton2Clicked()
{
     if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {
        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "SiteKey";

         ui->plainTextEdit2->setPlainText(name);

         bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

         if ( validKey){
             ui->checkBox_5->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             ui->checkBox_10->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             m_siteKeyCount++;
         } else {
             ui->checkBox_5->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
             ui->checkBox_10->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
         }
     }
}

//Push Button 1 reperesents a site key position for a drop load D1. This action will have to be detected by a getStatus
void MainWindow::onPushButton3Clicked()
{
     if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {
        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "DropKey";

         ui->plainTextEdit3->setPlainText(name);

         bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

         if ( validKey){
             ui->checkBox_6->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             ui->checkBox_11->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             m_dropKeyCount++;
         } else {
             ui->checkBox_6->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
             ui->checkBox_11->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
     }
     }
}

//Push Button 1 reperesents a driver key position D1. This action will have to be detected by a getStatus
void MainWindow::onPushButton4Clicked()
{
     if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {
        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "DriverKey";

         ui->plainTextEdit4->setPlainText(name);

         bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

         if ( validKey){
             ui->checkBox_7->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
              ui->checkBox_12->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             m_driverKeyCount++;
         } else {
             ui->checkBox_7->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
             ui->checkBox_12->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
         }
     }
}

//Push Button 1 reperesents a driver key position D2. This action will have to be detected by a getStatus
void MainWindow::onPushButton5Clicked()
{
    if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {

        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "DriverKey";

         ui->plainTextEdit5->setPlainText(name);

         bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

         if ( validKey){
             ui->checkBox_8->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             ui->checkBox_13->setStyleSheet("QCheckBox::indicator:unchecked{background-color:lightgreen}");
             m_driverKeyCount++;
         } else {
             ui->checkBox_8->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
              ui->checkBox_13->setStyleSheet("QCheckBox::indicator:unchecked{background-color:red}");
         }
    }
}

//Push Button 1 reperesents the ODM presenting a User FOB. This action will have to be detected by a getStatus
// The door will remain disabled from the time a successful Load is warranted until a signal representing
//the dock door has been raised. Once this is detected the beacon on the cabinet will turn green to
//indicate to the ODM that they may now enable the dorr so that the driver can take their keys
void MainWindow::onPushButton6Clicked()
{
     if( !ui->availableKeys->selectionModel()->selectedIndexes().isEmpty() ) {
        QModelIndex index = ui->availableKeys->selectionModel()->selectedIndexes().first();

        QString name =  m_availableKeysModel->data(index).toString();

        int sitePos = name.indexOf("Site");
        int driverPos = name.indexOf("Driver");
        int dropPos = name.indexOf("Drop");
        int userPos = name.indexOf("User");

        QString keyType = "UserKey";

         ui->plainTextEdit6->setPlainText(name);

         bool validKey = indicateIfKeyPlacedCorrectly(keyType, sitePos,driverPos,dropPos,userPos);

         if ( validKey){
             m_userKeyCount++;
            }
         else
         {
             ui->btnRed->setStyleSheet("background-color:red");
         }
    }
}

/*
 * This function will be invoked when the cabinet detects ahe open push button being pressed
 * It should kick off a main poll of the cabinet to check for key activity
 */
void MainWindow::onBtnOpenDoorClicked()
{
    //The load is complete once a signal from the dock door has been received
    if( m_loadType == COMPLETE )
        onBtnResetClicked();

    ui->gbCabinet->setEnabled(true);
    ui->gbUserActions->setEnabled(true);
    ui->btnRed->setStyleSheet("background-color:red");
    ui->btnClose->setEnabled(true);
}

// This function just resets all variables and ui components to base state
void MainWindow::resetUIAndCounters()
{

    ui->plainTextEdit1->clear();
    ui->plainTextEdit2->clear();
    ui->plainTextEdit3->clear();
    ui->plainTextEdit4->clear();
    ui->plainTextEdit5->clear();
    ui->plainTextEdit6->clear();
    ui->checkBox_4->setStyleSheet("background-color:lightgrey");
    ui->checkBox_5->setStyleSheet("background-color:lightgrey");
    ui->checkBox_6->setStyleSheet("background-color:lightgrey");
    ui->checkBox_7->setStyleSheet("background-color:lightgrey");
    ui->checkBox_8->setStyleSheet("background-color:lightgrey");
    ui->checkBox_9->setStyleSheet("background-color:lightgrey");
    ui->checkBox_10->setStyleSheet("background-color:lightgrey");
    ui->checkBox_11->setStyleSheet("background-color:lightgrey");
    ui->checkBox_12->setStyleSheet("background-color:lightgrey");
    ui->checkBox_13->setStyleSheet("background-color:lightgrey");
    ui->btnGreen->setStyleSheet("background-color:lightgrey");
    ui->btnRed->setStyleSheet("background-color:lightgrey");
    ui->btnGreen_2->setStyleSheet("background-color:lightgrey");
    ui->btnRed->setStyleSheet("background-color:lightgrey");
    ui->btnODMAccess->setEnabled(false);
    ui->btnClose->setEnabled(false);
    ui->btnOpenDoor->setEnabled(true);
    ui->btnReset->setEnabled(true);

    m_siteKeyCount = 0;
    m_driverKeyCount = 0;
    m_dropKeyCount = 0;
    m_userKeyCount = 0;
}

void MainWindow::onBtnResetClicked()
{
    if( m_loadType == COMPLETE ) {
        m_loadType = NULL_LOAD;
        ui->btnRed_2->setStyleSheet("background-color:lightgrey");

    }
    resetUIAndCounters();
}

void MainWindow::onBtnCloseClicked()
{

    // case1 where LIVE Load detected  ODM is required to approve opening of door for driver
    if( m_siteKeyCount > 0 &&  m_driverKeyCount > 0 && m_dropKeyCount == 0 ) {
        m_loadType = LIVE;

        ui->btnRed->setStyleSheet("background-color:lightgrey");
        ui->btnGreen->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:lightgrey");
        ui->btnGreen_2->setStyleSheet("background-color:lightgreen");
        QMessageBox::information(
               this,
               tr("Live Load Detected."),
               tr("Dock Doors may now be raised") );
        ui->btnRed->setStyleSheet("background-color:red");
        ui->btnGreen->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:red");
        ui->btnGreen_2->setStyleSheet("background-color:lightgrey");
        displayMessageAndRaiseDockDoor("Live Load Detected","Please note LIVE load in progress. Dock Door raised, loading commencing.");
         // DROP LOAD
    } else if( m_dropKeyCount > 0 ) {
        ui->btnRed->setStyleSheet("background-color:lightgrey");
        ui->btnGreen->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:lightgrey");
        ui->btnGreen_2->setStyleSheet("background-color:lightgreen");
        QMessageBox::information(
               this,
               tr("Drop Load Detected."),
               tr("Dock Doors may now be raised") );
        m_loadType = DROP;
        ui->btnRed->setStyleSheet("background-color:red");
        ui->btnGreen->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:red");
        ui->btnGreen_2->setStyleSheet("background-color:lightgrey");
        displayMessageAndRaiseDockDoor("Drop Load Detected","Please note that the cabinet will be inaccessible until the Drop Load is Complete.");
         //Dock door raised now safe to tell martial to release door lock on cabinet via ODM process

    }

    if (m_loadType == NULL_LOAD)
    {
        // Havnet' worked it out ye. Invalid
    }
}

void MainWindow::onBtnODMAccessClicked()
{
    //This button mimics the User key being detected by the system
    if( m_userKeyCount > 0) {
        ui->btnReset->setEnabled(true);
        ui->btnRed->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:red");
        ui->btnGreen_2->setStyleSheet("background-color:lightgrey");
        ui->btnGreen->setStyleSheet("background-color:lightgreen");
        m_loadType = COMPLETE;
        QMessageBox::information(
               this,
               tr("ODM Has authorised Key Removal"),
               tr("Keys may now be withdrawn.") );
        ui->btnReset->setEnabled(true);
        ui->btnRed->setStyleSheet("background-color:lightgrey");
        ui->btnRed_2->setStyleSheet("background-color:red");
        ui->btnGreen_2->setStyleSheet("background-color:lightgrey");
        ui->btnGreen->setStyleSheet("background-color:lightgreen");
        ui->btnOpenDoor->setEnabled(true);
        onBtnResetClicked();
    } else {
        QMessageBox::information(
               this,
               tr("ODM Required"),
               tr("ODM must present User key before keys may be withdrwan.") );

    }

}

void MainWindow::onTimeout()
{
    ui->btnRed->setStyleSheet("background-color:red");
    ui->btnGreen->setStyleSheet("background-color:lightgrey");
    ui->btnRed_2->setStyleSheet("background-color:red");
    ui->btnGreen_2->setStyleSheet("background-color:lightgrey");

    if( m_loadType == LIVE || m_loadType == DROP) {

        QMessageBox::information(
               this,
               tr("ODM Required"),
               tr("Load Complete, Dock Door raised, ODM must present User key before keys may be withdrwan.") );


         ui->btnRed->setStyleSheet("background-color:red");
         ui->btnGreen->setStyleSheet("background-color:lightgrey");
         ui->btnRed_2->setStyleSheet("background-color:lightgrey");
         ui->btnGreen_2->setStyleSheet("background-color:lightgreen");
    }

    //Need to detect
}

bool MainWindow::indicateIfKeyPlacedCorrectly( QString keytype, int sitePos, int driverPos, int dropPos, int userPos )
{
    if( keytype.indexOf("SiteKey") >= 0 ) {
        if(sitePos >= 0)
          return true;
        else return false;
    }

    if( keytype.indexOf("DriverKey") >= 0 ) {
        if(driverPos >= 0)
          return true;
        else return false;
    }

    if( keytype.indexOf("DropKey")  >= 0 ) {
        if(dropPos >= 0)
          return true;
        else return false;
    }

    if( keytype.indexOf("UserKey") >= 0 ) {
        if(userPos >= 0)
          return true;
        else return false;
    }
}


void MainWindow::displayMessageAndRaiseDockDoor(QString messageHeader, QString messageInfo)
{

    QMessageBox::information(
           this,
           (messageHeader),
           (messageInfo) );

     ui->btnClose->setEnabled(false);
     ui->btnOpenDoor->setEnabled(false);
     ui->btnReset->setEnabled(false);

     //Timer to represnt signal to open dock door
     m_raiseDockDoor->start();

     ui->pushButton6->setEnabled(true);
     ui->btnODMAccess->setEnabled(true);
     ui->plainTextEdit6->setEnabled(true);
     ui->lbI1Input6->setEnabled(true);
     ui->btnOpenDoor->setEnabled(false);

}

